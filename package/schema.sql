/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attributes`;

CREATE TABLE `attributes` (
  `id`     INT(11)                    NOT NULL AUTO_INCREMENT,
  `userId` INT(11)                    NOT NULL,
  `key`    ENUM('beers', 'can_drink') NOT NULL,
  `value`  TINYINT(4)                 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attributes` (`userId`, `key`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 55
  DEFAULT CHARSET = utf8;

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;

INSERT INTO `attributes` (`id`, `userId`, `key`, `value`)
  VALUES
  (1, 57, 'beers', 0),
  (2, 57, 'can_drink', 1);

/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table container
# ------------------------------------------------------------

DROP TABLE IF EXISTS `container`;

CREATE TABLE `container` (
  `id`     INT(11)                                NOT NULL AUTO_INCREMENT,
  `userId` INT(11)                                NOT NULL,
  `type`   ENUM('inventory', 'equipment', 'bank') NOT NULL,
  `slot`   SMALLINT(6)                            NOT NULL,
  `item`   SMALLINT(6) DEFAULT NULL,
  `amount` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `player_type_slot` (`userId`, `type`, `slot`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10639
  DEFAULT CHARSET = utf8;

LOCK TABLES `container` WRITE;
/*!40000 ALTER TABLE `container` DISABLE KEYS */;

INSERT INTO `container` (`id`, `userId`, `type`, `slot`, `item`, `amount`)
  VALUES
  (1, 57, 'inventory', 0, 4151, 1),
  (2, 57, 'inventory', 1, NULL, NULL),
  (3, 57, 'inventory', 2, NULL, NULL),
  (4, 57, 'inventory', 3, NULL, NULL),
  (5, 57, 'inventory', 4, NULL, NULL),
  (6, 57, 'inventory', 5, NULL, NULL),
  (7, 57, 'inventory', 6, NULL, NULL),
  (8, 57, 'inventory', 7, NULL, NULL),
  (9, 57, 'inventory', 8, NULL, NULL),
  (10, 57, 'inventory', 9, NULL, NULL),
  (11, 57, 'inventory', 10, NULL, NULL),
  (12, 57, 'inventory', 11, NULL, NULL),
  (13, 57, 'inventory', 12, NULL, NULL),
  (14, 57, 'inventory', 13, 995, 9000),
  (15, 57, 'inventory', 14, NULL, NULL),
  (16, 57, 'inventory', 15, NULL, NULL),
  (17, 57, 'inventory', 16, NULL, NULL),
  (18, 57, 'inventory', 17, NULL, NULL),
  (19, 57, 'inventory', 18, NULL, NULL),
  (20, 57, 'inventory', 19, NULL, NULL),
  (21, 57, 'inventory', 20, NULL, NULL),
  (22, 57, 'inventory', 21, NULL, NULL),
  (23, 57, 'inventory', 22, NULL, NULL),
  (24, 57, 'inventory', 23, NULL, NULL),
  (25, 57, 'inventory', 24, NULL, NULL),
  (26, 57, 'inventory', 25, NULL, NULL),
  (27, 57, 'inventory', 26, NULL, NULL),
  (28, 57, 'inventory', 27, NULL, NULL),
  (29, 57, 'equipment', 0, NULL, NULL),
  (30, 57, 'equipment', 1, NULL, NULL),
  (31, 57, 'equipment', 2, NULL, NULL),
  (32, 57, 'equipment', 3, NULL, NULL),
  (33, 57, 'equipment', 4, NULL, NULL),
  (34, 57, 'equipment', 5, NULL, NULL),
  (35, 57, 'equipment', 6, NULL, NULL),
  (36, 57, 'equipment', 7, NULL, NULL),
  (37, 57, 'equipment', 8, NULL, NULL),
  (38, 57, 'equipment', 9, NULL, NULL),
  (39, 57, 'equipment', 10, NULL, NULL),
  (40, 57, 'equipment', 11, NULL, NULL),
  (41, 57, 'equipment', 12, NULL, NULL),
  (42, 57, 'equipment', 13, NULL, NULL),
  (43, 57, 'bank', 0, NULL, NULL),
  (44, 57, 'bank', 1, NULL, NULL),
  (45, 57, 'bank', 2, NULL, NULL),
  (46, 57, 'bank', 3, NULL, NULL),
  (47, 57, 'bank', 4, NULL, NULL),
  (48, 57, 'bank', 5, NULL, NULL),
  (49, 57, 'bank', 6, NULL, NULL),
  (50, 57, 'bank', 7, NULL, NULL),
  (51, 57, 'bank', 8, NULL, NULL),
  (52, 57, 'bank', 9, NULL, NULL),
  (53, 57, 'bank', 10, NULL, NULL),
  (54, 57, 'bank', 11, NULL, NULL),
  (55, 57, 'bank', 12, NULL, NULL),
  (56, 57, 'bank', 13, NULL, NULL),
  (57, 57, 'bank', 14, NULL, NULL),
  (58, 57, 'bank', 15, NULL, NULL),
  (59, 57, 'bank', 16, NULL, NULL),
  (60, 57, 'bank', 17, NULL, NULL),
  (61, 57, 'bank', 18, NULL, NULL),
  (62, 57, 'bank', 19, NULL, NULL),
  (63, 57, 'bank', 20, NULL, NULL),
  (64, 57, 'bank', 21, NULL, NULL),
  (65, 57, 'bank', 22, NULL, NULL),
  (66, 57, 'bank', 23, NULL, NULL),
  (67, 57, 'bank', 24, NULL, NULL),
  (68, 57, 'bank', 25, NULL, NULL),
  (69, 57, 'bank', 26, NULL, NULL),
  (70, 57, 'bank', 27, NULL, NULL),
  (71, 57, 'bank', 28, NULL, NULL),
  (72, 57, 'bank', 29, NULL, NULL),
  (73, 57, 'bank', 30, NULL, NULL),
  (74, 57, 'bank', 31, NULL, NULL),
  (75, 57, 'bank', 32, NULL, NULL),
  (76, 57, 'bank', 33, NULL, NULL),
  (77, 57, 'bank', 34, NULL, NULL),
  (78, 57, 'bank', 35, NULL, NULL),
  (79, 57, 'bank', 36, NULL, NULL),
  (80, 57, 'bank', 37, NULL, NULL),
  (81, 57, 'bank', 38, NULL, NULL),
  (82, 57, 'bank', 39, NULL, NULL),
  (83, 57, 'bank', 40, NULL, NULL),
  (84, 57, 'bank', 41, NULL, NULL),
  (85, 57, 'bank', 42, NULL, NULL),
  (86, 57, 'bank', 43, NULL, NULL),
  (87, 57, 'bank', 44, NULL, NULL),
  (88, 57, 'bank', 45, NULL, NULL),
  (89, 57, 'bank', 46, NULL, NULL),
  (90, 57, 'bank', 47, NULL, NULL),
  (91, 57, 'bank', 48, NULL, NULL),
  (92, 57, 'bank', 49, NULL, NULL),
  (93, 57, 'bank', 50, NULL, NULL),
  (94, 57, 'bank', 51, NULL, NULL),
  (95, 57, 'bank', 52, NULL, NULL),
  (96, 57, 'bank', 53, NULL, NULL),
  (97, 57, 'bank', 54, NULL, NULL),
  (98, 57, 'bank', 55, NULL, NULL),
  (99, 57, 'bank', 56, NULL, NULL),
  (100, 57, 'bank', 57, NULL, NULL),
  (101, 57, 'bank', 58, NULL, NULL),
  (102, 57, 'bank', 59, NULL, NULL),
  (103, 57, 'bank', 60, NULL, NULL),
  (104, 57, 'bank', 61, NULL, NULL),
  (105, 57, 'bank', 62, NULL, NULL),
  (106, 57, 'bank', 63, NULL, NULL),
  (107, 57, 'bank', 64, NULL, NULL),
  (108, 57, 'bank', 65, NULL, NULL),
  (109, 57, 'bank', 66, NULL, NULL),
  (110, 57, 'bank', 67, NULL, NULL),
  (111, 57, 'bank', 68, NULL, NULL),
  (112, 57, 'bank', 69, NULL, NULL),
  (113, 57, 'bank', 70, NULL, NULL),
  (114, 57, 'bank', 71, NULL, NULL),
  (115, 57, 'bank', 72, NULL, NULL),
  (116, 57, 'bank', 73, NULL, NULL),
  (117, 57, 'bank', 74, NULL, NULL),
  (118, 57, 'bank', 75, NULL, NULL),
  (119, 57, 'bank', 76, NULL, NULL),
  (120, 57, 'bank', 77, NULL, NULL),
  (121, 57, 'bank', 78, NULL, NULL),
  (122, 57, 'bank', 79, NULL, NULL),
  (123, 57, 'bank', 80, NULL, NULL),
  (124, 57, 'bank', 81, NULL, NULL),
  (125, 57, 'bank', 82, NULL, NULL),
  (126, 57, 'bank', 83, NULL, NULL),
  (127, 57, 'bank', 84, NULL, NULL),
  (128, 57, 'bank', 85, NULL, NULL),
  (129, 57, 'bank', 86, NULL, NULL),
  (130, 57, 'bank', 87, NULL, NULL),
  (131, 57, 'bank', 88, NULL, NULL),
  (132, 57, 'bank', 89, NULL, NULL),
  (133, 57, 'bank', 90, NULL, NULL),
  (134, 57, 'bank', 91, NULL, NULL),
  (135, 57, 'bank', 92, NULL, NULL),
  (136, 57, 'bank', 93, NULL, NULL),
  (137, 57, 'bank', 94, NULL, NULL),
  (138, 57, 'bank', 95, NULL, NULL),
  (139, 57, 'bank', 96, NULL, NULL),
  (140, 57, 'bank', 97, NULL, NULL),
  (141, 57, 'bank', 98, NULL, NULL),
  (142, 57, 'bank', 99, NULL, NULL),
  (143, 57, 'bank', 100, NULL, NULL),
  (144, 57, 'bank', 101, NULL, NULL),
  (145, 57, 'bank', 102, NULL, NULL),
  (146, 57, 'bank', 103, NULL, NULL),
  (147, 57, 'bank', 104, NULL, NULL),
  (148, 57, 'bank', 105, NULL, NULL),
  (149, 57, 'bank', 106, NULL, NULL),
  (150, 57, 'bank', 107, NULL, NULL),
  (151, 57, 'bank', 108, NULL, NULL),
  (152, 57, 'bank', 109, NULL, NULL),
  (153, 57, 'bank', 110, NULL, NULL),
  (154, 57, 'bank', 111, NULL, NULL),
  (155, 57, 'bank', 112, NULL, NULL),
  (156, 57, 'bank', 113, NULL, NULL),
  (157, 57, 'bank', 114, NULL, NULL),
  (158, 57, 'bank', 115, NULL, NULL),
  (159, 57, 'bank', 116, NULL, NULL),
  (160, 57, 'bank', 117, NULL, NULL),
  (161, 57, 'bank', 118, NULL, NULL),
  (162, 57, 'bank', 119, NULL, NULL),
  (163, 57, 'bank', 120, NULL, NULL),
  (164, 57, 'bank', 121, NULL, NULL),
  (165, 57, 'bank', 122, NULL, NULL),
  (166, 57, 'bank', 123, NULL, NULL),
  (167, 57, 'bank', 124, NULL, NULL),
  (168, 57, 'bank', 125, NULL, NULL),
  (169, 57, 'bank', 126, NULL, NULL),
  (170, 57, 'bank', 127, NULL, NULL),
  (171, 57, 'bank', 128, NULL, NULL),
  (172, 57, 'bank', 129, NULL, NULL),
  (173, 57, 'bank', 130, NULL, NULL),
  (174, 57, 'bank', 131, NULL, NULL),
  (175, 57, 'bank', 132, NULL, NULL),
  (176, 57, 'bank', 133, NULL, NULL),
  (177, 57, 'bank', 134, NULL, NULL),
  (178, 57, 'bank', 135, NULL, NULL),
  (179, 57, 'bank', 136, NULL, NULL),
  (180, 57, 'bank', 137, NULL, NULL),
  (181, 57, 'bank', 138, NULL, NULL),
  (182, 57, 'bank', 139, NULL, NULL),
  (183, 57, 'bank', 140, NULL, NULL),
  (184, 57, 'bank', 141, NULL, NULL),
  (185, 57, 'bank', 142, NULL, NULL),
  (186, 57, 'bank', 143, NULL, NULL),
  (187, 57, 'bank', 144, NULL, NULL),
  (188, 57, 'bank', 145, NULL, NULL),
  (189, 57, 'bank', 146, NULL, NULL),
  (190, 57, 'bank', 147, NULL, NULL),
  (191, 57, 'bank', 148, NULL, NULL),
  (192, 57, 'bank', 149, NULL, NULL),
  (193, 57, 'bank', 150, NULL, NULL),
  (194, 57, 'bank', 151, NULL, NULL),
  (195, 57, 'bank', 152, NULL, NULL),
  (196, 57, 'bank', 153, NULL, NULL),
  (197, 57, 'bank', 154, NULL, NULL),
  (198, 57, 'bank', 155, NULL, NULL),
  (199, 57, 'bank', 156, NULL, NULL),
  (200, 57, 'bank', 157, NULL, NULL),
  (201, 57, 'bank', 158, NULL, NULL),
  (202, 57, 'bank', 159, NULL, NULL),
  (203, 57, 'bank', 160, NULL, NULL),
  (204, 57, 'bank', 161, NULL, NULL),
  (205, 57, 'bank', 162, NULL, NULL),
  (206, 57, 'bank', 163, NULL, NULL),
  (207, 57, 'bank', 164, NULL, NULL),
  (208, 57, 'bank', 165, NULL, NULL),
  (209, 57, 'bank', 166, NULL, NULL),
  (210, 57, 'bank', 167, NULL, NULL),
  (211, 57, 'bank', 168, NULL, NULL),
  (212, 57, 'bank', 169, NULL, NULL),
  (213, 57, 'bank', 170, NULL, NULL),
  (214, 57, 'bank', 171, NULL, NULL),
  (215, 57, 'bank', 172, NULL, NULL),
  (216, 57, 'bank', 173, NULL, NULL),
  (217, 57, 'bank', 174, NULL, NULL),
  (218, 57, 'bank', 175, NULL, NULL),
  (219, 57, 'bank', 176, NULL, NULL),
  (220, 57, 'bank', 177, NULL, NULL),
  (221, 57, 'bank', 178, NULL, NULL),
  (222, 57, 'bank', 179, NULL, NULL),
  (223, 57, 'bank', 180, NULL, NULL),
  (224, 57, 'bank', 181, NULL, NULL),
  (225, 57, 'bank', 182, NULL, NULL),
  (226, 57, 'bank', 183, NULL, NULL),
  (227, 57, 'bank', 184, NULL, NULL),
  (228, 57, 'bank', 185, NULL, NULL),
  (229, 57, 'bank', 186, NULL, NULL),
  (230, 57, 'bank', 187, NULL, NULL),
  (231, 57, 'bank', 188, NULL, NULL),
  (232, 57, 'bank', 189, NULL, NULL),
  (233, 57, 'bank', 190, NULL, NULL),
  (234, 57, 'bank', 191, NULL, NULL),
  (235, 57, 'bank', 192, NULL, NULL),
  (236, 57, 'bank', 193, NULL, NULL),
  (237, 57, 'bank', 194, NULL, NULL),
  (238, 57, 'bank', 195, NULL, NULL),
  (239, 57, 'bank', 196, NULL, NULL),
  (240, 57, 'bank', 197, NULL, NULL),
  (241, 57, 'bank', 198, NULL, NULL),
  (242, 57, 'bank', 199, NULL, NULL),
  (243, 57, 'bank', 200, NULL, NULL),
  (244, 57, 'bank', 201, NULL, NULL),
  (245, 57, 'bank', 202, NULL, NULL),
  (246, 57, 'bank', 203, NULL, NULL),
  (247, 57, 'bank', 204, NULL, NULL),
  (248, 57, 'bank', 205, NULL, NULL),
  (249, 57, 'bank', 206, NULL, NULL),
  (250, 57, 'bank', 207, NULL, NULL),
  (251, 57, 'bank', 208, NULL, NULL),
  (252, 57, 'bank', 209, NULL, NULL),
  (253, 57, 'bank', 210, NULL, NULL),
  (254, 57, 'bank', 211, NULL, NULL),
  (255, 57, 'bank', 212, NULL, NULL),
  (256, 57, 'bank', 213, NULL, NULL),
  (257, 57, 'bank', 214, NULL, NULL),
  (258, 57, 'bank', 215, NULL, NULL),
  (259, 57, 'bank', 216, NULL, NULL),
  (260, 57, 'bank', 217, NULL, NULL),
  (261, 57, 'bank', 218, NULL, NULL),
  (262, 57, 'bank', 219, NULL, NULL),
  (263, 57, 'bank', 220, NULL, NULL),
  (264, 57, 'bank', 221, NULL, NULL),
  (265, 57, 'bank', 222, NULL, NULL),
  (266, 57, 'bank', 223, NULL, NULL),
  (267, 57, 'bank', 224, NULL, NULL),
  (268, 57, 'bank', 225, NULL, NULL),
  (269, 57, 'bank', 226, NULL, NULL),
  (270, 57, 'bank', 227, NULL, NULL),
  (271, 57, 'bank', 228, NULL, NULL),
  (272, 57, 'bank', 229, NULL, NULL),
  (273, 57, 'bank', 230, NULL, NULL),
  (274, 57, 'bank', 231, NULL, NULL),
  (275, 57, 'bank', 232, NULL, NULL),
  (276, 57, 'bank', 233, NULL, NULL),
  (277, 57, 'bank', 234, NULL, NULL),
  (278, 57, 'bank', 235, NULL, NULL),
  (279, 57, 'bank', 236, NULL, NULL),
  (280, 57, 'bank', 237, NULL, NULL),
  (281, 57, 'bank', 238, NULL, NULL),
  (282, 57, 'bank', 239, NULL, NULL),
  (283, 57, 'bank', 240, NULL, NULL),
  (284, 57, 'bank', 241, NULL, NULL),
  (285, 57, 'bank', 242, NULL, NULL),
  (286, 57, 'bank', 243, NULL, NULL),
  (287, 57, 'bank', 244, NULL, NULL),
  (288, 57, 'bank', 245, NULL, NULL),
  (289, 57, 'bank', 246, NULL, NULL),
  (290, 57, 'bank', 247, NULL, NULL),
  (291, 57, 'bank', 248, NULL, NULL),
  (292, 57, 'bank', 249, NULL, NULL),
  (293, 57, 'bank', 250, NULL, NULL),
  (294, 57, 'bank', 251, NULL, NULL),
  (295, 57, 'bank', 252, NULL, NULL),
  (296, 57, 'bank', 253, NULL, NULL),
  (297, 57, 'bank', 254, NULL, NULL),
  (298, 57, 'bank', 255, NULL, NULL),
  (299, 57, 'bank', 256, NULL, NULL),
  (300, 57, 'bank', 257, NULL, NULL),
  (301, 57, 'bank', 258, NULL, NULL),
  (302, 57, 'bank', 259, NULL, NULL),
  (303, 57, 'bank', 260, NULL, NULL),
  (304, 57, 'bank', 261, NULL, NULL),
  (305, 57, 'bank', 262, NULL, NULL),
  (306, 57, 'bank', 263, NULL, NULL),
  (307, 57, 'bank', 264, NULL, NULL),
  (308, 57, 'bank', 265, NULL, NULL),
  (309, 57, 'bank', 266, NULL, NULL),
  (310, 57, 'bank', 267, NULL, NULL),
  (311, 57, 'bank', 268, NULL, NULL),
  (312, 57, 'bank', 269, NULL, NULL),
  (313, 57, 'bank', 270, NULL, NULL),
  (314, 57, 'bank', 271, NULL, NULL),
  (315, 57, 'bank', 272, NULL, NULL),
  (316, 57, 'bank', 273, NULL, NULL),
  (317, 57, 'bank', 274, NULL, NULL),
  (318, 57, 'bank', 275, NULL, NULL),
  (319, 57, 'bank', 276, NULL, NULL),
  (320, 57, 'bank', 277, NULL, NULL),
  (321, 57, 'bank', 278, NULL, NULL),
  (322, 57, 'bank', 279, NULL, NULL),
  (323, 57, 'bank', 280, NULL, NULL),
  (324, 57, 'bank', 281, NULL, NULL),
  (325, 57, 'bank', 282, NULL, NULL),
  (326, 57, 'bank', 283, NULL, NULL),
  (327, 57, 'bank', 284, NULL, NULL),
  (328, 57, 'bank', 285, NULL, NULL),
  (329, 57, 'bank', 286, NULL, NULL),
  (330, 57, 'bank', 287, NULL, NULL),
  (331, 57, 'bank', 288, NULL, NULL),
  (332, 57, 'bank', 289, NULL, NULL),
  (333, 57, 'bank', 290, NULL, NULL),
  (334, 57, 'bank', 291, NULL, NULL),
  (335, 57, 'bank', 292, NULL, NULL),
  (336, 57, 'bank', 293, NULL, NULL),
  (337, 57, 'bank', 294, NULL, NULL),
  (338, 57, 'bank', 295, NULL, NULL),
  (339, 57, 'bank', 296, NULL, NULL),
  (340, 57, 'bank', 297, NULL, NULL),
  (341, 57, 'bank', 298, NULL, NULL),
  (342, 57, 'bank', 299, NULL, NULL),
  (343, 57, 'bank', 300, NULL, NULL),
  (344, 57, 'bank', 301, NULL, NULL),
  (345, 57, 'bank', 302, NULL, NULL),
  (346, 57, 'bank', 303, NULL, NULL),
  (347, 57, 'bank', 304, NULL, NULL),
  (348, 57, 'bank', 305, NULL, NULL),
  (349, 57, 'bank', 306, NULL, NULL),
  (350, 57, 'bank', 307, NULL, NULL),
  (351, 57, 'bank', 308, NULL, NULL),
  (352, 57, 'bank', 309, NULL, NULL),
  (353, 57, 'bank', 310, NULL, NULL),
  (354, 57, 'bank', 311, NULL, NULL),
  (355, 57, 'bank', 312, NULL, NULL),
  (356, 57, 'bank', 313, NULL, NULL),
  (357, 57, 'bank', 314, NULL, NULL),
  (358, 57, 'bank', 315, NULL, NULL),
  (359, 57, 'bank', 316, NULL, NULL),
  (360, 57, 'bank', 317, NULL, NULL),
  (361, 57, 'bank', 318, NULL, NULL),
  (362, 57, 'bank', 319, NULL, NULL),
  (363, 57, 'bank', 320, NULL, NULL),
  (364, 57, 'bank', 321, NULL, NULL),
  (365, 57, 'bank', 322, NULL, NULL),
  (366, 57, 'bank', 323, NULL, NULL),
  (367, 57, 'bank', 324, NULL, NULL),
  (368, 57, 'bank', 325, NULL, NULL),
  (369, 57, 'bank', 326, NULL, NULL),
  (370, 57, 'bank', 327, NULL, NULL),
  (371, 57, 'bank', 328, NULL, NULL),
  (372, 57, 'bank', 329, NULL, NULL),
  (373, 57, 'bank', 330, NULL, NULL),
  (374, 57, 'bank', 331, NULL, NULL),
  (375, 57, 'bank', 332, NULL, NULL),
  (376, 57, 'bank', 333, NULL, NULL),
  (377, 57, 'bank', 334, NULL, NULL),
  (378, 57, 'bank', 335, NULL, NULL),
  (379, 57, 'bank', 336, NULL, NULL),
  (380, 57, 'bank', 337, NULL, NULL),
  (381, 57, 'bank', 338, NULL, NULL),
  (382, 57, 'bank', 339, NULL, NULL),
  (383, 57, 'bank', 340, NULL, NULL),
  (384, 57, 'bank', 341, NULL, NULL),
  (385, 57, 'bank', 342, NULL, NULL),
  (386, 57, 'bank', 343, NULL, NULL),
  (387, 57, 'bank', 344, NULL, NULL),
  (388, 57, 'bank', 345, NULL, NULL),
  (389, 57, 'bank', 346, NULL, NULL),
  (390, 57, 'bank', 347, NULL, NULL),
  (391, 57, 'bank', 348, NULL, NULL),
  (392, 57, 'bank', 349, NULL, NULL),
  (393, 57, 'bank', 350, NULL, NULL),
  (394, 57, 'bank', 351, NULL, NULL);

/*!40000 ALTER TABLE `container` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `details`;

CREATE TABLE `details` (
  `userId`         INT(11)      NOT NULL AUTO_INCREMENT,
  `username`       VARCHAR(12)  NOT NULL,
  `password`       CHAR(60)     NOT NULL,
  `rights`         TINYINT(4)   NOT NULL,
  `x`              SMALLINT(6)  NOT NULL,
  `y`              SMALLINT(6)  NOT NULL,
  `z`              TINYINT(4)   NOT NULL,
  `runEnergy`      DOUBLE       NOT NULL,
  `specialEnergy`  SMALLINT(11) NOT NULL,
  `combatStyle`    TINYINT(4)   NOT NULL,
  `gang`           VARCHAR(20) DEFAULT '',
  `gangRank`       VARCHAR(50) DEFAULT NULL,
  `respectPoints`  SMALLINT(11) NOT NULL,
  `currentMission` SMALLINT(20) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `username` (`username`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 58
  DEFAULT CHARSET = utf8;

LOCK TABLES `details` WRITE;
/*!40000 ALTER TABLE `details` DISABLE KEYS */;

INSERT INTO `details` (`userId`, `username`, `password`, `rights`, `x`, `y`, `z`, `runEnergy`, `specialEnergy`, `combatStyle`, `gang`, `gangRank`, `respectPoints`, `currentMission`)
  VALUES
  (57, 'Global', 'ass', 2, 3220, 3396, 0, 100, 100, 0, NULL, 'GOON', 0, - 1);

/*!40000 ALTER TABLE `details` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gang_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gang_data`;

CREATE TABLE `gang_data` (
  `id`     INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`   VARCHAR(50)      NOT NULL DEFAULT '',
  `leader` VARCHAR(20)      NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;



# Dump of table missions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `missions`;

CREATE TABLE `missions` (
  `id`      INT(11)     NOT NULL AUTO_INCREMENT,
  `userId`  INT(11)     NOT NULL,
  `questId` SMALLINT(6) NOT NULL,
  `stage`   TINYINT(6)  NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `player_type_slot` (`userId`, `questId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 28
  DEFAULT CHARSET = utf8;

LOCK TABLES `missions` WRITE;
/*!40000 ALTER TABLE `missions` DISABLE KEYS */;

INSERT INTO `missions` (`id`, `userId`, `questId`, `stage`)
  VALUES
  (1, 57, 7336, 0);

/*!40000 ALTER TABLE `missions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id`      INT(11)                                                   NOT NULL AUTO_INCREMENT,
  `userId`  INT(11)                                                   NOT NULL,
  `setting` ENUM('swapping', 'withdraw_as_notes', 'auto_retaliating') NOT NULL,
  `value`   TINYINT(4)                                                NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `player_setting` (`userId`, `setting`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 55
  DEFAULT CHARSET = utf8;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `userId`, `setting`, `value`)
  VALUES
  (1, 57, 'swapping', 1),
  (2, 57, 'withdraw_as_notes', 0);

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skills
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skills`;

CREATE TABLE `skills` (
  `userId`  INT(11) NOT NULL DEFAULT '0',
  `level`   INT(11) NOT NULL,
  `exp`     INT(1)  NOT NULL,
  `skillId` INT(11) NOT NULL,
  PRIMARY KEY (`userId`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = latin1;

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;

INSERT INTO `skills` (`userId`, `level`, `exp`, `skillId`)
  VALUES
  (57, 1, 0, 20);

/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;


/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
