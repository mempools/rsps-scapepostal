/**
 * Copyright (c) 2012-2013 Graham Edgecombe <graham@grahamedgecombe.com>
 * Copyright (c) 2012-2013 Jonathan Edgecombe <jonathanedgecombe@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
package org.hyperion.data;

import org.hyperion.rs2.model.player.Player;

import java.io.IOException;
import java.sql.SQLException;

/**
 * MySQL Serializer
 *
 * @author Graham
 * http://www.rune-server.org/runescape-development/rs2-server/snippets/360641-sql-2.html#post2993816
 */
public abstract class Serializer {

    /**
     * Loads the player information
     *
     * @param player The player save
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    public abstract void save(Player player) throws SQLException, IOException;

    /**
     * Saves the player information.
     *
     * @param player The player to load
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    public abstract void load(Player player) throws SQLException, IOException;
}
