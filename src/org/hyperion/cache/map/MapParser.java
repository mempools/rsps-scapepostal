package org.hyperion.cache.map;

import org.hyperion.rs2.model.region.MapTile;
import org.hyperion.cache.Cache;
import org.hyperion.cache.index.impl.MapIndex;
import org.hyperion.cache.util.ZipUtils;
import org.hyperion.rs2.model.Location;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * A class which parses map files in the game cache.
 *
 * @author Graham Edgecombe
 */
public class MapParser {

    /**
     * The cache.
     */
    private final Cache cache;
    /**
     * The area id.
     */
    private final int area;
    /**
     * The map listener.
     */
    private final MapListener listener;

    /**
     * Creates the map parser.
     *
     * @param cache The cache.
     * @param area The area id.
     * @param listener The listener.
     */
    public MapParser(Cache cache, int area, MapListener listener) {
        this.cache = cache;
        this.area = area;
        this.listener = listener;
    }

    /**
     * Parses the map file.
     *
     * @throws IOException
     */
    public void parse() throws IOException {
        int x = ((area >> 8) & 0xFF) * 64;
        int y = (area & 0xFF) * 64;
        MapIndex index = cache.getIndexTable().getMapIndex(area);
        ByteBuffer buf = ZipUtils.unzip(cache.getFile(4, index.getMapFile()));
        for (int _z = 0; _z < 4; _z++) {
            for (int _x = 0; _x < 64; _x++) {
                for (int _y = 0; _y < 64; _y++) {
                    MapTile t = new MapTile();
                    t.setLocation(Location.create(x + _x, y + _y, _z));
                    while (true) {
                        int opcode = buf.get() & 0xFF;
                        if (opcode == 0) {
                            t.setHeight(-1);
                            break;
                        } else if (opcode == 1) {
                            t.setHeight(buf.get());
                            break;
                        } else if (opcode <= 49) {
                            t.setOverlay(buf.get());
                            t.setShape(opcode - 2);
                        } else if (opcode <= 81) {
                            t.setFlags(opcode - 49);
                        } else {
                            t.setUnderlay(opcode - 81);
                        }
                    }
                    listener.tileParsed(t);
                }
            }
        }
    }
}
