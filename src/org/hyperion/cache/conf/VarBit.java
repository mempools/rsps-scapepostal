/*
 * Here comes the text of your license
 * Each line should be prefixed with  * 
 */
package org.hyperion.cache.conf;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.hyperion.cache.Archive;
import org.hyperion.cache.Cache;
import org.hyperion.cache.InvalidCacheException;

public class VarBit {

    public static int totalCount;
    public static VarBit[] varbitCache;
    public int configId;
    public int leastSignificantBit;
    public int mostSignificantBit;

    public static void main(String[] args) throws IOException, InvalidCacheException {
        load();
    }

    public static void load() throws IOException, InvalidCacheException {
        System.out.println("Loading..");
        Cache cache = new Cache(new File("./data/cache/"));
        ByteBuffer buf = new Archive(cache.getFile(0, 2)).getFileAsByteBuffer("varbit.dat");
        VarBit.totalCount = buf.getShort();
        if (VarBit.varbitCache == null) {
            VarBit.varbitCache = new VarBit[VarBit.totalCount];
        }
        for (int id = 0; id < VarBit.totalCount; id++) {
            if (VarBit.varbitCache[id] == null) {
                VarBit.varbitCache[id] = new VarBit();
            }
            VarBit.varbitCache[id].decode(buf);
        }
        System.out.println("Total count = " + totalCount);
    }

    public void decode(ByteBuffer buf) {
        do {
            int opcode = buf.get();
            if (opcode == 0) {
                return;
            }
            if (opcode == 1) {
                configId = buf.getShort();
                leastSignificantBit = buf.get();
                mostSignificantBit = buf.get();
                System.out.println(configId + ":" + leastSignificantBit);
            } else if (opcode == 10) {
                //ChannelBufferUtils.getRS2String(buf);
                String con = buf.toString();
                System.out.println("1");
            } else if (opcode == 3) {
                buf.getInt();
                System.out.println("3");
            } else if (opcode == 4) {
                buf.getInt();
                System.out.println("5");
            } else {
                System.out.println("Error unrecognised config code: " + opcode);
            }
        } while (true);
    }
}
