package org.hyperion.cache.obj;

import org.hyperion.rs2.model.region.MapTile;
import org.hyperion.cache.Cache;
import org.hyperion.cache.InvalidCacheException;
import org.hyperion.cache.index.impl.MapIndex;
import org.hyperion.cache.index.impl.StandardIndex;
import org.hyperion.cache.map.*;
import org.hyperion.Constants;
import org.hyperion.rs2.content.doors.Door;
import org.hyperion.rs2.model.GameObject;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.definition.GameObjectDefinition;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Manages all of the in-game objects.
 *
 * @author Graham Edgecombe
 */
public class ObjectManager implements LandscapeListener, ObjectDefinitionListener, MapListener {

    /**
     * Logger instance.
     */
    private static final Logger logger = Logger.getLogger(ObjectManager.class.getName());
    /**
     * The number of definitions loaded.
     */
    private int definitionCount = 0;
    /**
     * The count of objects loaded.
     */
    private int objectCount = 0;
    /**
     * The cache.
     */
    private Cache cache;

    /**
     * Loads the objects in the map.
     *
     * @throws IOException if an I/O error occurs.
     * @throws InvalidCacheException if the cache is invalid.
     */
    public void load() throws IOException, InvalidCacheException {
        cache = new Cache(Constants.CACHE_DIRECTORY);
        logger.info("Loading Object Definitions...");
        StandardIndex[] defIndices = cache.getIndexTable().getObjectDefinitionIndices();
        new ObjectDefinitionParser(cache, defIndices, this).parse();
        logger.info("Loaded " + definitionCount + " Object Definitions.");

        logger.info("Loading Map...");
        MapIndex[] mapIndices = cache.getIndexTable().getMapIndices();
        for (MapIndex index : mapIndices) {
            new LandscapeParser(cache, index.getIdentifier(), this).parse();
        }
        logger.info("Loaded " + objectCount + " Map Objects.");
    }

    @Override
    public void objectParsed(GameObject obj) {
        objectCount++;
        World.getWorld().getRegionManager().getRegionByLocation(obj.getLocation()).getGameObjects().add(obj);
        Door.parse(obj);
    }

    @Override
    public void objectDefinitionParsed(GameObjectDefinition def) {
        definitionCount++;
        GameObjectDefinition.addDefinition(def);
    }

    /**
     * Loads an area.
     *
     * @param x The Y coordinate
     * @param y The Y coordinate
     */
    public synchronized void loadArea(int x, int y) {
        try {
            int ident = (y & 0xFF) | ((x & 0xFF) << 8);
            new MapParser(cache, ident, this).parse();
            new LandscapeParser(cache, ident, this).parse();
            ident = (y & 0xFF) | ((x + 1 & 0xFF) << 8);
            new MapParser(cache, ident, this).parse();
            new LandscapeParser(cache, ident, this).parse();
            ident = (y + 1 & 0xFF) | ((x + 1 & 0xFF) << 8);
            new MapParser(cache, ident, this).parse();
            new LandscapeParser(cache, ident, this).parse();
            ident = (y + 1 & 0xFF) | ((x & 0xFF) << 8);
            new MapParser(cache, ident, this).parse();
            new LandscapeParser(cache, ident, this).parse();
        } catch (Exception e) {
            logger.severe("Could not load maps for area (" + x + "," + y + ")!");
        }
    }

    @Override
    public void tileParsed(MapTile tile) {
        World.getWorld().getRegionManager().getRegionByLocation(tile.getLocation()).setMapTile(tile, tile.getLocation());
    }

    /**
     * Creates a global object
     *
     * @param object The object to create
     */
    public static void createGlobalObject(GameObject object) {
        for (Player players : World.getWorld().getPlayers()) {
            if (players == null) {
                continue;
            }
            if (players.getLocation().isWithinDistance(object.getLocation())) {
                players.getActionSender().sendGameObject(object);
            }
        }
    }

    /**
     * Creates a global object
     *
     * @param object The object to create
     */
    public static void removeGlobalObject(GameObject object) {
        for (Player players : World.getWorld().getPlayers()) {
            if (players == null) {
                continue;
            }
            if (players.getLocation().isWithinDistance(object.getLocation())) {
                players.getActionSender().sendRemoveGameObject(object);
            }
        }
    }
}
