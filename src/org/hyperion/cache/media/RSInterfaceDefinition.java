package org.hyperion.cache.media;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hyperion.util.xStream;

/**
 * @author Global <http://rune-server.org/members/global>
 * A map wasn't necessary but w/e
 */
public class RSInterfaceDefinition {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(RSInterfaceDefinition.class.getName());

    /**
     * The RSInterface data definitions.
     */
    private static Map<Integer, RSInterface> definitions = new HashMap<>();

    /**
     * Gets the definitions
     *
     * @return The definitions.
     */
    public static Map<Integer, RSInterface> getDefinitions() {
        return definitions;
    }

    /**
     * Gets an interface data by the parent Id
     *
     * @param interfaceId The interface parent Id
     * @return The RSInterface
     */
    public RSInterface forId(int interfaceId) {
        return definitions.get(interfaceId);
    }

    /**
     * Loads the definitions.
     */
    public static void init() {
        logger.info("Loading Interface Data...");
        try {
            definitions = xStream.readXML(new File("./data/interfaceDump.xml"));
            //dump();
        } catch (IOException ex) {
            logger.severe("Error loading interface data " + ex.getMessage());
        }
        logger.info("Loaded " + definitions.size() + " Interface Data");
    }

    /**
     * Dumps the interface data.
     */
    public static void dump() {
        File file = new File("./package/tools/377 interface dump.txt");
        if (file.exists()) {
            file.delete();
        }
        try (PrintStream out = new PrintStream(file)) {
            StringBuilder bldr = new StringBuilder();
            for (int i = 0; i < 18786; i++) {
                RSInterface rs = getDefinitions().get(i);
                if (rs != null && rs.getConfigIds() != null && rs.getParentId() != -1) {
                    bldr.append("[").append("interface=").append(i).append(" ");
                    if (rs.getParentId() != 0) {
                        bldr.append("parent=").append(rs.getParentId()).append(" ");
                    }
                    bldr.append("configs=");
                    for (int[] opcodes : rs.getConfigIds()) {
                        for (int opcode : opcodes) {
                            bldr.append(opcode).append(",");
                        }
                    }
                    bldr.append("]\n");
                }
            }
            out.println(bldr.toString().replaceAll(",]", "]"));
            out.flush();
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RSInterfaceDefinition.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
