package org.hyperion.cache.media;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class RSInterface {

    /**
     * The interface Id.
     */
    private final int parentId;

    /**
     * The opcodes.
     */
    private final int[][] configs;

    /**
     * Constructs the interface
     *
     * @param parentId The parentId Id
     * @param configs The opcodes
     */
    public RSInterface(int parentId, int[][] configs) {
        this.parentId = parentId;
        this.configs = configs;
    }

    /**
     * Gets the interface id
     *
     * @return The interface id
     */
    public int getParentId() {
        return parentId;
    }

    /**
     * Gets the configuration IDs
     *
     * @return The configuration IDs
     */
    public int[][] getConfigIds() {
        return configs;
    }

}
