package org.hyperion.cache.item;

public interface ItemDefinitionListener {

    public void itemParsed(CacheItemDefinition cacheItemDefinition);
}
