package org.hyperion.cache.item;

import org.hyperion.cache.Cache;
import org.hyperion.cache.index.impl.StandardIndex;

import java.io.IOException;
import java.util.logging.Logger;

public class ItemManager implements ItemDefinitionListener {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(ItemManager.class.getName());

    /**
     * The definition count.
     */
    private int definitionCount = 0;

    /**
     * Loads the cache item definitions
     *
     * @param cache The cache
     * @throws IOException
     */
    public void load(Cache cache) throws IOException {
        logger.info("Loading Item Definitions...");
        StandardIndex[] defIndices = cache.getIndexTable().getItemDefinitionIndices();
        new ItemDefinitionParser(cache, defIndices, this).parse();
        logger.info("Loaded " + definitionCount + " Item Definitions.");
    }

    @Override
    public void itemParsed(CacheItemDefinition cacheItemDefinition) {
        definitionCount++;
        CacheItemDefinition.addItem(cacheItemDefinition);
    }

}
