package org.hyperion.cache.npc;

import org.hyperion.cache.Cache;
import org.hyperion.cache.InvalidCacheException;
import org.hyperion.Constants;
import org.hyperion.rs2.model.Animation;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Represents an actor.
 */
public final class ActorDefinition {

    /**
     * The logger
     */
    private static final Logger logger = Logger.getLogger(ActorDefinition.class.getName());
    /**
     * The actor definitions.
     */
    private static ActorDefinition[] definitions;

    /**
     * Initializes the class with the specified set of definitions.
     *
     * @param definitions The definitions.
     * @throws RuntimeException if there is an id mismatch.
     */
    public static void init(ActorDefinition[] definitions) {
        ActorDefinition.definitions = definitions;
        for (int id = 0; id < definitions.length; id++) {
            ActorDefinition def = definitions[id];
            if (def.getId() != id) {
                throw new RuntimeException("Actor definition id mismatch!");
            }
        }
    }

    /**
     * Gets the total number of actors.
     *
     * @return The total number of actors.
     */
    public static int count() {
        return definitions.length;
    }

    /**
     * Gets the actor definition for the specified id.
     *
     * @param id The id.
     * @return The definition.
     * @throws IndexOutOfBoundsException if the id is out of bounds.
     */
    public static ActorDefinition forId(int id) {
        if (id < 0 || id >= definitions.length) {
            return null;
        }
        return definitions[id];
    }

    /**
     * Loads the actor definition
     *
     * @throws InvalidCacheException
     * @throws IOException
     */
    public static void load() throws Exception {
        logger.info("Loading Actor Definitions...");
        Cache cache = new Cache(Constants.CACHE_DIRECTORY);
        ActorDefinitionParser parser = new ActorDefinitionParser(cache);
        ActorDefinition[] actorDefs = parser.parse();
        ActorDefinition.init(actorDefs);
        logger.info("Loaded " + definitions.length + " Actor Definitions");
    }

    /**
     * The actor's id.
     */
    private final int id;
    /**
     * The model id's of the actor.
     */
    private int[] modelIds;
    /**
     * The name of the actor.
     */
    private String name;
    /**
     * The description of the actor.
     */
    private String description;
    /**
     * The actor's size.
     */
    private int size = -1;
    /**
     * The actor's stand animation id.
     */
    private int standAnimationId = -1;
    /**
     * The actor's walk animation id.
     */
    private int walkAnimationId = -1;
    /**
     * The actor's turn around animation id.
     */
    private int turnAroundAnimationId = -1;
    /**
     * The actor's turn right animation id.
     */
    private int turnRightAnimationId = -1;
    /**
     * The actor's turn left animation id.
     */
    private int turnLeftAnimationId = -1;
    /**
     * The actor's actions.
     */
    private final String[] actions = new String[10];
    /**
     * The original model colors of the actor.
     */
    private int[] originalModelColors;
    /**
     * The modified model colors of the actor.
     */
    private int[] modifiedModelColors;
    /**
     * The head model id's of the actor.
     */
    private int[] headModelIds;
    /**
     * Whether or not the actor is visible on the minimap.
     */
    private boolean visibleOnMinimap = true;
    /**
     * The combat level of the actor.
     */
    private int combatLevel = -1;
    /**
     * The x size of the actor.
     */
    private int sizeX = 128;
    /**
     * The y size of the actor.
     */
    private int sizeY = 128;
    /**
     * Whether or not the actor is visible.
     */
    private boolean visible = false;
    /**
     * The brightness of the actor.
     */
    private int brightness;
    /**
     * The contrast of the actor.
     */
    private int contrast;
    /**
     * The head icon of the actor.
     */
    private int headIcon = -1;
    /**
     * The amount of degrees to turn for the actor.
     */
    private int degreesToTurn = 32;
    /**
     * The var bit id of the actor.
     */
    private int varBitId = -1;
    /**
     * The setting id of the actor.
     */
    private int settingId = -1;
    /**
     * The children id's of the actor.
     */
    private int[] childrenIds;
    /**
     * Whether or not the actor is clickable.
     */
    private boolean clickable = true;

    /**
     * Creates an actor definition with the default values.
     *
     * @param id The actor's id.
     */
    public ActorDefinition(int id) {
        this.id = id;
    }

    /**
     * Gets the actor's id.
     *
     * @return The actor's id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the model id size of this actor.
     *
     * @param size The size of the actor's model id array.
     */
    public void setModelIdSize(int size) {
        this.modelIds = new int[size];
    }

    /**
     * Sets a model id of this actor.
     *
     * @param index The actor's model index.
     * @param modelId The actor's model id.
     */
    public void setModelId(int index, int modelId) {
        this.modelIds[index] = modelId;
    }

    /**
     * Gets a model id of this actor.
     *
     * @param index The index of the actor's model id.
     * @return The model id of this actor with the specified index.
     * @throws IndexOutOfBoundsException if the index is out of bounds.
     */
    public int getModelId(int index) {
        if (index < 0 || index >= modelIds.length) {
            throw new IndexOutOfBoundsException();
        }
        return modelIds[index];
    }

    /**
     * Sets the name of this actor.
     *
     * @param name The actor's name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the name of this actor.
     *
     * @return The name of this actor, or {@code null} if it has no name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the description of this actor.
     *
     * @param description The actor's description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the description of this actor.
     *
     * @return The actor's description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the size of this actor.
     *
     * @param size The actor's name.
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Gets the size of this actor.
     *
     * @return The size of this actor.
     */
    public int getSize() {
        return size;
    }

    /**
     * Sets the stand animation id of this actor.
     *
     * @param standAnimationId The actor's stand animation id.
     */
    public void setStandAnimationId(int standAnimationId) {
        this.standAnimationId = standAnimationId;
    }

    /**
     * Gets the stand animation id of this actor.
     *
     * @return The stand animation id of this actor.
     */
    public Animation getStandAnimation() {
        return Animation.create(standAnimationId);
    }

    /**
     * Sets the walk animation id of this actor.
     *
     * @param walkAnimationId The actor's walk animation id.
     */
    public void setWalkAnimationId(int walkAnimationId) {
        this.walkAnimationId = walkAnimationId;
    }

    /**
     * Gets the walk animation id of this actor.
     *
     * @return The walk animation id of this actor.
     */
    public Animation getWalkAnimation() {
        return Animation.create(walkAnimationId);
    }

    /**
     * Sets the turn around animation id of this actor.
     *
     * @param turnAroundAnimationId The actor's turn around animation id.
     */
    public void setTurnAroundAnimationId(int turnAroundAnimationId) {
        this.walkAnimationId = turnAroundAnimationId;
    }

    /**
     * Gets the turn around animation id of this actor.
     *
     * @return The turn around animation id of this actor.
     */
    public Animation getTurnAroundAnimation() {
        return Animation.create(turnAroundAnimationId);
    }

    /**
     * Sets the turn right animation id of this actor.
     *
     * @param turnRightAnimationId The actor's turn right animation id.
     */
    public void setTurnRightAnimationId(int turnRightAnimationId) {
        this.walkAnimationId = turnRightAnimationId;
    }

    /**
     * Gets the turn right animation id of this actor.
     *
     * @return The right around animation id of this actor.
     */
    public Animation getTurnRightAnimation() {
        return Animation.create(turnRightAnimationId);
    }

    /**
     * Sets the turn left animation id of this actor.
     *
     * @param turnLeftAnimationId The actor's turn left animation id.
     */
    public void setTurnLeftAnimationId(int turnLeftAnimationId) {
        this.turnLeftAnimationId = turnLeftAnimationId;
    }

    /**
     * Gets the turn left animation id of this actor.
     *
     * @return The left around animation id of this actor.
     */
    public Animation getTurnLeftAnimation() {
        return Animation.create(turnLeftAnimationId);
    }

    /**
     * Sets a action.
     *
     * @param index The index of the action.
     * @param action The action.
     * @throws IndexOutOfBoundsException if the index is out of bounds.
     */
    public void setAction(int index, String action) {
        if (index < 0 || index >= actions.length) {
            throw new IndexOutOfBoundsException();
        }
        actions[index] = action;
    }

    /**
     * Gets a action.
     *
     * @param index The id.
     * @return The action.
     * @throws IndexOutOfBoundsException if the index is out of bounds.
     */
    public String getAction(int index) {
        if (index < 0 || index >= actions.length) {
            throw new IndexOutOfBoundsException();
        }
        return actions[index];
    }

    /**
     * Sets the original model color of this actor.
     *
     * @param index The original model color of the actor.
     */
    public void setOriginalModelColorSize(int index) {
        this.originalModelColors = new int[index];
    }

    /**
     * Sets a original model color of this actor.
     *
     * @param index The actor's original model color index.
     * @param modelId The actor's original model color.
     */
    public void setOriginalModelColor(int index, int modelId) {
        this.originalModelColors[index] = modelId;
    }

    /**
     * Gets a original model color of this actor.
     *
     * @param index The index of the actor's original model color.
     * @return The original model color of this actor with the specified index.
     * @throws IndexOutOfBoundsException if the index is out of bounds.
     */
    public int getOriginalModelColor(int index) {
        if (index < 0 || index >= originalModelColors.length) {
            throw new IndexOutOfBoundsException();
        }
        return originalModelColors[index];
    }

    /**
     * Sets the modified model color of this actor.
     *
     * @param index The modified model color of the actor.
     */
    public void setModifiedModelColorSize(int index) {
        this.modifiedModelColors = new int[index];
    }

    /**
     * Sets a modified model color of this actor.
     *
     * @param index The actor's modified model color index.
     * @param modelId The actor's modified model color.
     */
    public void setModifiedModelColor(int index, int modelId) {
        this.modifiedModelColors[index] = modelId;
    }

    /**
     * Gets a modified model color of this actor.
     *
     * @param index The index of the actor's modified model color.
     * @return The modified model color of this actor with the specified index.
     * @throws IndexOutOfBoundsException if the index is out of bounds.
     */
    public int getModifiedModelColor(int index) {
        if (index < 0 || index >= modifiedModelColors.length) {
            throw new IndexOutOfBoundsException();
        }
        return modifiedModelColors[index];
    }

    /**
     * Sets the head model id size of this actor.
     *
     * @param size The size of the actor's head model id array.
     */
    public void setHeadModelIdSize(int size) {
        this.headModelIds = new int[size];
    }

    /**
     * Sets a head model id of this actor.
     *
     * @param index The actor's head model index.
     * @param modelId The actor's head model id.
     */
    public void setHeadModelId(int index, int modelId) {
        this.headModelIds[index] = modelId;
    }

    /**
     * Gets a head model id of this actor.
     *
     * @param index The index of the actor's head model id.
     * @return The head model id of this actor with the specified index.
     * @throws IndexOutOfBoundsException if the index is out of bounds.
     */
    public int getHeadModelId(int index) {
        if (index < 0 || index >= headModelIds.length) {
            throw new IndexOutOfBoundsException();
        }
        return headModelIds[index];
    }

    /**
     * Checks whether or not the actor is visible on the minimap.
     *
     * @return Whether or not the actor is visible on the minimap.
     */
    public boolean isVisibleOnMinimap() {
        return visibleOnMinimap;
    }

    /**
     * Sets whether or not the actor is visible on the minimap.
     *
     * @param visibleOnMinimap Whether or not the actor is visible on the
     * minimap.
     */
    public void setVisibleOnMinimap(boolean visibleOnMinimap) {
        this.visibleOnMinimap = visibleOnMinimap;
    }

    /**
     * Gets the combat level of the actor.
     *
     * @return The combat level of the actor.
     */
    public int getCombatLevel() {
        return combatLevel;
    }

    /**
     * Sets the combat level of the actor.
     *
     * @param combatLevel The combat level of the actor.
     */
    public void setCombatLevel(int combatLevel) {
        this.combatLevel = combatLevel;
    }

    /**
     * Gets the x size of the actor.
     *
     * @return The x size of the actor.
     */
    public int getSizeX() {
        return sizeX;
    }

    /**
     * Sets the x size of the actor.
     *
     * @param sizeX The x size of the actor.
     */
    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    /**
     * Gets the y size of the actor.
     *
     * @return The y size of the actor.
     */
    public int getSizeY() {
        return sizeY;
    }

    /**
     * Sets the y size of the actor.
     *
     * @param sizeY The y size of the actor.
     */
    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    /**
     * Checks whether or not the actor is visible.
     *
     * @return Whether or not the actor is visible.
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Sets whether or not the actor is visible.
     *
     * @param visible Whether or not the actor is visible.
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Gets the brightness of the actor.
     *
     * @return The brightness of the actor.s
     */
    public int getBrightness() {
        return brightness;
    }

    /**
     * Sets the brightness of the actor.
     *
     * @param brightness The brightness of the actor.
     */
    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    /**
     * Gets the contrast of the actor.
     *
     * @return The contrast of the actor.
     */
    public int getContrast() {
        return contrast;
    }

    /**
     * Sets the contrast of the actor.
     *
     * @param contrast The contrast of the actor.
     */
    public void setContrast(int contrast) {
        this.contrast = contrast;
    }

    /**
     * Gets the head icon of the actor.
     *
     * @return The head icon of the actor.
     */
    public int getHeadIcon() {
        return headIcon;
    }

    /**
     * Sets the head icon of the actor.
     *
     * @param headIcon The head icon of the actor.
     */
    public void setHeadIcon(int headIcon) {
        this.headIcon = headIcon;
    }

    /**
     * Gets the amount of degrees for the actor to turn.
     *
     * @return The amount of degrees for the actor to turn.
     */
    public int getDegreesToTurn() {
        return degreesToTurn;
    }

    /**
     * Sets the amount of degrees for the actor to turn.
     *
     * @param degreesToTurn The amount of degrees for the actor to turn.
     */
    public void setDegreesToTurn(int degreesToTurn) {
        this.degreesToTurn = degreesToTurn;
    }

    /**
     * Gets the var bit id of the actor.
     *
     * @return The var bit id of the actor.
     */
    public int getVarBitId() {
        return varBitId;
    }

    /**
     * Sets the var bit id of the actor.
     *
     * @param varBitId The var bit id of the actor.
     */
    public void setVarBitId(int varBitId) {
        this.varBitId = varBitId;
    }

    /**
     * Gets the setting id of the actor.
     *
     * @return The setting id of the actor.
     */
    public int getSettingId() {
        return settingId;
    }

    /**
     * Sets the setting id of the actor.
     *
     * @param settingId The setting id of the actor.
     */
    public void setSettingId(int settingId) {
        this.settingId = settingId;
    }

    /**
     * Sets the children id size of this actor.
     *
     * @param size The size of the actor's children id array.
     */
    public void setChildrenIdSize(int size) {
        this.childrenIds = new int[size];
    }

    /**
     * Sets a child id of this actor.
     *
     * @param index The actor's child index.
     * @param childId The actor's child id.
     */
    public void setChildId(int index, int childId) {
        this.childrenIds[index] = childId;
    }

    /**
     * Gets a child id of this actor.
     *
     * @param index The index of the actor's child id.
     * @return The child id of this actor with the specified index.
     * @throws IndexOutOfBoundsException if the index is out of bounds.
     */
    public int getChildId(int index) {
        if (index < 0 || index >= childrenIds.length) {
            throw new IndexOutOfBoundsException();
        }
        return childrenIds[index];
    }

    /**
     * Checks whether or not the actor is clickable.
     *
     * @return Whether or not the actor is clickable.
     */
    public boolean isClickable() {
        return clickable;
    }

    /**
     * Sets whether or not the actor is clickable.
     *
     * @param clickable Whether or not the actor is clickable.
     */
    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }
}
