package org.hyperion.cache.npc;

import org.hyperion.cache.Archive;
import org.hyperion.cache.Cache;
import org.hyperion.cache.util.ByteBufferUtils;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * A class which parses actor definitions.
 */
public final class ActorDefinitionParser {

    /**
     * The indexed file system.
     */
    private final Cache cache;

    /**
     * Creates the actor definition parser.
     *
     * @param cache The indexed file system.
     */
    public ActorDefinitionParser(Cache cache) {
        this.cache = cache;
    }

    /**
     * Parses the actor definitions.
     *
     * @return The actor definitions.
     * @throws IOException if an I/O error occurs.
     */
    public ActorDefinition[] parse() throws IOException {
        Archive config = new Archive(cache.getFile(0, 2));
        ByteBuffer dat = config.getFileAsByteBuffer("npc.dat");
        ByteBuffer idx = config.getFileAsByteBuffer("npc.idx");

        int count = idx.getShort();
        int[] indices = new int[count];
        int index = 2;
        for (int i = 0; i < count; i++) {
            indices[i] = index;
            index += idx.getShort();
        }

        ActorDefinition[] defs = new ActorDefinition[count];
        for (int i = 0; i < count; i++) {
            dat.position(indices[i]);
            defs[i] = parseDefinition(i, dat);
        }

        return defs;
    }

    /**
     * Parses a single definition.
     *
     * @param id The actor's id.
     * @param buffer The buffer.
     * @return The definition.
     */
    private ActorDefinition parseDefinition(int id, ByteBuffer buffer) {
        ActorDefinition def = new ActorDefinition(id);

        while (true) {
            int code = buffer.get() & 0xFF;

            if (code == 0) {
                return def;
            } else if (code == 1) {
                int modelCount = buffer.get() & 0xFF;
                def.setModelIdSize(modelCount);
                for (int index = 0; index < modelCount; index++) {
                    def.setModelId(index, buffer.getShort() & 0xFFFF);
                }
            } else if (code == 2) {
                def.setName(ByteBufferUtils.getString(buffer));
            } else if (code == 3) {
                def.setDescription(ByteBufferUtils.getString(buffer));
            } else if (code == 12) {
                def.setSize(buffer.get());
            } else if (code == 13) {
                def.setStandAnimationId(buffer.getShort() & 0xFFFF);
            } else if (code == 14) {
                def.setWalkAnimationId(buffer.getShort() & 0xFFFF);
            } else if (code == 17) {
                def.setWalkAnimationId(buffer.getShort() & 0xFFFF);
                def.setTurnAroundAnimationId(buffer.getShort() & 0xFFFF);
                def.setTurnRightAnimationId(buffer.getShort() & 0xFFFF);
                def.setTurnLeftAnimationId(buffer.getShort() & 0xFFFF);
            } else if (code >= 30 && code < 40) {
                //  System.out.println("code:" + code);
                String str = ByteBufferUtils.getString(buffer);
                //    System.out.println("str:" + str);
                if (str.equalsIgnoreCase("hidden")) {
                    str = null;
                }
                def.setAction(code - 30, str);
            } else if (code == 40) {
                int modelColorCount = buffer.get() & 0xFF;
                def.setOriginalModelColorSize(modelColorCount);
                def.setModifiedModelColorSize(modelColorCount);
                for (int index = 0; index < modelColorCount; index++) {
                    def.setOriginalModelColor(index, buffer.getShort() & 0xFFFF);
                    def.setModifiedModelColor(index, buffer.getShort() & 0xFFFF);
                }
            } else if (code == 60) {
                int headModelCount = buffer.get() & 0xFF;
                def.setHeadModelIdSize(headModelCount);
                for (int index = 0; index < headModelCount; index++) {
                    def.setHeadModelId(index, buffer.getShort() & 0xFFFF);
                }
            } else if (code == 90) {
                // System.out.println("dummy1");
                buffer.getShort(); // dummy
            } else if (code == 91) {
                //  System.out.println("dummy1");
                buffer.getShort(); // dummy
            } else if (code == 92) {
                //    System.out.println("dummy1");
                buffer.getShort(); // dummy
            } else if (code == 93) {
                def.setVisibleOnMinimap(false);
            } else if (code == 95) {
                def.setCombatLevel(buffer.getShort() & 0xFFFF);
            } else if (code == 97) {
                def.setSizeX(buffer.getShort() & 0xFFFF);
            } else if (code == 98) {
                def.setSizeY(buffer.getShort() & 0xFFFF);
            } else if (code == 99) {
                def.setVisible(true);
            } else if (code == 100) {
                def.setBrightness(buffer.get());
            } else if (code == 101) {
                def.setContrast(buffer.get() * 5);
            } else if (code == 102) {
                def.setHeadIcon(buffer.getShort() & 0xFFFF);
            } else if (code == 103) {
                def.setDegreesToTurn(buffer.getShort() & 0xFFFF);
            } else if (code == 106) {
                int varBitId = buffer.getShort() & 0xFFFF;
                if (varBitId == 65535) {
                    varBitId = -1;
                }
                def.setVarBitId(varBitId);
                int settingId = buffer.getShort() & 0xFFFF;
                if (settingId == 65535) {
                    settingId = -1;
                }
                def.setSettingId(settingId);

                int childrenCount = buffer.get();
                //def.setChildrenIdSize(childrenCount + 1);
                for (int index = 0; index < childrenCount; index++) {
                    int childId = buffer.getShort() & 0xFFFF;
                    if (childId == 65535) {
                        childId = -1;
                    }
                    //def.setChildId(index, childId);
                }
            } else if (code == 107) {
                def.setClickable(false);
            }
        }
    }
}
