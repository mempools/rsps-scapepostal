package org.hyperion.loginserver;

public class NodeConfiguration {

    /**
     * The node Id.
     */
    private final int nodeid;

    /**
     * The host.
     */
    private final String host;

    /**
     * The port.
     */
    private final int port;

    /**
     * The password.
     */
    private final String password;

    /**
     * The description.
     */
    private final String description;

    /**
     * The world type.
     */
    private final WorldType type;

    /**
     * Creates the world
     *
     * @param nodeid The node Id.
     * @param host The host.
     * @param port The port.
     * @param password The password.
     * @param description The description
     * @param type The type.
     */
    public NodeConfiguration(int nodeid, String host, int port, String password, String description, WorldType type) {
        this.nodeid = nodeid;
        this.host = host;
        this.port = port;
        this.password = password;
        this.description = description;
        this.type = type;
    }

    /**
     * Gets the node Id.
     *
     * @return The node Id.
     */
    public int getNodeid() {
        return nodeid;
    }

    /**
     * Gets the host.
     *
     * @return The host
     */
    public String getHost() {
        return host;
    }

    /**
     * Gets the port.
     *
     * @return The port.
     */
    public int getPort() {
        return port;
    }

    /**
     * Gets the password.
     *
     * @return The password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets the description.
     *
     * @return The description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the world type
     *
     * @return The world type
     */
    public WorldType getType() {
        return type;
    }

    /**
     * The world types.
     */
    public enum WorldType {

        /**
         * Non-members
         */
        NON_MEMBERS,
        /**
         * Members
         */
        MEMBERS
    }
}
