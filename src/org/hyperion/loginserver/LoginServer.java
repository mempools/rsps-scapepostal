package org.hyperion.loginserver;

import org.hyperion.rs2.net.ChannelStorage;
import org.hyperion.database.ConnectionPool;
import org.hyperion.database.DatabaseConnection;
import org.hyperion.database.mysql.MySQLDatabaseConfiguration;
import org.hyperion.database.mysql.MySQLDatabaseConnection;
import org.hyperion.Constants;
import org.hyperion.rs2.model.player.PlayerDetails;
import org.hyperion.util.CommonConstants;
import org.hyperion.util.configuration.ConfigurationNode;
import org.hyperion.util.configuration.ConfigurationParser;
import org.hyperion.util.login.LoginPipelineFactory;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.AdaptiveReceiveBufferSizePredictor;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

/**
 * The login server.
 *
 * @author Graham Edgecombe
 */
public class LoginServer {

    /**
     * MySQL Connector
     */
    private ConnectionPool<MySQLDatabaseConnection> pool;

    /**
     * The list of nodes
     */
    private final ChannelStorage<Node> nodeStorage = new ChannelStorage<>();

    /**
     * Logger instance.
     */
    private static final Logger logger = Logger.getLogger(LoginServer.class.getName());

    /**
     * The acceptor.
     */
    private final ServerBootstrap loginBootstrap;

    /**
     * The acceptor.
     */
    private final ServerBootstrap worldListBootstrap;

    /**
     * The task queue.
     */
    private final BlockingQueue<Runnable> tasks = new LinkedBlockingQueue<>();

    /**
     * Login server loader.
     */
    private final LoginWorldLoader loader = new LoginWorldLoader(this);

    /**
     * The entry point of the program.
     *
     * @param args The command line arguments.
     * @throws java.io.IOException if an I/O error occurs.
     */
    public static void main(String[] args) throws IOException {
        LoginServer server = new LoginServer();
        server.bind();
        try {
            logger.info("Loading configuration...");
            server.loadConfiguration();
            logger.info("Loading nodes...");
            server.loadNodes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        server.start();
    }

    /**
     * Creates the login server.
     */
    public LoginServer() {
        logger.info("Starting Login Server...");
        loginBootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(
                Executors.newSingleThreadExecutor(), Executors.newSingleThreadExecutor()));
        loginBootstrap.setPipelineFactory(new LoginPipelineFactory(new LoginConnectionHandler(this)));
        loginBootstrap.setOption("receiveBufferSizePredictor", new AdaptiveReceiveBufferSizePredictor(1, 2048, 10240));
        loginBootstrap.setOption("child.receiveBufferSizePredictor", new AdaptiveReceiveBufferSizePredictor(1, 2048, 10240));
        worldListBootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(
                Executors.newSingleThreadExecutor(), Executors.newSingleThreadExecutor()));
        worldListBootstrap.setPipelineFactory(new LoginPipelineFactory(new WorldListConnectionHandler(this)));
    }

    /**
     * Binds the login server to the default port.
     *
     * @return The login server instance, for chaining.
     * @throws java.io.IOException if an I/O error occurs.
     */
    public LoginServer bind() throws IOException {
        logger.info("Binding to port : " + CommonConstants.LOGIN_PORT + "...");
        loginBootstrap.bind(new InetSocketAddress(CommonConstants.LOGIN_PORT));
        logger.info("Binding world list to port : " + CommonConstants.LOGIN_PORT + "...");
        worldListBootstrap.bind(new InetSocketAddress(CommonConstants.LOGIN_PORT + 1));
        return this;
    }

    /**
     * Starts the login server.
     */
    public void start() {
        logger.info("Ready.");
        while (true) {
            try {
                tasks.take().run();
            } catch (InterruptedException e) {
                continue;
            }
        }
    }

    /**
     * Pushes a task onto the queue.
     *
     * @param runnable The runnable.
     */
    public void pushTask(Runnable runnable) {
        tasks.add(runnable);
    }

    /**
     * Gets the login server loader.
     *
     * @return The loader.
     */
    public LoginWorldLoader getLoader() {
        return loader;
    }

    /**
     * Loads server configuration.
     *
     * @throws java.io.IOException if an I/O error occurs.
     */
    private void loadConfiguration() throws IOException {
        try (FileInputStream fis = new FileInputStream(Constants.CONFIGURATION_FILE)) {
            ConfigurationParser parser = new ConfigurationParser(fis);
            ConfigurationNode node = parser.parse();
            if (node.has("database")) {
                ConfigurationNode settings = node.nodeFor("database");
                if (settings.getBoolean("enabled")) {
                    MySQLDatabaseConfiguration config = new MySQLDatabaseConfiguration();
                    config.setHost(settings.getString("host"));
                    config.setPort(Integer.parseInt(settings.getString("port")));
                    config.setDatabase(settings.getString("database"));
                    config.setUsername(settings.getString("username"));
                    config.setPassword(settings.getString("password"));
                    pool = new ConnectionPool<>(config);
                }
            }
        }
    }

    /**
     * Load the nodes from the mysql database
     *
     * @throws Exception
     */
    public void loadNodes() throws Exception {
        DatabaseConnection connection = pool.nextFree();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT nodeid, host, port, password, description, type FROM nodes");
            while (rs.next()) {
                int nodeid = rs.getInt("nodeid");
                NodeManager.getNodeManager().addNodeConfiguration(nodeid, new NodeConfiguration(nodeid, rs.getString("host"), rs.getInt("port"), rs.getString("password"),
                        rs.getString("description"), NodeConfiguration.WorldType.values()[rs.getInt("type")]));
            }
            stmt.close();
        } finally {
            connection.returnConnection();
        }
        logger.info("Loaded " + NodeManager.getNodeManager().listConfigurations().size() + " nodes.");
    }

    /**
     * Get a ban if present
     *
     * @param pd The subject
     * @return The ban
     */
    public boolean checkBan(PlayerDetails pd) {
//        try {
//            DatabaseConnection connection = pool.nextFree();
//            try {
//                PreparedStatement statement = connection.prepareStatement("SELECT id FROM punishments WHERE subject IN(?, ?, ?) AND type IN ('BAN', 'UIDBAN', 'PROFILEBAN')");
//                statement.setString(1, pd.getName());
//                statement.setString(2, Integer.toString(pd.getUID()));
//                statement.setString(3, pd.getProfile());
//                try {
//                    if (statement.executeQuery().next()) {
//                        return true;
//                    }
//                } finally {
//                    statement.close();
//                }
//            } finally {
//                connection.returnConnection();
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        return false;
    }

    /**
     * A map of channel -> node
     *
     * @return The node storage
     */
    public ChannelStorage<Node> getNodeStorage() {
        return nodeStorage;
    }
}
