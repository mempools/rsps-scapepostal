package org.hyperion.loginserver;

import org.hyperion.rs2.WorldLoader.LoginResult;
import org.hyperion.rs2.model.player.PlayerDetails;
import org.hyperion.rs2.util.NameUtils;
import org.hyperion.util.buffer.BinaryPart;
import org.hyperion.util.buffer.BinaryPartUtil;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class LoginWorldLoader {

    /**
     * The login server instance.
     */
    private final LoginServer server;

    /**
     * Constructs the login world loader
     *
     * @param server The login server
     */
    public LoginWorldLoader(LoginServer server) {
        this.server = server;
    }

    /**
     * Check a login
     *
     * @param pd The player details
     * @return The LoginResult
     */
    public LoginResult checkLogin(PlayerDetails pd) {
        PlayerData playerData = null;
        int code = LoginResult.RESPONSE_OK;
        if (server.checkBan(pd)) {
            code = LoginResult.RESPONSE_BANNED;
        }
        File f = new File("data/savedGames/" + NameUtils.formatNameForProtocol(pd.getName()) + ".dat.gz");
        if (f.exists() && code == LoginResult.RESPONSE_OK) {
            try {
                InputStream is = new GZIPInputStream(new FileInputStream(f));

                String name = null;
                int rights = -1;

                BinaryPart dataPart = BinaryPartUtil.readPartFromInput(is);
                if (dataPart.getOpcode() != 0) {
                    code = LoginResult.RESPONSE_LOCKED;
                } else {
                    name = dataPart.getString();
                    String pass = dataPart.getString();
                    if (!name.equals(NameUtils.formatName(pd.getName()))) {
                        code = LoginResult.RESPONSE_INVALID_INFORMATION;
                    }
                    if (!pass.equals(pd.getPassword())) {
                        code = LoginResult.RESPONSE_INVALID_INFORMATION;
                    }
                }
                BinaryPart infoPart = BinaryPartUtil.readPartFromInput(is);
                if (infoPart.getOpcode() != 1) {
                    code = LoginResult.RESPONSE_LOCKED;
                } else {
                    rights = infoPart.getUnsigned();
                }
                if (code == LoginResult.RESPONSE_OK) {
                    playerData = new PlayerData(name, rights);
                }
                is.close();
            } catch (IOException ex) {
                code = LoginResult.RESPONSE_LOCKED;
            }
        }
        return new LoginResult(code, playerData);
    }

    /**
     * Load a player file as a ChannelBuffer
     *
     * @param pd The player details
     * @return The buffer
     */
    public ChannelBuffer loadPlayerFile(PlayerDetails pd) {
        try {
            File f = new File("data/savedGames/" + NameUtils.formatNameForProtocol(pd.getName()) + ".dat.gz");
            InputStream is = new GZIPInputStream(new FileInputStream(f));
            ChannelBuffer buf = ChannelBuffers.dynamicBuffer();
            while (true) {
                byte[] temp = new byte[1024];
                int read = is.read(temp, 0, temp.length);
                if (read == -1) {
                    break;
                } else {
                    buf.writeBytes(temp, 0, read);
                }
            }
            is.close();
            return buf;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Save a player
     *
     * @param name The name
     * @param data The raw byte data
     * @return True if saved
     */
    public boolean savePlayer(String name, byte[] data) {
        try {
            OutputStream os = new GZIPOutputStream(new FileOutputStream("data/savedGames/" + name + ".dat.gz"));
            os.write(data);
            os.flush();
            os.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
