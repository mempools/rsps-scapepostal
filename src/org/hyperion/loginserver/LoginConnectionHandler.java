package org.hyperion.loginserver;

import org.hyperion.rs2.util.ChannelBufferUtils;
import org.hyperion.util.login.LoginPacket;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;

/**
 * Handles the login server connections.
 *
 * @author Graham Edgecombe
 */
public class LoginConnectionHandler extends SimpleChannelHandler {

    /**
     * The login server.
     */
    private LoginServer server;

    /**
     * Creates the connection handler.
     *
     * @param server The server.
     */
    public LoginConnectionHandler(LoginServer server) {
        this.server = server;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        e.getChannel().close();
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        final Channel channel = e.getChannel();
        final LoginPacket packet = (LoginPacket) e.getMessage();
        server.pushTask(new Runnable() {
            public void run() {
                if (server.getNodeStorage().contains(channel)) {
                    server.getNodeStorage().get(channel).handlePacket(packet);
                } else {
                    handlePreAuthenticationPacket(channel, packet);
                }
            }
        });
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, final ChannelStateEvent evt) throws Exception {
        server.pushTask(new Runnable() {
            public void run() {
                if (server.getNodeStorage().contains(evt.getChannel())) {
                    NodeManager.getNodeManager().unregister(server.getNodeStorage().get(evt.getChannel()));
                }
            }
        });
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        // TODO pipeline
    }

    /**
     * Handles the authentication packet.
     *
     * @param channel The session.
     * @param message The message
     */
    private void handlePreAuthenticationPacket(Channel channel, LoginPacket message) {
        if (message.getOpcode() == LoginPacket.AUTH) {
            ChannelBuffer payload = message.getPayload();
            int node = payload.readShort();
            String password = ChannelBufferUtils.getRS2String(payload);
            boolean valid = NodeManager.getNodeManager().isNodeAuthenticationValid(node, password);
            Node n = new Node(server, channel, node);
            server.getNodeStorage().set(channel, n);
            NodeManager.getNodeManager().register(n);
            int code = valid ? 0 : 1;
            ChannelBuffer resp = ChannelBuffers.buffer(1);
            resp.writeByte((byte) code);
            channel.write(new LoginPacket(LoginPacket.AUTH_RESPONSE, resp));
        } else {
            channel.close();
        }
    }

}
