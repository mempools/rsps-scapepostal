package org.hyperion.loginserver;

import org.hyperion.loginserver.NodeConfiguration.WorldType;
import org.hyperion.rs2.util.ChannelBufferUtils;
import org.hyperion.util.login.LoginPacket;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;

/**
 * Handles the login server connections.
 *
 * @author Graham Edgecombe
 */
public class WorldListConnectionHandler extends SimpleChannelHandler {

    /**
     * The login server.
     */
    private final LoginServer server;

    /**
     * Creates the connection handler.
     *
     * @param server The server.
     */
    public WorldListConnectionHandler(LoginServer server) {
        this.server = server;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        e.getChannel().close();
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, final MessageEvent e) throws Exception {
        server.pushTask(new Runnable() {
            @Override
            public void run() {
                handleWorldList(e.getChannel(), (LoginPacket) e.getMessage());
            }
        });
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        /*
         * session.getFilterChain().addFirst("protocolCodecFilter", new
         * ProtocolCodecFilter(new LoginPipelineFactory()));
         */
    }

    private void handleWorldList(Channel channel, LoginPacket message) {
        ChannelBuffer data = ChannelBuffers.dynamicBuffer();
        data.writeByte((byte) NodeManager.getNodeManager().listConfigurations().size());
        if (message.getOpcode() == 0) {
            // Node list
            for (NodeConfiguration config : NodeManager.getNodeManager().listConfigurations()) {
                data.writeByte((byte) config.getNodeid());
                ChannelBufferUtils.putRS2String(data, config.getHost());// HOST
                data.writeShort((short) config.getPort());
                if (NodeManager.getNodeManager().isNodeOnline(config.getNodeid())) {
                    data.writeShort((short) NodeManager.getNodeManager().getNode(config.getNodeid()).getPlayerCount());
                } else {
                    data.writeShort(Short.MAX_VALUE);
                }
                ChannelBufferUtils.putRS2String(data, config.getDescription());
                data.writeByte((byte) (config.getType() == WorldType.MEMBERS ? 1 : 0));
            }
        } else {
            // Node update
            for (NodeConfiguration config : NodeManager.getNodeManager().listConfigurations()) {
                data.writeByte((byte) config.getNodeid());
                if (NodeManager.getNodeManager().isNodeOnline(config.getNodeid())) {
                    data.writeShort((short) NodeManager.getNodeManager().getNode(config.getNodeid()).getPlayerCount());
                } else {
                    data.writeShort(Short.MAX_VALUE);
                }
            }
        }
        channel.write(new LoginPacket(message.getOpcode(), data)).addListener(ChannelFutureListener.CLOSE);
    }
}
