package org.hyperion;

import java.io.File;

/**
 * Holds global server constants.
 *
 * @author Graham Edgecombe
 */
public class Constants {

    /**
     * The configuration file for the server.
     */
    public static final File CONFIGURATION_FILE = new File("./data/configuration.conf/");

    /**
     * The directory for the cache.
     */
    public static final File CACHE_DIRECTORY = new File("./data/cache/");

    /**
     * The directory for the engine scripts.
     */
    public static final File SCRIPTS_DIRECTORY = new File("./data/scripts");

    /**
     * The directory for the game logs.
     */
    public static final File GAME_LOGS = new File("./data/logs/");

    /**
     * The maximum amount of packets a player can have in the queue
     */
    public static final int PLAYER_MAX_PACKETS = 15;

    /**
     * The player cap.
     */
    public static final int MAX_PLAYERS = 2000;

    /**
     * The NPC cap.
     */
    public static final int MAX_NPCS = 32000;

    /**
     * An array of valid characters in a long username.
     */
    public static final char VALID_CHARS[] = {'_', 'a', 'b', 'c', 'd',
        'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
        'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3',
        '4', '5', '6', '7', '8', '9', '!', '@', '#', '$', '%', '^', '&',
        '*', '(', ')', '-', '+', '=', ':', ';', '.', '>', '<', ',', '"',
        '[', ']', '|', '?', '/', '`'};

    /**
     * Packed text translate table.
     */
    public static final char XLATE_TABLE[] = {' ', 'e', 't', 'a', 'o', 'i', 'h', 'n',
        's', 'r', 'd', 'l', 'u', 'm', 'w', 'c', 'y', 'f', 'g', 'p', 'b',
        'v', 'k', 'x', 'j', 'q', 'z', '0', '1', '2', '3', '4', '5', '6',
        '7', '8', '9', ' ', '!', '?', '.', ',', ':', ';', '(', ')', '-',
        '&', '*', '\\', '\'', '@', '#', '+', '=', '\243', '$', '%', '"',
        '[', ']'};

    /**
     * The maximum amount of items in a stack.
     */
    public static final int MAX_ITEMS = Integer.MAX_VALUE;

    /**
     * Difference in X coordinates for directions array.
     */
    public static final byte[] DIRECTION_DELTA_X = new byte[]{-1, 0, 1, -1, 1, -1, 0, 1};

    /**
     * Difference in Y coordinates for directions array.
     */
    public static final byte[] DIRECTION_DELTA_Y = new byte[]{1, 1, 1, 0, 0, -1, -1, -1};

    /**
     * Default sidebar interfaces array.
     */
    public static final int SIDEBAR_INTERFACES[][] = new int[][]{
        new int[]{1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 0},
        new int[]{3917, 638, 3213, 1644, 5608, 1151, 5065, 5715, 2449,
            4445, 147, 6299, 2423},};

//    /**
//     * Incoming packet sizes array.
//     */
//    public static final int PACKET_SIZES[] = {
//        0, 0, 0, 1, -1, 0, 0, 0, 0, 0, //0
//        0, 0, 0, 0, 8, 0, 6, 2, 2, 0, //10
//        0, 2, 0, 6, 0, 12, 0, 0, 0, 0, //20
//        0, 0, 0, 0, 0, 8, 4, 0, 0, 2, //30
//        2, 6, 0, 6, 0, -1, 0, 0, 0, 0, //40
//        0, 0, 0, 12, 0, 0, 0, 0, 8, 0, //50
//        8, 0, 0, 0, 0, 0, 0, 0, 0, 0, //60
//        6, 0, 2, 2, 8, 6, 0, -1, 0, 6, //70
//        0, 0, 0, 0, 0, 1, 4, 6, 0, 0, //80
//        0, 0, 0, 0, 0, 3, 0, 0, -1, 0, //90
//        0, 13, 0, -1, 0, 0, 0, 0, 0, 0,//100
//        0, 0, 0, 0, 0, 0, 0, 6, 0, 0, //110
//        1, 0, 6, 0, 0, 0, -1, 0, 2, 6, //120
//        0, 4, 6, 8, 0, 6, 0, 0, 0, 2, //130
//        0, 0, 0, 0, 0, 6, 0, 0, 0, 0, //140
//        0, 0, 1, 2, 0, 2, 6, 0, 0, 0, //150
//        0, 0, 0, 0, -1, -1, 0, 0, 0, 0,//160
//        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //170
//        0, 8, 0, 3, 0, 2, 0, 0, 8, 1, //180
//        0, 0, 12, 0, 0, 0, 0, 0, 0, 0, //190
//        2, 0, 0, 0, 0, 0, 0, 0, 4, 0, //200
//        4, 0, 0, 0, 7, 8, 0, 0, 10, 0, //210
//        0, 0, 0, 0, 0, 0, -1, 0, 6, 0, //220
//        1, 0, 0, 0, 6, 0, 6, 8, 1, 0, //230
//        0, 4, 0, 0, 0, 0, -1, 0, -1, 4,//240
//        0, 0, 6, 6, 0, 0, 0 //250
//    };
    /**
     * Incoming packet sizes array.
     */
    public static int[] PACKET_SIZES = new int[256];

    static {
        /*
         * this is from a 377 whitescape.. it differs quite a bit from the
         * clients version though?
         */
        for (int i = 0; i < PACKET_SIZES.length; i++) {
            PACKET_SIZES[i] = 0;
        }
        PACKET_SIZES[1] = 12;
        PACKET_SIZES[3] = 6;
        PACKET_SIZES[4] = 6;
        PACKET_SIZES[6] = 0;
        PACKET_SIZES[8] = 2;
        PACKET_SIZES[13] = 2;
        PACKET_SIZES[19] = 4;
        PACKET_SIZES[22] = 2;
        PACKET_SIZES[24] = 6;
        PACKET_SIZES[28] = -1;
        PACKET_SIZES[31] = 4;
        PACKET_SIZES[36] = 8;
        PACKET_SIZES[40] = 0;
        PACKET_SIZES[42] = 2;
        PACKET_SIZES[45] = 2;
        PACKET_SIZES[49] = -1;
        PACKET_SIZES[50] = 6;
        PACKET_SIZES[54] = 6;
        PACKET_SIZES[55] = 6;
        PACKET_SIZES[56] = -1;
        PACKET_SIZES[57] = 8;
        PACKET_SIZES[67] = 2;
        PACKET_SIZES[71] = 6;
        PACKET_SIZES[75] = 4;
        PACKET_SIZES[77] = 6;
        PACKET_SIZES[78] = 4;
        PACKET_SIZES[79] = 2;
        PACKET_SIZES[80] = 2;
        PACKET_SIZES[83] = 8;
        PACKET_SIZES[91] = 6;
        PACKET_SIZES[95] = 4;
        PACKET_SIZES[100] = 6;
        PACKET_SIZES[104] = 4;
        PACKET_SIZES[110] = 0;
        PACKET_SIZES[112] = 2;
        PACKET_SIZES[116] = 2;
        PACKET_SIZES[119] = 1;
        PACKET_SIZES[120] = 8;
        PACKET_SIZES[123] = 7;
        PACKET_SIZES[126] = 1;
        PACKET_SIZES[136] = 6;
        PACKET_SIZES[140] = 4;
        PACKET_SIZES[141] = 8;
        PACKET_SIZES[143] = 8;
        PACKET_SIZES[152] = 12;
        PACKET_SIZES[157] = 4;
        PACKET_SIZES[158] = 6;
        PACKET_SIZES[160] = 8;
        PACKET_SIZES[161] = 6;
        PACKET_SIZES[163] = 13;
        PACKET_SIZES[165] = 1;
        PACKET_SIZES[168] = 0;
        PACKET_SIZES[171] = -1;
        PACKET_SIZES[173] = 3;
        PACKET_SIZES[176] = 3;
        PACKET_SIZES[177] = 6;
        PACKET_SIZES[181] = 6;
        PACKET_SIZES[184] = 10;
        PACKET_SIZES[187] = 1;
        PACKET_SIZES[194] = 2;
        PACKET_SIZES[197] = 4;
        PACKET_SIZES[202] = 0;
        PACKET_SIZES[203] = 6;
        PACKET_SIZES[206] = 8;
        PACKET_SIZES[210] = 8;
        PACKET_SIZES[211] = 12;
        PACKET_SIZES[213] = -1;
        PACKET_SIZES[217] = 8;
        PACKET_SIZES[222] = 3;
        PACKET_SIZES[226] = 2;
        PACKET_SIZES[227] = 9;
        PACKET_SIZES[228] = 6;
        PACKET_SIZES[230] = 6;
        PACKET_SIZES[231] = 6;
        PACKET_SIZES[233] = 2;
        PACKET_SIZES[241] = 6;
        PACKET_SIZES[244] = -1;
        PACKET_SIZES[245] = 2;
        PACKET_SIZES[247] = -1;
        PACKET_SIZES[248] = 0;
    }

}
