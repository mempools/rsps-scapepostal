package org.hyperion;

import org.hyperion.rs2.RS2Server;
import org.hyperion.rs2.model.World;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.hyperion.jagcached.FileServer;

/**
 * A class to start both the file and game servers.
 *
 * @author Graham Edgecombe
 */
public class Server {

    /**
     * The protocol version.
     */
    public static final int VERSION = 377;

    /**
     * Logger instance.
     */
    private static final Logger logger = Logger.getLogger(Server.class.getName());

    /**
     * The entry point of the application.
     *
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        logger.info("Starting Hyperion...");
        World.getWorld().init(); // this starts off background loading
        try {
            new FileServer().start();
            new RS2Server().init().bind(RS2Server.PORT).start();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error starting Hyperion.", ex);
            System.exit(1);
        }
    }
}
