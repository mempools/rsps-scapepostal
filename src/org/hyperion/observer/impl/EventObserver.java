package org.hyperion.observer.impl;

import org.hyperion.observer.Observable;
import org.hyperion.observer.Observer;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.EventContext;

/**
 * An interface that handles event observers.
 *
 * @author Global <http://rune-server.org/members/global>
 */
public abstract class EventObserver implements Observer<EventContext> {

    /**
     * Handles the script listener
     *
     * @param player The player
     * @param context The script context
     */
    public abstract void execute(Player player, EventContext context);

    @Override
    public void update(Observable<EventContext> observable, Player player, EventContext ctx) throws Exception {
        execute(player, ctx);
    }
}
