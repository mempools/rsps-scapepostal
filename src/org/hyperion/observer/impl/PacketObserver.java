package org.hyperion.observer.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.EventContext;

/**
 * A class that handles packet observers
 *
 * @author Global <http://rune-server.org/members/global>
 */
public class PacketObserver extends EventObserver {

    @Override
    public void execute(Player player, EventContext context) {
        try {
            update(null, player, context);
        } catch (Exception ex) {
            Logger.getLogger(PacketObserver.class.getName()).log(Level.SEVERE, "Error handling packet observer:", ex);
        }
    }

}
