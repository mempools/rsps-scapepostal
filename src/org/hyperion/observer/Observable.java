package org.hyperion.observer;

import java.util.ArrayList;
import java.util.List;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.EventContext;

/**
 * Globally manages all observable events
 *
 * @author Global <http://rune-server.org/members/global>
 * @param <T> The event context class.
 */
public class Observable<T extends EventContext> {

    /**
     * The list of active observers.
     */
    private final List<Observer<T>> active = new ArrayList<>();

    /**
     * The list of in-active active.
     */
    private final List<Observer<T>> inactive = new ArrayList<>();

    /**
     * Submits an update to all active observers after removing the inactive
     * ones.
     *
     * @param player The player
     * @param context The context
     */
    public void submitUpdates(Player player, T context) {
        for (Observer<T> remove : inactive) {
            active.remove(remove);
        }

        for (Observer<T> observer : active) {
            try {
                observer.update(this, player, context);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Registers an observer
     *
     * @param observer The observer to register
     */
    public void register(Observer<T> observer) {
        active.add(observer);
    }

    /**
     * Un-registers an observer
     *
     * @param observer The observer to un-register
     */
    public void unregister(Observer<T> observer) {
        inactive.add(observer);
    }

    /**
     * Clears all active.
     */
    public void clear() {
        inactive.addAll(active);
    }
}
