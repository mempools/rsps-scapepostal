package org.hyperion.observer;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.EventContext;

/**
 * The observer class
 *
 * @author Global <http://rune-server.org/members/global>
 * @param <T> A class
 */
public interface Observer<T extends EventContext> {

    /**
     * Updates the observer
     *
     * @param observable The observable
     * @param player The player
     * @param context The event context
     * @throws Exception
     */
    public abstract void update(Observable<T> observable, Player player, T context) throws Exception;

}
