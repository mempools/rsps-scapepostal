package org.hyperion.script.listener;

import org.hyperion.observer.Observable;
import org.hyperion.observer.Observer;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.packet.context.DeathContext;
import org.hyperion.rs2.packet.context.DeathContext.DeathStage;

/**
 * @author Global - 10/29/13 5:24 PM
 */
public class DeathEventListener {

    /**
     * The death event listeners.
     */
    private final Observable<DeathContext> listeners = new Observable<>();

    /**
     * Adds a death event listener
     *
     * @param listener The death event listener to add.
     */
    public void add(Observer<DeathContext> listener) {
        listeners.register(listener);
    }

    /**
     * Removes a death event listener
     *
     * @param listener The death event listener to remove
     */
    public void remove(Observer<DeathContext> listener) {
        listeners.unregister(listener);
    }

    /**
     * Submits the death event
     *
     * @param victim The victim
     * @param stage The death stage
     */
    public void notify(Actor victim, DeathStage stage) {
        final Actor killer = (Actor) victim.getAttributes().get("killedBy");
        listeners.submitUpdates(null, new DeathContext(victim, killer, stage));
    }
}
