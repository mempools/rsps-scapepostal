package org.hyperion.script.listener.event;

import org.hyperion.observer.Observable;
import org.hyperion.observer.Observer;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.Player.Rights;
import org.hyperion.rs2.packet.context.CommandContext;

/**
 * @author Global - 10/29/13 6:28 PM
 */
public abstract class CommandEvent implements Observer<CommandContext> {

    /**
     * The rights.
     */
    private final Rights rights;

    /**
     * The command listener.
     *
     * @param rights The rights
     */
    public CommandEvent(Rights rights) {
        this.rights = rights;
    }

    /**
     * Handles the command
     *
     * @param player The player
     * @param command The command
     */
    public abstract void handleCommand(Player player, CommandContext command);

    @Override
    public void update(Observable<CommandContext> observable, Player player, CommandContext context) throws Exception {
        if (player.getRights().toInteger() >= rights.toInteger()) {
            handleCommand(player, context);
        } else {
            player.getActionSender().sendMessage("You don't have the rights for this command!");
        }
    }
}
