package org.hyperion.script.listener.event;

import org.hyperion.observer.Observable;
import org.hyperion.observer.Observer;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.EventContext;

/**
 * @author Global - 10/29/13 5:23 PM
 */
public abstract class LoginEvent implements Observer<EventContext> {

    /**
     * Handles the login hook
     *
     * @param player The player
     */
    public abstract void handle(Player player);

    @Override
    public void update(Observable<EventContext> observable, Player player, EventContext context) throws Exception {
        handle(player);
    }
}
