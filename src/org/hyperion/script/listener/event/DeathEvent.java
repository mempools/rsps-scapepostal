package org.hyperion.script.listener.event;

import org.hyperion.observer.Observable;
import org.hyperion.observer.Observer;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.Attributes;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.npc.NPCSpawnManager;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.region.Region;
import org.hyperion.rs2.packet.context.DeathContext;
import org.hyperion.rs2.packet.context.DeathContext.DeathStage;
import org.hyperion.rs2.tickable.Tickable;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public abstract class DeathEvent implements Observer<DeathContext> {

    /**
     * Handles the death event
     *
     * @param victim The victim.
     * @param killer The killer.
     * @param stage The death stage.
     */
    public abstract void handleDeath(Actor victim, Actor killer, DeathStage stage);

    @Override
    public void update(Observable<DeathContext> observable, Player player, final DeathContext context) throws Exception {
        final Actor victim = context.getVictim();
        final Actor killer = context.getKiller();
        handleDeath(victim, killer, context.getStage());
    }
}
