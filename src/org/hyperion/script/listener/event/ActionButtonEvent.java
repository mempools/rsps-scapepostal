package org.hyperion.script.listener.event;

import org.hyperion.observer.Observable;
import org.hyperion.observer.Observer;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.packet.context.ActionButtonContext;

/**
 * @author Global - 11/1/13 10:40 PM
 */
public abstract class ActionButtonEvent implements Observer<ActionButtonContext> {

    /**
     * Handles the button
     *
     * @param player The player
     * @param button The button
     */
    public abstract void handleButton(Player player, ActionButtonContext button);

    @Override
    public void update(Observable<ActionButtonContext> observable, Player player, ActionButtonContext context) throws Exception {
        handleButton(player, context);
    }

}
