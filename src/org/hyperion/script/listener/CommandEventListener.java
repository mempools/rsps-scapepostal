package org.hyperion.script.listener;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.packet.context.CommandContext;
import org.hyperion.script.listener.event.CommandEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Global - 10/29/13 6:39 PM
 */
public class CommandEventListener {

    /**
     * The map commands.
     */
    private final Map<String, CommandEvent> commands = new HashMap<>();

    public CommandEventListener() {
        //add commands
    }

    /**
     * Adds a command
     *
     * @param name The name of the command
     * @param listener The command listener
     */
    public void add(String name, CommandEvent listener) {
        commands.put(name.toLowerCase(), listener);
    }

    /**
     * Executes a command
     *
     * @param player The player
     * @param command The command
     */
    public void submit(Player player, CommandContext command) {
        final CommandEvent listener = commands.get(command.getName().toLowerCase());
        if (listener != null) {
            listener.handleCommand(player, command);
        }
    }
}
