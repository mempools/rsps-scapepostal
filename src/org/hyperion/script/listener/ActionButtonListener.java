package org.hyperion.script.listener;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.packet.context.ActionButtonContext;
import org.hyperion.observer.Observable;
import org.hyperion.observer.Observer;

/**
 * @author Global - 11/1/13 10:53 PM
 */
public class ActionButtonListener {

    /**
     * The map buttons.
     */
    private final Observable<ActionButtonContext> listeners = new Observable<>();

    /**
     * Gets the button listeners
     *
     * @return The button listeners
     */
    public Observable<ActionButtonContext> getButtonListeners() {
        return listeners;
    }

    public ActionButtonListener() {
        // observable.register(this);
    }

    /**
     * Adds a action button listener
     *
     * @param listener The action button listener to add
     */
    public void add(Observer<ActionButtonContext> listener) {
        listeners.register(listener);
    }

    /**
     * Removes a action button listener
     *
     * @param listener The action button listener to remove
     */
    public void remove(Observer<ActionButtonContext> listener) {
        listeners.unregister(listener);
    }

    /**
     * Executes a command
     *
     * @param player The player
     * @param context The command
     */
    public void submit(Player player, ActionButtonContext context) {
        listeners.submitUpdates(player, context);
    }
}
