package org.hyperion.script.listener;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.observer.Observable;
import org.hyperion.observer.Observer;
import org.hyperion.rs2.net.EventContext;

/**
 * @author Global - 10/29/13 5:24 PM
 */
public class LoginEventListener {

    /**
     * The login hook listeners
     */
    private final Observable<EventContext> listeners = new Observable<>();

    /**
     * Adds a login hook listener
     *
     * @param listener The login listener to add
     */
    public void add(Observer<EventContext> listener) {
        listeners.register(listener);
    }

    /**
     * Removes a login listener
     *
     * @param listener The login listener to remove
     */
    public void remove(Observer<EventContext> listener) {
        listeners.unregister(listener);
    }

    /**
     * Submits the login hook
     *
     * @param player The player
     */
    public void submit(Player player) {
        listeners.submitUpdates(player, new EventContext() {
        });
    }
}
