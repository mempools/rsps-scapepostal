package org.hyperion.script.listener;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.script.listener.event.LogoutEvent;

import org.hyperion.observer.Observable;
import org.hyperion.rs2.net.EventContext;

/**
 * @author Global - 10/29/13 5:24 PM
 */
public class LogoutEventListener {

    /**
     * The logout hook listeners
     */
    private final Observable<EventContext> listeners = new Observable<>();

    /**
     * Adds a logout listener
     *
     * @param listener The logout listener to add
     */
    public void add(LogoutEvent listener) {
        listeners.register(listener);
    }

    /**
     * Removes a logout listener
     *
     * @param listener The logout listener to remove
     */
    public void remove(LogoutEvent listener) {
        listeners.register(listener);
    }

    /**
     * Submits the logout hook
     *
     * @param player The player
     */
    public void submit(Player player) {
        listeners.submitUpdates(player, new EventContext() {
        });
    }
}
