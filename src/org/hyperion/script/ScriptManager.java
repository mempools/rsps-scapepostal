package org.hyperion.script;

import org.hyperion.Constants;
import org.jruby.RubyInstanceConfig.CompileMode;
import org.jruby.embed.LocalContextScope;
import org.jruby.embed.ScriptingContainer;

import java.io.*;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Manages server scripts.
 *
 * @author blakeman8192
 */
public class ScriptManager {

    /**
     * The logger
     */
    private static final Logger logger = Logger.getLogger(ScriptManager.class.getName());

    /**
     * The scripting container.
     */
    private ScriptingContainer container = new ScriptingContainer();

    /**
     * Gets the scripting container
     *
     * @return container The scripting container
     */
    public ScriptingContainer getContainer() {
        return container;
    }

    /**
     * Creates the jRuby Script Environment
     *
     * @param scriptContext The world context
     */
    public ScriptManager(ScriptContext scriptContext) {
        File scriptStartingPoint = Constants.SCRIPTS_DIRECTORY;
        try {
            container = new ScriptingContainer(LocalContextScope.SINGLETHREAD);
            container.setLoadPaths(Arrays.asList(scriptStartingPoint.getPath()));
            container.setCompileMode(CompileMode.JIT);
            // File file = new File(scriptStartingPoint + File.separator + "bootstrap.rb");
            File file = new File("./data/scripts/bootstrap.rb");
            container.put("$world", scriptContext);
            try (InputStream is = new FileInputStream(file)) {
                container.runScriptlet(is, file.getAbsolutePath());
            }
            /**
             * Loads all other scripts
             */
            start(container);
        } catch (IOException e) {
            logger.severe(e.getMessage());
        }
    }

    /**
     * Loads the scripts in the scripts directory
     *
     * @param container The scripting container
     */
    private void start(ScriptingContainer container) throws FileNotFoundException {
        int count = 0;
        File dir = Constants.SCRIPTS_DIRECTORY;
        File[] children = dir.listFiles();
        for (File script : children) {
            if (script.isDirectory()) {
                File[] files = script.listFiles();
                for (File f : files) {
                    count++;
                    final InputStream is = new FileInputStream(f);
                    container.runScriptlet(is, f.getAbsolutePath());
                }
            }
        }
        logger.info("Loaded " + count + " Ruby Scripts");
    }
}
