package org.hyperion.script;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.util.TextUtils;
import org.hyperion.script.listener.ActionButtonListener;
import org.hyperion.script.listener.CommandEventListener;
import org.hyperion.script.listener.DeathEventListener;
import org.hyperion.script.listener.LoginEventListener;
import org.hyperion.script.listener.LogoutEventListener;

public class ScriptContext {

    /**
     * The server context.
     */
    private final World world;

    /**
     * The login listener.
     */
    private final LoginEventListener loginListener = new LoginEventListener();

    /**
     * The logout listener.
     */
    private final LogoutEventListener logoutListener = new LogoutEventListener();

    /**
     * The command listener.
     */
    private final CommandEventListener commandListener = new CommandEventListener();

    /**
     * The command listener.
     */
    private final ActionButtonListener actionButtonListener = new ActionButtonListener();

    /**
     * The death event listener.
     */
    private final DeathEventListener deathEventListener = new DeathEventListener();

    /**
     * Creates the script context.
     *
     * @param world The server context.
     */
    public ScriptContext(World world) {
        this.world = world;
    }

    /**
     * Gets the world
     *
     * @return world The world
     */
    public World getWorld() {
        return world;
    }

    /**
     * Gets the login listener
     *
     * @return loginListener The login listener
     */
    public LoginEventListener getLoginListener() {
        return loginListener;
    }

    /**
     * Gets the logout listener
     *
     * @return logoutListener The logout listener
     */
    public LogoutEventListener getLogoutListener() {
        return logoutListener;
    }

    /**
     * Gets the command listener
     *
     * @return commandListener The command listener
     */
    public CommandEventListener getCommandListener() {
        return commandListener;
    }

    /**
     * Gets the action button listener
     *
     * @return actionButtonListener The action button listener
     */
    public ActionButtonListener getActionButtonListener() {
        return actionButtonListener;
    }

    /**
     * The death event listener
     *
     * @return deathEventListener The death event listener
     */
    public DeathEventListener getDeathEventListener() {
        return deathEventListener;
    }

    /**
     * Sends a packet event
     *
     * @param player The player to send the packet event to
     * @param context The packet context
     */
    public void sendPacketEvent(Player player, EventContext context) {
        String packetName = TextUtils.toSymbol(context.getClass().getSimpleName()).replace("_context", "");
        world.getScriptManager().getContainer().callMethod(null, "handle_packet", packetName, player, context);
    }
}
