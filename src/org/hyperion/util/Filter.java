package org.hyperion.util;

/**
 * A class which can be used to filter out items, by calling filter.accept()
 *
 * @param <T> The class type this filter will check
 * @author Nikki
 */
public interface Filter<T> {

    public boolean accept(T t);
}
