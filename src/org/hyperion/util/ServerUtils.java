package org.hyperion.util;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class ServerUtils {

    /**
     * The current OS.
     */
    private static final OperatingSystem operatingSystem = getSystem();

    /**
     * Gets the operating system.
     *
     * @return The operating system
     */
    public static OperatingSystem getOperatingSystem() {
        return operatingSystem;
    }

    /**
     * Gets the total memory
     *
     * @return The total memory
     */
    public static long getTotalMemory() {
        long memory = Runtime.getRuntime().totalMemory();
        try {
            OperatingSystemMXBean bean = ManagementFactory.getOperatingSystemMXBean();
            Method method = bean.getClass().getMethod("getTotalPhysicalMemorySize", new Class<?>[0]);
            method.setAccessible(true);
            memory = (Long) method.invoke(bean, new Object[0]);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
        }
        return memory;
    }

    /**
     * Gets the current amount of memory
     *
     * @return The current amount of memory
     */
    public static long getCurrentMemory() {
        return Runtime.getRuntime().totalMemory();
    }

    /**
     * Gets the free amount of memory
     *
     * @return the free amount of memory
     */
    public static long getFreeMemory() {
        long freeMemory = Runtime.getRuntime().freeMemory();
        return freeMemory / 1024;
    }

    /**
     * Gets the amount of used memory
     *
     * @return The amount of used memory
     */
    public static long getUsedMemory() {
        long usedMemory = getCurrentMemory() - getFreeMemory();
        return usedMemory / 1024;
    }

    /**
     * Gets the operating system
     *
     * @return The operating system
     */
    public static OperatingSystem getSystem() {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) {
            return OperatingSystem.WINDOWS;
        }
        if (osName.contains("mac")) {
            return OperatingSystem.MACOS;
        }
        if (osName.contains("SOLARIS") || osName.contains("sunos")) {
            return OperatingSystem.SOLARIS;
        }
        if (osName.contains("LINUX") || osName.contains("unix")) {
            return OperatingSystem.LINUX;
        }
        return OperatingSystem.UNKNOWN;
    }

    /**
     * The operating systems.
     */
    public enum OperatingSystem {

        LINUX,
        SOLARIS,
        WINDOWS,
        MACOS,
        UNKNOWN
    }
}
