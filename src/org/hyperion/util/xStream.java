package org.hyperion.util;

import org.hyperion.rs2.model.npc.NPCSpawnManager;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.Sun14ReflectionProvider;
import com.thoughtworks.xstream.io.xml.XppDriver;
import org.hyperion.rs2.model.Area;
import org.hyperion.rs2.model.*;
import org.hyperion.rs2.model.combat.MagicData;
import org.hyperion.rs2.model.combat.Poison.PoisonType;
import org.hyperion.rs2.model.container.Shop;
import org.hyperion.rs2.model.definition.EquipmentDefinition;
import org.hyperion.rs2.model.definition.EquipmentDefinition.WeaponStyle;
import org.hyperion.rs2.model.definition.GameObjectDefinition;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.Callable;
import org.hyperion.cache.media.RSInterface;
import org.hyperion.cache.media.RSInterfaceDefinition;

/**
 * Controls all the XML File types in the server
 */
public class xStream {

    /**
     * The xStream
     */
    private static final XStream XSTREAM = new XStream(new Sun14ReflectionProvider(), new XppDriver());

    /**
     * Loads XML content
     *
     * @param world The world
     * @throws IOException Error within the document file
     */
    public static void load(World world) throws IOException {
        world.getBackgroundLoader().submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                RSInterfaceDefinition.init();
                return null;
            }
        });
        world.getBackgroundLoader().submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                NPCSpawnManager.parse();
                return null;
            }
        });
        world.getBackgroundLoader().submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                Shop.parse();
                return null;
            }
        });
        world.getBackgroundLoader().submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                EquipmentDefinition.parse();
                return null;
            }
        });
        world.getBackgroundLoader().submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                MagicData.load();
                return null;
            }
        });
    }

    static {
        /**
         * Containers
         */
        alias("item", Item.class);

        /**
         * Other
         */
        alias("animation", Animation.class);
        alias("location", Location.class);
        alias("area", Area.class);
        alias("rsInterface", RSInterface.class);

        /**
         * Combat stuff
         */
        alias("weaponStyle", WeaponStyle.class);
        alias("poisonType", PoisonType.class);

        /**
         * Definitions
         */
        alias("objectDefinition", GameObjectDefinition.class);
        alias("equipmentDef", EquipmentDefinition.class);
        alias("skillRequirement", EquipmentDefinition.Skill.class);
    }

    /**
     * Writes the XML file, using try and finally will allow the file output to
     * close if an exception is thrown (will stop memory leaks)
     *
     * @param object The object getting written
     * @param file The file area and name
     * @throws IOException
     */
    public static void writeXML(Object object, File file) throws IOException {
        try (FileOutputStream out = new FileOutputStream(file)) {
            XSTREAM.toXML(object, out);
            out.flush();
        }
    }

    /**
     * Writes the XML file, using try and finally will allow the file output to
     * close if an exception is thrown (will stop memory leaks)
     *
     * @param object The object getting written
     * @return The XML
     */
    public static String writeXML(Object object) {
        return XSTREAM.toXML(object);
    }

    /**
     * Adds an alias for a name
     *
     * @param name alias
     * @param type real class
     */
    public static void alias(String name, Class<?> type) {
        XSTREAM.alias(name, type);
    }

    /**
     * Reads an object from an XML file.
     *
     * @param file The file.
     * @return The object.
     * @throws IOException if an I/O error occurs. Edit Sir Sean: Now uses
     * generic's
     * @author Graham Edgecombe
     */
    @SuppressWarnings("unchecked")
    public static <T> T readXML(File file) throws IOException {
        try (FileInputStream in = new FileInputStream(file)) {
            return (T) XSTREAM.fromXML(in);
        }
    }
}
