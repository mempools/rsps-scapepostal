package org.hyperion.database;

import org.hyperion.database.task.QueryTask;
import org.hyperion.database.task.TaskComparator;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

public class DatabaseTaskEngine {

    /**
     * If the task engine is running.
     */
    private boolean running = false;

    /**
     * The blocking queue tasks.
     */
    private final BlockingQueue<QueryTask> tasks = new PriorityBlockingQueue<>(10, new TaskComparator());

    /**
     * The important executor services.
     */
    private final ExecutorService importantService = Executors.newSingleThreadExecutor();

    /**
     * The normal executor services.
     */
    private final ExecutorService normalService = Executors.newFixedThreadPool(2);

    /**
     * Runs the database task engine.
     */
    public void run() {
        while (running) {
            try {
                final QueryTask task = tasks.take();
                switch (task.getPriority()) {
                    case NORMAL:
                        normalService.execute(new Runnable() {
                            @Override
                            public void run() {
                                task.execute();
                            }
                        });
                    case IMPORTANT:
                        importantService.execute(new Runnable() {
                            @Override
                            public void run() {
                                task.execute();
                            }
                        });
                        break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * The query priority levels.
     */
    public enum QueryPriority {

        /**
         * Normal.
         */
        NORMAL,
        /**
         * Important.
         */
        IMPORTANT
    }
}
