/**
 * Copyright (c) 2012-2013 Graham Edgecombe <graham@grahamedgecombe.com>
 * Copyright (c) 2012-2013 Jonathan Edgecombe <jonathanedgecombe@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
package org.hyperion.database;

import java.io.BufferedReader;
import org.hyperion.data.Serializer;
import org.hyperion.database.io.*;
import org.hyperion.rs2.WorldLoader;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.PlayerDetails;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.container.Container;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public final class SQLWorldLoader implements WorldLoader, Closeable {

    /**
     * The logger.
     */
    private static final Logger logger = LoggerFactory.getLogger(SQLWorldLoader.class);

    /**
     * The serializers.
     */
    private List<Serializer> serializers = new ArrayList<>();

    /**
     * The game connection.
     */
    private DatabaseConnection game = World.getWorld().getGameConnection().nextFree();

    /**
     * The forum connection.
     */
    private DatabaseConnection forum = World.getWorld().getForumConnection().nextFree();

    /**
     * The login statement.
     */
    private final PreparedStatement loginStatement;

    /**
     * The MySQL World loader
     *
     * @throws SQLException
     * @throws IOException
     */
    public SQLWorldLoader() throws SQLException, IOException {
        loginStatement = forum.prepareStatement("SELECT id FROM users WHERE username = ?;");
        game.getConnection().setAutoCommit(false);
        /**
         * Add the serializers.
         */
        serializers.add(new ProfileSerializer(forum));
        serializers.add(new PlayerSerializer(game));
        serializers.add(new SettingsSerializer(game));
        serializers.add(new SkillsSerializer(game));
        serializers.add(new MissionSerializer(game));
        serializers.add(new AttributesSerializer(game));
        serializers.add(new ContainerSerializer(game) {
            @Override
            public Container getContainer(Player player) {
                return player.getInventory();
            }

            @Override
            public String getType() {
                return "inventory";
            }
        });
        serializers.add(new ContainerSerializer(game) {
            @Override
            public Container getContainer(Player player) {
                return player.getEquipment();
            }

            @Override
            public String getType() {
                return "equipment";
            }
        });
        serializers.add(new ContainerSerializer(game) {
            @Override
            public Container getContainer(Player player) {
                return player.getBank();
            }

            @Override
            public String getType() {
                return "bank";
            }
        });
    }

    @Override
    public LoginResult checkLogin(PlayerDetails pd) {
        try {
            loginStatement.setString(1, pd.getName());
            try (ResultSet set = loginStatement.executeQuery()) {
                /**
                 * Check if the user exists.
                 */
                if (set.first()) {
                    if (checkPassword(pd) && !pd.getName().isEmpty() && !pd.getPassword().isEmpty()) {
                        Player player = new Player(pd);
                        player.getProfile().setUserId(set.getInt("id"));
                        player.setPassword(pd.getPassword());
                        return new LoginResult(LoginResult.RESPONSE_OK, player);
                    } else {
                        return new LoginResult(LoginResult.RESPONSE_INVALID_INFORMATION);
                    }
                }
                /*
                 * User doesn't exist, register as a new player OR change the
                 * response code & don't
                 */
                //Player player = new Player(pd);
                return new LoginResult(LoginResult.RESPONSE_INVALID_INFORMATION);
            }
        } catch (SQLException ex) {
            logger.warn("Loading player " + pd.getName() + " failed.", ex);
            return new LoginResult(LoginResult.RESPONSE_PROFILE_ERROR);
        }
    }

    @Override
    public boolean loadPlayer(Player player) {
        try {
            for (Serializer table : serializers) {
                table.load(player);
            }
        } catch (SQLException | IOException ex) {
            logger.warn("Error loading player " + player.getName() + " failed", ex);
        }
        return true;
    }

    @Override
    public boolean savePlayer(Player player) {
        try {
            for (Serializer table : serializers) {
                table.save(player);
            }
            game.getConnection().commit();
        } catch (SQLException | IOException e) {
            try {
                game.getConnection().rollback();
            } catch (SQLException ex) {
                logger.warn("Error roll backing", ex);
            }
            logger.warn("Saving player " + player.getName() + " failed", e);
        }
        return true;
    }

    /**
     * Checks for the player's password
     *
     * @param pd The player to check
     */
    private boolean checkPassword(PlayerDetails pd) {
        try {
            String i = "?i=534234";
            String name = pd.getName().replaceAll(" ", "%20");
            String pass = pd.getPassword().replaceAll(" ", "%20");
            URL url = new URL("http://thugcraft.no-ip.biz/forum/servValidate.php" + i + "&name=" + name + "&pass=" + pass);
            URLConnection connection = url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = reader.readLine();
            return line.equalsIgnoreCase("1");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void close() throws IOException {
        game.close();
    }
}
