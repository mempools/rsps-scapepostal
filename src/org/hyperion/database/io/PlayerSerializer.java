/**
 * Copyright (c) 2012-2013 Graham Edgecombe <graham@grahamedgecombe.com>
 * Copyright (c) 2012-2013 Jonathan Edgecombe <jonathanedgecombe@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
package org.hyperion.database.io;

import org.hyperion.data.Serializer;
import org.hyperion.database.DatabaseConnection;
import org.hyperion.rs2.content.gang.Gang;
import org.hyperion.rs2.content.gang.Gang.GangRank;
import org.hyperion.rs2.content.gang.GangManager;
import org.hyperion.rs2.content.mission.MissionManager;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.combat.CombatStyle;
import org.hyperion.rs2.util.NameUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerSerializer extends Serializer {

    /**
     * The load statement.
     */
    private final PreparedStatement loadStatement;
    /**
     * The save statement.
     */
    private final PreparedStatement saveStatement;

    /**
     * Creates the player table
     *
     * @param connection The connection
     * @throws SQLException
     */
    public PlayerSerializer(DatabaseConnection connection) throws SQLException {
        loadStatement = connection.prepareStatement("SELECT * FROM details WHERE userId = ?");
        saveStatement = connection.prepareStatement("REPLACE INTO details "
                + "(userId, username, password, rights, x, y, z,"
                + " runEnergy, specialEnergy, combatStyle, gang, gangRank, respectPoints, currentMission)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
    }

    @Override
    public void save(final Player player) throws SQLException {
        saveStatement.setInt(1, player.getProfile().getUserId());
        saveStatement.setString(2, player.getName());
        saveStatement.setString(3, player.getPassword());
        saveStatement.setInt(4, player.getRights().toInteger());
        saveStatement.setInt(5, player.getLocation().getX());
        saveStatement.setInt(6, player.getLocation().getY());
        saveStatement.setInt(7, (byte) player.getLocation().getZ());
        saveStatement.setDouble(8, player.getWalkingQueue().getRunningEnergy());
        saveStatement.setInt(9, player.getCombatSession().getSpecialAmount());
        saveStatement.setInt(10, player.getCombatSession().getCombatStyle().getId());
        if (player.getGang() != null) {
            saveStatement.setString(11, player.getGang().getName());
            saveStatement.setString(12, player.getGang().getRank().name());
            saveStatement.setInt(13, player.getGang().getRespectPoints());
        } else {
            saveStatement.setString(11, null);
            saveStatement.setString(12, GangRank.GOON.name());
            saveStatement.setInt(13, 0);
        }
        saveStatement.setInt(14, player.getMission() != null ? player.getMission().getButton() : -1);
        saveStatement.execute();
    }

    @Override
    public void load(final Player player) throws SQLException {
        loadStatement.setInt(1, player.getProfile().getUserId());
        ResultSet result = loadStatement.executeQuery();
        if (result.next()) {
            player.setName(NameUtils.formatName(player.getName()));
            player.setPassword(player.getPassword());
            player.setLocation(Location.create(result.getShort("x"), result.getShort("y"), result.getByte("z")));
            /**
             * This will CAUSE PROBLEMS! I have no idea why it isn't working, do
             * not uncomment.
             */
            //player.getWalkingQueue().setRunningEnergy(result.getInt("runEnergy")); //WILL CAUSE PROBLEMS
            player.getCombatSession().setSpecialAmount(result.getInt("specialEnergy"));
            player.getCombatSession().setCombatStyle(CombatStyle.forId(result.getInt("combatStyle")));
            player.setGang(result.getString("gang") != null ? GangManager.get(result.getString("gang")) : null);
            if (player.getGang() != null) {
                player.getGang().setRank(result.getString("gangRank") != null ? Gang.GangRank.valueOf(result.getString("gangRank")) : Gang.GangRank.GOON);
                player.getGang().setRespectPoints(result.getInt("respectPoints"));
            }
            int currentMission = result.getInt("currentMission");
            player.setMission(currentMission != -1 ? MissionManager.get(currentMission) : null);
        }
    }
}
