/**
 * Copyright (c) 2012-2013 Graham Edgecombe <graham@grahamedgecombe.com>
 * Copyright (c) 2012-2013 Jonathan Edgecombe <jonathanedgecombe@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
package org.hyperion.database.io;

import org.hyperion.data.Serializer;
import org.hyperion.database.DatabaseConnection;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.container.Container;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public abstract class ContainerSerializer extends Serializer {

    /**
     * The load statement.
     */
    private final PreparedStatement loadStatement;
    /**
     * The save statement.
     */
    private final PreparedStatement saveStatement;

    /**
     * Creates the container table
     *
     * @param connection The connection
     * @throws SQLException
     */
    public ContainerSerializer(DatabaseConnection connection) throws SQLException {
        this.loadStatement = connection.prepareStatement("SELECT * FROM container WHERE userId = ? AND type = ?;");
        this.saveStatement = connection.prepareStatement("INSERT INTO container (userId, type, slot, item, amount) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE item = VALUES(item), amount = VALUES(amount);");
    }

    @Override
    public void save(Player player) throws SQLException, IOException {
        saveStatement.setInt(1, player.getProfile().getUserId());
        saveStatement.setString(2, getType());
        Container container = getContainer(player);
        Item[] items = container.toArray();
        try {
            for (int slot = 0; slot < items.length; slot++) {
                Item item = items[slot];
                saveStatement.setInt(3, slot);
                if (item != null) {
                    saveStatement.setInt(4, item.getId());
                    saveStatement.setInt(5, item.getCount());
                } else {
                    saveStatement.setNull(4, Types.INTEGER);
                    saveStatement.setNull(5, Types.INTEGER);
                }
                saveStatement.addBatch();
            }
            saveStatement.executeBatch();
        } finally {
            saveStatement.clearBatch();
        }
    }

    @Override
    public void load(Player player) throws SQLException, IOException {
        loadStatement.setInt(1, player.getProfile().getUserId());
        loadStatement.setString(2, getType());

        Container container = getContainer(player);
        try (ResultSet set = loadStatement.executeQuery()) {
            while (set.next()) {
                int slot = set.getInt("slot");
                Item item = new Item(set.getInt("item"), set.getInt("amount"));
                if (set.wasNull()) {
                    container.set(slot, null);
                } else {
                    container.set(slot, item);
                }
            }
        }
    }

    /**
     * Gets the container
     *
     * @param player The player
     * @return The container
     */
    public abstract Container getContainer(Player player);

    /**
     * Gets the container type.
     *
     * @return The type
     */
    public abstract String getType();
}
