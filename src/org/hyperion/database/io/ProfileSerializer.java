/**
 * Copyright (c) 2012-2013 Graham Edgecombe <graham@grahamedgecombe.com>
 * Copyright (c) 2012-2013 Jonathan Edgecombe <jonathanedgecombe@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
package org.hyperion.database.io;

import org.hyperion.data.Serializer;
import org.hyperion.database.DatabaseConnection;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.Player.Rights;
import org.hyperion.rs2.util.NameUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.hyperion.rs2.model.player.MailMessage;

/**
 * Handles information/data from the RCS forum! From getting infractions, post
 * counts, messages, and anything from the forums.
 *
 * @author Global <http://rune-server.org/members/global>
 */
public class ProfileSerializer extends Serializer {

    /**
     * The load statement.
     */
    private final PreparedStatement loadStatement, infractions, messages;

    /**
     * Creates the profile table
     *
     * @param connection The connection
     * @throws SQLException
     */
    public ProfileSerializer(DatabaseConnection connection) throws SQLException {
        loadStatement = connection.prepareStatement("SELECT * FROM users WHERE username = ?");
        infractions = connection.prepareStatement("SELECT count(*) FROM infractions WHERE userId = ?");
        messages = connection.prepareStatement("SELECT * FROM messages WHERE receiver = ?");
    }

    @Override
    public void save(Player player) throws SQLException {
    }

    @Override
    public void load(Player player) throws SQLException {
        loadStatement.setString(1, NameUtils.formatName(player.getName()));
        ResultSet result = loadStatement.executeQuery();
        if (result.next()) {
            player.getProfile().setUserId(result.getInt("id"));
            player.getProfile().setLastLoggedIn(result.getInt("lastlogin"));
            player.getProfile().setLastLoggedInFrom(result.getString("lastip"));
            player.getProfile().setRegistrationDate(result.getString("reg_date"));
            player.setRights(Rights.getForumRights(result.getInt("acc_status")));
        }

        /*
         * Counts the rows in the infractions table TODO: see if it actually
         * works
         */
        infractions.setInt(1, player.getProfile().getUserId());
        result = infractions.executeQuery();
        int count = 0;
        while (result.next()) {
            count++;
        }
        player.getProfile().setInfractions(count);

        /*
         * Messages
         */
        messages.setString(1, player.getName());
        result = messages.executeQuery();
        if (result.next()) {
            int opened = result.getInt("opened");
            if (opened == MailMessage.FLAG_UNREAD) {
                MailMessage mailMessage = new MailMessage(result.getString("creator"), result.getString("receiver"), result.getString("title"), result.getString("message"));
                mailMessage.setStatus(result.getInt("opened"));
                player.getProfile().getInbox().add(mailMessage);
            }
        }
    }
}
