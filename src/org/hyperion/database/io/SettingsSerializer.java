/**
 * Copyright (c) 2012-2013 Graham Edgecombe <graham@grahamedgecombe.com>
 * Copyright (c) 2012-2013 Jonathan Edgecombe <jonathanedgecombe@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
package org.hyperion.database.io;

import org.hyperion.data.Serializer;
import org.hyperion.database.DatabaseConnection;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.Settings;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class SettingsSerializer extends Serializer {

    /**
     * The load statement.
     */
    private final PreparedStatement loadStatement;
    /**
     * The save statement.
     */
    private final PreparedStatement saveStatement;

    /**
     * Creates the settings table
     *
     * @param connection The connection
     * @throws SQLException
     */
    public SettingsSerializer(DatabaseConnection connection) throws SQLException {
        this.loadStatement = connection.prepareStatement("SELECT * FROM settings WHERE userId = ?;");
        this.saveStatement = connection.prepareStatement("INSERT INTO settings (userId, setting, value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value = VALUES(value);");
    }

    @Override
    public void load(Player player) throws SQLException, IOException {
        loadStatement.setInt(1, player.getProfile().getUserId());

        Settings settings = player.getSettings();
        ResultSet result = loadStatement.executeQuery();
        while (result.next()) {
            String setting = result.getString("setting");
            int value = result.getInt("value");

            switch (setting) {
                case "swapping":
                    settings.setSwapping(value != 0);
                    break;
                case "withdraw_as_notes":
                    settings.setWithdrawAsNotes(value != 0);
                    break;
                case "auto_retaliating":
                    break;

                default:
                    throw new IOException("unknown setting: " + setting);
            }
        }
    }

    @Override
    public void save(Player player) throws SQLException, IOException {
        saveStatement.setInt(1, player.getProfile().getUserId());

        Settings settings = player.getSettings();
        saveStatement.setString(2, "swapping");
        saveStatement.setInt(3, settings.isSwapping() ? 1 : 0);
        saveStatement.addBatch();

        saveStatement.setString(2, "withdraw_as_notes");
        saveStatement.setInt(3, settings.isWithdrawingAsNotes() ? 1 : 0);
        saveStatement.addBatch();

        /* saveStatement.setString(2, "auto_retaliating");
         saveStatement.setInt(3, settings.isAutoRetaliating() ? 1 : 0);
         saveStatement.addBatch();*/
        saveStatement.executeBatch();
    }
}
