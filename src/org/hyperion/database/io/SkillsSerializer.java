/**
 * Copyright (c) 2012-2013 Graham Edgecombe <graham@grahamedgecombe.com>
 * Copyright (c) 2012-2013 Jonathan Edgecombe <jonathanedgecombe@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
package org.hyperion.database.io;

import org.hyperion.data.Serializer;
import org.hyperion.database.DatabaseConnection;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.Skills;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SkillsSerializer extends Serializer {

    /**
     * The load statement.
     */
    private final PreparedStatement loadStatement;

    /**
     * The save statement.
     */
    private final PreparedStatement saveStatement;

    /**
     * Creates the skills table
     *
     * @param connection The connection
     * @throws SQLException
     */
    public SkillsSerializer(DatabaseConnection connection) throws SQLException {
        loadStatement = connection.prepareStatement("SELECT * FROM skills WHERE userId = ?");
        saveStatement = connection.prepareStatement("INSERT INTO skills (userId, skillId, level, exp) VALUES (?, ?, ?, ?) "
                + "ON DUPLICATE KEY UPDATE userId = ?, skillId = ?, level = ?, exp = ?");
    }

    @Override
    public void save(Player player) throws SQLException, IOException {
        try {
            for (int i = 0; i < Skills.SKILL_COUNT; i++) {
                saveStatement.setInt(1, player.getProfile().getUserId());
                saveStatement.setInt(2, i);//skill id
                saveStatement.setInt(3, player.getSkills().getLevel(i));
                saveStatement.setDouble(4, player.getSkills().getExperience(i));

                /*
                 * On duplicate
                 */
                saveStatement.setInt(5, player.getProfile().getUserId());
                saveStatement.setInt(6, i);//skill id
                saveStatement.setInt(7, player.getSkills().getLevel(i));
                saveStatement.setDouble(8, player.getSkills().getExperience(i));
                saveStatement.addBatch();
            }
            saveStatement.executeUpdate();
        } finally {
            saveStatement.clearBatch();
        }
    }

    @Override
    public void load(Player player) throws SQLException, IOException {
        loadStatement.setInt(1, player.getProfile().getUserId());
        ResultSet result = loadStatement.executeQuery();
        while (result.next()) {
            int skillId = result.getInt("skillId");
            player.getSkills().setSkill(skillId, result.getInt("level"), result.getDouble("exp"));
        }
    }
}
