/**
 * Copyright (c) 2012-2013 Graham Edgecombe <graham@grahamedgecombe.com>
 * Copyright (c) 2012-2013 Jonathan Edgecombe <jonathanedgecombe@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
package org.hyperion.database.io;

import org.hyperion.data.Serializer;
import org.hyperion.database.DatabaseConnection;
import org.hyperion.rs2.model.Attributes;
import org.hyperion.rs2.model.player.Player;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class AttributesSerializer extends Serializer {

    /**
     * The load statement.
     */
    private final PreparedStatement loadStatement;
    /**
     * The save statement.
     */
    private final PreparedStatement saveStatement;

    /**
     * Creates the settings table
     *
     * @param connection The connection
     * @throws java.sql.SQLException
     */
    public AttributesSerializer(DatabaseConnection connection) throws SQLException {
        this.loadStatement = connection.prepareStatement("SELECT * FROM attributes WHERE userId = ?;");
        this.saveStatement = connection.prepareStatement("INSERT INTO attributes (userId, `key`, value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value = VALUES(value);");
    }

    @Override
    public void load(Player player) throws SQLException, IOException {
        loadStatement.setInt(1, player.getProfile().getUserId());

        Attributes attributes = player.getAttributes();
        ResultSet result = loadStatement.executeQuery();
        while (result.next()) {
            String key = result.getString("key");
            int value = result.getInt("value");

            switch (key) {
                case "beers":
                    attributes.set("beers", value);
                    break;

                case "can_drink":
                    attributes.set("canDrink", value == 1);
                    break;

                default:
                    player.getActionSender().sendMessage("Unknown Attribute: " + key);
                    attributes.set(key, null);
            }
        }
    }

    @Override
    public void save(Player player) throws SQLException, IOException {
        saveStatement.setInt(1, player.getProfile().getUserId());
        Attributes attributes = player.getAttributes();

        saveStatement.setString(2, "beers");
        saveStatement.setObject(3, attributes.get("beers") != null ? attributes.get("beers") : 0);
        saveStatement.addBatch();

        saveStatement.setString(2, "can_drink");
        saveStatement.setInt(3, attributes.get("canDrink") != null ? 1 : 0);
        saveStatement.addBatch();

        saveStatement.executeBatch();
    }
}
