package org.hyperion.database.io;

import org.hyperion.data.Serializer;
import org.hyperion.database.DatabaseConnection;
import org.hyperion.rs2.content.mission.Mission;
import org.hyperion.rs2.content.mission.MissionManager;
import org.hyperion.rs2.model.player.Player;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Global - 7/1/13 4:37 PM
 */
public class MissionSerializer extends Serializer {

    /**
     * The load statement.
     */
    private final PreparedStatement loadStatement;
    /**
     * The save statement.
     */
    private final PreparedStatement saveStatement;

    /**
     * Creates the quest table
     *
     * @param connection The connection
     */
    public MissionSerializer(DatabaseConnection connection) throws SQLException {
        loadStatement = connection.prepareStatement("SELECT * FROM missions WHERE userId = ?");
        saveStatement = connection.prepareStatement("INSERT INTO missions (userId, questId, stage) VALUES (?, ?, ?) "
                + " ON DUPLICATE KEY UPDATE userId = ?, questId = ?, stage = ?");
    }

    @Override
    public void save(Player player) throws SQLException, IOException {
        try {
            for (Mission mission : MissionManager.getMissions().values()) {
                saveStatement.setInt(1, player.getProfile().getUserId());
                saveStatement.setInt(2, mission.getButton());
                saveStatement.setInt(3, player.getMissionStorage().getMissionStage(mission));

                saveStatement.setInt(4, player.getProfile().getUserId());
                saveStatement.setInt(5, mission.getButton());
                saveStatement.setInt(6, player.getMissionStorage().getMissionStage(mission));
                saveStatement.addBatch();
            }
            saveStatement.executeUpdate();
        } finally {
            saveStatement.clearBatch();
        }
    }

    @Override
    public void load(Player player) throws SQLException, IOException {
        loadStatement.setInt(1, player.getProfile().getUserId());
        ResultSet result = loadStatement.executeQuery();
        while (result.next()) {
            int mission = result.getInt("questId");
            int stage = result.getInt("stage");
            player.getMissionStorage().setMissionStage(mission, stage);
        }
    }
}
