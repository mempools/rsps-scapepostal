package org.hyperion.database.task;

import org.hyperion.database.DatabaseTaskEngine.QueryPriority;

import java.util.Comparator;

public class TaskComparator implements Comparator<QueryTask> {

    @Override
    public int compare(QueryTask o1, QueryTask o2) {
        return o1.getPriority() == QueryPriority.IMPORTANT
                && o2.getPriority() != QueryPriority.IMPORTANT ? 1 : 0;
    }
}
