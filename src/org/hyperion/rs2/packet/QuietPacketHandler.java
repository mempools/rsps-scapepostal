package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.DefaultContext;

/**
 * A packet handler which takes no action i.e. it ignores the packet.
 *
 * @author Graham Edgecombe
 */
public class QuietPacketHandler implements PacketHandler<DefaultContext> {

    @Override
    public void handle(Player player, DefaultContext packet) {
    }
}
