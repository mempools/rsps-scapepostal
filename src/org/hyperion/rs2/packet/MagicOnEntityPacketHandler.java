package org.hyperion.rs2.packet;

import org.hyperion.rs2.action.impl.AttackAction;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.MagicOnEntityContext;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class MagicOnEntityPacketHandler implements PacketHandler<MagicOnEntityContext> {

    @Override
    public void handle(Player player, MagicOnEntityContext context) {
        if (context.getSpell() == null) {
            return;
        }
        player.getActionSender().sendConfig(108, 0);
        player.getActionSender().sendString(352, "(none set)");
        player.getCombatSession().setCurrentSpell(context.getSpell());
        player.getActionQueue().addAction(new AttackAction(player, context.getCharacter()));
    }

}
