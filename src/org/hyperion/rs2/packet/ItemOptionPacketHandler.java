package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.container.*;
import org.hyperion.rs2.model.request.RequestType;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.ItemOptionContext;

/**
 * Remove item options.
 *
 * @author Graham Edgecombe
 */
public class ItemOptionPacketHandler implements PacketHandler<ItemOptionContext> {

    @Override
    public void handle(Player player, ItemOptionContext context) {
        int interfaceId = context.getInterface();
        int slot = context.getSlot();
        int id = context.getId();
        switch (context.getOption()) {
            case 1:
                handleItemOption1(player, interfaceId, slot, id);
                break;
            case 2:
                handleItemOption2(player, interfaceId, slot, id);
                break;
            case 3:
                handleItemOption3(player, interfaceId, slot, id);
                break;
            case 4:
                handleItemOption4(player, interfaceId, slot, id);
                break;
            case 5:
                handleItemOption5(player, interfaceId, slot, id);
                break;
        }
    }

    /**
     * Handles item option 1
     *
     * @param player The player
     * @param interfaceId The interface id
     * @param slot The item slot
     * @param id The item id
     */
    private void handleItemOption1(Player player, int interfaceId, int slot, int id) {
        switch (interfaceId) {
            case Equipment.INTERFACE:
                if (slot >= 0 && slot < Equipment.SIZE) {
                    Item item = player.getEquipment().get(slot);
                    if (!Container.transfer(player.getEquipment(), player.getInventory(), slot, id)) {
                        player.getActionSender().sendMessage("Not enough space in inventory.");
                        // indicate it failed
                    } else {
                        if (item != null && item.getEquipmentDefinition() != null) {
                            for (int i = 0; i < item.getEquipmentDefinition().getBonuses().length; i++) {
                                player.getSkills().setBonus(i, player.getSkills().getBonus(i) - item.getEquipmentDefinition().getBonus(i));
                            }
                            player.getSkills().sendBonuses();
                            if (slot == Equipment.SLOT_WEAPON) {
                                player.getEquipmentAnimations().setDefault();
                            }
                        }
                    }
                    return;
                }
                break;
            case Bank.PLAYER_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Inventory.SIZE) {
                    Bank.deposit(player, slot, id, 1);
                }
                break;
            case Bank.BANK_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Bank.SIZE) {
                    Bank.withdraw(player, slot, id, 1);
                }
                break;
            case Shop.SHOP_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Shop.SIZE) {
                    Shop.itemPrice(player, slot, id);
                }
                break;
            case Shop.PLAYER_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Inventory.SIZE) {
                    Shop.getItemValue(player, slot, id);
                }
                break;
            case Trade.TRADE_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Trade.SIZE) {
                    Trade.removeItem(player, slot, id, 1);
                }
                break;
            case Trade.PLAYER_INVENTORY_INTERFACE:
                if (player.getRequestManager().getRequestType() == RequestType.TRADE) {
                    if (slot >= 0 && slot < Inventory.SIZE) {
                        Trade.offerItem(player, slot, id, 1);
                    }
                }
                break;
        }
    }

    /**
     * Handles item option 2
     *
     * @param player The player
     * @param interfaceId The interface id
     * @param slot The item slot
     * @param id The item id
     */
    private void handleItemOption2(Player player, int interfaceId, int slot, int id) {
        switch (interfaceId) {
            case Bank.PLAYER_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Inventory.SIZE) {
                    Bank.deposit(player, slot, id, 5);
                }
                break;
            case Bank.BANK_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Bank.SIZE) {
                    Bank.withdraw(player, slot, id, 5);
                }
                break;
            case Shop.SHOP_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Shop.SIZE) {
                    Shop.buyItem(player, slot, id, 1);
                }
                break;
            case Shop.PLAYER_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Shop.SIZE) {
                    Shop.sellItem(player, slot, id, 1);
                }
                break;
            case Trade.TRADE_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Trade.SIZE) {
                    Trade.removeItem(player, slot, id, 5);
                }
                break;
            case Trade.PLAYER_INVENTORY_INTERFACE:
                if (player.getRequestManager().getRequestType() == RequestType.TRADE) {
                    if (slot >= 0 && slot < Inventory.SIZE) {
                        Trade.offerItem(player, slot, id, 5);
                    }
                }
                break;

        }
    }

    /**
     * Handles item option 3
     *
     * @param player The player
     * @param interfaceId The interface id
     * @param slot The item slot
     * @param id The item id
     */
    private void handleItemOption3(Player player, int interfaceId, int slot, int id) {
        switch (interfaceId) {
            case Bank.PLAYER_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Inventory.SIZE) {
                    Bank.deposit(player, slot, id, 10);
                }
                break;
            case Bank.BANK_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Bank.SIZE) {
                    Bank.withdraw(player, slot, id, 10);
                }
                break;
            case Shop.SHOP_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Shop.SIZE) {
                    Shop.buyItem(player, slot, id, 5);
                }
                break;
            case Shop.PLAYER_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Shop.SIZE) {
                    Shop.sellItem(player, slot, id, 5);
                }
                break;
            case Trade.TRADE_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Trade.SIZE) {
                    Trade.removeItem(player, slot, id, 10);
                }
                break;
            case Trade.PLAYER_INVENTORY_INTERFACE:
                if (player.getRequestManager().getRequestType() == RequestType.TRADE) {
                    if (slot >= 0 && slot < Inventory.SIZE) {
                        Trade.offerItem(player, slot, id, 10);
                    }
                }
                break;
        }
    }

    /**
     * Handles item option 4
     *
     * @param player The player
     * @param interfaceId The interface id
     * @param slot The item slot
     * @param id The item id
     */
    private void handleItemOption4(Player player, int interfaceId, int slot, int id) {
        switch (interfaceId) {
            case Bank.PLAYER_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Inventory.SIZE) {
                    Bank.deposit(player, slot, id, player.getInventory().getCount(id));
                }
                break;
            case Bank.BANK_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Bank.SIZE) {
                    Bank.withdraw(player, slot, id, player.getBank().getCount(id));
                }
                break;
            case Shop.SHOP_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Shop.SIZE) {
                    Shop.buyItem(player, slot, id, 10);
                }
                break;
            case Shop.PLAYER_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Shop.SIZE) {
                    Shop.sellItem(player, slot, id, 10);
                }
                break;
            case Trade.TRADE_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Trade.SIZE) {
                    Trade.removeItem(player, slot, id, player.getTrade().getCount(id));
                }
                break;
            case Trade.PLAYER_INVENTORY_INTERFACE:
                if (player.getRequestManager().getRequestType() == RequestType.TRADE) {
                    if (slot >= 0 && slot < Inventory.SIZE) {
                        Trade.offerItem(player, slot, id, player.getInventory().getCount(id));
                    }
                }
                break;
        }
    }

    /**
     * Handles item option 5
     *
     * @param player The player
     * @param interfaceId The interface id
     * @param slot The item slot
     * @param id The item id
     */
    private void handleItemOption5(Player player, int interfaceId, int slot, int id) {
        switch (interfaceId) {
            case Bank.PLAYER_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Inventory.SIZE) {
                    player.getInterfaceState().openEnterAmountInterface(interfaceId, slot, id);
                }
                break;
            case Bank.BANK_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Bank.SIZE) {
                    player.getInterfaceState().openEnterAmountInterface(interfaceId, slot, id);
                }
                break;
            case Trade.TRADE_INVENTORY_INTERFACE:
                if (slot >= 0 && slot < Trade.SIZE) {
                    player.getInterfaceState().openEnterAmountInterface(interfaceId, slot, id);
                }
                break;
            case Trade.PLAYER_INVENTORY_INTERFACE:
                if (player.getRequestManager().getRequestType() == RequestType.TRADE) {
                    if (slot >= 0 && slot < Inventory.SIZE) {
                        player.getInterfaceState().openEnterAmountInterface(interfaceId, slot, id);
                    }
                }
                break;
        }
    }
}
