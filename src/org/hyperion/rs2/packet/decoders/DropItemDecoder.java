package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.DropItemContext;

/**
 * @author Global - 7/15/13 6:20 AM
 */
public class DropItemDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        int itemId = packet.getLEShortA();
        int interfaceId = packet.getLEShort();
        int itemIndex = packet.getLEShortA();
        return new DropItemContext(itemId, interfaceId, itemIndex);
    }
}
