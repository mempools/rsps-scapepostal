package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.ActionButtonContext;

/**
 * @author Global - 7/15/13 6:01 AM
 */
public class ActionButtonDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        final int button = packet.getShort();
        return new ActionButtonContext(button);
    }
}
