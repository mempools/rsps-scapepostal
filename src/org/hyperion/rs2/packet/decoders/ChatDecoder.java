package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.ChatMessageContext;

/**
 * @author Global - 7/15/13 6:03 AM
 */
public class ChatDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        int effects = packet.getByteA() & 0xFF;
        int colour = packet.getByteA() & 0xFF;
        int size = packet.getLength() - 2;
        byte[] rawChatData = new byte[size];
        packet.get(rawChatData);
        byte[] chatData = new byte[size];
        for (int i = 0; i < size; i++) {
            chatData[i] = (byte) (rawChatData[size - i - 1] - 128);
        }

        return new ChatMessageContext(colour, effects, size, chatData);
    }
}
