package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.combat.MagicData;
import org.hyperion.rs2.model.combat.MagicData.Spell;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.DefaultContext;
import org.hyperion.rs2.packet.context.MagicOnEntityContext;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class MagicOnEntityDecoder implements PacketDecoder {

    private final int MAGIC_ON_NPC = 104;

    @Override
    public EventContext decode(Packet packet) {
        int index;
        int spellId;
        switch (packet.getOpcode()) {
            case MAGIC_ON_NPC:
                spellId = packet.getShort();//spell
                index = packet.getLEShort();//index
                NPC npc = (NPC) World.getWorld().getNPCs().get(index);
                Spell spell = MagicData.forId(spellId);
                return new MagicOnEntityContext(spell, npc);
            default:
                return new DefaultContext(packet);
        }
    }

}
