package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.DefaultContext;
import org.hyperion.rs2.packet.context.PlayerOptionContext;

/**
 * @author Global - 7/15/13 7:30 AM
 */
public class PlayerOptionDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        int index;
        switch (packet.getOpcode()) {
            case 245:
                index = packet.getLEShortA() & 0xFFFF;
                return new PlayerOptionContext(1, (Player) World.getWorld().getPlayers().get(index));
            case 37:
                index = packet.getShort() & 0xFFFF;
                return new PlayerOptionContext(2, (Player) World.getWorld().getPlayers().get(index));
            case 227:
                index = packet.getLEShortA() & 0xFFFF;
                return new PlayerOptionContext(3, (Player) World.getWorld().getPlayers().get(index));
            default:
                return new DefaultContext(packet);
        }
    }
}
