package org.hyperion.rs2.packet.decoders;

import java.util.LinkedList;
import java.util.List;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.CommandContext;

/**
 * @author Global - 7/15/13 6:16 AM
 */
public class CommandDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        String commandString = packet.getRS2String();
        String[] args = parse(commandString + "\"\"");//cheap fix
        String command = args[0].toLowerCase();
        return new CommandContext(command, args);
    }

    /**
     * Gets the arguments. Nikki's <http://www.rune-server.org/members/nikki/>
     * code, arguments are split with quotes. e.g.
     * {@code >::ban "player name" 10 "reason here"}
     *
     * @param source The command
     * @return The arguments
     */
    public static String[] parse(String source) {
        List<String> out = new LinkedList<>();
        StringBuilder entry = new StringBuilder();
        source = source.trim();
        byte[] str = source.getBytes();
        for (int i = 0; i < str.length; ++i) {
            char c = (char) str[i];
            if (c != '\"') {
                for (; i < str.length; ++i) {
                    c = (char) str[i];
                    if (c != ' ' && c != '\"') {
                        entry.append(c);
                    } else {
                        out.add(entry.toString());
                        entry.delete(0, entry.length());
                        break;
                    }
                }
            } else {
                i += 1;
                for (; i < str.length; ++i) {
                    c = (char) str[i];
                    if (c != '\"') {
                        entry.append(c);
                    } else {
                        for (; i < str.length; ++i) {
                            c = (char) str[i];
                            if (c != ' ' && c != '\"') {
                                i -= 1;
                                out.add(entry.toString());
                                entry.delete(0, entry.length());
                                break;
                            }
                        }
                        break;
                    }
                }
                if (entry.length() > 0) { // unclosed string literal
                    out.add(entry.toString());
                    entry.delete(0, entry.length());
                }
            }
        }
        return out.toArray(new String[out.size()]);
    }
}
