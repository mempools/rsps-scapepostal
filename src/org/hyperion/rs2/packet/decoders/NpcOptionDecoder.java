package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.DefaultContext;
import org.hyperion.rs2.packet.context.NpcOptionContext;

/**
 * @author Global - 7/15/13 7:11 AM
 */
public class NpcOptionDecoder implements PacketDecoder {

    /**
     * The NPC packet options.
     */
    private final int ATTACK_OPTION = 67, OPTION_TWO = 112, OPTION_THREE = 13;

    @Override
    public EventContext decode(Packet packet) {
        int index;
        NPC npc;
        switch (packet.getOpcode()) {
            case ATTACK_OPTION:
                index = packet.getShortA() & 0xFFFF;
                npc = (NPC) World.getWorld().getNPCs().get(index);
                return new NpcOptionContext(1, npc);
            case OPTION_TWO:
                index = packet.getLEShort();
                npc = (NPC) World.getWorld().getNPCs().get(index);
                return new NpcOptionContext(2, npc);
            case OPTION_THREE:
                index = packet.getLEShortA();
                npc = (NPC) World.getWorld().getNPCs().get(index);
                return new NpcOptionContext(3, npc);
            default:
                return new DefaultContext(packet);
        }
    }
}
