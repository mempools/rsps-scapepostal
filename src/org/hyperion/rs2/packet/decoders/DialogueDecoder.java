package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.DialogueContext;

/**
 * @author Global - 7/15/13 6:22 AM
 */
public class DialogueDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        return new DialogueContext(packet.getShort());
    }
}
