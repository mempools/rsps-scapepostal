package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.MoveItemContext;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class MoveItemDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        int newSlot = packet.getLEShortA();
        packet.getByteA(); // unknown
        packet.getShortA(); // frame
        int oldSlot = packet.getLEShort();
        return new MoveItemContext(newSlot, oldSlot);
    }

}
