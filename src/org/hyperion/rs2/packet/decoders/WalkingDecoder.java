package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.WalkingContext;

/**
 * @author Global - 7/15/13 8:34 AM
 */
public class WalkingDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        int size = packet.getLength();
        if (packet.getOpcode() == 213) {
            size -= 14;
        }
        final int steps = (size - 5) / 2;
        final int[][] path = new int[steps][2];

        final int firstX = packet.getLEShortA();
        final boolean runSteps = packet.getByte() == 1;
        final int firstY = packet.getLEShortA();

        for (int i = 0; i < steps; i++) {
            path[i][0] = packet.getByte();
            path[i][1] = packet.getByteS();
        }
        return new WalkingContext(packet, size, firstX, firstY, steps, runSteps, path);
    }
}
