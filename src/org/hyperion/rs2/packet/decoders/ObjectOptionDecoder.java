package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.DefaultContext;
import org.hyperion.rs2.packet.context.ObjectOptionContext;

/**
 * @author Global - 7/15/13 7:20 AM
 */
public class ObjectOptionDecoder implements PacketDecoder {

    /**
     * Option 1 opcode.
     */
    public static final int OPTION_1 = 181, OPTION_2 = 241, OPTION_3 = 50;

    @Override
    public EventContext decode(Packet packet) {
        int x;
        int y;
        int id;
        switch (packet.getOpcode()) {
            case OPTION_1:
                x = packet.getShortA() & 0xFFFF;
                y = packet.getLEShort() & 0xFFFF;
                id = packet.getLEShort() & 0xFFFF;
                return new ObjectOptionContext(1, World.getWorld().getRegionManager().getGameObject(Location.create(x, y), id));
            case OPTION_2:
                id = packet.getShortA() & 0xFFFF;
                x = packet.getShort() & 0xFFFF;
                y = packet.getShortA() & 0xFFFF;
                return new ObjectOptionContext(2, World.getWorld().getRegionManager().getGameObject(Location.create(x, y), id));
            case OPTION_3:
                y = packet.getShortA() & 0xFFFF;
                id = packet.getLEShort() & 0xFFFF;
                x = packet.getLEShortA() & 0xFFFF;
                return new ObjectOptionContext(3, World.getWorld().getRegionManager().getGameObject(Location.create(x, y), id));
            default:
                return new DefaultContext(packet);
        }
    }
}
