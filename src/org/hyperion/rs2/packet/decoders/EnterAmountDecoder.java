package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.EnterAmountContext;

/**
 * @author Global - 7/15/13 6:23 AM
 */
public class EnterAmountDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        return new EnterAmountContext(packet.getInt());
    }
}
