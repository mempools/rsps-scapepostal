package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.WieldContext;

/**
 * @author Global - 7/15/13 7:56 AM
 */
public class WieldDecoder implements PacketDecoder {

    @Override
    public EventContext decode(Packet packet) {
        int interfaceId = packet.getLEShort() & 0xFFFF;
        int id = packet.getLEShort() & 0xFFFF;
        int slot = packet.getShortA() & 0xFFFF;
        return new WieldContext(id, slot, interfaceId);
    }
}
