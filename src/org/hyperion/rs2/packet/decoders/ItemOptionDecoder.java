package org.hyperion.rs2.packet.decoders;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.packet.context.DefaultContext;
import org.hyperion.rs2.packet.context.ItemOptionContext;

/**
 * @author Global - 7/15/13 6:27 AM
 */
public class ItemOptionDecoder implements PacketDecoder {

    /**
     * Option 1 opcode.
     */
    private static final int OPTION_1 = 3;

    /**
     * Option 2 opcode.
     */
    private static final int OPTION_2 = 177;

    /**
     * Option 3 opcode.
     */
    private static final int OPTION_3 = 91;

    /**
     * Option 4 opcode.
     */
    private static final int OPTION_4 = 231;

    /**
     * Option 5 opcode.
     */
    private static final int OPTION_5 = 158;

    @Override
    public EventContext decode(Packet packet) {
        int interfaceId;
        int slot;
        int id;
        switch (packet.getOpcode()) {
            case OPTION_1:
                id = packet.getShortA() & 0xFFFF;
                interfaceId = packet.getShort() & 0xFFFF;
                slot = packet.getShort() & 0xFFFF;
                return new ItemOptionContext(1, interfaceId, slot, id);
            case OPTION_2:
                slot = packet.getShortA() & 0xFFFF;
                id = packet.getLEShort() & 0xFFFF;
                interfaceId = packet.getLEShort() & 0xFFFF;
                return new ItemOptionContext(2, interfaceId, slot, id);
            case OPTION_3:
                id = packet.getLEShort() & 0xFFFF;
                slot = packet.getLEShortA() & 0xFFFF;
                interfaceId = packet.getShort() & 0xFFFF;
                return new ItemOptionContext(3, interfaceId, slot, id);
            case OPTION_4:
                interfaceId = packet.getLEShortA() & 0xFFFF;
                slot = packet.getLEShort() & 0xFFFF;
                id = packet.getShort() & 0xFFFF;
                return new ItemOptionContext(4, interfaceId, slot, id);
            case OPTION_5:
                slot = packet.getLEShortA() & 0xFFFF;
                id = packet.getLEShortA() & 0xFFFF;
                interfaceId = packet.getLEShort() & 0xFFFF;
                return new ItemOptionContext(5, interfaceId, slot, id);
            default:
                return new DefaultContext(packet);
        }
    }
}
