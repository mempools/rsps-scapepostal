package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.DialogueContext;

public class DialoguePacketHandler implements PacketHandler<DialogueContext> {

    @Override
    public void handle(Player player, DialogueContext packet) {
        @SuppressWarnings("unused")
        int interfaceId = packet.getInterfaceId();
        if (player.getInterfaceState().getDialogue() != null) {
            player.getInterfaceState().getDialogue().advanceDialogue(player, 0);
        }
    }
}
