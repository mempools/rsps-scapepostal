package org.hyperion.rs2.packet;

import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.action.impl.WieldItemAction;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.container.Inventory;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.WieldContext;

/**
 * Handles the 'wield' option on items.
 *
 * @author Graham Edgecombe
 */
public class WieldPacketHandler implements PacketHandler<WieldContext> {

    @Override
    public void handle(final Player player, WieldContext packet) {
        final int id = packet.getId();
        final int slot = packet.getSlot();
        final int interfaceId = packet.getInterfaceId();
        switch (interfaceId) {
            case Inventory.INTERFACE:
                if (slot >= 0 && slot < Inventory.SIZE) {
                    Item item = player.getInventory().get(slot);
                    if (item != null && item.getId() == id) {
                        final Action action = new WieldItemAction(player, id, slot, 0);
                        action.execute();
                    }
                }
                break;
        }
    }
}
