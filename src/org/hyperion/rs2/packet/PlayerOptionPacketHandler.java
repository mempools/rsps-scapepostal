package org.hyperion.rs2.packet;

import org.hyperion.rs2.action.impl.AttackAction;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.request.RequestType;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.PlayerOptionContext;

public class PlayerOptionPacketHandler implements PacketHandler<PlayerOptionContext> {

    @Override
    public void handle(Player player, PlayerOptionContext packet) {
        switch (packet.getOption()) {
            case 1:
                option1(player, packet.getTarget());
                break;
            case 4:
                option4(player, packet.getTarget());
                break;
            case 5:
                option5(player, packet.getTarget());
                break;
        }
    }

    /**
     * Handles the first option
     *
     * @param player The player
     * @param target The target
     */
    private void option1(final Player player, Player target) {
        if (target != null && player.getLocation().isWithinInteractionDistance(target.getLocation())) {
            player.getActionQueue().addAction(new AttackAction(player, target));
        }
    }

    /**
     * Handles the fourth option
     *
     * @param player The player
     * @param target The target
     */
    private void option4(Player player, Player target) {
        if (target != null && target != player && !player.isDead() && !target.isDead()) {
            player.getRequestManager().request(RequestType.TRADE, target);
        }
    }

    /**
     * Handles the 5th option
     *
     * @param player The player
     * @param target The target
     */
    private void option5(Player player, Player target) {
        /**
         * Accept trade
         */
        if (target != null && target != player && !player.isDead() && !target.isDead()) {
            player.getRequestManager().request(RequestType.TRADE, target);
        }
    }
}
