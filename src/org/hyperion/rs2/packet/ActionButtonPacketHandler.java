package org.hyperion.rs2.packet;

import org.hyperion.rs2.content.skills.Prayers.Prayer;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.combat.AttackType;
import org.hyperion.rs2.model.combat.CombatStyle;
import org.hyperion.rs2.model.container.Trade;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.ActionButtonContext;

import java.util.logging.Logger;

/**
 * Handles clicking on most buttons in the interface.
 *
 * @author Graham Edgecombe
 */
public class ActionButtonPacketHandler implements PacketHandler<ActionButtonContext> {

    /**
     * The logger instance.
     */
    private static final Logger logger = Logger.getLogger(ActionButtonPacketHandler.class.getName());

    @Override
    public void handle(Player player, ActionButtonContext context) {
        final int button = context.getButton();
        /**
         * Prayer buttons
         */
        Prayer prayer = Prayer.forButton(button);
        if (prayer != null) {
            player.getPrayers().togglePrayer(prayer);
            return;
        }

        switch (button) {
            case 2458:
                player.getActionSender().sendLogout();
                break;
            case 5387:
                player.getSettings().setWithdrawAsNotes(false);
                break;
            case 5386:
                player.getSettings().setWithdrawAsNotes(true);
                break;
            case 8130:
                player.getSettings().setSwapping(true);
                break;
            case 8131:
                player.getSettings().setSwapping(false);
                break;

            /**
             * Equipment screen
             */
            case 27653:
                player.getActionSender().sendInterface(15106, false);
                break;
            /**
             * Items on Death
             */
            case 27654:
                player.getActionSender().sendInterface(17100, false);
                break;

            /**
             * Design Character Accept
             */
            case 3651:
                player.getActionSender().removeAllWindows();
                break;

            /**
             * DialogueManager Options.
             */
            /**
             * Option 1.
             */
            case 2461:
            case 2471:
            case 2482:
            case 2494:
                player.getInterfaceState().getDialogue().advanceDialogue(player, 0);
                break;
            /**
             * Option 2.
             */
            case 2462:
            case 2472:
            case 2483:
            case 2495:
                player.getInterfaceState().getDialogue().advanceDialogue(player, 1);
                break;
            /**
             * Option 3.
             */
            case 2473:
            case 2484:
            case 2496:
                player.getInterfaceState().getDialogue().advanceDialogue(player, 2);
                break;
            /**
             * Option 4.
             */
            case 2485:
            case 2497:
                player.getInterfaceState().getDialogue().advanceDialogue(player, 3);
                break;
            /**
             * Option 5.
             */
            case 2498:
                player.getInterfaceState().getDialogue().advanceDialogue(player, 4);
                break;

            /**
             * Trading buttons
             */
            case 3420:
                if (player.getRequestManager().getAcquaintance() != null) {
                    Trade.acceptTrade(player, player.getRequestManager().getAcquaintance(), 1);
                }
                break;
            case 3546:
                if (player.getRequestManager().getAcquaintance() != null) {
                    Trade.acceptTrade(player, player.getRequestManager().getAcquaintance(), 2);
                }
                break;

            /**
             * Toggle run
             */
            case 152:
            case 153:
                boolean isRunning = !player.getWalkingQueue().isRunning();
                player.getWalkingQueue().setRunningToggled(isRunning);
                break;
            /**
             * Journal
             */
            case 839: // Previous Page
                if (player.getJournal().getPage() > 0) {
                    player.getJournal().openJournal(player.getJournal().getPage() - 1);
                }
                break;
            case 841: // Next Page
                if (player.getJournal().getPage() < player.getJournal().getMaxPage()) {
                    player.getJournal().openJournal(player.getJournal().getPage() + 1);
                }
                break;
            case 10162: // close journal
            case 12568:
                player.getActionSender().removeAllWindows();
                break;

            /**
             * Combat related buttons
             */
            case 5860: //Unarmed - Accurate - Punch
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 5862: //Unarmed - Aggressive 1 - Kick
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 5861: //Unarmed - Defensive - Punch
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 12298: //Whip - Accurate - Flick
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 12297: //Whip - Controlled 2 - Lash
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.CONTROLLED_2);
                break;
            case 12296: //Whip - Defensive - Deflect
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 782: //Scythe - Accurate - Reap
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 785: //Scythe - Agressive 1 - Chop
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 784: //Scythe - Agressive 2 - Jab
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_2);
                break;
            case 783: //Scythe - Defensive - Block
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 1772: //Bow - Accurate
            case 1757: //X-Bow - Accurate
            case 4454: //Thrown - Accurate
                player.getCombatSession().setAttackType(AttackType.RANGE);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 1771: //Bow - Rapid
            case 1756: //X-Bow - Rapid
            case 4453: //Thrown - Rapid
                player.getCombatSession().setAttackType(AttackType.RANGE);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 1770: //Bow - Longrange
            case 1755: //X-Bow - Longrange
            case 4452: //Thrown - Longrange
                player.getCombatSession().setAttackType(AttackType.RANGE);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 2429: //Sword - Accurate - Chop
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 2432: //Sword - Agressive 1 - Slash
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 2431: //Sword - Controlled 3 - Lunge
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.CONTROLLED_3);
                break;
            case 2430: //Sword - Defensive - Block
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 336: //Staff - Accurate - Bash
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                //  player.getActionSender().sendConfig(108, player.getAutocastSpell() != null ? 2 : 0);
                break;
            case 335: //Staff - Agressive 1 - Pound
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                // player.getActionSender().sendConfig(108, player.getAutocastSpell() != null ? 2 : 0);
                break;
            case 334: //Staff - Defensive - Focus-block
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                //  player.getActionSender().sendConfig(108, player.getAutocastSpell() != null ? 2 : 0);
                break;
            case 3802: //Mace - Accurate - Pound
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 3805: //Mace - Agressive 1 - Pummel
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 3804: //Mace - Controlled 3 - Spike
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.CONTROLLED_3);
                break;
            case 3803: //Mace - Defensive - Block
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 2282: //Dagger - Accurate - Stab
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 2285: //Dagger - Agressive 1 - Lunge
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 2284: //Dagger - Agressive 2 - Slash
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_2);
                break;
            case 2283: //Dagger - Defensive - Block
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 5576: //Pickaxe - Accurate - Spike
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 5579: //Pickaxe - Agressive 1 - Impale
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 5578: //Pickaxe - Agressive 2 - Smash
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_2);
                break;
            case 5577: //Pickaxe - Defensive - Block
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 433: //Hammer - Accurate - Pound
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 432: //Hammer - Agressive 1 - Pummel
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 431: //Hammer - Defensive - Block
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 4711: //Two-hand - Accurate - Chop
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 4714: //Two-hand - Agressive 1 - Slash
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 4713: //Two-hand - Agressive 2 - Smash
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_2);
                break;
            case 4712: //Two-hand - Defensive - Block
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 1704: //Axe - Accurate - Chop
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                break;
            case 1707: //Axe - Agressive 1 - Hack
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 1706: //Axe - Agressive 2 - Smash
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_2);
                break;
            case 1705: //Axe - Defensive - Block
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 8466: //Halberd - Controlled 1 - Jab
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.CONTROLLED_1);
                break;
            case 8468: //Halberd - Agressive 1 - Swipe
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                break;
            case 8467: //Halberd - Defensive - Fend
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 4685: //Spear - Controlled 1 - Lunge
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.CONTROLLED_1);
                break;
            case 4688: //Spear - Controlled 2 - Swipe
                player.getCombatSession().setAttackType(AttackType.SLASH);
                player.getCombatSession().setCombatStyle(CombatStyle.CONTROLLED_2);
                break;
            case 4687: //Spear - Controlled 3 - Pound
                player.getCombatSession().setAttackType(AttackType.CRUSH);
                player.getCombatSession().setCombatStyle(CombatStyle.CONTROLLED_3);
                break;
            case 4686: //Spear - Defensive - Block
                player.getCombatSession().setAttackType(AttackType.STAB);
                player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                break;
            case 7622: //Special attack bar - Spears
            case 7537: //Special attack bar - Bows
            case 7587: //Special attack bar - Swords
            case 7487: //Special attack bar - Axes
            case 7612: //Special attack bar - Maces
            case 7788: //Special attack bar - Claws
            case 7562: //Special attack bar - Daggers
            case 12311: //Special attack bar - Abyssal Whip
            case 8481: //Special attack bar - Halberds
            case 7687: //Special attack bar - 2H's
            case 7662: //Special attack bar - Spears
            case 7637: //Special attack bar - Thrownaxes
                player.getCombatSession().inverseSpecial();
                break;
            case 349:
                if (player.getCombatSession().getAutoCastSpell() != null) {
                    player.getCombatSession().setAttackType(AttackType.MAGIC);
                    player.getCombatSession().setCombatStyle(CombatStyle.AUTOCAST);
                    player.getActionSender().sendConfig(108, 3);
                    player.getActionSender().sendConfig(43, 4);
                }
                break;
            default:
                World.getWorld().getScriptContext().getActionButtonListener().submit(player, context);
                break;
        }
    }
}
