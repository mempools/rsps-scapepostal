package org.hyperion.rs2.packet;

import org.hyperion.rs2.action.impl.MiningAction;
import org.hyperion.rs2.action.impl.MiningAction.Node;
import org.hyperion.rs2.action.impl.ProspectingAction;
import org.hyperion.rs2.action.impl.WoodcuttingAction;
import org.hyperion.rs2.action.impl.WoodcuttingAction.Tree;
import org.hyperion.rs2.content.doors.DoorAction;
import org.hyperion.rs2.model.GameObject;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.ObjectOptionContext;

/**
 * Object option packet handler.
 *
 * @author Graham Edgecombe
 */
public class ObjectOptionPacketHandler implements PacketHandler<ObjectOptionContext> {

    @Override
    public void handle(Player player, ObjectOptionContext packet) {
        switch (packet.getOption()) {
            case 1:
                handleOption1(player, packet.getGameObject());
                break;
            case 2:
                handleOption2(player, packet.getGameObject());
                break;
        }
    }

    /**
     * Handles option 1
     *
     * @param player The player
     * @param object The game object
     */
    private void handleOption1(Player player, GameObject object) {
        int id = object.getDefinition().getId();
        Location loc = object.getLocation();
        // doors
        if (DoorAction.isDoor(id)) {
            player.getActionQueue().addDistanceAction(player, loc, new DoorAction(player, id, loc), 1);
        }

        // woodcutting
        Tree tree = Tree.forId(id);
        if (tree != null && player.getLocation().isWithinInteractionDistance(loc)) {
            player.getActionQueue().addAction(new WoodcuttingAction(player, loc, tree));
        }
        // mining
        Node node = Node.forId(id);
        if (node != null && player.getLocation().isWithinInteractionDistance(loc)) {
            player.getActionQueue().addAction(new MiningAction(player, loc, node));
        }
    }

    /**
     * Handles option 2
     *
     * @param player The player
     * @param object The game object
     */
    private void handleOption2(Player player, GameObject object) {
        int id = object.getDefinition().getId();
        Location loc = object.getLocation();
        Node node = Node.forId(id);
        if (node != null && player.getLocation().isWithinInteractionDistance(loc)) {
            player.getActionQueue().addAction(new ProspectingAction(player, loc, node));
        }
    }
}
