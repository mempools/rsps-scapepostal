package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.ChatMessageContext;
import org.hyperion.rs2.util.TextUtils;

/**
 * Handles public chat messages.
 *
 * @author Graham Edgecombe
 */
public class ChatPacketHandler implements PacketHandler<ChatMessageContext> {

    /**
     * The max chat queue.
     */
    private static final int CHAT_QUEUE_SIZE = 4;

    @Override
    public void handle(Player player, ChatMessageContext context) {
        int effects = context.getEffects();
        int colour = context.getColour();
        int size = context.getSize();
        if (player.getChatMessageQueue().size() >= CHAT_QUEUE_SIZE) {
            return;
        }
        String unpacked = TextUtils.textUnpack(context.getText(), size);
        unpacked = TextUtils.filterText(unpacked);
        unpacked = TextUtils.optimizeText(unpacked);
        byte[] packed = new byte[size];
        TextUtils.textPack(packed, unpacked);
        player.getChatMessageQueue().add(new ChatMessageContext(effects, colour, size, packed));
    }
}
