package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.WalkingContext;

/**
 * A packet which handles walking requests.
 *
 * @author Graham Edgecombe
 */
public class WalkingPacketHandler implements PacketHandler<WalkingContext> {

    @Override
    public void handle(Player player, WalkingContext context) {
        player.getActionSender().removeAllWindows();

        /**
         * Reset queues
         */
        player.getWalkingQueue().reset();
        player.getActionQueue().reset();

        player.resetInteractingActor();
        if (!player.getWalkingQueue().canWalk()) {
            return;
        }

        player.getWalkingQueue().setRunningQueue(context.getRunSteps());
        player.getWalkingQueue().addStep(context.getFirstX(), context.getFirstY());

        for (int i = 0; i < context.getSteps(); i++) {
            context.getPath()[i][0] += context.getFirstX();
            context.getPath()[i][1] += context.getFirstY();
            player.getWalkingQueue().addStep(context.getPath()[i][0], context.getPath()[i][1]);
        }
        player.getWalkingQueue().finish();
    }
}
