package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.GroundItem;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.container.Inventory;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.DropItemContext;

/**
 * Whenever a Client attempts to drop an item, this class will be executed, It
 * creates a new GroundItem and removes the item from the Player's Inventory.
 *
 * @author Bloodraider
 */
public final class DropItemPacketHandler implements PacketHandler<DropItemContext> {

    @Override
    public void handle(Player player, DropItemContext context) {
        int itemId = context.getItemId();
        int interfaceId = context.getInterfaceId();
        int itemIndex = context.getItemIndex();

        switch (interfaceId) {
            case Inventory.INTERFACE:
                Item item = player.getInventory().get(itemIndex);
                if (item.getId() != itemId) {
                    return;
                }
                player.getActionQueue().clearNonWalkableActions();
                player.getActionSender().removeAllWindows();
                player.getInventory().remove(itemIndex, item);
                GroundItem.create(player, item, player.getLocation());
                break;
        }
    }
}
