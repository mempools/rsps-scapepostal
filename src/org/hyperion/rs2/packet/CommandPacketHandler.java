package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.npc.NPCSpawnManager;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.DummyPlayer;
import org.hyperion.rs2.model.definition.NPCDefinition;
import org.hyperion.rs2.model.region.TileMap;
import org.hyperion.rs2.model.region.TileMapBuilder;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hyperion.rs2.action.impl.AttackAction;
import org.hyperion.rs2.content.clan.ChannelManager;
import org.hyperion.rs2.content.dialogue.DialogueType;
import org.hyperion.rs2.content.journal.impl.InfoJournal;
import org.hyperion.rs2.content.journal.impl.MailJournal;
import org.hyperion.rs2.model.*;
import org.hyperion.rs2.model.Damage.Hit;
import org.hyperion.rs2.model.Damage.HitType;
import org.hyperion.rs2.model.UpdateFlags.UpdateFlag;
import org.hyperion.rs2.model.container.Bank;
import org.hyperion.rs2.model.definition.ItemDefinition;
import org.hyperion.rs2.model.region.Region;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.CommandContext;
import org.hyperion.rs2.pf.*;
import org.hyperion.rs2.model.region.Tile;
import org.hyperion.rs2.packet.context.ForceWalkContext;
import org.hyperion.util.xStream;

/**
 * Handles player commands (the ::words).
 *
 * @author Graham Edgecombe
 */
public class CommandPacketHandler implements PacketHandler<CommandContext> {

    @Override
    public void handle(Player player, CommandContext context) {
        World.getWorld().getScriptContext().getCommandListener().submit(player, context);
        String[] args = context.getArgs();
        String command = context.getName();

        /*
         * Game channel chat
         */
        if (command.startsWith("/") && command.length() > 1) {
            if (!ChannelManager.sendChannelCommand(player, args[0].substring(1))) {
                if (player.getGameChannel() != null) {
                    player.getGameChannel().sendMessage(player, args[0].substring(1));
                } else {
                    player.getActionSender().sendMessage("You need to be in a channel! Use /chelp");
                }
            }
        }
        //other commands
        try {
            if (command.equals("hiya")) {
                player.setJournal(new MailJournal(player));
                player.getJournal().openJournal(1);
            }
            if (command.equals("hiya2")) {
                player.setJournal(new InfoJournal(player, player));
                player.getJournal().openJournal(1);
            }
            if (command.equals("mute")) {
                String target = args[1];
                String reason = args[2];
                if (target == null || reason == null) {
                    player.getActionSender().sendMessage("Invalid input");
                    return;
                }
                Player victim = World.getWorld().getPlayer(target);
                if (victim == null) {
                    player.getActionSender().sendMessage(target + " isn't online!");
                    return;
                }
                //victim.setMuted(true);
                victim.getActionSender().sendMessage("You've been muted by " + player.getName() + " for " + reason);
                for (Player players : World.getWorld().getPlayers()) {
                    players.getActionSender().sendMessage(player.getName() + " has muted " + victim.getName() + ". Reason: " + reason);
                }

            }

            switch (command) {
                case "lol":
                    // new DummyPlayer();

                    for (Player npc : player.getLocalPlayers()) {
                        npc.getDamage().setPrimaryHit(new Hit(69, HitType.NORMAL_DAMAGE));
                        npc.getUpdateFlags().flag(UpdateFlag.PRIMARY_HIT);
                    }
                    for (NPC npc : player.getLocalNPCs()) {
                        npc.getDamage().setPrimaryHit(new Hit(69, HitType.NORMAL_DAMAGE));
                        npc.getUpdateFlags().flag(UpdateFlag.PRIMARY_HIT);
                    }
                    break;
                case "dee":
                    for (NPC npc : player.getLocalNPCs()) {
                        npc.playAnimation(npc.getAttackAnimation());
                    }
                    break;
                case "e":
                    player.playGraphics(Graphic.create(252, 0, 100));
                    break;
                case "ewf":
                    player.playAnimation(Animation.create(1062));
                    player.playGraphics(Graphic.create(252));
                    break;
                case "pickup":
                    String name = args[1];
                    ItemDefinition item = null;
                    for (ItemDefinition i : ItemDefinition.getDefinitions()) {
                        if (name.equalsIgnoreCase(i.getName()) && !i.isNoted()) {
                            item = i;
                        }
                    }
                    if (item != null) {
                        int count = 1;
                        if (args.length == 3) {
                            count = Integer.parseInt(args[2]);
                        }
                        Item spawnedItem = new Item(item.getId(), count);
                        if (player.getInventory().hasRoomFor(spawnedItem)) {
                            player.getInventory().add(spawnedItem);
                        } else {
                            player.getActionSender().sendMessage("No room in inventory");
                        }
                    }
                    break;
                case "search":
                    if (args.length == 2) {
                        String search = args[1];
                        int results = 0;
                        for (ItemDefinition searchedItem : ItemDefinition.getDefinitions()) {
                            if (searchedItem != null && searchedItem.getName().contains(search)
                                    && !searchedItem.isNoted()) {
                                results++;
                                if (results > 10) { //limit
                                    return;
                                }
                                player.getActionSender().sendMessage(searchedItem.getName() + " - " + searchedItem.getId());
                            }
                        }
                        player.getActionSender().sendMessage("Found " + results + " results.");
                    }
                    break;
                case "conf":
                    int conf = Integer.parseInt(args[1]);
                    player.getActionSender().sendInterfaceConfig(conf, true);
                    break;
                case "conf2":
                    for (int i = 0; i < 10000; i++) {
                        player.getActionSender().sendInterfaceConfig(i, true);
                    }
                    break;
                case "con":
                    int a = Integer.parseInt(args[1]);
                    int f = Integer.parseInt(args[2]);
                    player.getActionSender().sendConfig(a, f);
                    break;
                case "an":
                    for (NPC npc : player.getLocalNPCs()) {
                        npc.getActionQueue().addAction(new AttackAction(npc, player));
                    }
                    break;
                case "force":
                    int y = 3;
                    int dir = 0;
                    if (player.getLocation().getY() > 3520) {
                        y = -3;
                        dir = 2;
                    }
                    Location start = Location.create(0, 0);
                    Location dest = Location.create(0, y);
                    player.setForceWalk(new ForceWalkContext(start, dest, 33, 60, dir, 2));
                    player.playForceMovement(1, Animation.JOYJUMP, player.getForceWalk());
                    break;
                case "hit":
                    player.getDamage().setPrimaryHit(new Hit(0, HitType.NORMAL_DAMAGE));
                    player.getUpdateFlags().flag(UpdateFlag.PRIMARY_HIT);
                    break;
                case "cs":
                    NPCDefinition def2 = NPCDefinition.forId(Integer.parseInt(args[1]));
                    NPC npc = new NPC(def2, new NPCSpawnManager.SpawnData(def2.getId(), 0, 1, Direction.NORTH_EAST, player.getLocation(), null));
                    npc.setLocation(player.getLocation());
                    Region region = World.getWorld().getRegionManager().getRegionByLocation(npc.getLocation());
                    region.addNpc(npc);
                    World.getWorld().register(npc);

                    NPCSpawnManager.getSpawnList().add(npc.getSpawnData());
                    try {
                        xStream.alias("npcSpawns", NPCSpawnManager.SpawnData.class);
                        xStream.writeXML(NPCSpawnManager.getSpawnList(), new File("./data/npcSpawns.xml"));
                    } catch (IOException ex) {
                        Logger.getLogger(CommandPacketHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    player.getActionSender().sendMessage("Successfully added!");
                    break;
                case "s":
                    int face = Integer.parseInt(args[1]);
                    for (NPC n : World.getWorld().getNPCs()) {
                        if (n.getDefinition().getId() == 2949) {
                        }
                    }
                    break;
                case "mq":
                    if (player.getMission() != null) {
                        player.getMission().quit(player);
                    }
                    break;
                case "ac":
                    //abl.clear();
                    break;

                case "al":
//                    for (ActionButtonEvent a : abl.getButtonListeners()) {
//                        System.out.println(a.getClass().getName() + " : "+abl.getButtonListeners().indexOf(a));
//                    }
                    break;
            }
            if (command.equals("spawn")) {
                NPCDefinition def = NPCDefinition.forId(Integer.parseInt(args[1]));
                NPC npc = new NPC(def, new NPCSpawnManager.SpawnData(def.getId(), 0, 1, Direction.EAST, player.getLocation(), null));
                npc.setLocation(player.getLocation());
                Region region = World.getWorld().getRegionManager().getRegionByLocation(npc.getLocation());
                region.addNpc(npc);
                World.getWorld().register(npc);
            } else if (command.equals("reg")) {
                player.getActionSender().sendMessage("Region: " + player.getRegion().getCoordinates().getX() + ", " + player.getRegion().getCoordinates().getY());
            } else if (command.equals("bank")) {
                Bank.open(player, player.getBank());
            } else if (command.equals("gbank")) {
                Bank.open(player, player.getGang().getBank());
            } else if (command.equals("ee")) {
                player.getActionSender().sendInterface(18343, false).sendInterfaceAnimation(18343, 3457);
            } else if (command.equals("ti")) {
                player.getActionSender().sendDialogue("Hello", DialogueType.INFO, -1, null, "1", "2", "3", "4", "5");
            } else if (command.equals("max")) {
                for (int i = 0; i <= Skills.SKILL_COUNT - 1; i++) {
                    player.getSkills().setLevel(i, 99);
                    player.getSkills().setExperience(i, 13034431);
                }
            } else if (command.startsWith("empty")) {
                player.getInventory().clear();
                player.getActionSender().sendMessage("Your inventory has been emptied.");
            } else if (command.startsWith("dum")) {
                World.getWorld().register(new DummyPlayer());
            } else if (command.startsWith("j")) {
                player.setJournal(new InfoJournal(player, player)).openJournal(0);
            } else if (command.startsWith("ci")) {
                player.getActionSender().sendMessage("Current Interface: " + player.getInterfaceState().getCurrentInterface());
            } else if (command.startsWith("lvl")) {
                try {
                    player.getSkills().setLevel(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
                    player.getSkills().setExperience(Integer.parseInt(args[1]), player.getSkills().getXPForLevel(Integer.parseInt(args[2])) + 1);
                    player.getActionSender().sendMessage(Skills.SKILL_NAME[Integer.parseInt(args[1])] + " level is now " + Integer.parseInt(args[2]) + ".");
                } catch (NumberFormatException e) {
                    player.getActionSender().sendMessage("Syntax is ::lvl [skill] [lvl].");
                }
            } else if (command.startsWith("skill")) {
                try {
                    player.getSkills().setLevel(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
                    player.getActionSender().sendMessage(Skills.SKILL_NAME[Integer.parseInt(args[1])] + " level is temporarily boosted to " + Integer.parseInt(args[2]) + ".");
                } catch (NumberFormatException e) {
                    player.getActionSender().sendMessage("Syntax is ::skill [skill] [lvl].");
                }
            } else if (command.startsWith("enablepvp")) {
                try {
                    player.updatePlayerAttackOptions(true);
                    player.getActionSender().sendMessage("PvP combat enabled.");
                } catch (Exception e) {
                }
            } else if (command.startsWith("got")) {

                AStarPathFinderNew a = new AStarPathFinderNew();
                a.handle(player, Location.create(Integer.parseInt(args[1]), Integer.parseInt(args[2]), player.getLocation().getZ()));
            } else if (command.startsWith("goto2")) {

                PathFinder pf = new DumbPathFinder();
                Path p = pf.findPath(player.getLocation(), Location.create(Integer.parseInt(args[1]), Integer.parseInt(args[2]), player.getLocation().getZ()));

                if (p == null) {
                    return;
                }

                for (Point p2 : p.getPoints()) {
                    player.getWalkingQueue().addStep(p2.getX(), p2.getY());
                }
                player.getWalkingQueue().finish();
            } else if (command.startsWith("goto")) {
                try {
                    int radius = 16;

                    int x = Integer.parseInt(args[1]) - player.getLocation().getX() + radius;
                    int y = Integer.parseInt(args[2]) - player.getLocation().getY() + radius;

                    TileMapBuilder bldr = new TileMapBuilder(player.getLocation(), radius);
                    TileMap map = bldr.build();

                    PathFinder pf = new AStarPathFinder();
                    Path p = pf.findPath(player.getLocation(), radius, map, radius, radius, x, y);

                    if (p == null) {
                        return;
                    }

                    player.getWalkingQueue().reset();
                    for (Point p2 : p.getPoints()) {
                        player.getWalkingQueue().addStep(p2.getX(), p2.getY());
                    }
                    player.getWalkingQueue().finish();
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }

            } else if (command.startsWith("tmask")) {
                int radius = 0;
                TileMapBuilder bldr = new TileMapBuilder(player.getLocation(), radius);
                TileMap map = bldr.build();
                Tile t = map.getTile(0, 0);
                player.getActionSender().sendMessage("N: " + t.isNorthernTraversalPermitted()
                        + " E: " + t.isEasternTraversalPermitted()
                        + " S: " + t.isSouthernTraversalPermitted()
                        + " W: " + t.isWesternTraversalPermitted());
            }
        } catch (NumberFormatException ex) {
            player.getActionSender().sendMessage("Error while processing command.");
        }
    }
}
