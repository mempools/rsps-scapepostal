package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.player.InterfaceState;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.container.Bank;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.EnterAmountContext;

/**
 * A packet sent when the player enters a custom amount for banking etc.
 *
 * @author Graham Edgecombe
 */
public class EnterAmountPacketHandler implements PacketHandler<EnterAmountContext> {

    @Override
    public void handle(Player player, EnterAmountContext context) {
        int amount = context.getAmount();
        InterfaceState state = player.getInterfaceState();
        int enterAmountId = state.getEnterAmountId();
        int enterAmountSlot = state.getEnterAmountSlot();
        if (player.getInterfaceState().isEnterAmountInterfaceOpen()) {
            switch (state.getEnterAmountInterfaceId()) {
                case Bank.PLAYER_INVENTORY_INTERFACE:
                    Bank.deposit(player, enterAmountSlot, enterAmountId, amount);
                    break;
                case Bank.BANK_INVENTORY_INTERFACE:
                    Bank.withdraw(player, enterAmountSlot, enterAmountId, amount);
                    break;
            }
        }
    }
}
