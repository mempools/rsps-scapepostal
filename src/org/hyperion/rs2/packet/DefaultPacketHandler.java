package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.DefaultContext;

import java.util.logging.Logger;

/**
 * Reports information about unhandled packets.
 *
 * @author Graham Edgecombe
 */
public class DefaultPacketHandler implements PacketHandler<DefaultContext> {

    /**
     * The logger instance.
     */
    private static final Logger logger = Logger.getLogger(DefaultPacketHandler.class.getName());

    @Override
    public void handle(Player player, DefaultContext context) {
        Packet packet = context.getPacket();
        logger.info("Packet : [opcode=" + packet.getOpcode() + " length=" + packet.getLength() + " payload=" + packet.getPayload() + "]");
    }
}
