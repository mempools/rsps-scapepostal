package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.MoveItemContext;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class MoveItemPacketHandler implements PacketHandler<MoveItemContext> {

    @Override
    public void handle(Player player, MoveItemContext context) {
        player.getInventory().swap(context.getNewSlot(), context.getOldSlot());
    }

}
