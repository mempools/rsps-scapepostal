package org.hyperion.rs2.packet;

import org.hyperion.rs2.action.impl.AttackAction;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.NpcOptionContext;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class NpcOptionPacketHandler implements PacketHandler<NpcOptionContext> {

    @Override
    public void handle(Player player, NpcOptionContext context) {
        NPC npc = context.getTarget();
        if (npc == null) {
            return;
        }
        player.setInteractingActor(npc);

        switch (context.getOption()) {
            case 1:
                if (player.getLocation().isWithinInteractionDistance(npc.getLocation())) {
                    player.getActionQueue().addAction(new AttackAction(player, npc));
                }
                break;
        }
    }

}
