package org.hyperion.rs2.packet;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.packet.context.DefaultContext;

/**
 * A packet handler that is called when an interface is closed.
 *
 * @author Graham Edgecombe
 */
public class CloseInterfacePacketHandler implements PacketHandler<DefaultContext> {

    @Override
    public void handle(Player player, DefaultContext context) {
        player.getInterfaceState().interfaceClosed();
        player.getActionSender().removeAllWindows();
    }
}
