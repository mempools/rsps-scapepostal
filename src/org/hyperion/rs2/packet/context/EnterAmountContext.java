package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class EnterAmountContext implements EventContext {

    /**
     * The entered amount.
     */
    private int amount;

    /**
     * The enter amount packet listener
     *
     * @param amount The entered amount
     */
    public EnterAmountContext(int amount) {
        this.amount = amount;
    }

    /**
     * Gets the entered amount
     *
     * @return amount The entered amount
     */
    public int getAmount() {
        return amount;
    }
}
