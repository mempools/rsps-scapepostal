package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class DialogueContext implements EventContext {

    /**
     * Gets the dialogue interface id.
     */
    private int interfaceId;

    /**
     * The dialogue packet listener
     *
     * @param interfaceId The interface id
     */
    public DialogueContext(int interfaceId) {
        this.interfaceId = interfaceId;
    }

    /**
     * Gets the dialogue interface id
     *
     * @return interfaceId The dialogue interface id
     */
    public int getInterfaceId() {
        return interfaceId;
    }
}
