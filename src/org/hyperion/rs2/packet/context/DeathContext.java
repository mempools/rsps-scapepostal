package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.net.EventContext;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class DeathContext implements EventContext {

    /**
     * An enumeration of the death stages.
     */
    public enum DeathStage {

        /**
         * The <code>Before</code> stage is the stage when a player dies but
         * does not teleport yet, Used for like, the grim reaper death example.
         */
        BEFORE,
        /**
         * The <code>Action</code> stage is when the player dies and stuff like:
         * dropping items and such is handled.
         */
        ACTION,
        /**
         * The <code>After</code> stage is when the player gets teleported to
         * the default home location.
         */
        AFTER;
    }

    /**
     * The victim.
     */
    private final Actor victim;

    /**
     * The killer.
     */
    private final Actor killer;

    /**
     * The death stage.
     */
    private final DeathStage stage;

    /**
     * Creates the death event context
     *
     * @param victim The victim
     * @param killer The killer
     * @param stage The stage
     */
    public DeathContext(Actor victim, Actor killer, DeathStage stage) {
        this.victim = victim;
        this.killer = killer;
        this.stage = stage;
    }

    /**
     * Gets the victim
     *
     * @return The victim
     */
    public Actor getVictim() {
        return victim;
    }

    /**
     * Gets the killer
     *
     * @return The killer
     */
    public Actor getKiller() {
        return killer;
    }

    /**
     * Gets the death stage
     *
     * @return The death stage
     */
    public DeathStage getStage() {
        return stage;
    }

}
