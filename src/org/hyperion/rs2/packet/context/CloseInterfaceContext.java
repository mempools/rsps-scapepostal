package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 * @author Global - 7/15/13 6:13 AM
 */
public class CloseInterfaceContext implements EventContext {

    private final int _interface;

    public CloseInterfaceContext(int _interface) {
        this._interface = _interface;
    }

    public int getInterface() {
        return _interface;
    }
}
