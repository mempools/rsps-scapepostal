package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class PlayerOptionContext implements EventContext {

    /**
     * The option.
     */
    private int option;
    /**
     * The player.
     */
    private Player target;

    /**
     * The player
     *
     * @param option The option
     * @param target The player
     */
    public PlayerOptionContext(int option, Player target) {
        this.option = option;
        this.target = target;
    }

    /**
     * Gets the option
     *
     * @return option The option
     */
    public int getOption() {
        return option;
    }

    /**
     * Gets the player
     *
     * @return target The player
     */
    public Player getTarget() {
        return target;
    }
}
