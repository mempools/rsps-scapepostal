package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;

/**
 * @author Global - 7/15/13 6:12 AM
 */
public class DefaultContext implements EventContext {

    private Packet packet;

    public DefaultContext(Packet packet) {
        this.packet = packet;
    }

    public Packet getPacket() {
        return packet;
    }
}
