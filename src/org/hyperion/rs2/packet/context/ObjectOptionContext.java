package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.model.GameObject;
import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class ObjectOptionContext implements EventContext {

    /**
     * The option.
     */
    private int option;

    /**
     * The object
     */
    private GameObject gameObject;

    /**
     * The object option packet listener
     *
     * @param option The option
     * @param object The game object
     */
    public ObjectOptionContext(int option, GameObject object) {
        this.option = option;
        this.gameObject = object;
    }

    /**
     * Gets the option
     *
     * @return option The option
     */
    public int getOption() {
        return option;
    }

    /**
     * Gets the game object
     *
     * @return gameObject The game object
     */
    public GameObject getGameObject() {
        return gameObject;
    }
}
