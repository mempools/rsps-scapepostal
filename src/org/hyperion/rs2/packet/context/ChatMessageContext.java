package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 * Represents a single chat message.
 *
 * @author Graham Edgecombe
 */
public class ChatMessageContext implements EventContext {

    /**
     * The colour.
     */
    private final int colour;

    /**
     * The effects.
     */
    private final int effects;

    /**
     * The size.
     */
    private final int size;
    /**
     * The packed chat text.
     */
    private final byte[] text;

    /**
     * Creates a new chat message.
     *
     * @param colour The message colour.
     * @param effects The message effects.
     * @param size The message size.
     * @param text The packed chat text.
     */
    public ChatMessageContext(int colour, int effects, int size, byte[] text) {
        this.colour = colour;
        this.effects = effects;
        this.size = size;
        this.text = text;
    }

    /**
     * Gets the message colour.
     *
     * @return The message colour.
     */
    public int getColour() {
        return colour;
    }

    /**
     * Gets the message effects.
     *
     * @return The message effects.
     */
    public int getEffects() {
        return effects;
    }

    /**
     * Gets the message size.
     *
     * @return The message size.
     */
    public int getSize() {
        return size;
    }

    /**
     * Gets the packed message text.
     *
     * @return The packed message text.
     */
    public byte[] getText() {
        return text;
    }
}
