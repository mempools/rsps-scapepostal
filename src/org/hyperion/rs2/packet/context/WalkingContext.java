package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class WalkingContext implements EventContext {

    /**
     * The packet.
     */
    private Packet packet;

    /**
     * The size.
     */
    private int size;

    /**
     * The clicked X-coordinate.
     */
    private int firstX;

    /**
     * The clicked Y-coordinate.
     */
    private int firstY;

    /**
     * The steps.
     */
    private int steps;

    /**
     * If running through the steps.
     */
    private boolean runSteps;

    /**
     * The paths.
     */
    private int[][] path;

    /**
     * The walking packet context
     *
     * @param packet The packet
     * @param size The size
     * @param firstX The first X-Coordinate
     * @param firstY The first Y-Coordinate
     * @param steps The steps
     * @param runSteps The running steps
     * @param path The path
     */
    public WalkingContext(Packet packet, int size, int firstX, int firstY, int steps, boolean runSteps, int[][] path) {
        this.packet = packet;
        this.size = size;
        this.firstX = firstX;
        this.firstY = firstY;
        this.steps = steps;
        this.runSteps = runSteps;
        this.path = path;
    }

    /**
     * Gets the packet
     *
     * @return packet The packet
     */
    public Packet getPacket() {
        return packet;
    }

    /**
     * Gets the size
     *
     * @return The size
     */
    public int getSize() {
        return size;
    }

    /**
     * Gets the first X-coordinate.
     *
     * @return The first X-coordinate.
     */
    public int getFirstX() {
        return firstX;
    }

    /**
     * Gets the first Y-coordinate.
     *
     * @return The first Y-coordinate.
     */
    public int getFirstY() {
        return firstY;
    }

    /**
     * Gets the steps.
     *
     * @return The steps.
     */
    public int getSteps() {
        return steps;
    }

    /**
     * Gets the run steps flag.
     *
     * @return The run steps flag.
     */
    public boolean getRunSteps() {
        return runSteps;
    }

    /**
     * Gets the path.
     *
     * @return The path.
     */
    public int[][] getPath() {
        return path;
    }
}
