package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class CommandContext implements EventContext {

    /**
     * The name.
     */
    private final String name;
    /**
     * The arguments.
     */
    private final String[] args;

    /**
     * The name packet listener
     *
     * @param name The command name
     * @param args The arguments
     */
    public CommandContext(String name, String[] args) {
        this.name = name;
        this.args = args;
    }

    /**
     * Gets the name
     *
     * @return name The name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the arguments
     *
     * @return The arguments
     */
    public String[] getArgs() {
        return args;
    }
}
