package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag
 */
public class ActionButtonContext implements EventContext {

    /**
     * The button.
     */
    private final int button;

    /**
     * The action button packet listener
     *
     * @param button The button
     */
    public ActionButtonContext(int button) {
        this.button = button;
    }

    /**
     * Gets the action button
     *
     * @return button The action button
     */
    public int getButton() {
        return button;
    }
}
