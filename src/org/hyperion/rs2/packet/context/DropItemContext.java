package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class DropItemContext implements EventContext {

    /**
     * The item id.
     */
    private int itemId;
    /**
     * The interface id.
     */
    private int interfaceId;
    /**
     * The items index.
     */
    private int itemIndex;

    /**
     * The drop item packet listener
     *
     * @param itemId The item id
     * @param interfaceId The interface id
     * @param itemIndex The item index
     */
    public DropItemContext(int itemId, int interfaceId, int itemIndex) {
        this.itemId = itemId;
        this.interfaceId = interfaceId;
        this.itemIndex = itemIndex;
    }

    /**
     * Gets the item id
     *
     * @return itemId The item id
     */
    public int getItemId() {
        return itemId;
    }

    /**
     * Gets the interface id
     *
     * @return interfaceId The interface id
     */
    public int getInterfaceId() {
        return interfaceId;
    }

    /**
     * Gets the item index
     *
     * @return itemIndex The item index
     */
    public int getItemIndex() {
        return itemIndex;
    }
}
