package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class WieldContext implements EventContext {

    /**
     * The item id.
     */
    private int id;
    /**
     * The slot id.
     */
    private int slot;
    /**
     * The interface id.
     */
    private int interfaceId;

    /**
     * The wield packet listener
     *
     * @param id The item id
     * @param slot The slot
     * @param interfaceId The interface id
     */
    public WieldContext(int id, int slot, int interfaceId) {
        this.id = id;
        this.slot = slot;
        this.interfaceId = interfaceId;
    }

    /**
     * Gets the item id
     *
     * @return id The item id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the slot
     *
     * @return slot The slot
     */
    public int getSlot() {
        return slot;
    }

    /**
     * Gets the interface id
     *
     * @return interfaceId The interface id
     */
    public int getInterfaceId() {
        return interfaceId;
    }
}
