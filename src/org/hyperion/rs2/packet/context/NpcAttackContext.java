package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.net.EventContext;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class NpcAttackContext implements EventContext {

    /**
     * The NPC to attack.
     */
    private final NPC npc;

    /**
     * The constructor.
     *
     * @param npc The NPC.
     */
    public NpcAttackContext(NPC npc) {
        this.npc = npc;
    }

    /**
     * Gets the NPC.
     *
     * @return The NPC.
     */
    public NPC getNpc() {
        return npc;
    }

}
