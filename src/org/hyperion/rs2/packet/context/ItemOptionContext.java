package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class ItemOptionContext implements EventContext {

    /**
     * The option id.
     */
    private final int option;
    /**
     * The interface id.
     */
    private final int interfaceId;
    /**
     * The slot.
     */
    private final int slot;
    /**
     * The item id.
     */
    private final int id;

    /**
     * The item option packet listener
     *
     * @param option The option
     * @param interfaceId The interface id
     * @param slot The slot
     * @param id The item id
     */
    public ItemOptionContext(int option, int interfaceId, int slot, int id) {
        this.option = option;
        this.interfaceId = interfaceId;
        this.slot = slot;
        this.id = id;
    }

    /**
     * Gets the option
     *
     * @return option The option
     */
    public int getOption() {
        return option;
    }

    /**
     * Gets the interface
     *
     * @return interfaceId The interface id
     */
    public int getInterface() {
        return interfaceId;
    }

    /**
     * Gets the slot
     *
     * @return slot The slot
     */
    public int getSlot() {
        return slot;
    }

    /**
     * Gets the item id
     *
     * @return id The item id
     */
    public int getId() {
        return id;
    }
}
