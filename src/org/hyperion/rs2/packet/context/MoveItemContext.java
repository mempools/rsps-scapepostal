package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;

/**
 *
 * @author Global <http://rune-server.org/members/global>
 */
public class MoveItemContext implements EventContext {

    /**
     * The old item slot.
     */
    private final int oldSlot;

    /**
     * The new item slot.
     */
    private final int newSlot;

    /**
     * The constructor
     *
     * @param oldSlot The old item slot
     * @param newSlot The new item slot
     */
    public MoveItemContext(int oldSlot, int newSlot) {
        this.oldSlot = oldSlot;
        this.newSlot = newSlot;
    }

    /**
     * Gets the old item slot
     *
     * @return The old item slot
     */
    public int getOldSlot() {
        return oldSlot;
    }

    /**
     * Gets the new item slot
     *
     * @return The new item slot
     */
    public int getNewSlot() {
        return newSlot;
    }

}
