package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.net.EventContext;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.combat.MagicData.Spell;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class MagicOnEntityContext implements EventContext {

    /**
     * The character.
     */
    private final Actor character;

    /**
     * The spell.
     */
    private final Spell spell;

    /**
     * Creates the context
     *
     * @param spell The spell
     * @param character The character
     */
    public MagicOnEntityContext(Spell spell, Actor character) {
        this.spell = spell;
        this.character = character;
    }

    /**
     * Gets the character
     *
     * @return The character
     */
    public Actor getCharacter() {
        return character;
    }

    /**
     * Gets the spell
     *
     * @return The spell
     */
    public Spell getSpell() {
        return spell;
    }

}
