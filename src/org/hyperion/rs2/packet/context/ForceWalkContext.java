package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.net.EventContext;

/**
 * A class to handle force walk information
 *
 * @author Global <http://rune-server.org/members/global>
 */
public class ForceWalkContext implements EventContext {

    /**
     * The source location.
     */
    private final Location source;

    /**
     * The destination location.
     */
    private final Location destination;

    /**
     * The speed to get to the source location.
     */
    private final int speed;

    /**
     * The speed to return (?? I think I don't know).
     */
    private final int returnSpeed;

    /**
     * The direction to face.
     */
    private final int direction;

    /**
     * The force walk speed.
     */
    private final int tick;

    /**
     * The force walk constructor
     *
     * @param source The source location
     * @param destination The destination location
     * @param sourceSpeed The source speed
     * @param destinationSpeed The destination speed
     * @param direction The direction
     * @param tick The force walk speed
     */
    public ForceWalkContext(Location source, Location destination, int sourceSpeed, int destinationSpeed, int direction, int tick) {
        this.source = source;
        this.destination = destination;
        this.speed = sourceSpeed;
        this.returnSpeed = destinationSpeed;
        this.direction = direction;
        this.tick = tick;
    }

    /**
     * Gets the source location
     *
     * @return The source location
     */
    public Location getSource() {
        return source;
    }

    /**
     * The destination location
     *
     * @return The destination location
     */
    public Location getDestination() {
        return destination;
    }

    /**
     * The source speed
     *
     * @return The source speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * The destination speed
     *
     * @return The destination speed
     */
    public int getReturnSpeed() {
        return returnSpeed;
    }

    /**
     * Gets the facing direction
     *
     * @return The facing direction
     */
    public int getDirection() {
        return direction;
    }

    /**
     * Gets the tick speed of the force walk
     *
     * @return The tick
     */
    public int getTick() {
        return tick;
    }

}
