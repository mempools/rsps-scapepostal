package org.hyperion.rs2.packet.context;

import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.net.EventContext;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class NpcOptionContext implements EventContext {

    /**
     * The option.
     */
    private final int option;
    /**
     * The NPC.
     */
    private final NPC target;

    /**
     * The NPC option packet listener
     *
     * @param option The option
     * @param target The target
     */
    public NpcOptionContext(int option, NPC target) {
        this.option = option;
        this.target = target;
    }

    /**
     * Gets the option
     *
     * @return option The option
     */
    public int getOption() {
        return option;
    }

    /**
     * Gets the NPC
     *
     * @return target The NPC
     */
    public NPC getTarget() {
        return target;
    }
}
