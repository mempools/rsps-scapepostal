package org.hyperion.rs2.pf;

import org.hyperion.rs2.model.region.TileMapBuilder;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.region.Region;
import org.hyperion.rs2.model.region.TileMap;

/**
 * An implementation of a <code>PathFinder</code> which is 'dumb' and only looks
 * at surrounding tiles for a path, suitable for an NPC.
 *
 * @author Graham Edgecombe
 */
public class DumbPathFinder implements PathFinder {

    @Override
    public Path findPath(Location startLocation, Location endLocation) {
        if (startLocation.getZ() != endLocation.getZ()) {
            return null;
        }
        int curX = startLocation.getX();
        int curY = startLocation.getY();
        if (curX == endLocation.getX() && curY == endLocation.getY()) {
            return null;
        }
        Path p = new Path();
        Region reg = World.getWorld().getRegionManager().getRegionByLocation(startLocation);
        while (curX != endLocation.getX() || curY != endLocation.getY()) {
            int beforeX = curX;
            int beforeY = curY;

            if (curX > endLocation.getX()) {
                if (reg.getTile(Location.create(curX, curY, startLocation.getZ())).isWesternTraversalPermitted()
                        && reg.getTile(Location.create(curX - 1, curY, startLocation.getZ())).isEasternTraversalPermitted()) {
                    curX -= 1;
                }
            } else if (curX < endLocation.getX()) {
                if (reg.getTile(Location.create(curX, curY, startLocation.getZ())).isEasternTraversalPermitted()
                        && reg.getTile(Location.create(curX + 1, curY, startLocation.getZ())).isWesternTraversalPermitted()) {
                    curX += 1;
                }
            }

            if (curY > endLocation.getY()) {
                if (reg.getTile(Location.create(curX, curY, startLocation.getZ())).isSouthernTraversalPermitted()
                        && reg.getTile(Location.create(curX, curY - 1, startLocation.getZ())).isNorthernTraversalPermitted()) {
                    curY -= 1;
                }
            } else if (curY < endLocation.getY()) {
                if (reg.getTile(Location.create(curX, curY, startLocation.getZ())).isNorthernTraversalPermitted()
                        && reg.getTile(Location.create(curX, curY + 1, startLocation.getZ())).isSouthernTraversalPermitted()) {
                    curY += 1;
                }
            }

            if (curX != beforeX && curY != beforeY) {//diagonal

                if (curX > beforeX && curY > beforeY) {//north east
                    if (!reg.getTile(Location.create(beforeX, beforeY, startLocation.getZ())).isNorthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY, startLocation.getZ())).isEasternTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX + 1, beforeY, startLocation.getZ())).isWesternTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX + 1, beforeY, startLocation.getZ())).isNorthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY + 1, startLocation.getZ())).isSouthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY + 1, startLocation.getZ())).isEasternTraversalPermitted()) {
                        curX = beforeX;
                        curY = beforeY;
                    }
                }

                if (curX > beforeX && curY < beforeY) {//south east
                    if (!reg.getTile(Location.create(beforeX, beforeY, startLocation.getZ())).isSouthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY, startLocation.getZ())).isEasternTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX + 1, beforeY, startLocation.getZ())).isWesternTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX + 1, beforeY, startLocation.getZ())).isSouthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY - 1, startLocation.getZ())).isNorthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY - 1, startLocation.getZ())).isEasternTraversalPermitted()) {
                        curX = beforeX;
                        curY = beforeY;
                    }
                }

                if (curX < beforeX && curY > beforeY) {//north west
                    if (!reg.getTile(Location.create(beforeX, beforeY, startLocation.getZ())).isNorthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY, startLocation.getZ())).isWesternTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX - 1, beforeY, startLocation.getZ())).isEasternTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX - 1, beforeY, startLocation.getZ())).isNorthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY + 1, startLocation.getZ())).isSouthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY + 1, startLocation.getZ())).isWesternTraversalPermitted()) {
                        curX = beforeX;
                        curY = beforeY;
                    }
                }

                if (curX < beforeX && curY < beforeY) {//south west
                    if (!reg.getTile(Location.create(beforeX, beforeY, startLocation.getZ())).isSouthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY, startLocation.getZ())).isWesternTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX - 1, beforeY, startLocation.getZ())).isEasternTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX - 1, beforeY, startLocation.getZ())).isSouthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY - 1, startLocation.getZ())).isNorthernTraversalPermitted()
                            || !reg.getTile(Location.create(beforeX, beforeY - 1, startLocation.getZ())).isWesternTraversalPermitted()) {
                        curX = beforeX;
                        curY = beforeY;
                    }
                }

                if (curX == beforeX && curY == beforeY) {

                    if (curX > endLocation.getX()) {
                        if (reg.getTile(Location.create(curX, curY, startLocation.getZ())).isWesternTraversalPermitted()
                                && reg.getTile(Location.create(curX - 1, curY, startLocation.getZ())).isEasternTraversalPermitted()) {
                            curX -= 1;
                        }
                    } else if (curX < endLocation.getX()) {
                        if (reg.getTile(Location.create(curX, curY, startLocation.getZ())).isEasternTraversalPermitted()
                                && reg.getTile(Location.create(curX + 1, curY, startLocation.getZ())).isWesternTraversalPermitted()) {
                            curX += 1;
                        }
                    }

                    if (curX == beforeX) {
                        if (curY > endLocation.getY()) {
                            if (reg.getTile(Location.create(curX, curY, startLocation.getZ())).isSouthernTraversalPermitted()
                                    && reg.getTile(Location.create(curX, curY - 1, startLocation.getZ())).isNorthernTraversalPermitted()) {
                                curY -= 1;
                            }
                        } else if (curY < endLocation.getY()) {
                            if (reg.getTile(Location.create(curX, curY, startLocation.getZ())).isNorthernTraversalPermitted()
                                    && reg.getTile(Location.create(curX, curY + 1, startLocation.getZ())).isSouthernTraversalPermitted()) {
                                curY += 1;
                            }
                        }
                    }
                }
            }

            p.addPoint(new Point(curX, curY));

            if (curX == beforeX && curY == beforeY) {
                break;
            }
            if (curX == endLocation.getX() && curY == endLocation.getY()) {
                break;
            }
        }
        return p;
    }

    @Override
    public Path findPath(Location location, int radius, TileMap map, int srcX, int srcY, int dstX, int dstY) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
