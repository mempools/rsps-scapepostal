package org.hyperion.rs2.pf;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.hyperion.rs2.model.Direction;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.region.Region;

/**
 * @author Shiver
 */
public class AStarPathFinderNew {

    private static final class Point implements Comparable<Point> {

        private final Location end, position;
        private Point parent;
        private int cost;

        public Point(Location end, Point parent, Location position, int cost) {
            this.end = end;
            this.parent = parent;
            this.position = position;
            this.cost = cost;
        }

        /**
         * Gets the heuristic score for this point.
         *
         * @return the heuristic score
         */
        private int getHeuristicScore() {
            return Math.abs(position.getX() - end.getX())
                    + Math.abs(position.getY() - end.getY());
        }

        /**
         * Gets the cost of the path up to this point.
         *
         * @return the path cost
         */
        public int getPathCost() {
            Point current = this;
            int totalCost = 0;
            while (current.parent != null) {
                totalCost += current.cost;
                current = current.parent;
            }
            return totalCost;
        }

        /**
         * Gets the projected path cost if the point had a given parent and
         * cost.
         *
         * @param projectedParent the parent
         * @param costToParent the cost to the parent
         * @return the projected path cost
         */
        public int getProjectedPathCost(Point projectedParent, int costToParent) {
            Point current = projectedParent;
            int projectedCost = costToParent;
            while (current.parent != null) {
                projectedCost += current.cost;
                current = current.parent;
            }
            return projectedCost;
        }

        /**
         * Gets the total cost.
         *
         * @return the total cost
         */
        public int getTotalCost() {
            return getPathCost() + getHeuristicScore();
        }

        @Override
        public int compareTo(Point other) {
            return getTotalCost() - other.getTotalCost();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Point point = (Point) o;
            return position.equals(point.position);
        }

        @Override
        public int hashCode() {
            return position.hashCode();
        }

        public void setParent(Point parent) {
            this.parent = parent;
        }

        public void setCost(int cost) {
            this.cost = cost;
        }
    }

    public AStarPathFinderNew() {
    }

    public void handle(Player player, Location location) {
//        for(Location loc : getPath(player.getLocation(), location)){
//            player.getWalkingQueue().addStep(loc.getX(), loc.getY());
//        }
//        player.getWalkingQueue().finish();
        for (Location loc : getPath(player.getLocation(), location)) {
            System.out.println(loc.getX());
        }
    }

    public List<Location> getPath(Location start, Location end) {
        List<Point> openTiles = new LinkedList<>();
        List<Location> usedTiles = new LinkedList<>();
        Point current = new Point(end, null, start, 0);
        openTiles.add(current);
        while (!current.position.equals(end)) {
            if (openTiles.isEmpty()) {
                // no more walkable tiles
                break;
            }
            openTiles.remove(current);
            usedTiles.add(current.position);

            List<Location> adjacentTiles = getAdjacentWalkableTiles(current.position);
            if (usedTiles.containsAll(adjacentTiles)) {
                // we have tried every walkable tile
                break;
            }
            for (Location position : adjacentTiles) {
                if (usedTiles.contains(position)) {
                    continue;
                }
                int deltaX = position.getX() - current.position.getX(),
                        deltaY = position.getY() - current.position.getY();
                int cost = (int) Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
                Point point = new Point(end, current, position, cost);
                if (!openTiles.contains(point)) {
                    openTiles.add(point);
                } else {
                    Point other = openTiles.get(openTiles.indexOf(point));
                    if (other.getProjectedPathCost(point, cost) < other.getPathCost()) {
                        other.setParent(point);
                        other.setCost(cost);
                        Collections.sort(openTiles);
                    }
                }
            }
            Collections.sort(openTiles);
            current = openTiles.get(0);
        }
        List<Location> path = new LinkedList<>();
        while (current.parent != null) {
            path.add(current.position);
            current = current.parent;
        }
        return Lists.reverse(path);
    }

    /**
     * Gets the walkable tiles that are adjacent to a position.
     *
     * @param pos the position
     * @return the tiles
     */
    private List<Location> getAdjacentWalkableTiles(Location pos) {
        List<Location> tiles = new ArrayList<>();
        for (int deltaX = -1; deltaX <= 1; deltaX++) {
            for (int deltaY = -1; deltaY <= 1; deltaY++) {
                Location deltaLocation = Location.create(pos.getX() + deltaX, pos.getY() + deltaY);
                if (canStep(pos, deltaLocation)) {
                    tiles.add(deltaLocation);
                }
            }
        }
        return tiles;
    }

    public boolean canStep(Location position, Location next) {
        int deltaX = next.getX() - position.getX();
        int deltaY = next.getY() - position.getY();
        Direction oppositeDirection = Direction.fromDeltas(-deltaX, -deltaY);
        Region region = World.getWorld().getRegionManager().getRegionByLocation(next);
        return region.getMapTile(next).getFlags() == 1 && region.getMapTile(next).getFlags() != oppositeDirection.toInteger();
    }
}
