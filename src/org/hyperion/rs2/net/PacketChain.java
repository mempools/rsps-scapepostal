package org.hyperion.rs2.net;

import org.hyperion.rs2.model.player.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Sean
 */
public class PacketChain implements PacketHandler<EventContext> {

    /**
     * A list of packet handlers to chain.
     */
    private final List<PacketHandler<EventContext>> handlerChain = new ArrayList<>();

    /**
     * Creates a new packet chain
     *
     * @param handlers The game packet context.
     */
    @SafeVarargs
    public PacketChain(PacketHandler<EventContext>... handlers) {
        Collections.addAll(handlerChain, handlers);
    }

    @Override
    public void handle(Player player, EventContext context) {
        for (PacketHandler<EventContext> handler : handlerChain) {
            handler.handle(player, context);
        }
    }
}
