package org.hyperion.rs2.net;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.packet.*;
import org.hyperion.rs2.packet.decoders.*;

/**
 * Manages <code>PacketHandler</code>s.
 *
 * @author Graham Edgecombe
 */
public class PacketManager {

    /**
     * The instance.
     */
    private static final PacketManager INSTANCE = new PacketManager();

    /**
     * Gets the packet manager instance.
     *
     * @return The packet manager instance.
     */
    public static PacketManager getPacketManager() {
        return INSTANCE;
    }

    /**
     * The packet handler array.
     */
    private final PacketAssembler[] packetHandlers = new PacketAssembler[256];

    /**
     * Gets a specific packet
     *
     * @param opcode The packet opcode
     * @return The packet
     */
    public PacketAssembler get(int opcode) {
        return packetHandlers[opcode];
    }

    /**
     * Creates the packet manager.
     */
    public PacketManager() {
        /*
         * Set default handlers.
         */
        for (int i = 0; i < packetHandlers.length; i++) {
            if (packetHandlers[i] == null) {
                packetHandlers[i] = new PacketAssembler(new DefaultDecoder(), new DefaultPacketHandler());
            }
        }
    }

    /**
     * Binds an opcode to a handler.
     *
     * @param opcode The opcode.
     * @param decoder The decoder
     * @param handler The handler.
     */
    public void bind(PacketHandler<?> handler, PacketDecoder<?> decoder, int... opcode) {
        for (int i : opcode) {
            packetHandlers[i] = new PacketAssembler(decoder, handler);
        }
    }

    /**
     * Handles a packet.
     *
     * @param player The player.
     * @param packet The packet.
     */
    public void handle(Player player, Packet packet) {
        PacketAssembler assembler = get(packet.getOpcode());
        if (assembler != null) {
            PacketDecoder<?> decoder = assembler.getDecoder();
            if (decoder != null) {
                EventContext context = decoder.decode(packet);
                if (context != null) {
                    @SuppressWarnings("unchecked")
                    PacketHandler<EventContext> handler = (PacketHandler<EventContext>) assembler.getHandler();
                    if (handler != null) {
                        handler.handle(player, context);
                        World.getWorld().getScriptContext().sendPacketEvent(player, context);
                    }
                }
            } else {
                PacketHandler<?> handler = assembler.getHandler();
                if (handler != null) {
                    handler.handle(player, null);
                }
            }
        }
    }
}
