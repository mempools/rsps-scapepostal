package org.hyperion.rs2.net;

import org.hyperion.rs2.model.player.Player;

/**
 * An interface which describes a class that handles packets.
 *
 * @author Graham Edgecombe
 * @param <T> The packet handler
 */
public interface PacketHandler<T extends EventContext> {

    /**
     * Handles a single packet.
     *
     * @param player The player.
     * @param context The packet context.
     */
    public void handle(Player player, T context);
}
