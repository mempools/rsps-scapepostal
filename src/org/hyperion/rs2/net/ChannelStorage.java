package org.hyperion.rs2.net;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelLocal;

/**
 * A class to store attachments to channels
 *
 * @param <T> The class type
 * @author Nikki
 */
public class ChannelStorage<T> {

    /**
     * The channel local instance.
     */
    private final ChannelLocal<T> channelLocal = new ChannelLocal<>();

    /**
     * Remove a channel
     *
     * @param channel The channel
     */
    public void remove(Channel channel) {
        channelLocal.remove(channel);
    }

    /**
     * Sets a channel attachment
     *
     * @param channel The channel
     * @param t The attachment
     */
    public void set(Channel channel, T t) {
        channelLocal.set(channel, t);
    }

    /**
     * Get a channel attachment
     *
     * @param channel The channel
     * @return The attachment
     */
    public T get(Channel channel) {
        return channelLocal.get(channel);
    }

    /**
     * Check if the local channel contains the attachment
     *
     * @param channel The channel
     * @return channel
     */
    public boolean contains(Channel channel) {
        return channelLocal.get(channel) != null;
    }
}
