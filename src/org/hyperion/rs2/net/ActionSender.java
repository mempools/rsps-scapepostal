package org.hyperion.rs2.net;

import java.util.List;
import java.util.Random;
import org.hyperion.Constants;
import org.hyperion.rs2.WorldConstants;
import org.hyperion.rs2.content.dialogue.DialogueType;
import org.hyperion.rs2.content.menu.Menu;
import org.hyperion.rs2.content.menu.MenuManager;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.GameObject;
import org.hyperion.rs2.model.GroundItem;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.Palette;
import org.hyperion.rs2.model.Palette.PaletteTile;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.container.Inventory;
import org.hyperion.rs2.model.container.impl.EquipmentContainerListener;
import org.hyperion.rs2.model.container.impl.InterfaceContainerListener;
import org.hyperion.rs2.model.container.impl.WeaponContainerListener;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.request.RequestManager;
import org.hyperion.rs2.model.request.RequestManager.RequestState;
import org.hyperion.rs2.net.Packet.Type;

/**
 * A utility class for sending packets.
 *
 * @author Graham Edgecombe
 */
public class ActionSender {

    /**
     * The player.
     */
    private final Player player;

    /**
     * Creates an action sender for the specified player.
     *
     * @param player The player to create the action sender for.
     */
    public ActionSender(Player player) {
        this.player = player;
    }

    /**
     * Sends an inventory interface.
     *
     * @param interfaceId The interface id.
     * @param inventoryInterfaceId The inventory interface id.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendInventoryInterface(int interfaceId, int inventoryInterfaceId) {
        player.getInterfaceState().interfaceOpened(interfaceId);
        player.write(new PacketBuilder(128).putShortA(interfaceId).putLEShortA(inventoryInterfaceId).toPacket());
        return this;
    }

    /**
     * Sends all the login packets.
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendLogin() {
        player.setActive(true);
        sendWelcomeScreen();
        resetCamera();
        resetButtons();

        sendDetails();
        sendFriendServer(2);
        sendSkills();
        sendEnergy();
        player.getSkills().sendBonuses();

        sendMapRegion();
        sendSidebarInterfaces();

        InterfaceContainerListener inventoryListener = new InterfaceContainerListener(player, Inventory.INTERFACE);
        player.getInventory().addListener(inventoryListener);

        InterfaceContainerListener equipmentListener = new InterfaceContainerListener(player, Equipment.INTERFACE);
        player.getEquipment().addListener(equipmentListener);
        player.getEquipment().addListener(new EquipmentContainerListener(player));
        player.getEquipment().addListener(new WeaponContainerListener(player));

        sendInterfaceConfig(12323, true);
        sendConfig(300, 857);
        sendConfig(301, 1);
        sendInteractionOption("Attack", 1, true);
        sendInteractionOption("null", 2, false);
        sendInteractionOption("Follow", 3, false);
        sendInteractionOption("Trade with", 4, false);

        /**
         * Opens the default menu.
         */
        player.openMenu(MenuManager.getMenu(MenuManager.DEFAULT_MENU));
        cleanScrollInterface();
        return this;
    }

    /**
     * Sends the initial login packet (e.g. members, player id).
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendDetails() {
//        player.write(new PacketBuilder(249).putByteA((byte) (player.isMembers() ? 1 : 0)).putLEShortA(player.getIndex()).toPacket());
//        player.write(new PacketBuilder(107).toPacket());
        return this;
    }

    /**
     * Sends the welcome screen
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendWelcomeScreen() {
        PacketBuilder packet = new PacketBuilder(76);
        packet.putLEShort(0); // Days ago requested recovery question changes.
        packet.putLEShortA(0);//???
        packet.putShort(0);//???
        packet.putShort(0);//???
        packet.putLEShort(0);//???
        packet.putShortA(player.getProfile().getUnreadMessages()); // Unread messages.
        packet.putShortA(0);//???
        packet.putShort(player.getMembership().getDaysOfMembership()); // Days of membership credit remaining.
        packet.putLEInt((int) player.getProfile().getLastLoggedIn()); // Last IP.
        packet.putLEShort(0);//???
        packet.putByteS(player.isMembers() ? (byte) 1 : 0); // Membership
        sendFullScreenInterface(15244, 5993);
        player.write(packet.toPacket());
        return this;
    }

    /**
     * Sends a full screen interface
     *
     * @param interfaceID The interface Id
     * @param bgInterfaceID The background interface Id
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendFullScreenInterface(int interfaceID, int bgInterfaceID) {
        player.write(new PacketBuilder(253).putLEShort(bgInterfaceID).putShortA(interfaceID).toPacket());
        return this;
    }

    /**
     * Sends the player's skills.
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendSkills() {
        for (int i = 0; i < Skills.SKILL_COUNT; i++) {
            sendSkill(i);
        }
        return this;
    }

    /**
     * Sends a specific skill.
     *
     * @param skill The skill to send.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendSkill(int skill) {
        PacketBuilder bldr = new PacketBuilder(49);
        bldr.putByteC((byte) skill);
        bldr.put((byte) player.getSkills().getLevel(skill));
        bldr.putInt((int) player.getSkills().getExperience(skill));
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends all the side bar interfaces.
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendSidebarInterfaces() {
        final int[] icons = Constants.SIDEBAR_INTERFACES[0];
        final int[] interfaces = Constants.SIDEBAR_INTERFACES[1];
        for (int i = 0; i < icons.length; i++) {
            sendSidebarInterface(icons[i], interfaces[i]);
        }
        return this;
    }

    /**
     * Sends a specific sidebar interface.
     *
     * @param icon The sidebar icon.
     * @param interfaceId The interface id.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendSidebarInterface(int icon, int interfaceId) {
//        player.write(new PacketBuilder(71).putShort(interfaceId).putByteA((byte) icon).toPacket());
        player.write(new PacketBuilder(10).putByteS((byte) icon).putShortA(
                interfaceId).toPacket());
        return this;
    }

    /**
     * Sends a message.
     *
     * @param messages The message to send.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendMessage(String... messages) {
        for (String message : messages) {
            //player.write(new PacketBuilder(253, Type.VARIABLE).putRS2String(message).toPacket());
            player.write(new PacketBuilder(63, Type.VARIABLE).putRS2String(message).toPacket());
        }
        return this;
    }

    /**
     * Sends a global message.
     *
     * @param message The message(s) to send.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendGlobalMessage(String... message) {
        for (Player players : World.getWorld().getPlayers()) {
            players.getActionSender().sendMessage(message);
        }
        return this;
    }

    /**
     * Sends the quest interface
     *
     * @param title The title
     * @param lines The lines
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendScrollInterface(String title, String... lines) {
        sendString(8144, title);
        /*for (int i = 8145; i < 8348; i++) {
         sendString(i, "");
         }*/
        for (int i = 0; i < lines.length; i++) {
            sendString(8145 + i, "");
            sendString(8145 + i, lines[i]);
        }
        sendInterface(8134, false);
        return this;
    }

    /**
     * Cleans the scroll interface (8134)
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender cleanScrollInterface() {
        int[] stringLines = {
            8145, 8147, 8148, 8149, 8150, 8151, 8152, 8153, 8154, 8155, 8156, 8157, 8158, 8159, 8160, 8161, 8162,
            8163, 8164, 8165, 8166, 8167, 8168, 8169, 8170, 8171, 8172, 8173, 8174, 8175, 8176, 8177, 8178, 8179,
            8180, 8181, 8182, 8183, 8184, 8185, 8186, 8187, 8188, 8189, 8190, 8191, 8192, 8193, 8194, 8195, 12174,
            12175, 12176, 12177, 12178, 12179, 12180, 12181, 12182, 12183, 12184, 12185, 12186, 12187, 12188, 12189,
            12190, 12191, 12192, 12193, 12194, 12195, 12196, 12197, 12198, 12199, 12200, 12201, 12202, 12203, 12204,
            12205, 12206, 12207, 12208, 12209, 12210, 12211, 12212, 12213, 12214, 12215, 12216, 12217, 12218, 12219,
            12220, 12221, 12222, 12223};
        player.getActionSender().sendString(8144, "");
        for (int stringLine : stringLines) {
            player.getActionSender().sendString(stringLine, "");
        }
        return this;
    }

    /**
     * Cleans the quest tab
     *
     * @param section The section to clean
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendCleanQuestTab(int section) {
        player.getActionSender().sendString(Menu.QUEST_TAB_TITLE, WorldConstants.SERVER_NAME);
        switch (section) {
            case 0:
                for (int freeQuest : WorldConstants.FREE_QUESTS) {
                    sendString(freeQuest, "");
                }
                break;
            case 1:
                for (int memberQuest : WorldConstants.MEMBER_QUESTS) {
                    sendString(memberQuest, "");
                }
                break;
            default:
                for (int freeQuest : WorldConstants.FREE_QUESTS) {
                    sendString(freeQuest, "");
                }
                for (int memberQuest : WorldConstants.MEMBER_QUESTS) {
                    sendString(memberQuest, "");
                }
                break;
        }
        return this;
    }

    /**
     * Sends a menu string. This really means sending a string to the quest tab,
     * except a lot easier without having to find the quest IDs for the lines.
     * example:
     * <br>sendMenuString(FREE_QUESTS, 1, "line one", "line two");
     * <br>sendMenuString(FREE_QUESTS, 2, "line two");
     * <br>and so forth...
     *
     * @param type The menu type - free quests/member quests
     * @param line The line number
     * @param string The string
     */
    public void sendMenuString(int[] type, int line, String... string) {
        //sendString(type[line], string);
        for (String content : string) {
            sendString(type[line++], content);
        }
    }

    /**
     * Sends the map region load command.
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendMapRegion() {
        /*Palette palette = new Palette();
         palette.setTile(6, 6, 0, new PaletteTile(3210, 3210));
         palette.setTile(5, 6, 0, new PaletteTile(3200, 3200));
         palette.setTile(4, 6, 0, new PaletteTile(2497, 3100));
         palette.setTile(3, 6, 0, new PaletteTile(2662, 3305));
         palette.setTile(2, 6, 0, new PaletteTile(2529, 3307));
         sendConstructMapRegion(palette);*/
        player.setLastKnownRegion(player.getLocation());
        player.write(new PacketBuilder(222).putShort(
                player.getLocation().getRegionY() + 6).putLEShortA(
                        player.getLocation().getRegionX() + 6).toPacket());
        return this;
    }

    /**
     * Sends the packet to construct a map region.
     *
     * @param palette The palette of map regions.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendConstructMapRegion(Palette palette) {
        player.setLastKnownRegion(player.getLocation());
        PacketBuilder bldr = new PacketBuilder(53, Type.VARIABLE_SHORT);
        bldr.putShortA(player.getLocation().getRegionX() + 6);
        bldr.startBitAccess();
        for (int z = 0; z < 4; z++) {
            for (int x = 0; x < 13; x++) {
                for (int y = 0; y < 13; y++) {
                    PaletteTile tile = palette.getTile(x, y, z);
                    bldr.putBits(1, tile != null ? 1 : 0);
                    if (tile != null) {
                        bldr.putBits(26, tile.getX() << 14 | tile.getY() << 3
                                | tile.getZ() << 24 | tile.getRotation() << 1);
                    }
                }
            }
        }
        bldr.finishBitAccess();
        bldr.putShort(player.getLocation().getRegionY() + 6);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends the logout packet.
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendLogout() {
        player.getFriendsList().globalUpdate();
        player.write(new PacketBuilder(5).toPacket());
        return this;
    }

    /**
     * Sends a packet to update a group of items.
     *
     * @param interfaceId The interface id.
     * @param items The items.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendUpdateItems(int interfaceId, Item[] items) {
        PacketBuilder bldr = new PacketBuilder(206, Type.VARIABLE_SHORT);
        bldr.putShort(interfaceId);
        bldr.putShort(items.length);
        for (Item item : items) {
            if (item != null) {
                bldr.putLEShortA(item.getId() + 1);
                int count = item.getCount();
                if (count > 254) {
                    bldr.putByteC((byte) 255);
                    bldr.putLEInt(count);
                } else {
                    bldr.putByteC((byte) count);
                }
            } else {
                bldr.putLEShortA(0);
                bldr.putByteC((byte) 0);
            }
        }
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends a packet to update a single item.
     *
     * @param interfaceId The interface id.
     * @param slot The slot.
     * @param item The item.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendUpdateItem(int interfaceId, int slot, Item item) {
        PacketBuilder bldr = new PacketBuilder(134, Type.VARIABLE_SHORT);
        bldr.putShort(interfaceId).putSmart(slot);
        if (item != null) {
            bldr.putShort(item.getId() + 1);
            int count = item.getCount();
            if (count > 254) {
                bldr.put((byte) 255);
                bldr.putInt(count);
            } else {
                bldr.put((byte) count);
            }
        } else {
            bldr.putShort(0);
            bldr.put((byte) 0);
        }
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends a packet to update multiple (but not all) items.
     *
     * @param interfaceId The interface id.
     * @param slots The slots.
     * @param items The item array.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendUpdateItems(int interfaceId, int[] slots, Item[] items) {
        PacketBuilder bldr = new PacketBuilder(134, Type.VARIABLE_SHORT).putShort(interfaceId);
        for (int slot : slots) {
            Item item = items[slot];
            bldr.putSmart(slot);
            if (item != null) {
                bldr.putShort(item.getId() + 1);
                int count = item.getCount();
                if (count > 254) {
                    bldr.put((byte) 255);
                    bldr.putInt(count);
                } else {
                    bldr.put((byte) count);
                }
            } else {
                bldr.putShort(0);
                bldr.put((byte) 0);
            }
        }
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends the enter amount interface.
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendEnterAmountInterface() {
        player.write(new PacketBuilder(58).toPacket());
        return this;
    }

    /**
     * Sends the player an option.
     *
     * @param option The option
     * @param slot The slot to place the option in the menu.
     * @param top Flag which indicates the item should be placed at the top.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendInteractionOption(String option, int slot, boolean top) {
        PacketBuilder bldr = new PacketBuilder(157, Type.VARIABLE);
        bldr.putByteC((byte) slot);
        bldr.putRS2String(option);
        bldr.put(top ? (byte) 0 : (byte) 1);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends a string.
     *
     * @param id The interface id.
     * @param string The string.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendString(int id, String string) {
        if (!player.getInterfaceState().checkForInterfaceDuplication(id, string)) {
            int savedBytes = (string.length() + 4);
            return this;
        }
        PacketBuilder bldr = new PacketBuilder(232, Type.VARIABLE_SHORT);
        bldr.putLEShortA(id);
        bldr.putRS2String(string);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends a model in an interface.
     *
     * @param id The interface id.
     * @param zoom The zoom.
     * @param model The model id.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendInterfaceModel(int id, int zoom, int model) {
        PacketBuilder bldr = new PacketBuilder(21);
        bldr.putShort(zoom).putLEShort(model).putLEShortA(id);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends an interface
     *
     * @param interfaceId The interface ID
     * @param walkable If the interface is walkable
     * @return The action sender for instance and chaining
     */
    public ActionSender sendInterface(int interfaceId, boolean walkable) {
        player.getInterfaceState().interfaceOpened(interfaceId);
        player.write(new PacketBuilder(159).putLEShortA(interfaceId).toPacket());
        return this;
    }

    /**
     * Sends map coordinates
     *
     * @param x The X coordinate
     * @param y The Y coordinate
     * @return The action sender for instance and chaining
     */
    public ActionSender sendCoordinates(int x, int y) {
        PacketBuilder bldr = new PacketBuilder(75);
        bldr.putByteC(x - 8 * player.getLastKnownRegion().getRegionX());
        bldr.putByteA((byte) (y - 8 * player.getLastKnownRegion().getRegionY()));
        player.getSession().write(bldr.toPacket());
        return this;
    }

    /**
     * Creates a new Ground Item for this Player.
     *
     * @param g The Ground Item to add.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendGroundItemCreation(GroundItem g) {
        sendCoordinates(g.getLocation().getX(), g.getLocation().getY());
        PacketBuilder bldr = new PacketBuilder(107);
        bldr.putShort(g.getItem().getId()).putByteC(0).putShortA(g.getItem().getCount());
        player.getSession().write(bldr.toPacket());
        return this;
    }

    /**
     * Removes an old GroundItem.
     *
     * @param groundItem The GroundItem to remove.
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendGroundItemRemoval(GroundItem groundItem) {
        sendCoordinates(groundItem.getLocation().getX(), groundItem.getLocation().getY());
        PacketBuilder bldr = new PacketBuilder(208);
        bldr.putShortA(groundItem.getItem().getId());
        bldr.putByteA((byte) 0);
        player.getSession().write(bldr.toPacket());
        return this;
    }

    /**
     * Creates a client-side object
     *
     * @param object The game object
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendGameObject(GameObject object) {
        sendCoordinates(object.getLocation().getX(), object.getLocation().getY());
        PacketBuilder bldr = new PacketBuilder(152);
        if (object.getDefinition() != null) {
            int id = object.getDefinition().getId();
            bldr.putByteC((object.getType() << 2) + object.getRotation()).putLEShortA(id).putByteS((byte) 0);
        }
        player.getSession().write(bldr.toPacket());
        return this;
    }

    /**
     * Removes a client-side object
     *
     * @param object The game object
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendRemoveGameObject(GameObject object) {
        sendCoordinates(object.getLocation().getX(), object.getLocation().getY());
        PacketBuilder bldr = new PacketBuilder(88);
        bldr.putByteS((byte) 0).putByteS((byte) ((object.getType() << 2) + object.getRotation()));
        player.getSession().write(bldr.toPacket());
        return this;
    }

    /**
     * Sends a interface animation
     *
     * @param interfaceId The interface id
     * @param animation The animation
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendInterfaceAnimation(int interfaceId, int animation) {
        player.write(new PacketBuilder(2).putLEShortA(interfaceId).putShortA(animation).toPacket());
        return this;
    }

    /**
     * Sends a chat box interface
     *
     * @param id The interface id to set
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendChatboxInterface(int id) {
        PacketBuilder bldr = new PacketBuilder(109);
        bldr.putShort(id);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends chat box overlay
     *
     * @param id The overlay id to set
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendChatboxOverlay(int id) {
        PacketBuilder bldr = new PacketBuilder(158);
        bldr.putLEShort(id);
        player.write(bldr.toPacket());
        //update player appearance
        return this;
    }

    /**
     * Sends a player head
     *
     * @param interfaceId The interface id
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendPlayerHead(int interfaceId) {
        player.write(new PacketBuilder(255).putLEShortA(interfaceId).toPacket());
        return this;
    }

    /**
     * Sends an NPC head
     *
     * @param npcId The NPC id
     * @param interfaceId The interface Id
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendNpcHead(int npcId, int interfaceId) {
        PacketBuilder bldr = new PacketBuilder(162);
        bldr.putShortA(npcId);
        bldr.putLEShort(interfaceId);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends a flashing side bar icon
     *
     * @param i The icon id to flash
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendFlashSidebar(int i) {
        /*
         *  0 Attack type
         *   1 Stats
         *   2 Quests 
         *   3 Inventory 
         *   4 Wearing 
         *   5 Prayer 
         *   6 Magic 
         *   7 EMPTY/Left of friends list
         *   8 Friends list 
         *   9 	Ignore list 
         *   10 	Log out 
         *   11 	Settings 
         *   12 	Emotes 
         *   13 	Music 
         */
        player.write(new PacketBuilder(24).putByteA((byte) i).toPacket());
        return this;
    }

    /**
     * Resets all NPC and player animations in region
     *
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender resetAllAnimation() {
        player.write(new PacketBuilder(13).toPacket());
        return this;
    }

    /**
     * 0 - Active: Clickable and viewable 1 - Locked: viewable but not clickable
     * 2 - Blacked-out: Minimap is replaced with black background
     *
     * @param i The minimap state to set.
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendMinimapState(int i) {
        player.write(new PacketBuilder(99).put((byte) i).toPacket());
        return this;
    }

    /**
     * Handles other interfaces on close
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendCloseOtherInterface() {
        if (player.getRequestManager().getAcquaintance() != null) {
            if (player.getRequestManager().getState() != RequestState.FINISHED
                    && player.getRequestManager().getAcquaintance().getRequestManager().getState() != RequestState.FINISHED) {
                player.getRequestManager().cancelRequest();
            } else if (player.getRequestManager().getState() == RequestManager.RequestState.FINISHED) {
                player.getRequestManager().getAcquaintance().getRequestManager().setRequestType(null);
                player.getRequestManager().getAcquaintance().getRequestManager().setAcquaintance(null);
                player.getRequestManager().getAcquaintance().getRequestManager().setState(RequestState.NORMAL);
                player.getRequestManager().setRequestType(null);
                player.getRequestManager().setAcquaintance(null);
                player.getRequestManager().setState(RequestManager.RequestState.NORMAL);
            }

            if (player.getJournal() != null) {
                player.playAnimation(Animation.DEFAULT);
            }
            player.setJournal(null);
            resetAllAnimation();
        }
        return this;
    }

    /**
     * Remove all windows.
     *
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender removeAllWindows() {
        player.write(new PacketBuilder(29).toPacket());
        player.getInterfaceState().interfaceClosed();
        sendCloseOtherInterface();
        return this;
    }

    /**
     * Sends a dialogue
     *
     * @param title The title
     * @param dialogueType The dialogue type
     * @param entityId The entity's ID
     * @param animation The face animation
     * @param text The dialogue message
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendDialogue(String title, DialogueType dialogueType, int entityId, Animation animation, String... text) {
        int interfaceId;
        switch (dialogueType) {
            case NPC:
                if (text.length > 4 || text.length < 1) {
                    return this;
                }
                int[] npcInterfaces = {4882, 4887, 4893, 4900};
                interfaceId = npcInterfaces[text.length - 1];

                sendInterfaceAnimation(interfaceId + 1, animation.getId());
                sendString(interfaceId + 2, title);
                sendNpcHead(entityId, interfaceId + 1);
                for (int i = 0; i < text.length; i++) {
                    sendString(interfaceId + 3 + i, text[i]);
                }
                sendChatboxInterface(interfaceId);
                break;
            case PLAYER:
                if (text.length > 4 || text.length < 1) {
                    return this;
                }
                int[] playerInterfaces = {968, 973, 979, 986};
                interfaceId = playerInterfaces[text.length - 1];

                sendInterfaceAnimation(interfaceId + 1, animation.getId());
                sendString(interfaceId + 2, player.getName());
                sendPlayerHead(interfaceId + 1);
                for (int i = 0; i < text.length; i++) {
                    sendString(interfaceId + 3 + i, text[i]);
                }
                sendChatboxInterface(interfaceId);
                break;
            case OPTION:
                if (text.length > 5 || text.length < 2) {
                    return this;
                }
                int[] dialogueOptions = {2459, 2469, 2480, 2492};
                interfaceId = dialogueOptions[text.length - 2];

                sendString(interfaceId + 1, title);
                for (int i = 0; i < text.length; i++) {
                    sendString(interfaceId + 2 + i, text[i]);
                }
                sendChatboxInterface(interfaceId);
                break;
            case STATEMENT:
                if (text.length > 4 || text.length < 1) {
                    return this;
                }
                int[] dialogueMessage = {356, 359, 363, 368, 374};
                interfaceId = dialogueMessage[text.length - 1];
                for (int i = 0; i < text.length; i++) {
                    sendString(interfaceId + 1 + i, text[i]);
                }
                sendChatboxInterface(interfaceId);
                break;
            case MESSAGE_MODEL:
                if (text.length > 2 || text.length < 1) {
                    return this;
                }
                interfaceId = 6206;
                sendInterfaceModel(6210, 290, entityId);
                for (int i = 0; i < text.length; i++) {
                    sendString(interfaceId + 1 + i, text[i]);
                }
                sendChatboxInterface(interfaceId);
                break;
            case INFO:
                if (text.length > 5 || text.length < 1) {
                    return this;
                }
                interfaceId = 6179;
                sendString(interfaceId + 1, title);
                for (int i = 0; i < text.length; i++) {
                    sendString(interfaceId + 2 + i, text[i]);
                }
                sendChatboxInterface(interfaceId);
                break;
        }
        return this;
    }

    /**
     * Sends a configuration to the client.
     *
     * @param id configuration id.
     * @param val configuration value.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendConfig(int id, int val) {
        if (val > 254) {
            player.write(new PacketBuilder(115).putInt2(val).putLEShort(id).toPacket());
        } else {
            player.write(new PacketBuilder(182).putShortA(id).putByteS((byte) val).toPacket());
        }
        return this;
    }

    /**
     * Sends an interface configuration
     *
     * @param interfaceId The interface
     * @param hidden The hidden state
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendInterfaceConfig(int interfaceId, boolean hidden) {
        player.write(new PacketBuilder(82).put((byte) (hidden ? 1 : 0)).putShort(interfaceId).toPacket());
        return this;
    }

    /**
     * For finding an interface config. Some will replace the interface text
     * with the config id
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender printInterfaceConfig() {
        player.getActionSender().sendMessage("Setting....");
        for (int i = 0; i < 6000; i++) {
            sendConfig(i, i);
        }
        player.getActionSender().sendMessage("Done");
        return this;
    }

    /**
     * Sends the player's special bar configurations.
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender updateSpecial() {
        sendConfig(300, player.getCombatSession().getSpecialAmount() * 10);
        sendConfig(301, player.getCombatSession().isSpecialOn() ? 1 : 0);
        return this;
    }

    /**
     * Creates a hint arrow
     *
     * @param character The entity
     * @param id The id
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendHintArrow(Actor character, int id) {
        PacketBuilder bldr = new PacketBuilder(199);
        bldr.put((byte) (character.isPlayer() ? 10 : 1));
        bldr.putShort(id);
        bldr.putTriByte(0);
        player.write(bldr.toPacket());
        //createArrow(1, 945);
        return this;
    }

    /**
     * Sends a hint to a location.
     *
     * @param location The position to create the hint.
     * @param direction The tile direction
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendHintArrow(Location location, int direction) {
        PacketBuilder bldr = new PacketBuilder(254);
        bldr.put((byte) direction)
                .putShort(location.getX())
                .putShort(location.getY())
                .put((byte) location.getZ());
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Resets the hint arrow
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender resetArrow() {
        sendHintArrow(player, -1);
        return this;
    }

    /**
     * Makes the camera shake for the player
     *
     * @param intensity The intensity of the shake
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendCameraShake(int intensity) {
        player.write(new PacketBuilder(35).put((byte) 0).put((byte) intensity).put((byte) intensity).put((byte) intensity).toPacket());
        return this;
    }

    /**
     * Sends a camera.
     *
     * @param location The location of the camera.
     * @param speed The speed of the camera movement.
     * @param angle The angle of the camera.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendCamera(Location location, int speed, int angle) {
        PacketBuilder bldr = new PacketBuilder(167);
        bldr.put((byte) location.getX()).put((byte) location.getY());
        bldr.putShort(location.getZ());
        bldr.put((byte) speed).put((byte) angle);
        player.getSession().write(bldr.toPacket());
        return this;
    }

    /**
     * Sends a spinning camera.
     *
     * @param location The camera location.
     * @param turnSpeed The camera turning speed.
     * @param movementSpeed The camera movement speed.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendSpinningCamera(Location location, int turnSpeed, int movementSpeed) {
        player.write(new PacketBuilder(166).
                put((byte) (location.getX() / 64)).
                put((byte) (location.getY() / 64)).
                putShort(location.getZ()).put((byte) turnSpeed).put((byte) movementSpeed).toPacket());
        return this;
    }

    /**
     * Resets all cameras.
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender resetCamera() {
        player.write(new PacketBuilder(148).toPacket());
        return this;
    }

    /**
     * Resets the buttons
     *
     * @return The action sender instance, for chaining.
     */
    public ActionSender resetButtons() {
        player.write(new PacketBuilder(113).toPacket());
        return this;
    }

    /**
     * Resets the items in an interface
     *
     * @param interfaceId The interface to reset the items
     * @return The action sender instance, for chaining.
     */
    public ActionSender resetItems(int interfaceId) {
        player.write(new PacketBuilder(219).putLEInt(interfaceId).toPacket());
        return this;
    }

    /**
     * Send the skill menu with items
     *
     * @param items The items shown on the skill menu
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendSkillMenuItems(int items[]) {
        final PacketBuilder bldr = new PacketBuilder(134, Type.VARIABLE_SHORT);
        bldr.putShort(8847/*interface id*/);
        bldr.putShort(items.length);
        for (int i = 0; i < items.length; i++) {
            bldr.put((byte) 1);
            bldr.putLEShortA(items[i] + 1);
        }
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends the system update message
     *
     * @param seconds The seconds until system update
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendSystemUpdate(int seconds) {
        player.write(new PacketBuilder(190).putLEShort(seconds).toPacket());
        return this;
    }

    /**
     * Sends the characters weight
     *
     * @param weight The amount of weight
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendWeight(int weight) {
        player.write(new PacketBuilder(174).putShort(weight).toPacket());
        return this;
    }

    /**
     * Sends the characters energy level
     *
     * @param energy The current energy level
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendEnergy(int energy) {
        player.write(new PacketBuilder(125).put((byte) (energy & 0xff)).toPacket());
        return this;
    }

    /**
     * Sends the players energy
     *
     * @return the player Energy packet.
     */
    public ActionSender sendEnergy() {
        player.getSession().write(new PacketBuilder(125).put((byte) player.getWalkingQueue().getRunningEnergy()).toPacket());
        return this;
    }

    /**
     * Friend server friends list load status. Loading = 0 Connecting = 1 OK = 2
     *
     * @param status Value to set.
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendFriendServer(int status) {
        PacketBuilder bldr = new PacketBuilder(251);
        bldr.put((byte) status);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Send the status of a friend in a list
     *
     * @param name The player's name in a long
     * @param world The player's current world
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendFriendStatus(long name, int world) {
        player.write(new PacketBuilder(50).putLong(name).put((byte) world).toPacket());
        return this;
    }

    /**
     * Send the status of a friend in a list
     *
     * @param from The sender
     * @param rights The sender's rights
     * @param message The message
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendPrivateMessage(long from, int rights, byte[] message) {
        PacketBuilder bldr = new PacketBuilder(196, Type.VARIABLE);
        bldr.putLong(from).putInt((byte) new Random().nextInt()).put((byte) rights).put(message);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Send the ignore list of a player
     *
     * @param ignores The list of ignores
     * @return The action sender instance, for chaining.
     */
    public ActionSender sendIgnores(List<Long> ignores) {
        PacketBuilder pb = new PacketBuilder(214, Type.VARIABLE_SHORT);
        for (long name : ignores) {
            pb.putLong((byte) name);
        }
        player.write(pb.toPacket());
        return this;
    }

    /**
     * Creates a new projectile.
     *
     * @param src The source location.
     * @param dest The target location.
     * @param target The index of the target to follow. <ul> <li>Mob/NPC: The
     * server-index.</li> <li>Player: The -server-index.</li> <li>No Target:
     * <i>0</i></li> </ul>
     * @param gfx The id of the graphic to send.
     * @param initHeight The initial height level of the graphic.
     * @param endHeight The ending height level of the graphic.
     * @param delay The delay before the graphic is created (So an animation can
     * run, for example).
     * @param slowness The speed of the projectile (before it reaches its target
     * tile).
     * @param curvature The angle of the projectile's arc.
     * @param offset The point where the projectile spawns. Should be between
     * <i>0</i> and <i>50</i>.
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendProjectile(Location src, Location dest, Entity target, int gfx, int initHeight, int endHeight, int delay, int slowness, int curvature, int offset) {
        if (player.getLocation().getZ() != src.getZ()) {
            return this;
        }
        sendCoordinates(src.getX() - 3, src.getY() - 2);

        int distance = (int) src.getDistanceFromLocation(dest);
        int duration = delay + slowness + (distance * 5);

        PacketBuilder bldr = new PacketBuilder(181);
        bldr.put((byte) dest.getZ());
        bldr.put((byte) ((dest.getX() - src.getX()) * -1));
        bldr.put((byte) ((dest.getY() - src.getY()) * -1));
        bldr.putShort(target == null ? 0 : target.isPlayer() ? -(target.getClientIndex() + 1) : target.getClientIndex() + 1);
        bldr.putShort(gfx);
        bldr.put((byte) initHeight);
        bldr.put((byte) endHeight);
        bldr.putShort(delay);
        bldr.putShort(duration);
        bldr.put((byte) curvature);
        bldr.put((byte) (64));
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Animates an object
     *
     * @param location The object location
     * @param animationID The animation id
     * @param tileObjectType The object type
     * @param orientation The orientation
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendAnimateObject(Location location, int animationID, int tileObjectType, int orientation) {
        for (Player plr : World.getWorld().getPlayers()) {
            int yy = location.getY() - (plr.getLastKnownRegion().getRegionY() * 8);
            int xx = location.getX() - (plr.getLastKnownRegion().getRegionX() * 8);
            plr.getSession().write(new PacketBuilder(85).putByteC(yy).putByteC(xx).toPacket());
            plr.getSession().write(new PacketBuilder(160).putByteS((byte) 0).putByteS((byte) ((tileObjectType << 2) + (orientation & 3))).putShortA(animationID).toPacket());
        }
        return this;
    }

    /**
     * Sends a sound
     *
     * @param id The sound id
     * @param type The type
     * @param delay The delay
     * @param volume The volume
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendSound(int id, int type, int delay, int volume) {
        PacketBuilder bldr = new PacketBuilder(174);
        bldr.putShort(id);
        bldr.put((byte) type);
        bldr.putShort(delay);
        bldr.putShort(volume);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends a song
     *
     * @param id The song id
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendSong(int id) {
        PacketBuilder bldr = new PacketBuilder(74);
        bldr.putLEShort(id);
        player.write(bldr.toPacket());
        return this;
    }

    /**
     * Sends a quick song (? I think it sends a song without the fade? idk)
     *
     * @param id The song id
     * @param songDelay The song delay
     * @return The ActionSender instance, for chaining.
     */
    public ActionSender sendQuickSong(int id, int songDelay) {
        PacketBuilder bldr = new PacketBuilder(121);
        bldr.putLEShort(id);
        bldr.putLEShort(songDelay);
        player.write(bldr.toPacket());
        return this;
    }
}
