package org.hyperion.rs2.net;

/**
 * @param <T>
 * @author Sean
 */
public interface PacketDecoder<T extends EventContext> {

    /**
     * Decodes a certain packet sent from the client.
     *
     * @param packet The packet to decode.
     * @return The packet to decode.
     */
    public T decode(Packet packet);
}
