package org.hyperion.rs2;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;

import java.util.logging.Logger;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public class ShutDownHookEvent extends Thread {

    /**
     * The logger
     */
    private static final Logger logger = Logger.getLogger(ShutDownHookEvent.class.getName());

    @Override
    public void run() {
        logger.info("Saving all players...");
        int count = 0;
        synchronized (World.getWorld().getPlayers()) {
            for (Player player : World.getWorld().getPlayers()) {
                World.getWorld().getWorldLoader().savePlayer(player);
                player.getActionSender().sendLogout();
                count++;
            }
        }
        logger.info("Saved " + count + " players");
        logger.info("Shutting Down...");
    }
}
