package org.hyperion.rs2.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Time utility class
 *
 * @author Peterbjornx
 */
public class TimeUtils {

    /**
     * The Gregorian Calender.
     */
    private static Calendar calendar = new GregorianCalendar();

    /**
     * The current date
     */
    private static SpecificDate currentDate = new SpecificDate(calendar);

    /**
     * The date
     */
    public static class SpecificDate {

        /**
         * The day.
         */
        private final int day;

        /**
         * The month.
         */
        private final int month;

        /**
         * The year.
         */
        private final int year;

        /**
         * The date.
         */
        private final Date date;

        /**
         * Creates a date
         *
         * @param day The day
         * @param month The month
         * @param year The year
         */
        public SpecificDate(int day, int month, int year) {
            this.day = day;
            this.month = month;
            this.year = year;
            this.date = new Date(year, month, day);
        }

        /**
         * Creates a date with a <code>Calendar</code>
         *
         * @param cal The calendar
         */
        public SpecificDate(Calendar cal) {
            this.day = cal.get(cal.get(Calendar.DAY_OF_MONTH));
            this.month = cal.get(Calendar.MONTH);
            this.year = cal.get(Calendar.YEAR);
            this.date = cal.getTime();
        }

        /**
         * Creates a specific date
         *
         * @param date The date
         */
        public SpecificDate(Date date) {
            this.day = date.getDate();
            this.month = date.getMonth();
            this.year = date.getYear();
            this.date = date;
        }

        @Override
        public String toString() {
            return month + "/" + day + "/" + year;
        }

        /**
         * Gets the day
         *
         * @return day The day
         */
        public int getDay() {
            return day;
        }

        /**
         * Gets the month
         *
         * @return month The month
         */
        public int getMonth() {
            return month;
        }

        /**
         * Gets the year
         *
         * @return year The year
         */
        public int getYear() {
            return year;
        }

        /**
         * Gets the date
         *
         * @return The date
         */
        public Date getDate() {
            return date;
        }
    }

    /**
     * Gets the current date
     *
     * @return The current date
     */
    public static SpecificDate getCurrentDate() {
        return currentDate;
    }

    /**
     * Gets a Windows(r) filename safe date and time
     *
     * @param ts The timestamp to use
     * @return The string generated
     */
    public static String getSafeTimeAndDate(long ts) {
        java.util.Date today = new java.util.Date(ts);
        SimpleDateFormat formatter = new SimpleDateFormat("MM_dd_yyyy_H_mm_ss");
        return formatter.format(today);
    }

    /**
     * Gets a human readable representation of the timestamp <code>ts</code>
     *
     * @param ts The timestamp to use
     * @return The string generated
     */
    public static String getTimeAndDate(long ts) {
        java.util.Date today = new java.util.Date(ts);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy H:mm:ss");
        return formatter.format(today);
    }

    /**
     * Formats a long to the Jagex date format floored.
     *
     * @param unixtime The time.
     * @return The Jagex date format.
     */
    public static int toJagexDateFormatFloor(long unixtime) {
        double i = (unixtime / 0x5265c00L);
        return (int) Math.floor(i); // / 0x5265c00L == Millisecs per day
    }

    /**
     * Formats a long to the Jagex date format ceiled.
     *
     * @param unixtime The time.
     * @return The Jagex date format.
     */
    public static int toJagexDateFormatCeil(long unixtime) {
        double i = (unixtime / 0x5265c00L) + 1;
        return (int) Math.floor(i); // / 0x5265c00L == Millisecs per day
    }

    /**
     * Formats a long to the Jagex date.
     *
     * @param unixtime The time.
     * @return The Jagex date format.
     */
    public static int toJagexDateFormat(long unixtime) {
        return (int) (unixtime / 0x5265c00L) - 11745; /// 0x5265c00L == Millisecs per day
    }
}
