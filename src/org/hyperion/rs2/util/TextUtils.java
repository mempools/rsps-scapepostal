package org.hyperion.rs2.util;

import java.util.Enumeration;
import org.hyperion.Constants;

import java.util.LinkedList;
import java.util.Vector;

/**
 * Text utility class.
 *
 * @author Graham Edgecombe
 */
public class TextUtils {

    /**
     * Unpacks text.
     *
     * @param packedData The packet text.
     * @param size The length.
     * @return The string.
     */
    public static String textUnpack(byte packedData[], int size) {
        byte[] decodeBuf = new byte[4096];
        int idx = 0, highNibble = -1;
        for (int i = 0; i < size * 2; i++) {
            int val = packedData[i / 2] >> (4 - 4 * (i % 2)) & 0xf;
            if (highNibble == -1) {
                if (val < 13) {
                    decodeBuf[idx++] = (byte) Constants.XLATE_TABLE[val];
                } else {
                    highNibble = val;
                }
            } else {
                decodeBuf[idx++] = (byte) Constants.XLATE_TABLE[((highNibble << 4) + val) - 195];
                highNibble = -1;
            }
        }
        return new String(decodeBuf, 0, idx);
    }

    /**
     * Optimises text.
     *
     * @param text The text to optimise.
     * @return The text.
     */
    public static String optimizeText(String text) {
        char buf[] = text.toCharArray();
        boolean endMarker = true;
        for (int i = 0; i < buf.length; i++) {
            char c = buf[i];
            if (endMarker && c >= 'a' && c <= 'z') {
                buf[i] -= 0x20;
                endMarker = false;
            }
            if (c == '.' || c == '!' || c == '?') {
                endMarker = true;
            }
        }
        return new String(buf, 0, buf.length);
    }

    /**
     * Packs text.
     *
     * @param packedData The destination of the packed text.
     * @param text The unpacked text.
     */
    public static void textPack(byte packedData[], String text) {
        if (text.length() > 80) {
            text = text.substring(0, 80);
        }
        text = text.toLowerCase();
        int carryOverNibble = -1;
        int ofs = 0;
        for (int idx = 0; idx < text.length(); idx++) {
            char c = text.charAt(idx);
            int tableIdx = 0;
            for (int i = 0; i < Constants.XLATE_TABLE.length; i++) {
                if (c == (byte) Constants.XLATE_TABLE[i]) {
                    tableIdx = i;
                    break;
                }
            }
            if (tableIdx > 12) {
                tableIdx += 195;
            }
            if (carryOverNibble == -1) {
                if (tableIdx < 13) {
                    carryOverNibble = tableIdx;
                } else {
                    packedData[ofs++] = (byte) (tableIdx);
                }
            } else if (tableIdx < 13) {
                packedData[ofs++] = (byte) ((carryOverNibble << 4) + tableIdx);
                carryOverNibble = -1;
            } else {
                packedData[ofs++] = (byte) ((carryOverNibble << 4) + (tableIdx >> 4));
                carryOverNibble = tableIdx & 0xf;
            }
        }
        if (carryOverNibble != -1) {
            packedData[ofs++] = (byte) (carryOverNibble << 4);
        }
    }

    /**
     * Filters invalid characters out of a string.
     *
     * @param s The string.
     * @return The filtered string.
     */
    public static String filterText(String s) {
        StringBuilder bldr = new StringBuilder();
        for (char c : s.toLowerCase().toCharArray()) {
            boolean valid = false;
            for (char validChar : Constants.XLATE_TABLE) {
                if (validChar == c) {
                    valid = true;
                }
            }
            if (valid) {
                bldr.append((char) c);
            }
        }
        return bldr.toString();
    }

    /**
     * Parses an integer array
     *
     * @param string The string
     * @param delim
     * @return The array
     */
    public static Integer[] parseIntArray(String string, String delim) {
        string = string.trim();
        delim = delim.trim();
        if (!string.contains(delim)) {
            return new Integer[]{Integer.parseInt(string)};
        }
        String[] split = string.split(delim);
        LinkedList<Integer> list = new LinkedList<>();
        for (String s : split) {
            if (s == null || s.equals("") || s.equals(delim)) {
                continue;
            }
            list.add(Integer.parseInt(s));
        }
        return list.toArray(new Integer[list.size()]);
    }

    /**
     * Changes the IP Address to an integer
     *
     * @param address The IP Address
     * @return The IP Address as an integer
     */
    public static int IPAddressToInteger(String address) {
        String[] sections = address.split("\\.");
        byte[] values = new byte[4];
        for (int i = 0; i < 4; i++) {
            values[i] = (byte) Integer.parseInt(sections[i]);
        }
        return ((values[0] & 0xFF) << 24) | ((values[1] & 0xFF) << 16)
                | ((values[2] & 0xFF) << 8) | (values[3] & 0xFF);
    }

    /**
     * Spaces on capital letters, example: HelloWorld -> hello_world
     *
     * @param s The string to set
     */
    public static String toSymbol(String s) {
        return s.replaceAll("(\\p{Ll})(\\p{Lu})", "$1_$2").toLowerCase().replaceAll(" ", "_");
    }

    /**
     * Format an enum object or other object from all uppercase to first
     * uppercase.
     *
     * @param object The object to format
     * @return The formatted name
     */
    public static String formatEnum(Object object) {
        String s = object.toString().toLowerCase();
        return Character.toUpperCase(s.charAt(0)) + (s.substring(1).replaceAll("_", " "));
    }

    /**
     * Converts a 0x hex value to an integer
     *
     * @param object The object
     * @return The integer
     */
    public static int getZeroXValue(Object object) {
        return object.toString().startsWith("0x") ? Integer.parseInt(object.toString().substring(2), 16) : Integer.parseInt(object.toString());
    }

    /**
     * Formats integer to string and adds commas 2143944 -> 2,143,944
     *
     * @param i The integer to format
     * @return The formatted integer
     */
    public static String formatNumber(int i) {
        //return NumberFormat.getNumberInstance(Locale.US).format(i);
        return String.format("%,d", i);
    }

    /**
     * Gets the indefinite article of a 'thing'.
     *
     * @param thing The thing.
     * @return The indefinite article.
     */
    public static String getIndefiniteArticle(String thing) {
        char first = thing.toLowerCase().charAt(0);
        boolean vowel = first == 'a' || first == 'e' || first == 'i' || first == 'o' || first == 'u';
        return vowel ? "an" : "a";
    }

    /**
     * Wraps texts.
     * <http://progcookbook.blogspot.com/2006/02/text-wrapping-function-for-java.html>
     * <pre>
     * {@code
     * String text = "this is a long line of text that needs to be wrapped";
     * String [] lines = wrapText(text, 20);
     * for (int i = 0; i < lines.length; i++) {
     *    System.out.println(lines[i]);
     * }
     * }
     * </pre>
     *
     * @param ignoreWrap Weather to wrap or not
     * @param text The text to be wrapped
     * @param len The length
     * @return The wrapped text
     */
    public static String[] wrapText(boolean ignoreWrap, String text, int len) {
        // return empty array for null text
        if (text == null) {
            return new String[]{};
        }
        if (ignoreWrap) {
            return new String[]{text};
        }

        // return text if len is zero or less
        if (len <= 0) {
            return new String[]{text};
        }

        // return text if less than length
        if (text.length() <= len) {
            return new String[]{text};
        }

        char[] chars = text.toCharArray();
        Vector lines = new Vector();
        StringBuffer line = new StringBuffer();
        StringBuffer word = new StringBuffer();

        for (int i = 0; i < chars.length; i++) {
            word.append(chars[i]);

            if (chars[i] == ' ') {
                if ((line.length() + word.length()) > len) {
                    lines.add(line.toString());
                    line.delete(0, line.length());
                }

                line.append(word);
                word.delete(0, word.length());
            }
        }

        // handle any extra chars in current word
        if (word.length() > 0) {
            if ((line.length() + word.length()) > len) {
                lines.add(line.toString());
                line.delete(0, line.length());
            }
            line.append(word);
        }

        // handle extra line
        if (line.length() > 0) {
            lines.add(line.toString());
        }

        String[] ret = new String[lines.size()];
        int c = 0; // counter
        for (Enumeration e = lines.elements(); e.hasMoreElements(); c++) {
            ret[c] = (String) e.nextElement();
        }

        return ret;
    }
}
