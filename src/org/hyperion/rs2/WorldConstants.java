package org.hyperion.rs2;

import org.hyperion.rs2.model.World;
import org.hyperion.util.configuration.ConfigurationNode;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class WorldConstants {

    /**
     * The configuration node.
     */
    public static final ConfigurationNode config = World.getWorld().getNode().nodeFor("game");

    /**
     * The server name.
     */
    public static final String SERVER_NAME = config.getString("serverName");

    /**
     * The experience bonus.
     */
    public static final int EXP_BONUS = config.getInteger("expBonus");

    /**
     * The free quests in numerical order.
     */
    public static final int[] FREE_QUESTS = {640, 663, 7332, 7333, 7334, 7336, 7383, 7339,
        7338, 7340, 7346, 7341, 7342, 7337, 7343, 7335,
        7344, 7345, 7347, 7348};

    /**
     * The member quests in numerical order.
     */
    public static final int[] MEMBER_QUESTS = {12772, 7352, 12129, 8438,
        18517, 15847, 15487, 12852, 7354, 7355, 7356,
        8679, 7459, 7357, 14912, 249, 6024, 191, 15235,
        15592, 6987, 15098, 15352, 18306, 15499,
        668, 18684, 6027, 18157, 15847, 16128, 12836,
        16149, 15841, 7353, 7358, 17510, 7359, 14169,
        10115, 14604, 7360, 12282, 13577, 12839, 7361,
        11857, 7362, 7363, 7364, 10135, 4508, 11907,
        7365, 7366, 7367, 13389, 7368, 11132, 7369,
        12389, 13974, 7370, 8137, 7371, 12345, 7372,
        8115, 8576, 12139, 7373, 7374, 8969, 7375, 7376,
        1740, 3278, 7378, 6518, 7379, 7380, 7381, 11858,
        9927, 7349, 7350, 7351, 13356};

}
