package org.hyperion.rs2;

import org.apache.commons.codec.digest.DigestUtils;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.PlayerDetails;
import org.hyperion.rs2.util.NameUtils;
import org.hyperion.util.buffer.BinaryPart;
import org.hyperion.util.buffer.BinaryPartUtil;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * An implementation of the <code>WorldLoader</code> class that saves players in
 * binary, gzip-compressed files in the <code>data/savedGames/</code> directory.
 *
 * @author Graham Edgecombe
 * @author Nikki
 */
public class GenericWorldLoader implements WorldLoader {

    @Override
    public LoginResult checkLogin(PlayerDetails pd) {
        Player player = null;
        int code = LoginResult.RESPONSE_OK;
        File f = new File("data/savedGames/" + NameUtils.formatNameForProtocol(pd.getName()) + ".dat.gz");
        if (f.exists()) {
            try {
                try (InputStream is = new GZIPInputStream(new FileInputStream(f))) {
                    BinaryPart part = BinaryPartUtil.readPartFromInput(is);
                    if (part.getOpcode() != 0) {
                        code = LoginResult.RESPONSE_LOCKED;
                    } else {
                        String name = part.getString();
                        String pass = part.getString();
                        String hex = DigestUtils.shaHex(pd.getPassword());
                        if (!name.equals(NameUtils.formatName(pd.getName()))) {
                            code = LoginResult.RESPONSE_INVALID_INFORMATION;
                        }
                        if (!pass.equals(hex)) {
                            code = LoginResult.RESPONSE_INVALID_INFORMATION;
                        }
                    }
                }
            } catch (IOException ex) {
                code = LoginResult.RESPONSE_LOCKED;
            }
        }
        if (code == LoginResult.RESPONSE_OK) {
            player = new Player(pd);
        }
        return new LoginResult(code, player);
    }

    @Override
    public boolean savePlayer(Player player) {
        try {
            try (OutputStream os = new GZIPOutputStream(new FileOutputStream(
                    "data/savedGames/"
                    + NameUtils.formatNameForProtocol(player.getName())
                    + ".dat.gz"))) {
                ChannelBuffer buffer = ChannelBuffers.dynamicBuffer();
                player.serialize(buffer);
                byte[] data = new byte[buffer.readableBytes()];
                buffer.readBytes(data);
                os.write(data);
                os.flush();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    @Override
    public boolean loadPlayer(Player player) {
        try {
            File f = new File("data/savedGames/"
                    + NameUtils.formatNameForProtocol(player.getName())
                    + ".dat.gz");
            ChannelBuffer buffer;
            try (InputStream is = new GZIPInputStream(new FileInputStream(f))) {
                buffer = ChannelBuffers.buffer((int) f.length());
                while (true) {
                    byte[] temp = new byte[1024];
                    int read = is.read(temp, 0, temp.length);
                    if (read == -1) {
                        break;
                    } else {
                        buffer.writeBytes(temp, 0, read);
                    }
                }
            }
            player.deserialize(buffer);
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

}
