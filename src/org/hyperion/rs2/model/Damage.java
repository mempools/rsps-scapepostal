package org.hyperion.rs2.model;

/**
 * Represents a single instance of damage.
 *
 * @author Graham
 */
public class Damage {

    /**
     * Represents the four types of damage.
     *
     * @author Graham
     */
    public static enum HitType {

        NO_DAMAGE(0), // blue
        NORMAL_DAMAGE(1), // red
        POISON_DAMAGE(2), // green
        DISEASE_DAMAGE(3);    // orange
        /**
         * The type
         */
        private final int type;

        /**
         * Constructor.
         *
         * @param type The corresponding integer for damage hitType.
         */
        private HitType(int type) {
            this.type = type;
        }

        /**
         * Get the damage hitType.
         *
         * @return The damage hitType, as an <code>int</code>.
         */
        public int getType() {
            return type;
        }
    }

    /**
     * Nested class <code>Hit</code>, handling a single hit.
     *
     * @author Graham
     */
    public static class Hit {

        /**
         * The hit type.
         */
        private final HitType hitType;
        /**
         * The damage.
         */
        private final int damage;

        /**
         * Creates the hit
         *
         * @param damage The damage
         * @param type The type
         */
        public Hit(int damage, HitType type) {
            this.hitType = type;
            this.damage = damage;
        }

        /**
         * The hit type
         *
         * @return hitType The hit type
         */
        public HitType getHitType() {
            return hitType;
        }

        /**
         * The damage
         *
         * @return damage The damage
         */
        public int getDamage() {
            return damage;
        }
    }

    /**
     * Constructor method.
     */
    public Damage() {
        primaryHit = null;
        secondaryHit = null;
    }

    /**
     * Represents a hit.
     */
    /**
     * The primary hit
     */
    private Hit primaryHit;

    /**
     * The secondary hit
     */
    private Hit secondaryHit;

    /**
     * Sets a hit.
     *
     * @param hit The hit to clone.
     */
    public void setPrimaryHit(Hit hit) {
        this.primaryHit = hit;
    }

    /**
     * Sets the second hit
     *
     * @param hit The hit
     */
    public void setSecondaryHit(Hit hit) {
        this.secondaryHit = hit;
    }

    /**
     * Gets the primary hit damage.
     *
     * @return An <code>int</code> of the damage of this hit.
     */
    public int getPrimaryDamage() {
        if (primaryHit == null) {
            return 0;
        }
        return primaryHit.damage;
    }

    /**
     * Gets the second hit damage.
     *
     * @return An <code>int</code> of the damage of this hit.
     */
    public int getSecondaryDamage() {
        if (secondaryHit == null) {
            return 0;
        }
        return secondaryHit.damage;
    }

    /**
     * Gets the primary hit hitType.
     *
     * @return The hitType of hit.
     */
    public int getPrimaryHitType() {
        if (primaryHit == null) {
            return HitType.NO_DAMAGE.getType();
        }
        return primaryHit.hitType.getType();
    }

    /**
     * Gets the second hit hitType.
     *
     * @return The hitType of hit.
     */
    public int getSecondaryHitType() {
        if (secondaryHit == null) {
            return HitType.NO_DAMAGE.getType();
        }
        return secondaryHit.hitType.getType();
    }

    /**
     * Gets the primary hit
     *
     * @return primaryHit The primary hit
     */
    public Hit getPrimary() {
        return primaryHit;
    }

    /**
     * Gets the secondary hit
     *
     * @return secondaryHit The secondary hit
     */
    public Hit getSecondary() {
        return secondaryHit;
    }

    /**
     * Destroy this hit.
     */
    public void clear() {
        primaryHit = null;
        secondaryHit = null;
    }
}
