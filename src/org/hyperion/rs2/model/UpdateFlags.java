package org.hyperion.rs2.model;

import java.util.BitSet;

/**
 * Holds update flags.
 *
 * @author Graham Edgecombe
 */
public class UpdateFlags {

    /**
     * The bit set (flag data).
     */
    private final BitSet flags = new BitSet();

    /**
     * Represents a single type of update flag.
     *
     * @author Graham Edgecombe
     */
    public enum UpdateFlag {

        /**
         * Appearance update.
         */
        APPEARANCE,
        /**
         * Chat update.
         */
        CHAT,
        /**
         * Graphics update.
         */
        GRAPHICS,
        /**
         * Animation update.
         */
        ANIMATION,
        /**
         * Forced chat update.
         */
        FORCED_CHAT,
        /**
         * Interacting entity update.
         */
        FACE_ENTITY,
        /**
         * Face coordinate entity update.
         */
        FACE_COORDINATE,
        /**
         * Primary hit update.
         */
        PRIMARY_HIT,
        /**
         * Secondary hit update.
         */
        SECONDARY_HIT,
        /**
         * Update flag used to transform NPC to another.
         */
        TRANSFORM,
        /**
         * Update flag to update force movement.
         */
        FORCE_MOVEMENT;
    }

    /**
     * Checks if an update required.
     *
     * @return <code>true</code> if 1 or more flags are set, <code>false</code>
     * if not.
     */
    public boolean isUpdateRequired() {
        return !flags.isEmpty();
    }

    /**
     * Flags (sets to true) a flag.
     *
     * @param flag The flag to flag.
     */
    public void flag(UpdateFlag flag) {
        flags.set(flag.ordinal(), true);
    }

    /**
     * Sets a flag.
     *
     * @param flag The flag.
     * @param value The value.
     */
    public void set(UpdateFlag flag, boolean value) {
        flags.set(flag.ordinal(), value);
    }

    /**
     * Gets the value of a flag.
     *
     * @param flag The flag to get the value of.
     * @return The flag value.
     */
    public boolean get(UpdateFlag flag) {
        return flags.get(flag.ordinal());
    }

    /**
     * Resets all update flags.
     */
    public void reset() {
        flags.clear();
    }
}
