package org.hyperion.rs2.model.player;

import org.hyperion.rs2.model.request.RequestManager;
import org.hyperion.data.Persistable;
import org.hyperion.rs2.content.chat.FriendsList;
import org.hyperion.rs2.content.chat.IgnoreList;
import org.hyperion.rs2.content.journal.Journal;
import org.hyperion.rs2.content.menu.Menu;
import org.hyperion.rs2.content.mission.Mission;
import org.hyperion.rs2.content.mission.MissionStorage;
import org.hyperion.rs2.model.UpdateFlags.UpdateFlag;
import org.hyperion.rs2.model.combat.CombatAction;
import org.hyperion.rs2.model.combat.CombatStyle;
import org.hyperion.rs2.model.container.Bank;
import org.hyperion.rs2.model.container.Container;
import org.hyperion.rs2.model.container.Container.Type;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.container.Trade;
import org.hyperion.rs2.model.region.Region;
import org.hyperion.rs2.net.ActionSender;
import org.hyperion.rs2.net.ISAACCipher;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.util.NameUtils;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;

import java.util.LinkedList;
import java.util.Queue;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hyperion.rs2.content.clan.GameChannel;
import org.hyperion.rs2.content.menu.impl.DefaultMenu;
import org.hyperion.rs2.content.skills.Prayers.Prayer;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.Appearance;
import org.hyperion.rs2.model.Area;
import org.hyperion.rs2.model.GroundItem;
import org.hyperion.rs2.packet.context.ChatMessageContext;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.container.Inventory;

/**
 * Represents a player-controller character.
 *
 * @author Graham Edgecombe
 */
public class Player extends Actor implements Persistable {

    /**
     * Represents the rights of a player.
     *
     * @author Graham Edgecombe
     */
    public enum Rights {

        /**
         * A standard account.
         */
        PLAYER(0),
        /**
         * A player-moderator account.
         */
        MODERATOR(1),
        /**
         * An administrator account.
         */
        ADMINISTRATOR(2);

        /**
         * The integer representing this rights level.
         */
        private final int value;

        /**
         * Creates a rights level.
         *
         * @param value The integer representing this rights level.
         */
        private Rights(int value) {
            this.value = value;
        }

        /**
         * Gets rights by a specific integer.
         *
         * @param value The integer returned by {@link #toInteger()}.
         * @return The rights level.
         */
        public static Rights getRights(int value) {
            if (value == 1) {
                return MODERATOR;
            } else if (value == 2) {
                return ADMINISTRATOR;
            } else {
                return PLAYER;
            }
        }

        /**
         * Gets the forum rights
         *
         * @param value The forum right integer
         * @return The rights level
         */
        public static Rights getForumRights(int value) {
            /*
             * Configured for RSC Script
             */
            if (value == 4) {
                return ADMINISTRATOR;
            } else {
                return PLAYER;
            }
        }

        /**
         * Gets an integer representing this rights level.
         *
         * @return An integer representing this rights level.
         */
        public int toInteger() {
            return value;
        }
    }

    /**
     * Attributes specific to our session.
     */
    /**
     * The <code>Channel</code>.
     */
    private final Channel session;

    /**
     * The ISAAC cipher for incoming data.
     */
    private final ISAACCipher inCipher;

    /**
     * The ISAAC cipher for outgoing data.
     */
    private final ISAACCipher outCipher;

    /**
     * The action sender.
     */
    private final ActionSender actionSender = new ActionSender(this);

    /**
     * A queue of pending chat messages.
     */
    private final Queue<ChatMessageContext> chatMessages = new LinkedList<>();

    /**
     * The current chat message.
     */
    private ChatMessageContext currentChatMessage;

    /**
     * Active flag: if the player is not active certain changes (e.g. items)
     * should not send packets as that indicates the player is still loading.
     */
    private boolean active = false;

    /**
     * The busy flag.
     */
    private boolean busy = false;

    /**
     * The interface state.
     */
    private final InterfaceState interfaceState = new InterfaceState(this);
    /**
     * A queue of packets that are pending.
     */
    private final Queue<Packet> pendingPackets = new LinkedList<>();

    /**
     * A queue of pending chat messages.
     */
    private final Queue<Packet> queuedPackets = new LinkedList<>();

    /**
     * The request manager which manages trading and dueling requests.
     */
    private final RequestManager requestManager = new RequestManager(this);

    /**
     * Player details.
     */
    /**
     * The profile.
     */
    private final Profile profile = new Profile(this);

    /**
     * The membership.
     */
    private final Membership membership = new Membership(this);

    /**
     * The game channel.
     */
    private GameChannel gameChannel = null;

    /**
     * The mission storage.
     */
    private final MissionStorage missionStorage = new MissionStorage();

    /**
     * The mission.
     */
    private Mission mission = null;

    /**
     * The menu.
     */
    private Menu menu = null;

    /**
     * The journal
     */
    private Journal journal = null;

    /**
     * Core login details.
     */
    /**
     * The UID, i.e. number in <code>random.dat</code>.
     */
    private final int uid;

    /**
     * The name.
     */
    private String name;

    /*
     * Attributes.
     */
    /**
     * The name expressed as a long.
     */
    private long nameLong;

    /**
     * The player's appearance information.
     */
    private final Appearance appearance = new Appearance();

    /**
     * The password.
     */
    private String password;

    /**
     * The player's animations.
     */
    private final EquipmentAnimations equipmentAnimations = new EquipmentAnimations();

    /**
     * The rights level.
     */
    private Rights rights = Rights.PLAYER;

    /**
     * The members flag.
     */
    private boolean members = true;

    /**
     * The player's bank.
     */
    private final Container bank = new Container(Type.ALWAYS_STACK, Bank.SIZE);

    /**
     * The player's trade.
     */
    private final Container trade = new Container(Type.STANDARD, Trade.SIZE);

    /**
     * The cached update block.
     */
    private Packet cachedUpdateBlock;

    /**
     * The player's settings.
     */
    private final Settings settings = new Settings();

    /**
     * The friends list
     */
    private final FriendsList friendsList = new FriendsList(this);

    /**
     * An ignore list
     */
    private final IgnoreList ignoreList = new IgnoreList(this);

    /*
     * Cached details.
     */
    private boolean isFocused = true;

    /**
     * Creates a player based on the details object.
     *
     * @param details The details object.
     */
    public Player(PlayerDetails details) {
        super();
        this.session = details.getSession();
        this.inCipher = details.getInCipher();
        this.outCipher = details.getOutCipher();
        this.name = details.getName();
        this.nameLong = NameUtils.nameToLong(this.name);
        this.password = details.getPassword();
        this.uid = details.getUID();
        this.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
        this.setTeleporting(true);
    }

    /**
     * Gets the request manager.
     *
     * @return The request manager.
     */
    public RequestManager getRequestManager() {
        return requestManager;
    }

    /**
     * Gets the player's name expressed as a long.
     *
     * @return The player's name expressed as a long.
     */
    public long getNameAsLong() {
        return nameLong;
    }

    /**
     * Gets the player's settings.
     *
     * @return The player's settings.
     */
    public Settings getSettings() {
        return settings;
    }

    /**
     * Get the player's friends list
     *
     * @return The friends list
     */
    public FriendsList getFriendsList() {
        return friendsList;
    }

    /**
     * Get the player's ignore list
     *
     * @return The ignore list
     */
    public IgnoreList getIgnoreList() {
        return ignoreList;
    }

    /**
     * Writes a packet to the <code>IoSession</code>. If the player is not yet
     * active, the packets are queued.
     *
     * @param packet The packet to write.
     */
    public void write(Packet packet) {
        synchronized (this) {
            if (!active) {
                pendingPackets.add(packet);
            } else {
                for (Packet pendingPacket : pendingPackets) {
                    session.write(pendingPacket);
                }
                pendingPackets.clear();
                session.write(packet);
            }
        }
    }

    /**
     * Gets the player's bank.
     *
     * @return The player's bank.
     */
    public Container getBank() {
        return bank;
    }

    /**
     * Gets the player's trade.
     *
     * @return The player's trade.
     */
    public Container getTrade() {
        return trade;
    }

    /**
     * Gets the interface state.
     *
     * @return The interface state.
     */
    public InterfaceState getInterfaceState() {
        return interfaceState;
    }

    /**
     * Checks if there is a cached update block for this cycle.
     *
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean hasCachedUpdateBlock() {
        return cachedUpdateBlock != null;
    }

    /**
     * Gets the cached update block.
     *
     * @return The cached update block.
     */
    public Packet getCachedUpdateBlock() {
        return cachedUpdateBlock;
    }

    /**
     * Sets the cached update block for this cycle.
     *
     * @param cachedUpdateBlock The cached update block.
     */
    public void setCachedUpdateBlock(Packet cachedUpdateBlock) {
        this.cachedUpdateBlock = cachedUpdateBlock;
    }

    /**
     * Resets the cached update block.
     */
    public void resetCachedUpdateBlock() {
        cachedUpdateBlock = null;
    }

    /**
     * Gets the current chat message.
     *
     * @return The current chat message.
     */
    public ChatMessageContext getCurrentChatMessage() {
        return currentChatMessage;
    }

    /**
     * Sets the current chat message.
     *
     * @param currentChatMessage The current chat message to set.
     */
    public void setCurrentChatMessage(ChatMessageContext currentChatMessage) {
        this.currentChatMessage = currentChatMessage;
    }

    /**
     * Gets the queue of pending chat messages.
     *
     * @return The queue of pending chat messages.
     */
    public Queue<ChatMessageContext> getChatMessageQueue() {
        return chatMessages;
    }

    /**
     * Get the packet queue
     *
     * @return The queued packets
     */
    public Queue<Packet> getPacketQueue() {
        synchronized (queuedPackets) {
            return queuedPackets;
        }
    }

    /**
     * Gets the profile
     *
     * @return profile The profile
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * Gets the membership details
     *
     * @return The membership
     */
    public Membership getMembership() {
        return membership;
    }

    /**
     * Gets the game channel
     *
     * @return gameChannel The game channel
     */
    public GameChannel getGameChannel() {
        return gameChannel;
    }

    /**
     * Sets the game channel
     *
     * @param clan The game channel to set
     * @return The gameChannel
     */
    public GameChannel setGameChannel(GameChannel clan) {
        return this.gameChannel = clan;
    }

    /**
     * Gets the current mission
     *
     * @return mission The mission
     */
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets the mission
     *
     * @param mission The mission to set
     * @return The mission
     */
    public Mission setMission(Mission mission) {
        return this.mission = mission;
    }

    /**
     * Gets the mission storage
     *
     * @return missionStorage The mission storage
     */
    public MissionStorage getMissionStorage() {
        return missionStorage;
    }

    /**
     * Gets the current menu the player is on
     *
     * @return menu The menu
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * Gets the journal
     *
     * @return journal The journal
     */
    public Journal getJournal() {
        return journal;
    }

    /**
     * Opens a menu
     *
     * @param menu The menu to open
     * @return The menu
     */
    public Menu openMenu(Menu menu) {
        if (getInterfaceState().getCurrentMenu() == menu) {
            return new DefaultMenu();
        }
        getInterfaceState().setCurrentMenu(menu);
        menu.sendContent(this);
        return this.menu = menu;
    }

    /**
     * Sets the journal
     *
     * @param journal The journal to set
     * @return The journal
     */
    public Journal setJournal(Journal journal) {
        return this.journal = journal;
    }

    /**
     * Gets the player's appearance.
     *
     * @return The player's appearance.
     */
    public Appearance getAppearance() {
        return appearance;
    }

    /**
     * Gets the equipment animations
     *
     * @return The player's animations
     */
    public EquipmentAnimations getEquipmentAnimations() {
        return equipmentAnimations;
    }

    /**
     * Gets the action sender.
     *
     * @return The action sender.
     */
    @Override
    public ActionSender getActionSender() {
        return actionSender;
    }

    /**
     * Gets the incoming ISAAC cipher.
     *
     * @return The incoming ISAAC cipher.
     */
    public ISAACCipher getInCipher() {
        return inCipher;
    }

    /**
     * Gets the outgoing ISAAC cipher.
     *
     * @return The outgoing ISAAC cipher.
     */
    public ISAACCipher getOutCipher() {
        return outCipher;
    }

    /**
     * Gets the player's name.
     *
     * @return The player's name.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets the player's name.
     *
     * @param name The player's name.
     * @return The name
     */
    public String setName(String name) {
        return this.name = name;
    }

    /**
     * Sets the name to long.
     *
     * @param name name to set.
     * @return The name as long
     */
    public long setNameLong(long name) {
        return this.nameLong = name;
    }

    /**
     * Gets the player's password.
     *
     * @return The player's password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the player's password.
     *
     * @param pass The password.
     */
    public void setPassword(String pass) {
        this.password = pass;
    }

    /**
     * Gets the player's UID.
     *
     * @return The player's UID.
     */
    public int getUID() {
        return uid;
    }

    /**
     * Gets the <code>Channel</code>.
     *
     * @return The player's <code>Channel</code>.
     */
    public Channel getSession() {
        return session;
    }

    /**
     * Gets the rights.
     *
     * @return The player's rights.
     */
    public Rights getRights() {
        return rights;
    }

    /**
     * Sets the rights.
     *
     * @param rights The rights level to set.
     */
    public void setRights(Rights rights) {
        this.rights = rights;
    }

    /**
     * Checks if this player has a member's account.
     *
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean isMembers() {
        return members;
    }

    /**
     * Sets the members flag.
     *
     * @param members The members flag.
     */
    public void setMembers(boolean members) {
        this.members = members;
    }

    /**
     * Is the entity busy?
     *
     * @return isBusy flag
     */
    public boolean isBusy() {
        /*
         * If an interface is open
         */
        return getInterfaceState().getCurrentInterface() > -1 ? true : busy;
    }

    /**
     * Sets the busy flag
     *
     * @param b The busy flag
     * @return If the player is busy or not
     */
    public boolean isBusy(boolean b) {
        return this.busy = b;
    }

    /**
     * Checks if the player is on the client or not
     *
     * @return If the player is focused or not
     */
    public boolean isFocused() {
        return isFocused;
    }

    /**
     * Sets the focus state
     *
     * @param state The focus change
     * @return The focus state
     */
    public boolean isFocused(boolean state) {
        return isFocused = state;
    }

    @Override
    public String toString() {
        return Player.class.getName() + " [name=" + name + " rights=" + rights + " members=" + members + " index=" + this.getIndex() + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Player other = (Player) obj;
        return other.name.equals(name);
    }

    @Override
    public int hashCode() {
        HashCodeBuilder code = new HashCodeBuilder(1, 71);
        code.append(this.name);
        code.append(this.nameLong);
        return code.build() * 2;
    }

    /**
     * Gets the active flag.
     *
     * @return The active flag.
     */
    public boolean isActive() {
        synchronized (this) {
            return active;
        }
    }

    /**
     * Sets the active flag.
     *
     * @param active The active flag.
     */
    public void setActive(boolean active) {
        synchronized (this) {
            this.active = active;
        }
    }

    /**
     * Updates the player's options when in a PvP area.
     *
     * @param enable The enable flag
     */
    public void updatePlayerAttackOptions(boolean enable) {
        if (enable) {
            actionSender.sendInteractionOption("Attack", 1, true);
        }
    }

    /**
     * Gets two containers, one being the items you keep on death, and the
     * second being the lost items.
     *
     * @return Two containers, one being the items you keep on death, and the
     * second being the lost items.
     */
    public Container[] getItemsOnDeath() {
        int count = 3;
        if (getPrayers().isPrayerOn(Prayer.PROTECT_ITEM)) { //protect item
            count++;
        }
//        if (getCombatState().getSkullTicks() > 0) {
//            count -= 3;
//        }
        Container topItems = new Container(Type.STANDARD, count);
        Container temporaryInventory = new Container(Type.STANDARD, Inventory.SIZE);
        Container temporaryEquipment = new Container(Type.STANDARD, Equipment.SIZE);
        // Add all of our items with their designated slots.
        for (int i = 0; i < Inventory.SIZE; i++) {
            Item item = getInventory().get(i);
            if (item != null) {
                temporaryInventory.add(item, i);
            }
        }
        for (int i = 0; i < Equipment.SIZE; i++) {
            Item item = getEquipment().get(i);
            if (item != null) {
                temporaryEquipment.add(item, i);
            }
        }

        /*
         * Checks our inventory and equipment for any items that can be added to the top list.
         */
        for (int i = 0; i < Inventory.SIZE; i++) {
            Item item = temporaryInventory.get(i);
            if (item != null) {
                item = new Item(temporaryInventory.get(i).getId(), 1);
                for (int k = 0; k < count; k++) {
                    Item topItem = topItems.get(k);
                    if (topItem == null || item.getDefinition().getValue() > topItem.getDefinition().getValue()) {
                        if (topItem != null) {
                            topItems.remove(topItem);
                        }
                        topItems.add(item);
                        temporaryInventory.remove(item);
                        if (topItem != null) {
                            temporaryInventory.add(topItem);
                        }
                        if (item.getDefinition().isStackable() && temporaryInventory.getCount(item.getId()) > 0) {
                            //If stackable and we still have some items remaining, we need to stay on this slot to check if we can add any others.
                            i--;
                        }
                        break;
                    }
                }
            }
        }
        for (int i = 0; i < Equipment.SIZE; i++) {
            Item item = temporaryEquipment.get(i);
            if (item != null) {
                item = new Item(temporaryEquipment.get(i).getId(), 1);
                for (int k = 0; k < count; k++) {
                    Item topItem = topItems.get(k);
                    if (topItem == null || item.getDefinition().getValue() > topItem.getDefinition().getValue()) {
                        if (topItem != null) {
                            topItems.remove(topItem);
                        }
                        topItems.add(item);
                        temporaryEquipment.remove(item);
                        if (topItem != null) {
                            temporaryEquipment.add(topItem);
                        }
                        if (item.getDefinition().isStackable() && temporaryEquipment.getCount(item.getId()) > 0) {
                            //If stackable and we still have some items remaining, we need to stay on this slot to check if we can add any others.
                            i--;
                        }
                        break;
                    }
                }
            }
        }

        /*
         * For the rest of the items we have, we add them to a new container.
         */
        Container lostItems = new Container(Type.STANDARD, Inventory.SIZE + Equipment.SIZE);
        for (Item lostItem : temporaryInventory.toArray()) {
            if (lostItem != null) {
                lostItems.add(lostItem);
            }
        }
        for (Item lostItem : temporaryEquipment.toArray()) {
            if (lostItem != null) {
                lostItems.add(lostItem);
            }
        }
        return new Container[]{topItems, lostItems};
    }

    @Override
    public void dropLoot(Actor killer) {
        Player player = this;
        if (killer != null) {
            if (killer.isPlayer()) {
                player = (Player) killer;
            }
            if (player.getRights() == Rights.ADMINISTRATOR) {
                getActionSender().sendMessage("You didn't lose any items because an Admin killed you.");
                return;
            }
        }
//        if (BoundaryManager.isWithinBoundaryNoZ(getLocation(), "SafeZone")) {
//            return;
//        }
        Container[] items = getItemsOnDeath();
        Container itemsKept = items[0];
        Container itemsLost = items[1];

        boolean inventoryFiringEvents = getInventory().isFiringEvents();
        getInventory().setFiringEvents(false);
        boolean equipmentFiringEvents = getEquipment().isFiringEvents();
        getEquipment().setFiringEvents(false);
        try {
            getInventory().clear();
            getEquipment().clear();

            for (Item item : itemsLost.toArray()) {
                if (item != null) {
                    GroundItem.create(player, item, getLocation());
                }
            }
            GroundItem.create(player, new Item(526), getLocation());

            for (Item item : itemsKept.toArray()) {
                if (item != null) {
                    getInventory().add(item);
                }
            }

            getEquipment().fireItemsChanged();
            getInventory().fireItemsChanged();
        } finally {
            getInventory().setFiringEvents(inventoryFiringEvents);
            getEquipment().setFiringEvents(equipmentFiringEvents);
        }
    }

    @Override
    public void deserialize(ChannelBuffer buf) {
        /*
         * See the generic world loaders
         */
    }

    @Override
    public void serialize(ChannelBuffer buf) {
        /*
         * See the generic world loaders
         */
    }

    @Override
    public void addToRegion(Region region) {
        region.addPlayer(this);
    }

    @Override
    public void removeFromRegion(Region region) {
        region.removePlayer(this);
    }

    @Override
    public int getClientIndex() {
        return this.getIndex() + 32768;
    }

    @Override
    public Animation getAttackAnimation() {
        return getCombatSession().getCombatStyle() == CombatStyle.AGGRESSIVE_1 ? Animation.create(423) : Animation.create(422);
    }

    @Override
    public Animation getDeathAnimation() {
        return Animation.DEATH;
    }

    @Override
    public Animation getDefendAnimation() {
        return (getEquipment().get(Equipment.SLOT_SHIELD) != null || getEquipment().get(Equipment.SLOT_WEAPON) != null) ? Animation.create(404) : Animation.create(424);
    }

    @Override
    public boolean isNPC() {
        return false;
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public boolean isObject() {
        return false;
    }

    @Override
    public CombatAction getDefaultCombatAction() {
        return null;
    }

    @Override
    public int getCombatCooldownDelay() {
        int speed;
        Item weapon = getEquipment().get(Equipment.SLOT_WEAPON);
        if (weapon != null && weapon.getEquipmentDefinition() != null) {
            speed = weapon.getEquipmentDefinition().getSpeed();
        } else {
            speed = 4;
        }
        return speed;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public int getHeight() {
        return 1;
    }

    @Override
    public int getWidth() {
        return 1;
    }

    @Override
    public Location getCenterLocation() {
        return getLocation();
    }

    @Override
    public Area getWalkingArea() {
        return null;
    }
}
