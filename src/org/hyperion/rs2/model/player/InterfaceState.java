package org.hyperion.rs2.model.player;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.content.dialogue.Dialogue;
import org.hyperion.rs2.content.menu.Menu;
import org.hyperion.rs2.model.container.Bank;
import org.hyperion.rs2.model.container.Container;
import org.hyperion.rs2.model.container.ContainerListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains information about the state of interfaces open in the client.
 *
 * @author Graham Edgecombe
 */
public class InterfaceState {

    /**
     * A map of interface values.
     */
    private final Map<Integer, InterfaceValue> interfaceValues = new HashMap<>();

    /**
     * The interface value constructor.
     */
    public class InterfaceValue {

        /**
         * The interface Id.
         */
        public int interfaceId;

        /**
         * The interface configurations / values.
         */
        public String value;

        /**
         * Constructs the interface value.
         *
         * @param interfaceId The interface Id.
         * @param value The interface value.
         */
        public InterfaceValue(int interfaceId, String value) {
            this.interfaceId = interfaceId;
            this.value = value;
        }

    }
    /**
     * The current open interface.
     */
    private int currentInterface = -1;
    /**
     * The active enter amount interface.
     */
    private int enterAmountInterfaceId = -1;
    /**
     * The active enter amount id.
     */
    private int enterAmountId;
    /**
     * The active enter amount slot.
     */
    private int enterAmountSlot;
    /**
     * The current dialogue.
     */
    private int currentDialogue;
    /**
     * The next dialogue id.
     */
    private int[] nextDialogueId = new int[]{-1, -1, -1, -1, -1, -1};
    /**
     * The custom dialogue.
     */
    private Dialogue dialogue;
    /**
     * Gets the current shop interface.
     */
    private int currentShop;
    /**
     * The current menu.
     */
    private Menu currentMenu;
    /**
     * The player.
     */
    private final Player player;
    /**
     * A list of container listeners used on interfaces that have containers.
     */
    private final List<ContainerListener> containerListeners = new ArrayList<>();

    /**
     * Creates the interface state.
     *
     * @param player The player
     */
    public InterfaceState(Player player) {
        this.player = player;
    }

    /**
     * Checks if the specified interface is open.
     *
     * @param id The interface id.
     * @return <code>true</code> if the interface is open, <code>false</code> if
     * not.
     */
    public boolean isInterfaceOpen(int id) {
        return currentInterface == id;
    }

    /**
     * Gets the current shop
     *
     * @return currentShop The current shop
     */
    public int getCurrentShop() {
        return currentShop;
    }

    /**
     * Gets the current open interface.
     *
     * @return The current open interface.
     */
    public int getCurrentInterface() {
        return currentInterface;
    }

    /**
     * Called when a shop is opened
     *
     * @param id The shop id
     * @return The shop id
     */
    public int shopOpened(int id) {
        if (currentShop != -1) {
            interfaceClosed();
        }
        return currentShop = id;
    }

    /**
     * Called when an interface is opened.
     *
     * @param id The interface.
     */
    public void interfaceOpened(int id) {
        if (currentInterface != -1) {
            interfaceClosed();
        }
        currentInterface = id;
    }

    /**
     * Called when an interface is closed.
     */
    public void interfaceClosed() {
        currentShop = -1;
        currentInterface = -1;
        enterAmountInterfaceId = -1;
        for (ContainerListener listener : containerListeners) {
            player.getInventory().removeListener(listener);
            player.getEquipment().removeListener(listener);
            player.getBank().removeListener(listener);
            player.getTrade().removeListener(listener);
        }
        currentDialogue = -1;
        nextDialogueId = new int[]{-1, -1, -1, -1, -1, -1};
        dialogue = null;
    }

    /**
     * Adds a listener to an interface that is closed when the inventory is
     * closed.
     *
     * @param container The container.
     * @param containerListener The listener.
     */
    public void addListener(Container container, ContainerListener containerListener) {
        container.addListener(containerListener);
        containerListeners.add(containerListener);
    }

    /**
     * Called to open the enter amount interface.
     *
     * @param interfaceId The interface id.
     * @param slot The slot.
     * @param id The id.
     */
    public void openEnterAmountInterface(int interfaceId, int slot, int id) {
        enterAmountInterfaceId = interfaceId;
        enterAmountSlot = slot;
        enterAmountId = id;
        player.getActionSender().sendEnterAmountInterface();
    }

    /**
     * Checks if the enter amount interface is open.
     *
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean isEnterAmountInterfaceOpen() {
        return enterAmountInterfaceId != -1;
    }

    /**
     * Called when the enter amount interface is closed.
     *
     * @param amount The amount that was entered.
     */
    public void closeEnterAmountInterface(int amount) {
        try {
            switch (enterAmountInterfaceId) {
                case Bank.PLAYER_INVENTORY_INTERFACE:
                    Bank.deposit(player, enterAmountSlot, enterAmountId, amount);
                    break;
                case Bank.BANK_INVENTORY_INTERFACE:
                    Bank.withdraw(player, enterAmountSlot, enterAmountId, amount);
                    break;
            }
        } finally {
            enterAmountInterfaceId = -1;
        }
    }

    /**
     * Gets the enter amount interface id
     *
     * @return enterAmountInterfaceId The enter amount interface id
     */
    public int getEnterAmountInterfaceId() {
        return enterAmountInterfaceId;
    }

    /**
     * Gets the enter amount id
     *
     * @return enterAmountId The enter amount id
     */
    public int getEnterAmountId() {
        return enterAmountId;
    }

    /**
     * Gets the enter amount slot
     *
     * @return enterAmountSlot The enter amount slot
     */
    public int getEnterAmountSlot() {
        return enterAmountSlot;
    }

    /**
     * Gets the open dialogue id
     *
     * @return the currentDialogue
     */
    public int getCurrentDialogue() {
        return currentDialogue;
    }

    /**
     * Sets the current dialogue id
     *
     * @param currentDialogue the currentDialogue to set
     */
    public void setCurrentDialogue(int currentDialogue) {
        this.currentDialogue = currentDialogue;
    }

    /**
     * Gets the next dialogue id
     *
     * @param index The index
     * @return The nextDialogueId to set
     */
    public int getNextDialogueId(int index) {
        return nextDialogueId[index];
    }

    /**
     * Sets the next dialogue id
     *
     * @param index The index
     * @param nextDialogueId The nextDialogueId to set
     */
    public void setNextDialogueId(int index, int nextDialogueId) {
        this.nextDialogueId[index] = nextDialogueId;
    }

    /**
     * Gets the custom dialogue
     *
     * @return dialogue The custom dialogue.
     */
    public Dialogue getDialogue() {
        return dialogue;
    }

    /**
     * Sets the dialogue.
     *
     * @param dialogue The dialogue to set.
     * @return The dialogue
     */
    public Dialogue setDialogue(Dialogue dialogue) {
        return this.dialogue = dialogue;
    }

    /**
     * Gets the current menu
     *
     * @return currentMenu The current menu
     */
    public Menu getCurrentMenu() {
        return currentMenu;
    }

    /**
     * Sets the currentMenu
     *
     * @param currentMenu The current menu to set
     * @return The menu
     */
    public Menu setCurrentMenu(Menu currentMenu) {
        return this.currentMenu = currentMenu;
    }

    /**
     * Checks if the player is in the same menu. This is mainly use to check if
     * buttons are performed within specific menus.
     *
     * @param check The menu to check
     * @return If is in the same menu
     */
    public boolean isInMenu(Menu check) {
        return this.currentMenu == check;
    }

    /**
     * Checks for duplicate values in the interface value map, if so, don't add
     * to the map, otherwise add the new interface values to the map
     *
     * @param interfaceId The interface Id
     * @param value The interface value
     * @return Whether if there was a duplicate or not
     */
    public boolean checkForInterfaceDuplication(int interfaceId, String value) {
        if (interfaceValues.containsKey(interfaceId)) {
            InterfaceValue interfaceValue = interfaceValues.get(interfaceId);
            if (value.equals(interfaceValue.value)) {
                return false;
            }
        }
        interfaceValues.put(interfaceId, new InterfaceValue(interfaceId, value));
        return true;
    }

    /**
     * Gets the interface values.
     *
     * @return The map of interface values.
     */
    public Map<Integer, InterfaceValue> getInterfaceValues() {
        return interfaceValues;
    }

}
