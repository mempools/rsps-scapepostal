package org.hyperion.rs2.model.player;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class Report {

    /**
     * Array that holds the rule name in rule number order.
     *
     * @author Cyberus
     */
    private static final String[] REASONS = {"Offensive language",
        "Item scamming", "Password scamming", "Bug abuse",
        "Jagex staff impersonation", "Account sharing/trading", "Macroing",
        "Multipile logging in", "Encouraging others to break the rules",
        "Misuse of customer support", "Advertising / website",
        "Real world item trading"};
    
   

}
