package org.hyperion.rs2.model.player;

import org.hyperion.rs2.util.TimeUtils;
import org.hyperion.rs2.util.TimeUtils.SpecificDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hyperion.rs2.model.Item;

/**
 * A player's profile that holds information for viewing, registration date,
 * activity, how long they've been on, pk points, stats, and etc, This will
 * mainly be for handling site registration data and other information explained
 * below and is mainly for the forum (RSC) use.
 * <p/>
 * Logging data off the player (trade logs, chat logs, reports, etc), and other
 * statistics data
 *
 * @author global <http://rune-server.org/members/global/>
 */
public class Profile {

    /**
     * The player.
     */
    private final Player player;
    /**
     * The player's user id.
     */
    private int userId;
    /**
     * The last date that the players recovery questions were set.
     */
    private String recoveryQuestionsLastSet = "never";
    /**
     * The last logged in time.
     */
    private long lastLoggedIn = 0;
    /**
     * The last connected host.
     */
    private String lastLoggedInFrom = "never";
    /**
     * The registration date.
     */
    private String registrationDate = "1/1/1111";
    /**
     * The infractions.
     */
    private int infractions;
    /**
     * The player's mail.
     */
    private List<MailMessage> mail = new ArrayList<>();

    /**
     * The player's profile
     *
     * @param player The player to get
     */
    public Profile(Player player) {
        this.player = player;
    }

    /**
     * Gets the coin value of the player's inventory and equipment
     *
     * @return The value.
     */
    public int getCarriedWealth() {
        int count = 0;
        for (Item equipment : player.getEquipment().toArray()) {
            if (equipment != null) {
                count += equipment.getDefinition().getValue() * equipment.getCount();
            }
        }
        for (Item inventory : player.getInventory().toArray()) {
            if (inventory != null) {
                count += inventory.getDefinition().getValue() * inventory.getCount();
            }
        }
        return count;
    }

    /**
     * Gets the amount of unread messages in the inbox.
     *
     * @return The unread messages count
     */
    public int getUnreadMessages() {
        int unread = 0;
        for (MailMessage m : getInbox()) {
            if (m != null) {
                if (m.getStatus() == MailMessage.FLAG_UNREAD) {
                    unread++;
                }
            }
        }
        return unread;
    }

    /**
     * Gets the registration date
     *
     * @return The registration date in <code>SpecificDate</code> format
     */
    public SpecificDate getRegistrationDate() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM/dd/yyyy");
            Date date = new Date(sdf.parse(registrationDate).getTime());
            return new SpecificDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Gets the last set recovery questions
     *
     * @return the recoveryQuestionsLastSet
     */
    public String getRecoveryQuestionsLastSet() {
        return recoveryQuestionsLastSet;
    }

    /**
     * Gets the amount of days this player hasn't logged in since.
     *
     * @return The amount of days this player hasn't logged in since.
     */
    public int getLastLoggedInDays() {
        return TimeUtils.toJagexDateFormatFloor(lastLoggedIn - System.currentTimeMillis()); //floored as we only want it to send the "yesterday" string if it has actually been since yesterday
    }

    /**
     * Gets the last logged in date.
     *
     * @return the lastLoggedIn The last logged in
     */
    public long getLastLoggedIn() {
        return lastLoggedIn;
    }

    /**
     * Gets the last logged in from location
     *
     * @return the lastLoggedInFrom The last logged in from location
     */
    public String getLastLoggedInFrom() {
        return lastLoggedInFrom;
    }

    /**
     * Gets the user id.
     *
     * @return userId The user id.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Gets the amount of infractions
     *
     * @return infractions The infractions
     */
    public int getInfractions() {
        return infractions;
    }

    /**
     * Sets the user id.
     *
     * @param userId The user id.
     * @return The user Id
     */
    public int setUserId(int userId) {
        return this.userId = userId;
    }

    /**
     * Sets the last logged in from
     *
     * @param lastLoggedInFrom the lastLoggedInFrom to set
     * @return Sets the last logged in from location
     */
    public String setLastLoggedInFrom(String lastLoggedInFrom) {
        return this.lastLoggedInFrom = lastLoggedInFrom;
    }

    /**
     * Sets the registration date
     *
     * @param registrationDate The registration date to set
     * @return Sets the registration date
     */
    public String setRegistrationDate(String registrationDate) {
        return this.registrationDate = registrationDate;
    }

    /**
     * Sets the last recovery questions
     *
     * @param recoveryQuestionsLastSet the recoveryQuestionsLastSet to set
     * @return The last recovery question date
     */
    public String setRecoveryQuestionsLastSet(String recoveryQuestionsLastSet) {
        return this.recoveryQuestionsLastSet = recoveryQuestionsLastSet;
    }

    /**
     * Sets the last logged in date.
     *
     * @param lastLoggedIn the lastLoggedIn to set
     * @return The last logged in date
     */
    public long setLastLoggedIn(long lastLoggedIn) {
        return this.lastLoggedIn = lastLoggedIn;
    }

    /**
     * Sets the amount of infractions
     *
     * @param infractions The amount to set
     * @return The infractions
     */
    public int setInfractions(int infractions) {
        return this.infractions = infractions;
    }

    /**
     * Gets the list of mail
     *
     * @return The list of mail
     */
    public List<MailMessage> getInbox() {
        return mail;
    }

    /**
     * Sets the mail
     *
     * @param mail The mail to add
     */
    public void setMail(List<MailMessage> mail) {
        this.mail = mail;
    }
}
