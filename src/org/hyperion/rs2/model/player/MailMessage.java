package org.hyperion.rs2.model.player;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class MailMessage {

    /**
     * The mail status.
     */
    public static final int FLAG_UNREAD = 0;
    public static final int FLAG_READ = 1;

    /**
     * The creator.
     */
    private final String creator;

    /**
     * The receiver..
     */
    private final String receiver;

    /**
     * The mail subject.
     */
    private final String subject;

    /**
     * The mail status.
     */
    private int status;

    /**
     * The mail message.
     */
    private final String message;

    /**
     * Constructs a mail
     *
     * @param recipient The creator
     * @param to The to address
     * @param subject The mail subject
     * @param message The message
     */
    public MailMessage(String recipient, String to, String subject, String message) {
        this.creator = recipient;
        this.receiver = to;
        this.status = 0;
        this.subject = subject;
        this.message = message;
    }

    /**
     * Gets the creator
     *
     * @return The creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * Gets the receiver
     *
     * @return The receiver
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * Gets the mail status
     *
     * @return The mail status
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the mail status
     *
     * @param status The status to set
     * @return The mail status
     */
    public int setStatus(int status) {
        return this.status = status;
    }

    /**
     * Gets the mail subject
     *
     * @return The mail subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Gets the mail message
     *
     * @return The message
     */
    public String getMessage() {
        return message;
    }

}
