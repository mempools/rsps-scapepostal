package org.hyperion.rs2.model.player;

import org.hyperion.rs2.model.Animation;

/**
 * The weapon animations class
 *
 * @author Sir Sean
 */
public class EquipmentAnimations {

    /**
     * The default stand animation
     */
    private static final Animation STAND = Animation.create(808);
    /**
     * The default walk animation
     */
    private static final Animation WALK = Animation.create(819);
    /**
     * The default run animation
     */
    private static final Animation RUN = Animation.create(824);
    /**
     * The default attack animation(s)
     */
    private static final Animation[] ATTACK = new Animation[]{Animation.create(422), Animation.create(423), Animation.create(422), Animation.create(422)};
    /**
     * The default defend animation
     */
    private static final Animation DEFEND = Animation.create(424);
    /**
     * The default stand turning animation
     */
    private static final Animation STAND_TURN = Animation.create(823);
    /**
     * The default 180 turn animation
     */
    private static final Animation TURN_180 = Animation.create(820);
    /**
     * The default clockwise turn
     */
    private static final Animation TURN_90_CLOCKWISE = Animation.create(821);
    /**
     * The default counter clockwise turn
     */
    private static final Animation TURN_90_COUNTER_CLOCKWISE = Animation.create(822);
    /**
     *
     * The stand animation.
     */
    private Animation standAnimation;
    /**
     * The run animation.
     */
    private Animation runAnimation;
    /**
     * The walk animation.
     */
    private Animation walkAnimation;
    /**
     * The attack animation
     */
    private Animation[] attackAnimation;
    /**
     * The death animation
     */
    private Animation deathAnimation;
    /**
     * The defend animation
     */
    private Animation defendAnimation;
    /**
     * The stand-turn animation.
     */
    private Animation standTurnAnimation;
    /**
     * The turn 90 clockwise animation.
     */
    private Animation turn90CWAnimation;
    /**
     * The turn 90 counter clockwise animation.
     */
    private Animation turn90CCWAnimation;
    /**
     * The turn 180 animation.
     */
    private Animation turn180Animation;

    /**
     * Constructor
     */
    public EquipmentAnimations() {
    }

    /**
     * Sets the defaults
     *
     * @return The class instance for chaining.
     */
    public final EquipmentAnimations setDefault() {
        setStandAnimation(Animation.create(808));
        setRunAnimation(Animation.create(824));
        setWalkAnimation(Animation.create(819));
        setStandTurnAnimation(Animation.create(823));
        setTurn90CWAnimation(Animation.create(821));
        setTurn90CCWAnimation(Animation.create(822));
        setTurn180Animation(Animation.create(820));
        setAttackAnimations(new Animation[]{Animation.create(422), Animation.create(423), Animation.create(422), Animation.create(422)});
        return this;
    }

    /**
     * Gets the stand animation
     *
     * @return standAnimation The stand animation to set
     */
    public Animation getStandAnimation() {
        if (standAnimation == null) {
            return STAND;
        }
        return standAnimation;
    }

    /**
     * Sets the stand animation
     *
     * @param standAnimation The stand animation to set
     */
    public void setStandAnimation(Animation standAnimation) {
        this.standAnimation = standAnimation;
    }

    /**
     * Gets the run animation
     *
     * @return runAnimation The run animation
     */
    public Animation getRunAnimation() {
        if (runAnimation == null) {
            return RUN;
        }
        return runAnimation;
    }

    /**
     * Sets the run animation
     *
     * @param runAnimation The run animation to set
     */
    public void setRunAnimation(Animation runAnimation) {
        this.runAnimation = runAnimation;
    }

    /**
     * Gets the walk animation
     *
     * @return walkAnimation The walk animation
     */
    public Animation getWalkAnimation() {
        if (walkAnimation == null) {
            return WALK;
        }
        return walkAnimation;
    }

    /**
     * Sets the walk animation
     *
     * @param walkAnimation The walk animation to set
     */
    public void setWalkAnimation(Animation walkAnimation) {
        this.walkAnimation = walkAnimation;
    }

    /**
     * Gets the stand turn animation
     *
     * @return standTurnAnimation The stand turn animation
     */
    public Animation getStandTurnAnimation() {
        if (standTurnAnimation == null) {
            return STAND_TURN;
        }
        return standTurnAnimation;
    }

    /**
     * Sets the stand turn animation
     *
     * @param standTurnAnimation The stand turn animation to set
     * @return The animation
     */
    public Animation setStandTurnAnimation(Animation standTurnAnimation) {
        return this.standTurnAnimation = standTurnAnimation;
    }

    /**
     * Gets the attack animations
     *
     * @return attackAnimation The attack animations
     */
    public Animation[] getAttackAnimations() {
        if (attackAnimation == null) {
            return ATTACK;
        }
        return attackAnimation;
    }

    /**
     * Sets the attack animations
     *
     * @param attackAnimation The attack animations to set
     * @return attackAnimation The attack animations
     */
    public Animation[] setAttackAnimations(Animation[] attackAnimation) {
        return this.attackAnimation = attackAnimation;
    }

    /**
     * Gets the attack animation
     *
     * @param index The index
     * @return attackAnimation The attack animation
     */
    public Animation getAttackAnimation(int index) {
        return attackAnimation[index];
    }

    /**
     * Sets the attack animation
     *
     * @param index The index
     * @param attackAnimation The attack animation to set
     */
    public void setAttackAnimation(int index, Animation attackAnimation) {
        this.attackAnimation[index] = attackAnimation;
    }

    /**
     * Gets the death animation
     *
     * @return deathAnimation The death animation
     */
    public Animation getDeathAnimation() {
        return deathAnimation;
    }

    /**
     * Sets the death animation
     *
     * @param deathAnimation The death animation to set
     */
    public void setDeathAnimation(Animation deathAnimation) {
        this.deathAnimation = deathAnimation;
    }

    /**
     * Gets the defend animation
     *
     * @return defendAnimation The defend animation
     */
    public Animation getDefendAnimation() {
        if (defendAnimation == null) {
            return DEFEND;
        }
        return defendAnimation;
    }

    /**
     * Sets the defend animation
     *
     * @param defendAnimation The defend animation to set
     */
    public void setDefendAnimation(Animation defendAnimation) {
        this.defendAnimation = defendAnimation;
    }

    /**
     * Gets the turn 90CW animation
     *
     * @return turn90ClockwiseAnimation The turn 90CW animation
     */
    public Animation getTurn90CW() {
        if (turn90CWAnimation == null) {
            return TURN_90_CLOCKWISE;
        }
        return turn90CWAnimation;
    }

    /**
     * Sets the turn 90CW animation
     *
     * @param turn90CWAnimation The turn 90CW animation to set
     */
    public void setTurn90CWAnimation(Animation turn90CWAnimation) {
        this.turn90CWAnimation = turn90CWAnimation;
    }

    /**
     * Gets the turn 90CC animation
     *
     * @return turn90CCWAnimation The turn 90CC animation
     */
    public Animation getTurn90CCWAnimation() {
        if (turn90CCWAnimation == null) {
            return TURN_90_COUNTER_CLOCKWISE;
        }
        return turn90CCWAnimation;
    }

    /**
     * Sets the turn 90CC animation
     *
     * @param turn90CCWAnimation The turn 90CC animation to set
     */
    public void setTurn90CCWAnimation(Animation turn90CCWAnimation) {
        this.turn90CCWAnimation = turn90CCWAnimation;
    }

    /**
     * Gets the turn 180 animation
     *
     * @return turn180Animation The turn 180 animation
     */
    public Animation getTurn180Animation() {
        if (turn180Animation == null) {
            return TURN_180;
        }
        return turn180Animation;
    }

    /**
     * Sets the turn 180 animation
     *
     * @param turn180Animation The turn 180 animation to set
     * @return The animation.
     */
    public Animation setTurn180Animation(Animation turn180Animation) {
        return this.turn180Animation = turn180Animation;
    }
}
