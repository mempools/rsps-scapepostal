package org.hyperion.rs2.model.player;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.util.NameUtils;

/**
 * Creates a dummy player.
 *
 * @author global <http://rune-server.org/members/global/>
 */
public class DummyPlayer extends Player {

    /**
     * Creates the dummy player
     */
    public DummyPlayer() {
        super(new PlayerDetails(null, "", "", 0, null, null, null));
        setName("Gaylord Douchebag");
        setPassword("password");
        setRights(Rights.MODERATOR);
        setNameLong(NameUtils.nameToLong(getName()));
        setMembers(true);
        setLocation(Actor.DEFAULT_LOCATION);
        setActive(true);

        getEquipment().add(new Item(1042), Equipment.SLOT_HELM);
        getEquipment().add(new Item(4151), Equipment.SLOT_WEAPON);
        getEquipment().add(new Item(1094), Equipment.SLOT_BOTTOMS);
    }

    @Override
    public void write(Packet packet) {
        //do nothing since it's a dummy
    }

}
