package org.hyperion.rs2.model.player;

import java.util.Date;
import org.hyperion.rs2.util.TimeUtils.SpecificDate;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class Membership {

    /**
     * The player.
     */
    private final Player player;

    /**
     * Today's date.
     */
    private final Date todaysDate = new Date();

    /**
     * The start date.
     */
    private SpecificDate startDate;

    /**
     * The expiration date.
     */
    private SpecificDate expirationDate = new SpecificDate(1, 6, 113);//june 6, 2014

    /**
     * Constructs the membership class
     *
     * @param player
     */
    public Membership(Player player) {
        this.player = player;
    }

    /**
     * Gets the start date of the membership
     *
     * @return The start date
     */
    public SpecificDate getStartDate() {
        return startDate;
    }

    /**
     * Gets the players membership expiration date.
     *
     * @return The players membership expiration date.
     */
    public SpecificDate getExpirationDate() {
        return expirationDate;
    }

    /**
     * Gets the amount of days remaining this player has left of membership.
     *
     * @return The amount of days remaining this player has left of membership.
     */
    public int getDaysOfMembership() {
        return Days.daysBetween(new DateTime(expirationDate.getDate()), new DateTime(todaysDate)).getDays();
    }

    /**
     * Sets the players membership expiry date.
     *
     * @param date The date
     * @return The specific date
     */
    public SpecificDate setMembershipExpiryDate(SpecificDate date) {
        return this.expirationDate = date;
    }

    /**
     * Sets the amount of membership days.
     *
     * @param days The amount of membership days to set.
     * @return The specific date
     */
    public SpecificDate setMembershipDays(int days) {
        return this.expirationDate = new SpecificDate(days, expirationDate.getMonth(), expirationDate.getYear());
    }

    /**
     * Sets the membership date
     *
     * @param date The date to set
     * @return The start date
     */
    public SpecificDate setStartDate(SpecificDate date) {
        return this.startDate = date;
    }

    /**
     * Checks if the player is a member
     *
     * @return Whether the player is a member or not.
     */
    public boolean isMembers() {
        if (player.isMembers() && startDate != null) {
            return true;
        }
        return todaysDate.before(expirationDate.getDate());
    }

}
