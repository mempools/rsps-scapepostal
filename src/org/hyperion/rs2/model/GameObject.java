package org.hyperion.rs2.model;

import org.hyperion.rs2.model.container.Container;
import org.hyperion.rs2.model.container.Container.Type;
import org.hyperion.rs2.model.container.Inventory;
import org.hyperion.rs2.model.definition.GameObjectDefinition;
import org.hyperion.rs2.model.region.Region;

/**
 * Represents a single game object.
 *
 * @author Graham Edgecombe
 */
public class GameObject extends Entity {

    /**
     * The location.
     */
    private Location location;
    /**
     * The definition.
     */
    private GameObjectDefinition definition;
    /**
     * The object container
     */
    private Container inventory = new Container(Type.ALWAYS_STACK, Inventory.SIZE);
    /**
     * The type.
     */
    private int type;
    /**
     * The rotation.
     */
    private int rotation;
    /**
     * The owner
     */
    private String owner;
    /**
     * If the object is global
     */
    private boolean global;

    /**
     * Creates the game object.
     *
     * @param definition The definition.
     * @param location The location.
     * @param type The type.
     * @param rotation The rotation.
     */
    public GameObject(GameObjectDefinition definition, Location location, int type, int rotation) {
        this.setLocation(location);
        this.definition = definition;
        this.location = location;
        this.type = type;
        this.rotation = rotation;
        this.owner = "";
        this.global = true;
    }

    /**
     * Creates a game object for a player.
     *
     * @param definition The definition
     * @param location The location
     * @param type The type
     * @param rotation The rotation
     * @param owner The owner
     */
    public GameObject(GameObjectDefinition definition, Location location, int type, int rotation, String owner) {
        this.setLocation(location);
        this.definition = definition;
        this.location = location;
        this.type = type;
        this.rotation = rotation;
        this.global = true;
        this.owner = owner;
    }

    /**
     * Gets the location.
     *
     * @return The location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Gets the definition.
     *
     * @return The definition.
     */
    public GameObjectDefinition getDefinition() {
        return definition;
    }

    /**
     * Gets the type.
     *
     * @return The type.
     */
    public int getType() {
        return type;
    }

    /**
     * Gets the rotation.
     *
     * @return The rotation.
     */
    public int getRotation() {
        return rotation;
    }

    /**
     * Gets the object owner
     *
     * @return The owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Checks if the object is global
     *
     * @return If global or not
     */
    public boolean isGlobal() {
        return global;
    }

    /**
     * Gets the object inventory
     *
     * @return The inventory
     */
    public Container getInventory() {
        return inventory;
    }

    @Override
    public int getSize() {
        return 3;
    }

    @Override
    public Location getCenterLocation() {
        return Location.create(getLocation().getX() + (getWidth() / 2),
                getLocation().getY() + (getHeight() / 2), getLocation().getZ());
    }

    @Override
    public int getClientIndex() {
        return 0;
    }

    @Override
    public int getHeight() {
        if (definition == null) {
            return 1;
        }
        return definition.getHeight();
    }

    @Override
    public int getWidth() {
        if (definition == null) {
            return 1;
        }
        return definition.getWidth();
    }

    @Override
    public boolean isNPC() {
        return false;
    }

    @Override
    public boolean isObject() {
        return true;
    }

    @Override
    public boolean isPlayer() {
        return false;
    }

    @Override
    public void addToRegion(Region region) {
        region.addObject(this);
    }

    @Override
    public void removeFromRegion(Region region) {
//		region.removeObject(this); //TODO: for now..
        this.setRegion(null);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof GameObject)) {
            return false;
        }
        GameObject obj = (GameObject) other;
        return obj.getLocation().equals(this.getLocation())
                && obj.getDefinition().getId() == this.getDefinition().getId() && obj.getType() == this.getType();
    }
}
