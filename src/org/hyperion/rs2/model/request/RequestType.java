package org.hyperion.rs2.model.request;

import org.hyperion.rs2.model.request.impl.TradeRequest;

/**
 * Represents the different types of request.
 *
 * @author Graham Edgecombe
 */
public enum RequestType {

    /**
     * A trade request.
     */
    TRADE(":tradereq:", new TradeRequest()),
    /**
     * A duel request.
     */
    DUEL(":duelreq:", null);
    /**
     * The client-side name of the request.
     */
    private final String clientName;
    /**
     * The request listener for this request.
     */
    private final RequestListener listener;

    /**
     * Creates a type of request.
     *
     * @param clientName The name of the request client-side.
     * @param listener The request listener.
     */
    private RequestType(String clientName, RequestListener listener) {
        this.clientName = clientName;
        this.listener = listener;
    }

    /**
     * Gets the client name.
     *
     * @return The client name.
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * Gets the request listener.
     *
     * @return The request listener.
     */
    public RequestListener getRequestListener() {
        return listener;
    }
}
