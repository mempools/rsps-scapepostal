package org.hyperion.rs2.model.request;

import org.hyperion.rs2.model.player.Player;

/**
 * The request manager manages
 *
 * @author Graham Edgecombe
 */
public class RequestManager {

    /**
     * Holds the different states the manager can be in.
     *
     * @author Graham Edgecombe
     */
    public enum RequestState {

        /**
         * Nobody has offered a request.
         */
        NORMAL,
        /**
         * Somebody has offered some kind of request.
         */
        REQUESTED,
        /**
         * The player is participating in an existing request of this type, so
         * cannot accept new requests at all.
         */
        PARTICIPATING,
        /**
         * The player has confirmed on the first screen.
         */
        CONFIRM_1,
        /**
         * The player has confirmed on the second screen.
         */
        CONFIRM_2,
        /**
         * The player has finished
         */
        FINISHED;
    }

    /**
     * The player.
     */
    private final Player player;
    /**
     * The current 'acquaintance'.
     */
    private Player acquaintance;
    /**
     * The current state.
     */
    private RequestState state = RequestState.NORMAL;
    /**
     * The current request type.
     */
    private RequestType requestType;

    /**
     * Creates the request manager.
     *
     * @param player The player whose requests the manager is managing.
     */
    public RequestManager(Player player) {
        this.player = player;
    }

    /**
     * Requests a type of request.
     *
     * @param type The type of request.
     * @param acquaintance The other player.
     */
    public void request(RequestType type, Player acquaintance) {
        if (acquaintance.isBusy()) {
            player.getActionSender().sendMessage("Other player is busy at the moment.");
            return;
        }
        player.getActionQueue().cancelQueuedActions();
        player.getRequestManager().setAcquaintance(acquaintance);
        player.getRequestManager().setRequestType(type);

        if (acquaintance.getRequestManager().getAcquaintance() == player
                && acquaintance.getRequestManager().getRequestType() == type) {
            player.getRequestManager().setState(RequestState.PARTICIPATING);
            acquaintance.getRequestManager().setState(RequestState.PARTICIPATING);
            getRequestType().getRequestListener().accepted(player, acquaintance);
        } else if (getState() != RequestState.PARTICIPATING) {
            player.getActionSender().sendMessage("Sending " + type.getClientName().replace("req", "").replace(":", "") + " offer...");
            acquaintance.getActionSender().sendMessage(player.getName() + type.getClientName());
        }
    }

    /**
     * Cancels a request.
     */
    public void cancelRequest() {
        if (player.getRequestManager().getState() != RequestState.NORMAL) {
            player.getRequestManager().setState(RequestState.NORMAL);

            if (getAcquaintance().getRequestManager().getAcquaintance() == player
                    && getAcquaintance().getRequestManager().getRequestType() == requestType) {
                getAcquaintance().getRequestManager().setRequestType(null);
                getAcquaintance().getRequestManager().setAcquaintance(null);
                getAcquaintance().getRequestManager().setState(RequestState.NORMAL);
            }

            getRequestType().getRequestListener().cancelled(player, acquaintance);
            setRequestType(null);
            setAcquaintance(null);
        }
    }

    /**
     * Finishes a request.
     */
    public void finishRequest() {
        if (getState() != RequestState.FINISHED) {
            setState(RequestState.FINISHED);
            if (acquaintance.getRequestManager().getAcquaintance() == player
                    && acquaintance.getRequestManager().getRequestType() == requestType) {
                acquaintance.getRequestManager().setState(RequestState.NORMAL);//we don't want the acquaintance to go cancelling our claim!
                acquaintance.getRequestManager().setAcquaintance(null);
                acquaintance.getRequestManager().setState(RequestState.NORMAL);
            }
            this.getRequestType().getRequestListener().finished(player, acquaintance);
        }
    }

    /**
     * Gets the acquaintance
     *
     * @return acquaintance The acquaintance
     */
    public Player getAcquaintance() {
        return acquaintance;
    }

    /**
     * Gets the request state
     *
     * @return state The request state
     */
    public RequestState getState() {
        return state;
    }

    /**
     * Gets the request type
     *
     * @return requestType The request type
     */
    public RequestType getRequestType() {
        return requestType;
    }

    /**
     * Sets the acquaintance
     *
     * @param acquaintance The acquaintance to set
     * @return The player.
     */
    public Player setAcquaintance(Player acquaintance) {
        return this.acquaintance = acquaintance;
    }

    /**
     * Sets the request state
     *
     * @param state The request state to set
     * @return The request state
     */
    public RequestState setState(RequestState state) {
        return this.state = state;
    }

    /**
     * Sets request type
     *
     * @param requestType The request type to set
     * @return The request state
     */
    public RequestType setRequestType(RequestType requestType) {
        return this.requestType = requestType;
    }
}
