package org.hyperion.rs2.model.request.impl;

import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.container.Trade;
import org.hyperion.rs2.model.request.RequestListener;

/**
 * A class which listens to trade requests.
 *
 * @author Graham Edgecombe
 */
public class TradeRequest implements RequestListener {

    @Override
    public void accepted(Player player, Player partner) {
        Trade.open(player, partner);
    }

    @Override
    public void cancelled(Player player, Player partner) {
        if (player.getTrade().size() > 0) {
            for (Item item : player.getTrade().toArray()) {
                if (item != null) {
                    player.getInventory().add(item);
                }
            }
            player.getTrade().clear();
        }
        if (partner.getTrade().size() > 0) {
            for (Item item : partner.getTrade().toArray()) {
                if (item != null) {
                    partner.getInventory().add(item);
                }
            }
            partner.getTrade().clear();
        }
        player.getActionSender().removeAllWindows();
        player.getActionSender().sendMessage("Trade cancelled.");

        partner.getActionSender().removeAllWindows();
        partner.getActionSender().sendMessage("The trade was cancelled by the other player.");
    }

    @Override
    public void finished(Player player, Player partner) {
        if (partner.getTrade().size() > 0) {
            for (Item item : partner.getTrade().toArray()) {
                if (item != null) {
                    player.getInventory().add(item);
                }
            }
            partner.getTrade().clear();
        }
        if (player.getTrade().size() > 0) {
            for (Item item : player.getTrade().toArray()) {
                if (item != null) {
                    partner.getInventory().add(item);
                }
            }
            player.getTrade().clear();
        }
        player.getActionSender().removeAllWindows();
        player.getActionSender().sendMessage("Trade complete.");
        player.getRequestManager().setRequestType(null);
        player.getRequestManager().setAcquaintance(null);

        partner.getActionSender().removeAllWindows();
        partner.getActionSender().sendMessage("Trade complete.");
        partner.getRequestManager().setRequestType(null);
        partner.getRequestManager().setAcquaintance(null);
    }
}
