package org.hyperion.rs2.model;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.DummyPlayer;
import org.hyperion.rs2.model.region.Region;

/**
 * Represents a game object in the world with a location.
 *
 * @author Graham Edgecombe
 */
public abstract class Entity {

    /**
     * The current location.
     */
    private Location location;
    /**
     * The last known map region.
     */
    private Location lastKnownRegion = this.getLocation();
    /**
     * The last location
     */
    private Location lastLocation;
    /**
     * The current region.
     */
    private Region currentRegion;

    /**
     * Sets the last location
     *
     * @param location The location to set
     * @return The last location
     */
    public Location setLastLocation(Location location) {
        return this.lastLocation = location;
    }

    /**
     * Gets the current location.
     *
     * @return The current location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Sets the current location.
     *
     * @param location The current location.
     */
    public void setLocation(Location location) {
        setLastLocation(this.getLocation());
        this.location = location;
        Region newRegion = World.getWorld().getRegionManager().getRegionByLocation(location);
        if (newRegion != getRegion()) {
            if (getRegion() != null) {
                removeFromRegion(getRegion());
            }
            setRegion(newRegion);
            addToRegion(getRegion());
        }
    }

    /**
     * Gets the last location after dying
     *
     * @return The last location
     */
    public Location getLastLocation() {
        return lastLocation;
    }

    /**
     * Gets the last known map region.
     *
     * @return The last known map region.
     */
    public Location getLastKnownRegion() {
        return lastKnownRegion;
    }

    /**
     * Sets the last known map region.
     *
     * @param lastKnownRegion The last known map region.
     */
    public void setLastKnownRegion(Location lastKnownRegion) {
        this.lastKnownRegion = lastKnownRegion;
    }

    /**
     * Gets the current region.
     *
     * @return The current region.
     */
    public Region getRegion() {
        return currentRegion;
    }

    /**
     * Sets the current region.
     *
     * @param region The region to set.
     */
    public void setRegion(Region region) {
        this.currentRegion = region;
    }

    /**
     * Create a projectile
     *
     * @param pd The projectile to create.
     * @param caster The caster
     * @param target The target
     */
    public void playProjectile(Projectile pd, Entity caster, Entity target) {
        for (Region region : currentRegion.getSurroundingRegions()) {
            for (Player player : region.getPlayers()) {
                if (player.getLocation().isWithinDistance(this.getLocation())) {
                    player.getActionSender().sendProjectile(caster.getLocation().applySizeDistoration(caster.getSize()), target.getLocation(), target, pd.getGraphic(), pd.getInitHeight(), pd.getEndHeight(), pd.getDelay(), pd.getSpeed(), pd.getCurvature(), pd.getRadius());
                }
            }
        }
    }

    /**
     * If the character is an A.I. aka fake player (dummy player)
     *
     * @return If a fake player or not
     */
    public boolean isAi() {
        return this instanceof DummyPlayer;
    }

    /**
     * The size of the entity
     *
     * @return The size of the entity
     */
    public abstract int getSize();

    /**
     * Gets the width of the entity.
     *
     * @return The width of the entity.
     */
    public abstract int getWidth();

    /**
     * Gets the width of the entity.
     *
     * @return The width of the entity.
     */
    public abstract int getHeight();

    /**
     * Gets the center location of the entity.
     *
     * @return The center location of the entity.
     */
    public abstract Location getCenterLocation();

    /**
     * Is this entity a player.
     *
     * @return If {@link Entity} is a {@link Player}, then {@code true} and if
     * not {@code false}.
     */
    public abstract boolean isPlayer();

    /**
     * Is this entity an NPC.
     *
     * @return If {@link Entity} is an {@link NPC}, then {@code true} and if not
     * {@code false}.
     */
    public abstract boolean isNPC();

    /**
     * Is this entity an Object.
     *
     * @return If {@link Entity} is a {@link GameObject}, then {@code true} and
     * if not {@code false}.
     */
    public abstract boolean isObject();

    /**
     * Gets the client-side index of an entity.
     *
     * @return The client-side index.
     */
    public abstract int getClientIndex();

    /**
     * Removes this entity from the specified region.
     *
     * @param region The region.
     */
    public abstract void removeFromRegion(Region region);

    /**
     * Adds this entity to the specified region.
     *
     * @param region The region.
     */
    public abstract void addToRegion(Region region);

    /**
     * Destroys this entity.
     */
    public void destroy() {
        removeFromRegion(currentRegion);
    }
}
