package org.hyperion.rs2.model.npc;

import org.hyperion.rs2.model.definition.NPCDefinition;
import org.hyperion.rs2.model.region.Region;
import org.hyperion.util.xStream;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.hyperion.rs2.model.Area;
import org.hyperion.rs2.model.Direction;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.tickable.impl.RandomWalkTick;

/**
 * @author Rs2-474 Team
 */
public class NPCSpawnManager {

    /**
     * The logger instance.
     */
    private static final Logger logger = Logger.getLogger(NPCSpawnManager.class.getName());
    /**
     * The spawn list
     */
    private static final List<SpawnData> spawnList = new ArrayList<>();

    /**
     * Gets the spawn lists.
     *
     * @return spawnList The spawn list.
     */
    public static List getSpawnList() {
        return spawnList;
    }

    /**
     * Loads the NPC spawn list from XML, and spawns all the NPC's.
     *
     * @throws IOException
     */
    public static void parse() throws IOException {
        logger.info("Loading NPC spawns...");
        xStream.alias("npcSpawns", SpawnData.class);
        List<SpawnData> spawns = xStream.readXML(new File("./data/npcSpawns.xml"));
        for (SpawnData spawn : spawns) {
            NPCDefinition def = NPCDefinition.forId(spawn.getId());
            if (def != null) {
                NPC npc = new NPC(def, spawn);
                npc.setLocation(spawn.getLocation());
                spawnList.add(spawn);
                Region region = World.getWorld().getRegionManager().getRegionByLocation(npc.getLocation());
                region.addNpc(npc);
                World.getWorld().register(npc);
                npc.face(spawn.getLocation());
                World.getWorld().submit(new RandomWalkTick(npc, spawn.getRandomWalkTick()));
            }
        }
        logger.info("Loaded " + spawnList.size() + " NPC spawns.");
    }

    /**
     * spawns an NPC.
     *
     * @param data The spawn data
     */
    public static void spawn(SpawnData data) {
        NPCDefinition def = NPCDefinition.forId(data.getId());
        if (def != null) {
            NPC npc = new NPC(def, data);
            npc.setLocation(data.getLocation());
            Region region = World.getWorld().getRegionManager().getRegionByLocation(npc.getLocation());
            region.addNpc(npc);
            World.getWorld().register(npc);
        }
    }

    /**
     * The spawn data for an {@link NPC}.
     */
    public static class SpawnData {

        /**
         * The NPC ID to spawn.
         */
        private final int npcId;
        /**
         * The re-spawn tick.
         */
        private final int respawnTick;
        /**
         * The random walk tick.
         */
        private final int randomWalkTick;
        /**
         * The direction the spawned NPC should face.
         */
        private final Direction direction;
        /**
         * The spawn location.
         */
        private final Location spawnLocation;
        /**
         * The walking area.
         */
        private final Area walkArea;

        /**
         * The NPC spawn data
         *
         * @param npcId The NPC id
         * @param respawnTick The re-spawn tick
         * @param randomWalkTick The random walk tick
         * @param spawnLocation The spawn location
         * @param direction The NPX facing direction
         * @param walkArea The walk area
         */
        public SpawnData(int npcId, int respawnTick, int randomWalkTick, Direction direction, Location spawnLocation, Area walkArea) {
            this.npcId = npcId;
            this.respawnTick = respawnTick;
            this.randomWalkTick = randomWalkTick;
            this.spawnLocation = spawnLocation;
            this.walkArea = walkArea;
            this.direction = direction;
        }

        /**
         * Gets the id of this NPCSpawnManager.
         *
         * @return The NPC id.
         */
        private int getId() {
            return npcId;
        }

        /**
         * Gets the re-spawn tick
         *
         * @return The re-spawn tick
         */
        public int getRespawnTick() {
            return respawnTick;
        }

        /**
         * Gets the random walk tick
         *
         * @return The random walk tick
         */
        public int getRandomWalkTick() {
            return randomWalkTick;
        }

        /**
         * Gets the facing direction
         *
         * @return The direction.
         */
        public Direction getDirection() {
            return direction;
        }

        /**
         * Gets the location associated with this NPCSpawnManager.
         *
         * @return The spawn location.
         */
        public Location getLocation() {
            return spawnLocation;
        }

        /**
         * Gets the walk area
         *
         * @return walkArea The walk area
         */
        public Area getWalkArea() {
            return walkArea;
        }
    }
}
