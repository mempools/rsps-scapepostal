package org.hyperion.rs2.model.npc;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.Area;
import org.hyperion.rs2.model.GroundItem;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.definition.NPCDefinition;
import org.hyperion.rs2.model.npc.NPCSpawnManager.SpawnData;
import org.hyperion.rs2.model.combat.CombatAction;
import org.hyperion.rs2.model.combat.impl.Melee;
import org.hyperion.rs2.model.container.Container;
import org.hyperion.rs2.model.region.Region;
import org.hyperion.rs2.net.ActionSender;

/**
 * <p>
 * Represents a non-player character in the in-game world.</p>
 *
 * @author Graham Edgecombe
 */
public class NPC extends Actor {

    /**
     * The definition.
     */
    private final NPCDefinition definition;
    /**
     * The Spawn data.
     */
    private final SpawnData spawnData;
    /**
     * The combat cool down delay.
     */
    private final int combatCooldownDelay = 4;

    /**
     * Creates the NPC with the specified definition.
     *
     * @param definition The definition.
     * @param spawnData The spawn data
     */
    public NPC(NPCDefinition definition, SpawnData spawnData) {
        this.definition = definition;
        this.spawnData = spawnData;
        for (int i = 0; i < definition.getSkills().length; i++) {
            getSkills().setSkill(i, definition.getSkill(i), getSkills().getXPForLevel(definition.getSkill(i) + 1));
        }
    }

    /**
     * Gets the NPC definition.
     *
     * @return The NPC definition.
     */
    public NPCDefinition getDefinition() {
        return definition;
    }

    /**
     * Gets the NPC spawn data.
     *
     * @return The NPC Spawn data.
     */
    public SpawnData getSpawnData() {
        return spawnData;
    }

    /**
     * Checks if the NPC can move
     *
     * @return If can move or not
     */
    public boolean canMove() {
        if (isDead() || getWalkingArea() == null) {
            return false;
        }
        Location maxLocation = getWalkingArea().getFirstPoint();
        Location minLocation = getWalkingArea().getSecondPoint();
        //getLocation -> getSpawnLocation
        return minLocation != null && maxLocation != null && !(minLocation == getLocation() && maxLocation == getLocation());
    }

    @Override
    public void dropLoot(Actor killer) {
        GroundItem.create(killer.getName(), new Item(526), getLocation());
    }

    @Override
    public String getName() {
        return definition.getName();
    }

    @Override
    public void addToRegion(Region region) {
        region.addNpc(this);
    }

    @Override
    public void removeFromRegion(Region region) {
        region.removeNpc(this);
    }

    @Override
    public int getClientIndex() {
        return this.getIndex();
    }

    @Override
    public boolean isNPC() {
        return true;
    }

    @Override
    public boolean isPlayer() {
        return false;
    }

    @Override
    public boolean isObject() {
        return false;
    }

    @Override
    public ActionSender getActionSender() {
        return null;
    }

    @Override
    public Animation getAttackAnimation() {
        return definition.getAttackAnimation();
    }

    @Override
    public Animation getDeathAnimation() {
        return definition.getDeathAnimation();
    }

    @Override
    public Animation getDefendAnimation() {
        return Animation.DEFAULT;
    }

    @Override
    public CombatAction getDefaultCombatAction() {
        return Melee.getAction();
    }

    @Override
    public int getCombatCooldownDelay() {
        return combatCooldownDelay;
    }

    @Override
    public int getSize() {
        return definition.getSize();
    }

    @Override
    public int getWidth() {
        return getSize();
    }

    @Override
    public int getHeight() {
        return getSize();
    }

    @Override
    public Location getCenterLocation() {
        return Location.create(getLocation().getX() + (getWidth() / 2), getLocation().getY() + (getHeight() / 2), getLocation().getZ());
    }

    @Override
    public Area getWalkingArea() {
        return spawnData.getWalkArea();
    }

}
