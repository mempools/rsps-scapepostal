package org.hyperion.rs2.model;

import org.hyperion.rs2.pf.LineOfSightPathFinder;
import org.hyperion.rs2.pf.Path;
import org.hyperion.rs2.pf.PathFinder;

import java.util.ArrayList;
import java.util.List;
import org.hyperion.rs2.model.region.Region;

/**
 * Represents a single location in the game world.
 *
 * @author Graham Edgecombe
 */
public class Location {

    /**
     * The x coordinate.
     */
    private final int x;
    /**
     * The y coordinate.
     */
    private final int y;
    /**
     * The z coordinate.
     */
    private final int z;

    /**
     * The direction delta
     */
    private static final int[][] directionDelta = new int[][]{{0, -1}, {1, 0}, {0, 1}, {-1, 0}, {1, -1}, {1, 1}, {-1, 1}, {-1, -1}};

    /**
     * Creates a location.
     *
     * @param x The x coordinate.
     * @param y The y coordinate.
     * @param z The z coordinate.
     * @return The location.
     */
    public static Location create(int x, int y, int z) {
        return new Location(x, y, z);
    }

    /**
     * Creates a location.
     *
     * @param x The x coordinate.
     * @param y The y coordinate.
     * @return The location.
     */
    public static Location create(int x, int y) {
        return new Location(x, y, 0);
    }

    /**
     * Creates a location.
     *
     * @param x The x coordinate.
     * @param y The y coordinate.
     * @param z The z coordinate.
     */
    private Location(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Gets the absolute x coordinate.
     *
     * @return The absolute x coordinate.
     */
    public int getX() {
        return x;
    }

    /**
     * Gets the absolute y coordinate.
     *
     * @return The absolute y coordinate.
     */
    public int getY() {
        return y;
    }

    /**
     * Gets the z coordinate, or height.
     *
     * @return The z coordinate.
     */
    public int getZ() {
        return z;
    }

    /**
     * Gets the local x coordinate relative to this region.
     *
     * @return The local x coordinate relative to this region.
     */
    public int getLocalX() {
        return getLocalX(this);
    }

    /**
     * Gets the local y coordinate relative to this region.
     *
     * @return The local y coordinate relative to this region.
     */
    public int getLocalY() {
        return getLocalY(this);
    }

    /**
     * Gets the local x coordinate relative to a specific region.
     *
     * @param l The region the coordinate will be relative to.
     * @return The local x coordinate.
     */
    public int getLocalX(Location l) {
        return x - 8 * l.getRegionX();
    }

    /**
     * Gets the local y coordinate relative to a specific region.
     *
     * @param l The region the coordinate will be relative to.
     * @return The local y coordinate.
     */
    public int getLocalY(Location l) {
        return y - 8 * l.getRegionY();
    }

    /**
     * Gets the region x coordinate.
     *
     * @return The region x coordinate.
     */
    public int getRegionX() {
        return (x >> 3) - 6;
    }

    /**
     * Gets the region y coordinate.
     *
     * @return The region y coordinate.
     */
    public int getRegionY() {
        return (y >> 3) - 6;
    }

    /**
     * Checks if this location is within range of another.
     *
     * @param other The other location.
     * @return <code>true</code> if the location is in range, <code>false</code>
     * if not.
     */
    public boolean isWithinDistance(Location other) {
        if (z != other.z) {
            return false;
        }
        int deltaX = other.x - x, deltaY = other.y - y;
        return deltaX <= 14 && deltaX >= -15 && deltaY <= 14 && deltaY >= -15;
    }

    /**
     * Checks if a coordinate is within range of another.
     *
     * @param victim The victim
     * @param distance The distance
     * @return <code>true</code> if the location is in range, <code>false</code>
     * if not.
     */
    public boolean isWithinDistance(Actor victim, int distance) {
        if (x >= (victim.getCenterLocation().getX() - distance)
                && x <= (victim.getCenterLocation().getX()
                + (victim.getWidth() > 1 ? victim.getWidth() : 0)
                + distance) && getY() >= (victim.getCenterLocation().getY() - distance)
                && getY() <= (victim.getCenterLocation().getY()
                + (victim.getHeight() > 1 ? victim.getHeight() : 0) + distance)) {
            if (victim.getWidth() <= 1 && victim.getHeight() <= 1 && distance == 1) {
                return (getX() == victim.getCenterLocation().getX() && getY() != victim.getCenterLocation().getY()
                        || getX() != victim.getCenterLocation().getX() && getY() == victim.getCenterLocation().getY());
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a coordinate is within range of another.
     *
     * @param source The source
     * @param other The other
     * @param distance The distance
     * @return <code>true</code> if the location is in range, <code>false</code>
     * if not.
     */
    public boolean isWithinDistance(Actor source, Actor other, int distance) {
        Location myClosestTile = this.closestTileOf(other.getLocation(), source.getWidth(), source.getHeight());
        Location theirClosestTile = other.getLocation().closestTileOf(this, other.getWidth(), other.getHeight());

        return myClosestTile.distanceToPoint(theirClosestTile) <= distance;
    }

    /**
     * Checks if a coordinate is within range of another.
     *
     * @param location The location
     * @param distance The distance
     * @return <code>true</code> if the location is in range, <code>false</code>
     * if not.
     */
    public boolean isWithinDistance(Location location, int distance) {
        int objectX = location.getX();
        int objectY = location.getY();
        for (int i = 0; i <= distance; i++) {
            for (int j = 0; j <= distance; j++) {
                if ((objectX + i) == x && ((objectY + j) == y || (objectY - j) == y || objectY == y)) {
                    return true;
                } else if ((objectX - i) == x && ((objectY + j) == y || (objectY - j) == y || objectY == y)) {
                    return true;
                } else if (objectX == x && ((objectY + j) == y || (objectY - j) == y || objectY == y)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if a coordinate is within range of another.
     *
     * @param other The other location
     * @param width The width
     * @param height The height
     * @param distance The distance
     * @return <code>true</code> if the location is in range, <code>false</code>
     * if not.
     */
    public boolean isWithinDistance(Location other, int width, int height, int distance) {
        return x >= (other.getX() - distance) && x <= (other.getX() + (width > 1 ? width : 0) + distance)
                && getY() >= (other.getY() - distance) && getY() <= (other.getY() + (height > 1 ? height : 0) + distance)
                && (!(width == 1 && height == 1 && distance == 1)
                || (getX() == other.getX() && getY() != other.getY()
                || getX() != other.getX() && getY() == other.getY()
                || getX() == other.getX() && getY() == other.getY()));
    }

    /**
     * Checks if this location is within interaction range of another.
     *
     * @param other The other location.
     * @param block The blocks. E.g. Halberds can hit 1 farther away from the
     * <param>other</param>. <param>block</param>
     * would be set to 3. SOURCE(other) | EMPTYBLOCK | ATTACKER
     * @return <code>true</code> if the location is in range, <code>false</code>
     * if not.
     */
    public boolean isWithinInteractionDistance(Location other, int block) {
        if (z != other.z) {
            return false;
        }
        int deltaX = other.x - x, deltaY = other.y - y;
        return deltaX <= block && deltaX >= -block && deltaY <= block && deltaY >= -block;
    }

    /**
     * Checks if this location is within interaction range of another.
     *
     * @param other The other location.
     * @return <code>true</code> if the location is in range, <code>false</code>
     * if not.
     */
    public boolean isWithinInteractionDistance(Location other) {
        if (z != other.z) {
            return false;
        }
        int deltaX = other.x - x, deltaY = other.y - y;
        return deltaX <= 2 && deltaX >= -3 && deltaY <= 2 && deltaY >= -3;
    }

    /**
     * Checks to see if in sight
     *
     * @param character The entity to check
     * @return <code>true</code> if in sight, <code>false</code> if not.
     */
    public boolean lineOfSight(Actor character) {
        PathFinder pf = new LineOfSightPathFinder();
        Location tile = closestTileToEntity(character);
        Path p = pf.findPath(this, tile);

        if (p == null) {
            return true;
        }

        if (p.getPoints().size() > 0 && (p.getPoints().getLast().getX() != tile.getX() || p.getPoints().getLast().getY() != tile.getY())) {
            return false;
        }

        return true;
    }

    /**
     * Gets the closest tile of this location from a specific point.
     *
     * @param from The from location
     * @param width The width
     * @param height The height
     * @return The location
     */
    public Location closestTileOf(Location from, int width, int height) {
        if (width < 2 && height < 2) {
            return this;
        }
        Location location = null;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Location loc = Location.create(this.x + x, this.y + y, this.z);
                if (location == null || loc.distanceToPoint(from) < location.distanceToPoint(from)) {
                    location = loc;
                }
            }
        }
        return location;
    }

    /**
     * Checks if a coordinate is within range of another.
     *
     * @param attacker The attacker
     * @param victim The victim
     * @return <code>true</code> if the location is in range, <code>false</code>
     * if not.
     */
    public int distanceToEntity(Actor attacker, Actor victim) {
        if (attacker.getWidth() == 1 && attacker.getHeight() == 1
                && victim.getWidth() == 1 && victim.getHeight() == 1) {
            return distanceToPoint(victim.getLocation());
        }
        int lowestDistance = 100;
        List<Location> myTiles = entityTiles(attacker);
        List<Location> theirTiles = entityTiles(victim);
        for (Location myTile : myTiles) {
            for (Location theirTile : theirTiles) {
                int dist = myTile.distanceToPoint(theirTile);
                if (dist <= lowestDistance) {
                    lowestDistance = dist;
                }
            }
        }
        return lowestDistance;
    }

    /**
     * The list of tiles this entity occupies.
     *
     * @param character The entity.
     * @return The list of tiles this entity occupies.
     */
    public List<Location> entityTiles(Actor character) {
        List<Location> myTiles = new ArrayList<>();
        myTiles.add(character.getLocation());
        if (character.getWidth() > 1) {
            for (int i = 1; i < character.getWidth(); i++) {
                myTiles.add(Location.create(character.getLocation().getX() + i,
                        character.getLocation().getY(), character.getLocation().getZ()));
            }
        }
        if (character.getHeight() > 1) {
            for (int i = 1; i < character.getHeight(); i++) {
                myTiles.add(Location.create(character.getLocation().getX(),
                        character.getLocation().getY() + i, character.getLocation().getZ()));
            }
        }
        int myHighestVal = (character.getWidth() > character.getHeight() ? character.getWidth() : character.getHeight());
        if (myHighestVal > 1) {
            for (int i = 1; i < myHighestVal; i++) {
                myTiles.add(Location.create(character.getLocation().getX() + i,
                        character.getLocation().getY() + i, character.getLocation().getZ()));
            }
        }
        return myTiles;
    }

    /**
     * Checks if this location is next to another.
     *
     * @param other The other location.
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean isNextTo(Location other) {
        return z == other.z && (getX() == other.getX() && getY() != other.getY() || getX() != other.getX() && getY() == other.getY() || getX() == other.getX() && getY() == other.getY());
    }

    /**
     * Gets the closest tile of an entity to this location.
     *
     * @param character The character
     * @return The location
     */
    public Location closestTileOfEntity(Actor character) {
        if (character.getWidth() < 2 && character.getHeight() < 2) {
            Location location = null;
            List<Location> entityLocations = new ArrayList<>();
            entityLocations.add(Location.create(character.getLocation().getX(), character.getLocation().getY() + 1, character.getLocation().getZ()));
            entityLocations.add(Location.create(character.getLocation().getX() + 1, character.getLocation().getY(), character.getLocation().getZ()));
            entityLocations.add(Location.create(character.getLocation().getX(), character.getLocation().getY() - 1, character.getLocation().getZ()));
            entityLocations.add(Location.create(character.getLocation().getX() - 1, character.getLocation().getY(), character.getLocation().getZ()));
            for (Location loc : entityLocations) {
                if (location == null || distanceToPoint(loc) < distanceToPoint(location)) {
                    location = loc;
                }
            }
            return location;
        }
        Location location = null;
        for (int x = 0; x < character.getWidth(); x++) {
            for (int y = 0; y < character.getHeight(); y++) {
                Location loc = Location.create(character.getLocation().getX() + x, character.getLocation().getY() + y, character.getLocation().getZ());
                if (loc.isNextTo(this) && (location == null || !location.isNextTo(this))) {
                    location = loc;
                } else if (location == null || loc.distanceToPoint(this) < location.distanceToPoint(this)) {
                    location = loc;
                }
            }
        }
        return closestTileToEntity(character);
    }

    /**
     * Gets the closest tile of an entity to this location.
     *
     * @param character The character
     * @return The location
     */
    public Location closestTileToEntity(Actor character) {
        if (character.getWidth() < 2 && character.getHeight() < 2) {
            return character.getLocation();
        }
        Location location = null;
        for (int x = 0; x < character.getWidth(); x++) {
            for (int y = 0; y < character.getHeight(); y++) {
                Location loc = Location.create(character.getLocation().getX() + x, character.getLocation().getY() + y, character.getLocation().getZ());
                if (loc.isNextTo(this) && (location == null || !location.isNextTo(this))) {
                    location = loc;
                } else if (location == null || loc.distanceToPoint(this) < location.distanceToPoint(this)) {
                    location = loc;
                }
            }
        }
        return location;
    }

    /**
     * Gets the distance to a location.
     *
     * @param other The location.
     * @return The distance from the other location.
     */
    public int distanceToPoint(Location other) {
        int absX = x;
        int absY = y;
        int pointX = other.getX();
        int pointY = other.getY();
        return (int) Math.sqrt(Math.pow(absX - pointX, 2) + Math.pow(absY - pointY, 2));
    }

    @Override
    public int hashCode() {
        return z << 30 | x << 15 | y;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Location)) {
            return false;
        }
        Location loc = (Location) other;
        return loc.x == x && loc.y == y && loc.z == z;
    }

    @Override
    public String toString() {
        return "[" + x + "," + y + "," + z + "]";
    }

    /**
     * Creates a new location based on this location.
     *
     * @param diffX X difference.
     * @param diffY Y difference.
     * @param diffZ Z difference.
     * @return The new location.
     */
    public Location transform(int diffX, int diffY, int diffZ) {
        return Location.create(x + diffX, y + diffY, z + diffZ);
    }

    /**
     * Transform a direction
     *
     * @param direction The direction to transform
     * @return The new transformed location
     */
    public Location transformDirectional(int direction) {
        if (direction > directionDelta.length) {
            return this;
        }
        return transform(directionDelta[direction][0], directionDelta[direction][1], 0);
    }

    /**
     * Gets the Location of the Entity based on their size.
     *
     * @param size The size.
     * @return New location.
     */
    public Location applySizeDistoration(int size) {
        int distort = (int) Math.floor((size - 1) / 2);
        return create(x + distort, y + distort, z);
    }

    /**
     * Gets the distance from a point.
     *
     * @param other The target.
     * @return blah distance.
     */
    public double getDistanceFromLocation(Location other) {
        return Math.round(Math.sqrt(((other.getX() - getX()) ^ 2) + ((other.getY() - getY()) ^ 2)));
    }

    /**
     * Checks if a location is a members region
     *
     * @param location The location
     * @return If the location is members or not
     */
    public boolean isMembers(Location location) {
        if (location.getX() >= 3272 && location.getX() <= 3320 && location.getY() >= 2752 && location.getY() <= 2809) {
            return false;
        }
        if (location.getX() >= 2640 && location.getX() <= 2677 && location.getY() >= 2638 && location.getY() <= 2679) {
            return false;
        }
        int regionX = location.getX() >> 3;
        int regionY = location.getY() >> 3;
        int regionId = ((regionX / 8) << 8) + (regionY / 8);
        for (Region r : World.getWorld().getRegionManager().getSurroundingRegions(location)) {
//            if (r.id() == regionId) {
//                return r.members();
//            }
            return true;
        }
        return false;
    }

}
