package org.hyperion.rs2.model;

/**
 * Represents a moving graphic.
 *
 * @author Michael Bull
 */
public class Projectile {

    /**
     * Projectile graphic.
     */
    private final int graphic;
    /**
     * The initial height.
     */
    private final int initHeight;
    /**
     * The end height.
     */
    private final int endHeight;
    /**
     * The delay.
     */
    private final int delay;
    /**
     * The speed.
     */
    private final int slowness;
    /**
     * The curvature.
     */
    private final int curvature;
    /**
     * The radius.
     */
    private final int radius;

    /**
     * Creates a new projectile.
     *
     * @param graphic The graphic.
     * @param initHeight The height.
     * @param endHeight The end height.
     * @param delay The delay.
     * @param slowness The speed.
     * @param curvature The curvature.
     * @param radius The radius.
     * @return The new projectile.
     */
    public static Projectile create(int graphic, int initHeight, int endHeight, int delay, int slowness, int curvature, int radius) {
        return new Projectile(graphic, initHeight, endHeight, delay, slowness, curvature, radius);
    }

    /**
     * Constructor
     *
     * @param graphic The graphic.
     * @param initHeight The height.
     * @param endHeight The end height.
     * @param delay The delay.
     * @param slowness The speed.
     * @param curvature The curvature.
     * @param radius The radius.
     */
    private Projectile(int graphic, int initHeight, int endHeight, int delay, int slowness, int curvature, int radius) {
        this.graphic = graphic;
        this.initHeight = initHeight;
        this.endHeight = endHeight;
        this.delay = delay;
        this.slowness = slowness;
        this.curvature = curvature;
        this.radius = radius;
    }

    /**
     * Gets the projectile graphic.
     *
     * @return gfx The graphic.
     */
    public int getGraphic() {
        return graphic;
    }

    /**
     * Gets the initial height.
     *
     * @return initHeight The height.
     */
    public int getInitHeight() {
        return initHeight;
    }

    /**
     * Gets the end height.
     *
     * @return endHeight The end height.
     */
    public int getEndHeight() {
        return endHeight;
    }

    /**
     * Gets the delay.
     *
     * @return delay The delay.
     */
    public int getDelay() {
        return delay;
    }

    /**
     * Gets the speed.
     *
     * @return slowness The speed.
     */
    public int getSpeed() {
        return slowness;
    }

    /**
     * Gets the curvature.
     *
     * @return curvature The curvature.
     */
    public int getCurvature() {
        return curvature;
    }

    /**
     * Gets the radius.
     *
     * @return radius The radius.
     */
    public int getRadius() {
        return radius;
    }
}
