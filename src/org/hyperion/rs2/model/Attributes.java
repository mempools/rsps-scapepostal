package org.hyperion.rs2.model;

import java.util.concurrent.ConcurrentHashMap;

/**
 * The attribute system from Rs2-Server.
 */
public class Attributes {

    /**
     * The attributes.
     */
    private ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();

    /**
     * Attributes.
     */
    public final static String KILLED_BY = "killedBy";

    /**
     * Gets the attributes
     *
     * @return attributes The attributes
     */
    public ConcurrentHashMap<String, Object> getAttributes() {
        return attributes;
    }

    /**
     * Creates the attribute instance.
     */
    public Attributes() {
    }

    /**
     * Gets an attribute key
     *
     * @param key The key
     * @return The object.
     */
    public Object get(String key) {
        return attributes.get(key);
    }

    /**
     * Sets a attribute attribute key
     *
     * @param key The key
     * @param value The value
     * @return The object.
     */
    public Object set(String key, Object value) {
        return attributes.put(key, value);
    }

    /**
     * Removes an attribute
     *
     * @param key The key to remove
     * @return The object.
     */
    public Object remove(String key) {
        return attributes.remove(key);
    }

    /**
     * Removes all attributes
     */
    public void removeAll() {
        if (attributes != null && attributes.size() > 0 && attributes.keySet().size() > 0) {
            attributes = new ConcurrentHashMap<>();
        }
    }

    @Override
    public int hashCode() {
//        HashCodeBuilder bldr = new HashCodeBuilder(1,2);
//        bldr.append(attributes);
//        bldr.toHashCode();
        return attributes.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Attributes other = (Attributes) obj;
        return other.attributes.equals(attributes);
    }
}
