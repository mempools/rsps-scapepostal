package org.hyperion.rs2.model.definition;

import org.hyperion.util.xStream;

import java.io.File;
import java.util.logging.Logger;
import org.hyperion.cache.npc.ActorDefinition;
import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.Skills;

/**
 * @author Cyberus <http://rune-server.org/members/Cyberus/> Beastiary
 * Definition loader
 * http://www.rune-server.org/runescape-development/rs2-server/snippets/477093-beast-definition-loader.html
 * Sorry, I honestly don't feel like documenting/commenting every method in this
 * file
 */
public class NPCDefinition {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(NPCDefinition.class.getName());
    /**
     * The beast count.
     */
    public static final int BEAST_COUNT = 15055;
    /**
     * The definitions.
     */
    private static NPCDefinition[] definitions;

    /**
     * Gets the NPC definition array
     *
     * @return The NPC definitions.
     */
    public static NPCDefinition[] getDefinitions() {
        return definitions;
    }
    /**
     * The beast id.
     */
    private final int id;
    /**
     * The name.
     */
    private final String name;
    /**
     * The description.
     */
    private final String description;
    /**
     * The weakness.
     */
    private final String weakness;
    /**
     * Slayer name.
     */
    private final String slayercat;
    /**
     * The death animation.
     */
    private final Animation deathAnimation;
    /**
     * The attack animation.
     */
    private final Animation attackAnimation;
    /**
     * The size.
     */
    private final int size;
    /**
     * The combat level.
     */
    private final int level;
    /**
     * The beast's skill levels.
     */
    private int[] skills = new int[Skills.SKILL_COUNT];
    /**
     * If poisonous.
     */
    private final boolean poisonous;
    /**
     * If attackable.
     */
    private final boolean attackable;
    /**
     * If aggressive.
     */
    private final boolean aggressive;
    /**
     * If members only.
     */
    private final boolean members;
    /**
     * The areas.
     */
    private final String[] areas;

    /**
     * Constructs an NPC definition
     *
     * @param id The id
     * @param name The name
     * @param description The description
     * @param weakness The weakness
     * @param slayercat The slayer cat
     * @param deathAnimation The death animation
     * @param attackAnimation The attack animation
     * @param size The size
     * @param level The combat level
     * @param skills The skill levels
     * @param poisonous If poisonous
     * @param attackable If attackable
     * @param aggressive If aggressive
     * @param members If members only
     * @param areas The areas
     */
    public NPCDefinition(int id, String name, String description, String weakness, String slayercat,
            Animation deathAnimation, Animation attackAnimation, int size, int level, int[] skills,
            boolean poisonous, boolean attackable, boolean aggressive, boolean members, String[] areas) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.weakness = weakness;
        this.slayercat = slayercat;
        this.deathAnimation = deathAnimation;
        this.attackAnimation = attackAnimation;
        this.size = size;
        this.level = level;
        this.skills = skills;
        this.poisonous = poisonous;
        this.attackable = attackable;
        this.aggressive = aggressive;
        this.members = members;
        this.areas = areas;
    }

    @Override
    public String toString() {
        return id + ":" + name + ":" + description + ":" + weakness + ":" + slayercat + ":" + deathAnimation + ":"
                + attackAnimation + ":" + size + ":" + skills + ":" + level + ":" + poisonous + ":" + attackable + ":" + aggressive + ":" + members + ":"
                + areas;
    }

    /**
     * Gets an NPC by id.
     *
     * @param id The id of the NPC to get.
     * @return The {@link NPCDefintion}.
     */
    public static NPCDefinition forId(int id) {
        return NPCDefinition.definitions[id];
    }

    /**
     * Gets the cache definition of the NPC.
     *
     * @param id The id of the NPC to get
     * @return The {@link ActorDefinition}.
     */
    public static ActorDefinition getActorDefinition(int id) {
        return ActorDefinition.forId(id);
    }

    /**
     * Loads beastiary data
     *
     * @throws Exception If File wasn't found
     */
    public static void init() throws Exception {
        logger.info("Loading NPC Definitions...");
        xStream.alias("npcDefinition", NPCDefinition.class);
        NPCDefinition.definitions = xStream.readXML(new File("./data/npcDefinitions.xml"));
        logger.info("Loaded " + definitions.length + " NPC Definitions");
    }

    /**
     * Gets the beast id
     *
     * @return id The beast id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the name
     *
     * @return name The name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the description
     *
     * @return description The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the weakness
     *
     * @return weakness The weakness
     */
    public String getWeakness() {
        return weakness;
    }

    /**
     * Gets the slayer cat.
     *
     * @return slayercat The slayer cat
     */
    public String getSlayercat() {
        return slayercat;
    }

    /**
     * Gets the death animation
     *
     * @return deathAnimation The death animation
     */
    public Animation getDeathAnimation() {
        return deathAnimation;
    }

    /**
     * Gets the attack animation
     *
     * @return attackAnimation The attack animation
     */
    public Animation getAttackAnimation() {
        return attackAnimation;
    }

    /**
     * Gets the size
     *
     * @return size The size
     */
    public int getSize() {
        return size;
    }

    /**
     * Gets the combat level
     *
     * @return level The combat level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Gets the beast's skill levels
     *
     * @return skills The beast's skill levels
     */
    public int[] getSkills() {
        return skills;
    }

    /**
     * Gets a skill by the skill Id
     *
     * @param skill The skill to get
     * @return The skill
     */
    public int getSkill(int skill) {
        return skills[skill];
    }

    /**
     * Gets the poisonous flag
     *
     * @return poisonous If poisonous
     */
    public boolean isPoisonous() {
        return poisonous;
    }

    /**
     * Gets the attackable flag
     *
     * @return attackable If attackable
     */
    public boolean isAttackable() {
        return attackable;
    }

    /**
     * Gets the aggressive flag
     *
     * @return aggressive If aggressive
     */
    public boolean isAggressive() {
        return aggressive;
    }

    /**
     * Gets the members flag
     *
     * @return members If members
     */
    public boolean isMembers() {
        return members;
    }

    /**
     * Gets the areas
     *
     * @return areas The areas
     */
    public String[] getAreas() {
        return areas;
    }
}
