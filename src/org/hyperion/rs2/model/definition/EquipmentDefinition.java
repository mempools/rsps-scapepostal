package org.hyperion.rs2.model.definition;

import org.hyperion.rs2.model.player.EquipmentAnimations;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.Sound;
import org.hyperion.rs2.model.combat.Poison.PoisonType;
import org.hyperion.rs2.model.combat.RangeData.*;
import org.hyperion.rs2.model.container.Equipment.EquipmentType;
import org.hyperion.util.xStream;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Defines the equipment information that is requested
 *
 * @author Sir Sean
 */
public class EquipmentDefinition {

    /**
     * Logger instance.
     */
    private static final Logger logger = Logger.getLogger(EquipmentDefinition.class.getName());

    /**
     * We shall allocate a size for the HashMap!
     */
    public static final int ALLOCATED_SIZE = 2384;

    /**
     * The <code>EquipmentDefinition</code> map.
     */
    private static Map<Integer, EquipmentDefinition> definitions;

    /**
     * Gets the equipment definitions
     *
     * @return the definition map
     */
    public static Map<Integer, EquipmentDefinition> getDefinitions() {
        return definitions;
    }

    /**
     * Gets a definition for the specified id.
     *
     * @param id The id.
     * @return The definition.
     */
    public static EquipmentDefinition forId(int id) {
        return definitions.get(id);
    }

    /**
     * The equipment weight
     */
    private double weight;

    /**
     * The weapon speed
     */
    private int speed;

    /**
     * The hit delay (ticks)
     */
    private int delay;

    /**
     * The item bonuses
     */
    private int[] bonuses;

    /**
     * The weapon type
     */
    private WeaponStyle[] weaponStyles;

    /**
     * The equipment types
     */
    private EquipmentType type;

    /**
     * The equipment animations
     */
    private EquipmentAnimations animations;

    /**
     * The equipment's poison type.
     */
    private PoisonType poisonType;

    /**
     * The equipments skill requirements.
     */
    private Map<Skill, Integer> skillRequirements;

    /**
     * The bow type of this item.
     */
    private BowType bowType;

    /**
     * The arrow type of this item.
     */
    private ArrowType arrowType;

    /**
     * The range weapon type of this item.
     */
    private RangeWeaponType rangeWeaponType;

    /**
     * The amount of special energy this item uses.
     */
    private int specialConsumption;

    /**
     * The amount of hits this special deals.
     */
    private int specialHits;

    /**
     * The sound hit
     */
    private Sound normalHitSound;

    /**
     * The special hit sound
     */
    private Sound specialHitSound;

    /**
     * The types of combat weapons
     *
     * @author Sir Sean
     */
    public enum WeaponStyle {

        /**
         * Range type weapons
         */
        RANGED,
        /**
         * Magic type weapons
         */
        MAGIC,
        /**
         * Melee type weapons
         */
        MELEE,
        /**
         * Weapon has a special attack/effect
         */
        SPECIAL,
        /**
         * Unset weapons
         */
        NONE
    }

    /**
     * Loads the item definitions.
     *
     * @throws IOException if an I/O error occurs.
     * @throws IllegalStateException if the definitions have been loaded
     * already.
     */
    public static void parse() throws IOException {
        File file = new File("./data/equipmentDefinition.xml");
        if (definitions != null) {
            throw new IllegalStateException("Equipment definitions already loaded.");
        }
        logger.info("Loading Equipment Definitions...");

        /**
         * Load equipment definitions.
         */
        definitions = new HashMap<>(ALLOCATED_SIZE);
        if (file.exists()) {
            definitions = xStream.readXML(file);
            logger.info("Loaded " + definitions.size() + " equipment definitions.");
        } else {
            logger.info("Equipment definitions not found!");
        }
//        for (int i = 0; i < ALLOCATED_SIZE; i++) {
//            EquipmentDefinition def = definitions.get(i);
//            WeightDefinition weightDef = WeightDefinition.forId(i);
//            if (def != null) {
//                if (weightDef != null) {
//                    def.setWeight(weightDef.getWeight());
//                }
//                if (ItemDefinition.forId(i).getName().contains("(p)")) {
//                    definitions.put(i, new EquipmentDefinition(def.getSpeed(), def.getBonuses(), def.getWeaponStyles(),
//                            def.getType(), def.getAnimation(), PoisonType.POISON, def.getSkillRequirements(), def.getSpecialConsumption(), def.getSpecialHits()));
//
//                } else if (ItemDefinition.forId(i).getName().contains("(+)")) {
//                    definitions.put(i, new EquipmentDefinition(def.getSpeed(), def.getBonuses(), def.getWeaponStyles(),
//                            def.getType(), def.getAnimation(), PoisonType.EXTRA_POISON, def.getSkillRequirements(), def.getSpecialConsumption(), def.getSpecialHits()));
//
//                } else if (ItemDefinition.forId(i).getName().contains("(s)")) {
//                    definitions.put(i, new EquipmentDefinition(def.getSpeed(), def.getBonuses(), def.getWeaponStyles(),
//                            def.getType(), def.getAnimation(), PoisonType.SUPER_POISON, def.getSkillRequirements(), def.getSpecialConsumption(), def.getSpecialHits()));
//
//                } else {
//                    definitions.put(i, new EquipmentDefinition(def.getSpeed(), def.getBonuses(), def.getWeaponStyles(),
//                            def.getType(), def.getAnimation(), PoisonType.NONE, def.getSkillRequirements(), def.getSpecialConsumption(), def.getSpecialHits()));
//
//                }
//            }
//        }
//        System.out.println("Dumped ");
//        xStream.writeXML(definitions, new File("data/equipmentDefinitionDump.xml"));

        /*please leave incase I need to redump any invalid values, thanks.*/
    }

    /**
     * Gets the equipment weight
     *
     * @return The equipment weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * The weapon speed
     *
     * @return the weapon speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * The hit delay
     *
     * @return The hit delay
     */
    public int getDelay() {
        return delay;
    }

    /**
     * Gets a bonus by its index.
     *
     * @param index The bonus index.
     * @return The bonus amount.
     */
    public int getBonus(int index) {
        return bonuses[index];
    }

    /**
     * The item bonuses
     *
     * @return the item bonuses
     */
    public int[] getBonuses() {
        return bonuses;
    }

    /**
     * Gets the weapon type
     *
     * @return The weapon type
     */
    public WeaponStyle[] getWeaponStyles() {
        return weaponStyles;
    }

    /**
     * Gets the weapon type by its index.
     *
     * @param index The index
     * @return The weapon type by its index.
     */
    public WeaponStyle getWeaponStyle(int index) {
        return weaponStyles[index];
    }

    /**
     * Gets the equipment types
     *
     * @return The equipment types
     */
    public EquipmentType getType() {
        return type;
    }

    /**
     * Gets the equipment animation instance
     *
     * @return the equipmentAnimation
     */
    public EquipmentAnimations getAnimation() {
        return animations;
    }

    /**
     * Gets the weapons poison type
     *
     * @return The weapons poison type
     */
    public PoisonType getPoisonType() {
        return poisonType;
    }

    /**
     * Gets the weapons skill requirements.
     *
     * @return The weapons skill requirements.
     */
    public Map<Skill, Integer> getSkillRequirements() {
        return skillRequirements;
    }

    /**
     * Gets the weapons skill requirements.
     *
     * @param index The skill index.
     * @return The weapons skill requirements.
     */
    public int getSkillRequirement(int index) {
        return skillRequirements.get(Skill.skillForId(index));
    }

    /**
     * Gets the amount of special energy this item consumes.
     *
     * @return The amount of special energy this item consumes.
     */
    public int getSpecialConsumption() {
        return specialConsumption;
    }

    /**
     * Gets the amount of hits this special attack deals.
     *
     * @return The amount of hits this special attack deals.
     */
    public int getSpecialHits() {
        return specialHits;
    }

    /**
     * Gets the bow type
     *
     * @return The bow type
     */
    public BowType getBowType() {
        return bowType;
    }

    /**
     * Gets the arrow type
     *
     * @return The arrow type
     */
    public ArrowType getArrowType() {
        return arrowType;
    }

    /**
     * Gets the range weapon type
     *
     * @return The range weapon type
     */
    public RangeWeaponType getRangeWeaponType() {
        return rangeWeaponType;
    }

    /**
     * Gets the normal hit sound
     *
     * @return The normal hit sound
     */
    public Sound getNormalHitSound() {
        return normalHitSound;
    }

    /**
     * Gets the special hit sound
     *
     * @return The special hit sound
     */
    public Sound getSpecialHitSound() {
        return specialHitSound;
    }

    /**
     * Checks if the <code>Equipment</code> contains the specified
     * <code>WeaponStyle</code>.
     *
     * @param weaponStyle The weapon style to get
     * @return If contains the specified weapon style
     */
    public boolean containsWeaponStyle(WeaponStyle weaponStyle) {
        for (int i = 0; i < getWeaponStyles().length; i++) {
            if (getWeaponStyle(i) == weaponStyle) {
                return true;
            }
        }
        return false;
    }

    /**
     * The skill statistic enum.
     *
     * @author Michael
     */
    public enum Skill {

        ATTACK(Skills.ATTACK),
        DEFENCE(Skills.DEFENCE),
        HITPOINTS(Skills.HITPOINTS),
        RANGE(Skills.RANGE),
        MAGIC(Skills.MAGIC),
        PRAYER(Skills.PRAYER),
        STRENGTH(Skills.STRENGTH);

        /**
         * The list of skills.
         */
        private static Map<Integer, Skill> skills = new HashMap<>();

        /**
         * Gets a skill by it's id
         *
         * @param skill The skill id to get
         * @return The skills
         */
        public static Skill skillForId(int skill) {
            return skills.get(skill);
        }

        /**
         * Populates the skill list.
         */
        static {
            for (Skill skill : Skill.values()) {
                skills.put(skill.getId(), skill);
            }
        }

        /**
         * The id of the skill.
         */
        private int id;

        private Skill(int id) {
            this.id = id;
        }

        /**
         * @return the id
         */
        public int getId() {
            return id;
        }
    }
}
