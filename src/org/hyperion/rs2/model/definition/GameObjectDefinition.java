package org.hyperion.rs2.model.definition;

/**
 * Represents a single type of object.
 *
 * @author Graham Edgecombe
 */
public class GameObjectDefinition {

    /**
     * The definitions array.
     */
    private static GameObjectDefinition[] defContainer;

    /**
     * Sets the definition size
     *
     * @param size The size to set
     */
    public static void setDefinitionSize(int size) {
        defContainer = new GameObjectDefinition[size];
    }

    /**
     * Adds a definition. TODO better way?
     *
     * @param def The definition.
     */
    public static void addDefinition(GameObjectDefinition def) {
        defContainer[def.getId()] = def;
    }

    /**
     * Gets an object definition by its id.
     *
     * @param id The id.
     * @return The definition.
     */
    public static GameObjectDefinition forId(int id) {
        return defContainer[id];
    }

    /**
     * The id.
     */
    private int id;
    /**
     * The name.
     */
    private String name;
    /**
     * The description.
     */
    private String desc;
    /**
     * X size.
     */
    private int width;
    /**
     * Y size.
     */
    private int height;
    /**
     * Solid flag.
     */
    private boolean solid;
    /**
     * Walkable flag.
     */
    private boolean walkable;
    /**
     * 'Has actions' flag.
     */
    private boolean hasActions;
    /**
     * The actions this object has.
     */
    private String[] actions;

    /**
     * Creates the definition.
     *
     * @param id The id.
     * @param name The name of the object.
     * @param desc The description of the object.
     * @param sizeX The x size of the object.
     * @param sizeY The y size of the object.
     * @param isSolid Solid flag.
     * @param isWalkable Walkable flag.
     * @param hasActions Flag which indicates if this object has any actions.
     */
    public GameObjectDefinition(int id, String name, String desc, int sizeX, int sizeY, boolean isSolid, boolean isWalkable, boolean hasActions, String[] actions) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.width = sizeX;
        this.height = sizeY;
        this.solid = isSolid;
        this.walkable = isWalkable;
        this.hasActions = hasActions;
        if (this.hasActions) {
            this.actions = actions;
        } else {
            this.actions = new String[0];
        }
    }

    /**
     * Gets the id.
     *
     * @return The id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Gets the name.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the description.
     *
     * @return The description.
     */
    public String getDescription() {
        return desc;
    }

    /**
     * Gets the x size.
     *
     * @return The x size.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the y size.
     *
     * @return The y size.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Checks if this object is solid.
     *
     * @return The solid flag.
     */
    public boolean isSolid() {
        return solid;
    }

    /**
     * Checks if this object is walkable.
     *
     * @return The walkable flag.
     */
    public boolean isWalkable() {
        return walkable;
    }

    /**
     * Checks if this object has any actions.
     *
     * @return A flag indicating that this object has some actions.
     */
    public boolean hasActions() {
        return hasActions;
    }

    /**
     * Gets the actions this object has.
     *
     * @return The actions this object has.
     */
    public String[] getActions() {
        return actions;
    }
}
