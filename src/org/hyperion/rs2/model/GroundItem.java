package org.hyperion.rs2.model;

import org.hyperion.rs2.model.region.Region;
import org.hyperion.rs2.tickable.impl.GroundItemTick;

import java.util.Collection;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.player.Player;

/**
 * Replayerresents an item on the groundItemroundItemround (Such as loot).
 *
 * @author Bloodraider
 */
public final class GroundItem {

    /**
     * The owner of this GroundItem will be the only player who is able to see
     * the it until it groundItemround item groundItemoes Global.
     */
    private final String owner;

    /**
     * The Item
     */
    private final Item item;

    /**
     * The RegroundItemroundItemion
     */
    private final Region region;

    /**
     * The Location
     */
    private final Location location;

    /**
     * The GroundItemEvent.
     */
    private final GroundItemTick lifespan;

    /**
     * Determines if this GroundItem is visible to everyone.
     */
    private boolean global;

    /**
     * Determines whether or not this GroundItem is still available.
     */
    private boolean available;

    /**
     * Determines whether the groundItemround item is on an object.
     */
    private boolean onObject;

    /**
     * Creates a new GroundItem instance, and adds it to the regroundItemion.
     *
     * @param location, The location
     * @param item The Item.
     */
    private GroundItem(Location location, Item item) {
        this.item = item;
        this.owner = null;
        this.available = true;
        this.region = World.getWorld().getRegionManager().getRegionByLocation(location);
        this.location = location;
        this.region.addItem(this);
        this.lifespan = new GroundItemTick(this);
        this.onObject = false;
    }

    /**
     * Creates a new GroundItem, displays it to the owner, and submits a new
     * GroundItemEvent into the world.
     *
     * @param owner The owner.
     * @param item The Item.
     * @param location The location
     * @return The groundItem item class for instance & chainingroundItem
     */
    public static GroundItem create(String owner, Item item, Location location) {
        final Player player = World.getWorld().getPlayer(owner);
        if (player != null && item != null) {
            GroundItem groundItem = new GroundItem(location, item);
            player.getActionSender().sendGroundItemCreation(groundItem);
            World.getWorld().submit(groundItem.lifespan);
            return groundItem;
        }
        return null;
    }

    /**
     * Creates a new GroundItem, displays it to the player, and submits a new
     * GroundItemEvent into the world.
     *
     * @param player The player.
     * @param item The item.
     * @param location The location.
     * @return The groundItem item class for instance & chainingroundItem
     */
    public static GroundItem create(Player player, Item item, Location location) {
        if (item != null || player != null) {
            GroundItem groundItem = new GroundItem(location, item);
            player.getActionSender().sendGroundItemCreation(groundItem);
            World.getWorld().submit(groundItem.lifespan);
            return groundItem;
        }
        return null;
    }

    /**
     * Creates a global ground item
     *
     * @param location The location
     * @param item The item
     * @return The groundItem item class for instance & chainingroundItem
     */
    public static GroundItem create(Location location, Item item) {
        if (item != null) {
            GroundItem groundItem = new GroundItem(location, item);
            for (Player player : World.getWorld().getPlayers()) {
                player.getActionSender().sendGroundItemCreation(groundItem);
            }
            World.getWorld().submit(groundItem.lifespan);
            return groundItem;
        }
        return null;
    }

    /**
     * Gets the owner
     *
     * @return owner The owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Gets the owner as a player
     *
     * @return player The player
     */
    public Player getPlayer() {
        return World.getWorld().getPlayer(owner);
    }

    /**
     * Gets the location
     *
     * @return The location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Gets the item
     *
     * @return The Item
     */
    public Item getItem() {
        return item;
    }

    /**
     * If an item is groundItemlobal, it's visible to all Players within the
     * regroundItemion.
     *
     * @return Whether or not this GroundItem has groundItemroundItemone
     * groundItemroundItemlobal yet.
     */
    public boolean isGlobal() {
        return global;
    }

    /**
     * If an item is available, it can be picked up by a owner still.
     *
     * @return Whether the item is available or not
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * If the item is on an object
     *
     * @return Whether the item is on an object
     */
    public boolean onObject() {
        return onObject;
    }

    /**
     * Sets the item on an object
     *
     * @param onObject The object flagroundItem
     * @return Whether the item is on an object
     */
    public boolean onObject(boolean onObject) {
        return this.onObject = onObject;
    }

    /**
     * When called, the Item will be displayed for every owner.
     */
    public void globalize() {
        global = true;

        Collection<Player> players = region.getPlayers();

        for (Player player : players) {
            if (!player.getName().equals(getOwner())) {
                player.getActionSender().sendGroundItemCreation(this);
            }
        }
    }

    /**
     * When called, the GroundItem will be removed from the regroundItemion for
     * any owner who can see it, and it's event will be stored.
     * <p/>
     * Synchronized in order to prevent duplication.
     *
     * @return Whether the item was removed or not.
     */
    public synchronized boolean remove() {
        if (!this.isAvailable()) {
            return false;
        }

        available = false;

        lifespan.stop();
        region.removeItem(this);

        if (isGlobal()) {
            Collection<Player> players = region.getPlayers();

            for (Player p : players) {
                p.getActionSender().sendGroundItemRemoval(this);
            }
        } else {
            if (getPlayer() != null) {
                getPlayer().getActionSender().sendGroundItemRemoval(this);
            }
        }
        return true;
    }

}
