package org.hyperion.rs2.model.combat;

import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.Graphic;
import org.hyperion.rs2.model.Item;
import org.hyperion.util.xStream;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public class MagicData {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(MagicData.class.getName());
    /**
     * The map of spells.
     */
    private static final Map<Integer, Spell> spells = new HashMap<>();

    /**
     * Loads the spell data.
     *
     * @throws Exception If the file wasn't found
     */
    public static void load() throws Exception {
        logger.info("Loading Spells...");
        xStream.alias("spell", Spell.class);
        Map<Integer, Spell> loadedSpells = xStream.readXML(new File("./data/spells.xml"));
        spells.putAll(loadedSpells);
        logger.info("Loaded " + spells.size() + " Spells");
    }

    /**
     * Gets a spell by it's id.
     *
     * @param id The id of the spell to get
     * @return The spell
     */
    public static Spell forId(int id) {
        for (Spell spell : spells.values()) {
            if (spell.getId() == id) {
                return spell;
            }
        }
        return null;
    }

    /**
     * The spell book
     */
    public static enum SpellBook {

        /**
         * The modern magic book
         */
        MODERN_MAGICS(0, 1151),
        /**
         * The ancient magic book
         */
        ANCIENT_MAGICKS(1, 12855);

        /**
         * The book id
         */
        private final int id;

        /**
         * The interface id
         */
        private final int interfaceId;

        /**
         * Creates the spell book
         *
         * @param id The spell book id
         */
        private SpellBook(int id, int interfaceId) {
            this.id = id;
            this.interfaceId = interfaceId;
        }

        /**
         * Gets the spell book id
         *
         * @return id The spell book id
         */
        public int getSpellBookId() {
            return id;
        }

        /**
         * Gets the interface id
         *
         * @return The interface id
         */
        public int getInterfaceId() {
            return interfaceId;
        }

        /**
         * Gets a spell book by it's id
         *
         * @param spellBook The spell book id to get
         * @return The spell book
         */
        public static SpellBook forId(int spellBook) {
            for (SpellBook book : SpellBook.values()) {
                if (book.getSpellBookId() == spellBook) {
                    return book;
                }
            }
            return null;
        }
    }

    /**
     * The spell data
     */
    public static class Spell {

        /**
         * The id
         */
        private final int id;
        /**
         * The name
         */
        private final String name;
        /**
         * The required level
         */
        private final int requiredLevel;
        /**
         * The spell book
         */
        private final SpellBook spellBook;
        /**
         * The required runes
         */
        private final Item[] requiredRunes;
        /**
         * The required item (staffs)
         */
        private Item requiredItem;
        /**
         * The cast animation
         */
        private final Animation castAnimation;
        /**
         * The projectile Id
         */
        private final int projectileId;
        /**
         * The projectile start graphic
         */
        private final Graphic startGraphic;
        /**
         * The projectile end graphic
         */
        private final Graphic endGraphic;

        /**
         * Creates the spell.
         *
         * @param id The spell id
         * @param name The name
         * @param requiredLevel The required level
         * @param spellBook The spell book
         * @param requiredRunes The required runes
         * @param requiredItem The required item
         * @param castAnimation The cast animation
         * @param projectileId The projectile id
         * @param startGraphic The projectile start graphic
         * @param endGraphic The projectile end graphic
         */
        private Spell(int id, String name, int requiredLevel, SpellBook spellBook, Item[] requiredRunes, Item requiredItem,
                Animation castAnimation, int projectileId, Graphic startGraphic, Graphic endGraphic) {
            this.id = id;
            this.requiredLevel = requiredLevel;
            this.spellBook = spellBook;
            this.requiredRunes = requiredRunes;
            this.name = name;
            this.castAnimation = castAnimation;
            this.projectileId = projectileId;
            this.startGraphic = startGraphic;
            this.endGraphic = endGraphic;
        }

        /**
         * Gets the spell id.
         *
         * @return The spell id.
         */
        public int getId() {
            return id;
        }

        /**
         * Gets the spell name
         *
         * @return name The spell name
         */
        public String getName() {
            return name;
        }

        /**
         * Gets the required level
         *
         * @return requiredLevel The required level
         */
        public int getRequiredLevel() {
            return requiredLevel;
        }

        /**
         * Gets the spell book
         *
         * @return spellBook The spell book
         */
        public SpellBook getSpellBook() {
            return spellBook;
        }

        /**
         * Gets the required runes
         *
         * @return requiredRunes The required runes
         */
        public Item[] getRequiredRunes() {
            return requiredRunes;
        }

        /**
         * Gets a rune from the required runes
         *
         * @param index The rune to get
         * @return The required rune
         */
        public Item getRequiredRunes(int index) {
            return requiredRunes[index];
        }

        /**
         * Gets the required item
         *
         * @return requiredItem The required item
         */
        public Item getRequiredItem() {
            return requiredItem;
        }

        /**
         * Gets the cast animation
         *
         * @return castAnimation The cast animation
         */
        public Animation getCastAnimation() {
            return castAnimation;
        }

        /**
         * Gets the projectile id
         *
         * @return projectileId The projectile id
         */
        public int getProjectileId() {
            return projectileId;
        }

        /**
         * Gets the projectile start graphic
         *
         * @return startGraphic The start graphic
         */
        public Graphic getStartGraphic() {
            return startGraphic;
        }

        /**
         * Gets the projectile end graphic
         *
         * @return endGraphic The end graphic
         */
        public Graphic getEndGraphic() {
            return endGraphic;
        }
    }
}
