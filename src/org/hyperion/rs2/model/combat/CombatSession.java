package org.hyperion.rs2.model.combat;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.combat.MagicData.Spell;
import org.hyperion.rs2.model.combat.MagicData.SpellBook;

/**
 * Class that handles the <code>Entity</entity>'s combat session, and holds combat data/information.
 *
 * @author global <http://rune-server.org/members/global/>
 */
public class CombatSession {

    /**
     * The Entity.
     */
    private Actor character;
    /**
     * The character's combat state.
     */
    private boolean isInCombat = false;
    /**
     * The combat style
     */
    private CombatStyle combatStyle = CombatStyle.ACCURATE;
    /**
     * The attack style
     */
    private AttackType attackType;
    /**
     * Special state
     */
    private boolean isSpecialOn;
    /**
     * Special amount
     */
    private int specialAmount = 100;
    /**
     * The mob's spell book.
     */
    private SpellBook spellBook = SpellBook.MODERN_MAGICS;
    /**
     * The mob's current spell
     */
    private Spell currentSpell;
    /**
     * The mob's auto cast spell
     */
    private Spell autoCastSpell;
    /**
     * The mob's poison damage.
     */
    private int poisonDamage = 0;
    /**
     * If the character can be poisoned
     */
    private boolean canBePoisoned;

    /**
     * The combat session for the entity
     *
     * @param character The entity
     */
    public CombatSession(Actor character) {
        this.character = character;
    }

    /**
     * Gets the special state
     *
     * @return The special state
     */
    public boolean isSpecialOn() {
        return isSpecialOn;
    }

    /**
     * Gets the special amount
     *
     * @return specialAmount The isSpecialOn amount
     */
    public int getSpecialAmount() {
        return specialAmount;
    }

    /**
     * Gets the combat style id
     *
     * @return combatStyle the Id
     */
    public CombatStyle getCombatStyle() {
        return combatStyle;
    }

    /**
     * Gets the spell book
     *
     * @return the spellBook
     */
    public SpellBook getSpellBook() {
        return spellBook;
    }

    /**
     * Gets the current spell
     *
     * @return currentSpell The current spell
     */
    public Spell getCurrentSpell() {
        return currentSpell;
    }

    /**
     * Gets the current auto cast spell
     *
     * @return autoCastSpell The current spell
     */
    public Spell getAutoCastSpell() {
        return autoCastSpell;
    }

    /**
     * Inverses the special attack flag.
     */
    public void inverseSpecial() {
        this.isSpecialOn = !this.isSpecialOn;
        if (character.isPlayer()) {
            character.getActionSender().updateSpecial();
        }
    }

    /**
     * Sets the current spell
     *
     * @param spell The spell to set
     */
    public Spell setCurrentSpell(Spell spell) {
        return this.currentSpell = spell;
    }

    /**
     * Sets the combat style
     *
     * @param style The combat style
     * @return The Id
     */
    public CombatStyle setCombatStyle(CombatStyle style) {
        return combatStyle = style;
    }

    /**
     * Sets the special state
     *
     * @param state The state
     */
    public void setSpecial(boolean state) {
        this.isSpecialOn = state;
        if (character.isPlayer()) {
            character.getActionSender().sendConfig(301, isSpecialOn() ? 1 : 0);
        }
    }

    /**
     * Sets the isSpecialOn amount
     *
     * @param specialAmount The setted isSpecialOn amount
     */
    public void setSpecialAmount(int specialAmount) {
        this.specialAmount = specialAmount;
    }

    /**
     * Increases the isSpecialOn amount (adds)
     *
     * @param amount The amount to add
     */
    public void increaseSpecialAmount(int amount) {
        if (amount > (100 - this.specialAmount)) {
            amount = 100 - this.specialAmount;
        }
        this.specialAmount += amount;
        if (character.isPlayer()) {
            character.getActionSender().sendConfig(300, getSpecialAmount() * 10);
        }
    }

    /**
     * Decreases (subtracts) the isSpecialOn amount
     *
     * @param amount The amount to substract
     */
    public void decreaseSpecialAmount(int amount) {
        if (amount > specialAmount) {
            amount = specialAmount;
        }
        this.specialAmount -= amount;
        if (character.isPlayer()) {
            character.getActionSender().sendConfig(300, getSpecialAmount() * 10);
        }
    }

    /**
     * Sets the spell book
     *
     * @param spellBook the spellBook to set
     */
    public void setSpellBook(SpellBook spellBook) {
        character.getActionSender().sendSidebarInterface(6, spellBook.getInterfaceId());
        this.spellBook = spellBook;
    }

    /**
     * Sets the mob's attack type.
     *
     * @param attackType The attack type to set.
     */
    public AttackType setAttackType(AttackType attackType) {
        return this.attackType = attackType;
    }

    /**
     * Gets the attack type.
     *
     * @return The attack type.
     */
    public AttackType getAttackType() {
        return attackType;
    }

    /**
     * Returns the combat state of this entity.
     *
     * @return <code>boolean</code> The character's combat state.
     */
    public boolean isInCombat() {
        return isInCombat;
    }

    /**
     * Set the character's combat state.
     *
     * @param isInCombat This character's combat state.
     */
    public void setInCombat(boolean isInCombat) {
        this.isInCombat = isInCombat;
    }

    /**
     * Gets the poison damage
     *
     * @return The poisonDamage
     */
    public int getPoisonDamage() {
        return poisonDamage;
    }

    /**
     * Sets the poison damage
     *
     * @param poisonDamage the poisonDamage to set
     */
    public int setPoisonDamage(int poisonDamage, Actor attacker) {
        this.poisonDamage = poisonDamage;
        /* if(character.getPoisonDrainTick() == null && poisonDamage > 0) {
         character.setPoisonDrainTick(new PoisonDrainTick(mob, attacker));
         World.getWorld().submit(character.getPoisonDrainTick());
         } else if(character.getPoisonDrainTick() != null && poisonDamage < 1) {
         character.getPoisonDrainTick().stop();
         character.setPoisonDrainTick(null);
         }             */
        return poisonDamage;
    }

    /**
     * Decreases poison damage
     *
     * @param poisonDamage the poisonDamage to set
     */
    public int decreasePoisonDamage(int poisonDamage) {
        this.poisonDamage -= poisonDamage;
        /*  if(character.getPoisonDrainTick() != null && this.poisonDamage < 1) {
         character.getPoisonDrainTick().stop();
         character.setPoisonDrainTick(null);
         }           */
        return poisonDamage;
    }

    /**
     * If can be poisoned
     *
     * @return If can be poisoned
     */
    public boolean canBePoisoned() {
        return canBePoisoned;
    }

    /**
     * Sets if could be poisoned
     *
     * @param canBePoisoned the canBePoisoned to set
     */
    public boolean setCanBePoisoned(boolean canBePoisoned) {
        return this.canBePoisoned = canBePoisoned;
    }
}
