package org.hyperion.rs2.model.combat;

import org.hyperion.rs2.content.skills.Prayers.Prayer;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.combat.RangeData.BowType;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.definition.EquipmentDefinition;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public class Formulae {

    /**
     * Calculates a entity's max melee hit. Credits to Rs2
     *
     * @param character The character
     * @param special If using special
     *
     * @return The max hit
     */
    public static int calculateMeleeMaxHit(Actor character, boolean special) {
        if (character.isNPC()) {
            return getMaxHit(Skills.ATTACK, character.getSkills().getCombatLevel());
        }
        int maxHit = 0;
        double specialMultiplier = 1;
        double prayerMultiplier = 1;
        double otherBonusMultiplier = 1; //TODO: void melee = 1.2, slayer helm = 1.15, salve amulet = 1.15, salve amulet(e) = 1.2
        int strengthLevel = character.getSkills().getLevel(Skills.STRENGTH);
        int combatStyleBonus = 0;

        if (character.getPrayers().isPrayerOn(Prayer.BURST_OF_STRENGTH)) {
            prayerMultiplier = 1.05;
        } else if (character.getPrayers().isPrayerOn(Prayer.SUPERHUMAN_STRENGTH)) {
            prayerMultiplier = 1.1;
        } else if (character.getPrayers().isPrayerOn(Prayer.ULTIMATE_STRENGTH)) {
            prayerMultiplier = 1.15;
        }

        switch (character.getCombatSession().getCombatStyle()) {
            case AGGRESSIVE_1:
            case AGGRESSIVE_2:
                combatStyleBonus = 3;
                break;
            case CONTROLLED_1:
            case CONTROLLED_2:
            case CONTROLLED_3:
                combatStyleBonus = 1;
                break;
        }

        if (fullVoidMelee(character)) {
            otherBonusMultiplier = 1.1;
        }

        int effectiveStrengthDamage = (int) ((strengthLevel * prayerMultiplier * otherBonusMultiplier) + combatStyleBonus);
        double baseDamage = 1.3 + (effectiveStrengthDamage / 10) + (character.getSkills().getBonus(10) / 80) + ((effectiveStrengthDamage * character.getSkills().getBonus(10)) / 640);

        if (special) {
            if (character.getEquipment().get(Equipment.SLOT_WEAPON) != null) {
                switch (character.getEquipment().get(Equipment.SLOT_WEAPON).getId()) {
                    case 11694:
                        specialMultiplier = 1.34375;
                        break;
                    case 11696:
                        specialMultiplier = 1.1825;
                        break;
                    case 11698:
                    case 11700:
                        specialMultiplier = 1.075;
                        break;
                    case 3101:
                    case 3204:
                    case 5698:
                        specialMultiplier = 1.1;
                        break;
                    case 1305:
                        specialMultiplier = 1.15;
                        break;
                    case 1434:
                        specialMultiplier = 1.45;
                        break;
                }
            }
        }

        maxHit = (int) (baseDamage * specialMultiplier);

        if (fullDharok(character)) {
            int hpLost = character.getSkills().getLevelForExperience(Skills.HITPOINTS) - character.getSkills().getLevel(Skills.HITPOINTS);
            maxHit += hpLost / 2;
        }
        return maxHit;
    }

    /**
     * Calculates a entity's range max hit.
     *
     * @param character The character
     * @param special If using special
     *
     * @return The max hit
     */
    public static int calculateRangeMaxHit(Actor character, boolean special) {
        int maxHit = 0;
        double specialMultiplier = 1;
        double prayerMultiplier = 1;
        double otherBonusMultiplier = 1;
        int rangedStrength = character.getSkills().getBonus(12);
        Item weapon = character.getEquipment().get(Equipment.SLOT_WEAPON);
        BowType bow = EquipmentDefinition.forId(weapon != null ? weapon.getId() : -1).getBowType();

        if (bow != null && bow == BowType.CRYSTAL_BOW) {
            /**
             * Crystal Bow does not use arrows, so we don't use the arrows range
             * strength bonus.
             */
            rangedStrength = character.getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition().getBonus(12);
        }
        int rangeLevel = character.getSkills().getLevel(Skills.RANGE);
        int combatStyleBonus = 0;

        //TODO: Add new prayers: sharp eye, hawk eye, eagle eye
        switch (character.getCombatSession().getCombatStyle()) {
            case ACCURATE:
                combatStyleBonus = 3;
                break;
        }

        if (fullVoidRange(character)) {
            otherBonusMultiplier = 1.1;
        }

        int effectiveRangeDamage = (int) ((rangeLevel * prayerMultiplier * otherBonusMultiplier) + combatStyleBonus);
        double baseDamage = 1.3 + (effectiveRangeDamage / 10) + (rangedStrength / 80) + ((effectiveRangeDamage * rangedStrength) / 640);

        if (special) {
            if (character.getEquipment().get(Equipment.SLOT_ARROWS) != null) {
                switch (character.getEquipment().get(Equipment.SLOT_ARROWS).getId()) {
                    case 11212:
                        specialMultiplier = 1.5;
                        break;
                    case 9243:
                        specialMultiplier = 1.15;
                        break;
                    case 9244:
                        specialMultiplier = 1.45;
                        break;
                    case 9245:
                        specialMultiplier = 1.15;
                        break;
                    case 9236:
                        specialMultiplier = 1.25;
                        break;
                    case 882:
                    case 884:
                    case 886:
                    case 888:
                    case 890:
                    case 892:
                        if (character.getEquipment().get(Equipment.SLOT_WEAPON) != null && character.getEquipment().get(Equipment.SLOT_WEAPON).getId() == 11235) {
                            specialMultiplier = 1.3;
                        }
                        break;
                }
            }
        }

        maxHit = (int) (baseDamage * specialMultiplier);

        return maxHit;
    }

    /**
     * Checks if wearing full void melee armor
     *
     * @param character The entity to check
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public static boolean fullVoidMelee(Actor character) {
        return character.getEquipment() != null
                && character.getEquipment().contains(8839)
                && character.getEquipment().contains(8840)
                && character.getEquipment().contains(8842)
                && character.getEquipment().contains(11665);
    }

    /**
     * Checks if wearing full void range armor
     *
     * @param character The entity to check
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public static boolean fullVoidRange(Actor character) {
        return character.getEquipment() != null
                && character.getEquipment().contains(8839)
                && character.getEquipment().contains(8840)
                && character.getEquipment().contains(11664)
                && character.getEquipment().contains(8097);
    }

    /**
     * Checks if wearing full void made armor
     *
     * @param character The entity to check
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public static boolean fullVoidMage(Actor character) {
        return character.getEquipment() != null
                && character.getEquipment().contains(8839)
                && character.getEquipment().contains(8840)
                && character.getEquipment().contains(11663)
                && character.getEquipment().contains(8098);
    }

    /**
     * Checks if wearing full guthans
     *
     * @param character The entity to check
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public static boolean fullGuthan(Actor character) {
        return character.getEquipment() != null
                && character.getEquipment().contains(4724)
                && character.getEquipment().contains(4726)
                && character.getEquipment().contains(4728)
                && character.getEquipment().contains(4730);
    }

    /**
     * Checks if wearing full torags
     *
     * @param character The entity to check
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public static boolean fullTorag(Actor character) {
        return character.getEquipment() != null
                && character.getEquipment().contains(4745)
                && character.getEquipment().contains(4747)
                && character.getEquipment().contains(4749)
                && character.getEquipment().contains(4751);
    }

    /**
     * Checks if wearing full karils
     *
     * @param character The entity to check
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public static boolean fullKaril(Actor character) {
        return character.getEquipment() != null
                && character.getEquipment().contains(4732)
                && character.getEquipment().contains(4734)
                && character.getEquipment().contains(4736)
                && character.getEquipment().contains(4738);
    }

    /**
     * Checks if wearing full ahrims robes
     *
     * @param character The entity to check
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public static boolean fullAhrim(Actor character) {
        return character.getEquipment() != null
                && character.getEquipment().contains(4708)
                && character.getEquipment().contains(4710)
                && character.getEquipment().contains(4712)
                && character.getEquipment().contains(4714);
    }

    /**
     * Checks if wearing full dharok
     *
     * @param character The entity to check
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public static boolean fullDharok(Actor character) {
        return character.getEquipment() != null
                && character.getEquipment().contains(4716)
                && character.getEquipment().contains(4718)
                && character.getEquipment().contains(4720)
                && character.getEquipment().contains(4722);
    }

    /**
     * Checks if wearing full veracs
     *
     * @param character The entity to check
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public static boolean fullVerac(Actor character) {
        return character.getEquipment() != null
                && character.getEquipment().contains(4753)
                && character.getEquipment().contains(4755)
                && character.getEquipment().contains(4757)
                && character.getEquipment().contains(4759);
    }

    /**
     * Credits to DFDreamed
     */
    /**
     * Gets the max hit by combat level.
     *
     * @param level The combat level
     * @return The max hit
     */
    private static int getMaxHit(int skill, int level) {
        final double levelPadding = level / 100;
        switch (skill) {
            /**
             * The max hit for the Attack skill.
             */
            case Skills.ATTACK:
                return (int) Math.round(level * (2.15 + (0.85 * levelPadding)));
            /**
             * The max hit for the Defense skill.
             */
            case Skills.DEFENCE:
                return (int) Math.round(level * (1.0 + (0.5 * levelPadding)));
            /**
             * The max hit.
             */
            default:
                return 10 + (int) Math.round(level * (1.45 + (1.0 * levelPadding)));
        }
    }
}
