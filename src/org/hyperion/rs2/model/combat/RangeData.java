package org.hyperion.rs2.model.combat;

import org.hyperion.rs2.model.Graphic;

import java.util.HashMap;
import java.util.Map;

public class RangeData {

    /**
     * An enum that represents a type of range bow.
     *
     * @author Michael
     */
    public static enum BowType {

        LONGBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW},
                new int[]{10, 10, 10}),
        SHORTBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW},
                new int[]{7, 7, 9}),
        OAK_SHORTBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW},
                new int[]{7, 7, 9}),
        OAK_LONGBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW},
                new int[]{10, 10, 10}),
        WILLOW_LONGBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW,
            ArrowType.STEEL_ARROW}, new int[]{10, 10, 10}),
        WILLOW_SHORTBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW,
            ArrowType.STEEL_ARROW}, new int[]{7, 7, 9}),
        MAPLE_LONGBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW,
            ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW}, new int[]{10, 10, 10}),
        MAPLE_SHORTBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW,
            ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW}, new int[]{7, 7, 9}),
        YEW_LONGBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW,
            ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW,
            ArrowType.ADAMANT_ARROW}, new int[]{10, 10, 10}),
        YEW_SHORTBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW,
            ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW,
            ArrowType.ADAMANT_ARROW}, new int[]{7, 7, 9}),
        MAGIC_LONGBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW,
            ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW,
            ArrowType.ADAMANT_ARROW, ArrowType.RUNE_ARROW}, new int[]{10, 10, 10}),
        MAGIC_SHORTBOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW,
            ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW,
            ArrowType.ADAMANT_ARROW, ArrowType.RUNE_ARROW}, new int[]{7, 7, 9}),
        CRYSTAL_BOW(new ArrowType[0], new int[]{10, 10, 10}),
        KARILS_XBOW(new ArrowType[]{ArrowType.BOLT_RACK}, new int[]{7, 7, 9}),
        DARK_BOW(new ArrowType[]{ArrowType.BRONZE_ARROW, ArrowType.IRON_ARROW,
            ArrowType.STEEL_ARROW, ArrowType.MITHRIL_ARROW,
            ArrowType.ADAMANT_ARROW, ArrowType.RUNE_ARROW,
            ArrowType.DRAGON_ARROW}, new int[]{10, 10, 10}),
        BRONZE_CBOW(new ArrowType[]{ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT},
                new int[]{7, 7, 9}),
        IRON_CBOW(new ArrowType[]{ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT},
                new int[]{7, 7, 9}),
        STEEL_CBOW(new ArrowType[]{ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT,
            ArrowType.STEEL_BOLT}, new int[]{7, 7, 9}),
        MITH_CBOW(new ArrowType[]{ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT,
            ArrowType.STEEL_BOLT, ArrowType.MITHRIL_BOLT}, new int[]{7, 7, 9}),
        ADAMANT_CBOW(new ArrowType[]{ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT,
            ArrowType.STEEL_BOLT, ArrowType.MITHRIL_BOLT,
            ArrowType.ADAMANT_BOLT}, new int[]{7, 7, 9}),
        RUNE_CBOW(new ArrowType[]{ArrowType.BRONZE_BOLT, ArrowType.IRON_BOLT,
            ArrowType.STEEL_BOLT, ArrowType.MITHRIL_BOLT,
            ArrowType.ADAMANT_BOLT, ArrowType.RUNE_BOLT}, new int[]{7, 7, 9}),;

        /**
         * The arrows this bow can use.
         */
        private final ArrowType[] validArrows;

        /**
         * The distances required to be near the victim based on the mob's
         * combat style.
         */
        private final int[] distances;

        /**
         * Creates a bow type
         *
         * @param validArrows The valid arrows
         * @param distances The distances
         */
        private BowType(ArrowType[] validArrows, int[] distances) {
            this.validArrows = validArrows;
            this.distances = distances;
        }

        /**
         * Gets the valid arrows this bow can use.
         *
         * @return The valid arrows this bow can use.
         */
        public ArrowType[] getValidArrows() {
            return validArrows;
        }

        /**
         * Gets a valid arrow this bow can use by its index.
         *
         * @param index The arrow index.
         * @return The valid arrow this bow can use by its index.
         */
        public ArrowType getValidArrow(int index) {
            return validArrows[index];
        }

        /**
         * Gets a distance required to be near the victim.
         *
         * @param index The combat style index.
         * @return The distance required to be near the victim
         */
        public int getDistance(int index) {
            return distances[index];
        }
    }

    /**
     * An enum for all arrow types, this includes the drop rate percentage of
     * this arrow (the higher quality the less likely it is to disappear).
     *
     * @author Michael Bull
     * @author Sir Sean
     */
    public static enum ArrowType {

        BRONZE_ARROW(0.75, Graphic.create(19, 0, 100), 10),
        IRON_ARROW(0.7, Graphic.create(18, 0, 100), 9),
        STEEL_ARROW(0.65, Graphic.create(20, 0, 100), 11),
        MITHRIL_ARROW(0.6, Graphic.create(21, 0, 100), 12),
        ADAMANT_ARROW(0.5, Graphic.create(22, 0, 100), 13),
        RUNE_ARROW(0.4, Graphic.create(24, 0, 100), 15),
        BOLT_RACK(1.1, null, 27),
        DRAGON_ARROW(0.3, Graphic.create(1111, 0, 100), 1115),
        BRONZE_BOLT(0.75, null, 27),
        IRON_BOLT(0.7, null, 27),
        STEEL_BOLT(0.65, null, 27),
        MITHRIL_BOLT(0.6, null, 27),
        ADAMANT_BOLT(0.5, null, 27),
        RUNE_BOLT(0.4, null, 27),
        CRYSTAL_ARROW(1.1, Graphic.create(249, 0, 100), 249);

        /**
         * The percentage chance for the arrow to disappear once fired.
         */
        private final double dropRate;

        /**
         * The pullback graphic.
         */
        private final Graphic pullback;

        /**
         * The projectile id.
         */
        private final int projectile;

        /**
         * Creates an arrow type
         *
         * @param dropRate The drop rate
         * @param pullback The pullback graphic
         * @param projectile The projectile graphic Id
         */
        private ArrowType(double dropRate, Graphic pullback, int projectile) {
            this.dropRate = dropRate;
            this.pullback = pullback;
            this.projectile = projectile;
        }

        /**
         * Gets the arrow's percentage chance to disappear once fired
         *
         * @return The arrow's percentage chance to disappear once fired.
         */
        public double getDropRate() {
            return dropRate;
        }

        /**
         * Gets the arrow's pullback graphic.
         *
         * @return The arrow's pullback graphic.
         */
        public Graphic getPullbackGraphic() {
            return pullback;
        }

        /**
         * Gets the arrow's projectile id.
         *
         * @return The arrow's projectile id.
         */
        public int getProjectileId() {
            return projectile;
        }
    }

    /**
     * An enum that represents all range weapons, e.g. throwing knives and
     * javelins.
     *
     * @author Michael
     */
    public static enum RangeWeaponType {

        BRONZE_KNIFE(0.75, Graphic.create(219, 0, 100), 212, new int[]{4, 4, 6}),
        IRON_KNIFE(0.7, Graphic.create(220, 0, 100), 213, new int[]{4, 4, 6}),
        STEEL_KNIFE(0.65, Graphic.create(221, 0, 100), 214, new int[]{4, 4, 6}),
        MITHRIL_KNIFE(0.6, Graphic.create(223, 0, 100), 216, new int[]{4, 4, 6}),
        ADAMANT_KNIFE(0.5, Graphic.create(224, 0, 100), 217, new int[]{4, 4, 6}),
        RUNE_KNIFE(0.4, Graphic.create(225, 0, 100), 218, new int[]{4, 4, 6}),
        BLACK_KNIFE(0.6, Graphic.create(222, 0, 100), 215, new int[]{4, 4, 6}),
        BRONZE_DART(0.75, Graphic.create(1234, 0, 100), 226, new int[]{3, 3, 5}),
        IRON_DART(0.7, Graphic.create(1235, 0, 100), 227, new int[]{3, 3, 5}),
        STEEL_DART(0.65, Graphic.create(1236, 0, 100), 228, new int[]{3, 3, 5}),
        MITHRIL_DART(0.6, Graphic.create(1237, 0, 100), 229, new int[]{3, 3, 5}),
        ADAMANT_DART(0.5, Graphic.create(1239, 0, 100), 230, new int[]{3, 3, 5}),
        RUNE_DART(0.4, Graphic.create(1240, 0, 100), 231, new int[]{3, 3, 5}),
        BLACK_DART(0.6, Graphic.create(1238, 0, 100), 231, new int[]{3, 3, 5}),
        BRONZE_THROWNAXE(0.75, Graphic.create(42, 0, 100), 36, new int[]{4, 4, 6}),
        IRON_THROWNAXE(0.7, Graphic.create(43, 0, 100), 35, new int[]{4, 4, 6}),
        STEEL_THROWNAXE(0.65, Graphic.create(44, 0, 100), 37, new int[]{4, 4, 6}),
        MITHRIL_THROWNAXE(0.6, Graphic.create(45, 0, 100), 38, new int[]{4, 4, 6}),
        ADAMANT_THROWNAXE(0.5, Graphic.create(46, 0, 100), 39, new int[]{4, 4, 6}),
        RUNE_THROWNAXE(0.4, Graphic.create(48, 0, 100), 41, new int[]{4, 4, 6}),
        BRONZE_JAVELIN(0.75, Graphic.create(206, 0, 100), 200, new int[]{4, 4, 6}),
        IRON_JAVELIN(0.7, Graphic.create(207, 0, 100), 201, new int[]{4, 4, 6}),
        STEEL_JAVELIN(0.65, Graphic.create(208, 0, 100), 202, new int[]{4, 4, 6}),
        MITHRIL_JAVELIN(0.6, Graphic.create(209, 0, 100), 203, new int[]{4, 4, 6}),
        ADAMANT_JAVELIN(0.5, Graphic.create(210, 0, 100), 204, new int[]{4, 4, 6}),
        RUNE_JAVELIN(0.4, Graphic.create(211, 0, 100), 205, new int[]{4, 4, 6}),
        OBSIDIAN_RING(0.45, null, 442, new int[]{7, 7, 9}),;

        /**
         * The percentage chance for the arrow to disappear once fired.
         */
        private final double dropRate;

        /**
         * The pullback graphic.
         */
        private final Graphic pullback;

        /**
         * The projectile id.
         */
        private final int projectile;

        /**
         * The distances required for each attack type.
         */
        private final int[] distances;

        /**
         * The range weapon type
         *
         * @param dropRate The drop rate
         * @param pullback The pullback graphic
         * @param projectile The projectile Id
         * @param distances The distances
         */
        private RangeWeaponType(double dropRate, Graphic pullback, int projectile, int[] distances) {
            this.dropRate = dropRate;
            this.pullback = pullback;
            this.projectile = projectile;
            this.distances = distances;
        }

        /**
         * Gets the drop rate
         *
         * @return the dropRate
         */
        public double getDropRate() {
            return dropRate;
        }

        /**
         * Gets the pullback graphic
         *
         * @return the pullback
         */
        public Graphic getPullbackGraphic() {
            return pullback;
        }

        /**
         * Gets the projectile ID
         *
         * @return the projectile
         */
        public int getProjectileId() {
            return projectile;
        }

        /**
         * Gets the distances
         *
         * @return the distances
         */
        public int[] getDistances() {
            return distances;
        }

        /**
         * Gets a distance
         *
         * @param index The index
         * @return the distances
         */
        public int getDistance(int index) {
            return distances[index];
        }
    }

    public static enum KnifeType {

        BRONZE_KNIFE(new int[]{864, 870, 5654, 5661}, 0.75, Graphic.create(219, 0, 100), 212, new int[]{4, 4, 6}),
        IRON_KNIFE(new int[]{863, 871, 5655, 5662}, 0.7, Graphic.create(220, 0, 100), 213, new int[]{4, 4, 6}),
        STEEL_KNIFE(new int[]{865, 872, 5656, 5663}, 0.65, Graphic.create(221, 0, 100), 214, new int[]{4, 4, 6}),
        MITHRIL_KNIFE(new int[]{866, 873, 5657, 5664}, 0.6, Graphic.create(223, 0, 100), 216, new int[]{4, 4, 6}),
        ADAMANT_KNIFE(new int[]{867, 875, 5659, 5666}, 0.5, Graphic.create(224, 0, 100), 217, new int[]{4, 4, 6}),
        RUNE_KNIFE(new int[]{868, 876, 5660, 5667}, 0.4, Graphic.create(225, 0, 100), 218, new int[]{4, 4, 6}),
        BLACK_KNIFE(new int[]{869, 874, 5658, 5665}, 0.6, Graphic.create(222, 0, 100), 215, new int[]{4, 4, 6});

        /**
         * A map of knife types.
         */
        private static final Map<Integer, KnifeType> knifeTypes = new HashMap<>();

        /**
         * Gets an knife type by its item ID.
         *
         * @param knifeType The knife item id.
         * @return The knife type, or <code>null</code> if the id is not an
         * knife type.
         */
        public static KnifeType forId(int knifeType) {
            return knifeTypes.get(knifeType);
        }

        /**
         * Populates the knife type map.
         */
        static {
            for (KnifeType knifeType : KnifeType.values()) {
                for (int i = 0; i < knifeType.ids.length; i++) {
                    knifeTypes.put(knifeType.ids[i], knifeType);
                }
            }
        }

        /**
         * The knifes item id's.
         */
        private int[] ids;

        /**
         * The percentage chance for the knife to disappear once fired.
         */
        private final double dropRate;

        /**
         * The pullback graphic.
         */
        private final Graphic pullback;

        /**
         * The projectile id.
         */
        private final int projectile;

        /**
         * The distances required to be near the victim based on the mob's
         * combat style.
         */
        private final int[] distances;

        /**
         * Creates a knife type
         *
         * @param ids The knife IDs
         * @param dropRate The drop rate
         * @param pullback The pullback graphic
         * @param projectile The projectile graphic
         * @param distances The distances
         */
        private KnifeType(int ids[], double dropRate, Graphic pullback, int projectile, int[] distances) {
            this.ids = ids;
            this.dropRate = dropRate;
            this.distances = distances;
            this.pullback = pullback;
            this.projectile = projectile;
        }

        /**
         * Gets the IDS
         *
         * @return The IDs
         */
        public int[] getIds() {
            return ids;
        }

        /**
         * Gets a type
         *
         * @param index The index
         * @return the id
         */
        public int getId(int index) {
            return ids[index];
        }

        /**
         * Gets the knife's percentage chance to disappear once fired
         *
         * @return The knife's percentage chance to disappear once fired.
         */
        public double getDropRate() {
            return dropRate;
        }

        /**
         * Gets a distance required to be near the victim.
         *
         * @param index The combat style index.
         * @return The distance required to be near the victim
         */
        public int getDistance(int index) {
            return distances[index];
        }

        /**
         * Gets the knife's pullback graphic.
         *
         * @return The knife's pullback graphic.
         */
        public Graphic getPullbackGraphic() {
            return pullback;
        }

        /**
         * Gets the knife's projectile id.
         *
         * @return The knife's projectile id.
         */
        public int getProjectileId() {
            return projectile;
        }

    }

    public static enum DartType {

        BRONZE_DART(new int[]{806, 812, 5628, 5635}, 0.75, Graphic.create(232, 0, 100), 226, new int[]{3, 3, 5}),
        IRON_DART(new int[]{807, 813, 5629, 5636}, 0.7, Graphic.create(233, 0, 100), 227, new int[]{3, 3, 5}),
        STEEL_DART(new int[]{808, 814, 5630, 5637}, 0.65, Graphic.create(234, 0, 100), 228, new int[]{3, 3, 5}),
        MITHRIL_DART(new int[]{809, 815, 5632, 5639}, 0.6, Graphic.create(235, 0, 100), 229, new int[]{3, 3, 5}),
        ADAMANT_DART(new int[]{810, 816, 3633, 5640}, 0.5, Graphic.create(236, 0, 100), 230, new int[]{3, 3, 5}),
        RUNE_DART(new int[]{811, 817, 5636, 5641}, 0.4, Graphic.create(237, 0, 100), 231, new int[]{3, 3, 5}),
        BLACK_DART(new int[]{3093, 3094, 5631, 5638}, 0.6, Graphic.create(237, 0, 100), 231, new int[]{3, 3, 5});

        /**
         * A map of knife types.
         */
        private static final Map<Integer, DartType> dartTypes = new HashMap<>();

        /**
         * Gets an dart type by its item ID.
         *
         * @param dartType The dart item id.
         * @return The dart type, or <code>null</code> if the id is not an dart
         * type.
         */
        public static DartType forId(int dartType) {
            return dartTypes.get(dartType);
        }

        /**
         * Populates the dart type map.
         */
        static {
            for (DartType dartType : DartType.values()) {
                for (int i = 0; i < dartType.ids.length; i++) {
                    dartTypes.put(dartType.ids[i], dartType);
                }
            }
        }

        /**
         * The dart's item id's.
         */
        private int[] ids;

        /**
         * The percentage chance for the knife to disappear once fired.
         */
        private final double dropRate;

        /**
         * The pullback graphic.
         */
        private final Graphic pullback;

        /**
         * The projectile id.
         */
        private final int projectile;

        /**
         * The distances required to be near the victim based on the mob's
         * combat style.
         */
        private final int[] distances;

        /**
         * Creates a dart type
         *
         * @param ids The dart IDs
         * @param dropRate The drop rate
         * @param pullback The pullback graphic
         * @param projectile The projectile ID
         * @param distances The distances
         */
        private DartType(int ids[], double dropRate, Graphic pullback, int projectile, int[] distances) {
            this.ids = ids;
            this.dropRate = dropRate;
            this.distances = distances;
            this.pullback = pullback;
            this.projectile = projectile;
        }

        /**
         * The dart IDS
         *
         * @return The IDs
         */
        public int[] getIds() {
            return ids;
        }

        /**
         * @param index The index
         * @return the id
         */
        public int getId(int index) {
            return ids[index];
        }

        /**
         * Gets the dart's percentage chance to disappear once fired
         *
         * @return The dart's percentage chance to disappear once fired.
         */
        public double getDropRate() {
            return dropRate;
        }

        /**
         * Gets a distance required to be near the victim.
         *
         * @param index The combat style index.
         * @return The distance required to be near the victim
         */
        public int getDistance(int index) {
            return distances[index];
        }

        /**
         * Gets the dart's pullback graphic.
         *
         * @return The dart's pullback graphic.
         */
        public Graphic getPullbackGraphic() {
            return pullback;
        }

        /**
         * Gets the dart's projectile id.
         *
         * @return The dart's projectile id.
         */
        public int getProjectileId() {
            return projectile;
        }

    }

    public static enum ThrownaxeType {

        BRONZE_THROWNAXE(new int[]{800}, 0.75, Graphic.create(42, 0, 100), 36, new int[]{4, 4, 6}),
        IRON_THROWNAXE(new int[]{801}, 0.7, Graphic.create(43, 0, 100), 35, new int[]{4, 4, 6}),
        STEEL_THROWNAXE(new int[]{802}, 0.65, Graphic.create(44, 0, 100), 37, new int[]{4, 4, 6}),
        MITHRIL_THROWNAXE(new int[]{803}, 0.6, Graphic.create(45, 0, 100), 38, new int[]{4, 4, 6}),
        ADAMANT_THROWNAXE(new int[]{804}, 0.5, Graphic.create(46, 0, 100), 39, new int[]{4, 4, 6}),
        RUNE_THROWNAXE(new int[]{805}, 0.4, Graphic.create(48, 0, 100), 41, new int[]{4, 4, 6});

        /**
         * A map of knife types.
         */
        private static final Map<Integer, ThrownaxeType> thrownaxeTypes = new HashMap<>();

        /**
         * Gets an thrownaxe type by its item ID.
         *
         * @param thrownaxeType The thrownaxe item id.
         * @return The thrownaxe type, or <code>null</code> if the id is not an
         * thrownaxe type.
         */
        public static ThrownaxeType forId(int thrownaxeType) {
            return thrownaxeTypes.get(thrownaxeType);
        }

        /**
         * Populates the thrownaxe type map.
         */
        static {
            for (ThrownaxeType thrownaxeType : ThrownaxeType.values()) {
                for (int i = 0; i < thrownaxeType.ids.length; i++) {
                    thrownaxeTypes.put(thrownaxeType.ids[i], thrownaxeType);
                }
            }
        }

        /**
         * The thrownaxe's item id's.
         */
        private int[] ids;

        /**
         * The percentage chance for the knife to disappear once fired.
         */
        private final double dropRate;

        /**
         * The pullback graphic.
         */
        private final Graphic pullback;

        /**
         * The projectile id.
         */
        private final int projectile;

        /**
         * The distances required to be near the victim based on the mob's
         * combat style.
         */
        private final int[] distances;

        /**
         * Creates throwing axe types
         *
         * @param ids The throwing axe IDs
         * @param dropRate The drop rate
         * @param pullback The pullback graphic
         * @param projectile The projectile ID
         * @param distances The distances
         */
        private ThrownaxeType(int ids[], double dropRate, Graphic pullback, int projectile, int[] distances) {
            this.ids = ids;
            this.dropRate = dropRate;
            this.distances = distances;
            this.pullback = pullback;
            this.projectile = projectile;
        }

        /**
         * Gets the IDS
         *
         * @return The IDs
         */
        public int[] getIds() {
            return ids;
        }

        /**
         * Gets a type
         *
         * @param index The index
         * @return the id
         */
        public int getId(int index) {
            return ids[index];
        }

        /**
         * Gets the thrownaxe's percentage chance to disappear once fired
         *
         * @return The thrownaxe's percentage chance to disappear once fired.
         */
        public double getDropRate() {
            return dropRate;
        }

        /**
         * Gets a distance required to be near the victim.
         *
         * @param index The combat style index.
         * @return The distance required to be near the victim
         */
        public int getDistance(int index) {
            return distances[index];
        }

        /**
         * Gets the thrownaxe's pullback graphic.
         *
         * @return The thrownaxe's pullback graphic.
         */
        public Graphic getPullbackGraphic() {
            return pullback;
        }

        /**
         * Gets the thrownaxe's projectile id.
         *
         * @return The thrownaxe's projectile id.
         */
        public int getProjectileId() {
            return projectile;
        }

    }

    public static enum JavelinType {

        BRONZE_JAVELIN(new int[]{825, 831, 5642, 5648}, 0.75, Graphic.create(206, 0, 100), 20, new int[]{4, 4, 6}),
        IRON_JAVELIN(new int[]{826, 832, 5643, 5649}, 0.7, Graphic.create(207, 0, 100), 201, new int[]{4, 4, 6}),
        STEEL_JAVELIN(new int[]{827, 833, 5644, 5650}, 0.65, Graphic.create(208, 0, 100), 202, new int[]{4, 4, 6}),
        MITHRIL_JAVELIN(new int[]{828, 834, 5645, 5651}, 0.6, Graphic.create(209, 0, 100), 203, new int[]{4, 4, 6}),
        ADAMANT_JAVELIN(new int[]{829, 835, 5646, 5652}, 0.5, Graphic.create(210, 0, 100), 204, new int[]{4, 4, 6}),
        RUNE_JAVELIN(new int[]{830, 836, 5647, 5653}, 0.4, Graphic.create(211, 0, 100), 205, new int[]{4, 4, 6});

        /**
         * A map of javelin types.
         */
        private static final Map<Integer, JavelinType> javelinTypes = new HashMap<>();

        /**
         * Gets an javelin type by its item ID.
         *
         * @param javelinType The javelin item id.
         * @return The javelin type, or <code>null</code> if the id is not an
         * javelin type.
         */
        public static JavelinType forId(int javelinType) {
            return javelinTypes.get(javelinType);
        }

        /**
         * Populates the javelin type map.
         */
        static {
            for (JavelinType javelinType : JavelinType.values()) {
                for (int i = 0; i < javelinType.ids.length; i++) {
                    javelinTypes.put(javelinType.ids[i], javelinType);
                }
            }
        }

        /**
         * The javelin item id's.
         */
        private int[] ids;

        /**
         * The percentage chance for the knife to disappear once fired.
         */
        private final double dropRate;

        /**
         * The pullback graphic.
         */
        private final Graphic pullback;

        /**
         * The projectile id.
         */
        private final int projectile;

        /**
         * The distances required to be near the victim based on the mob's
         * combat style.
         */
        private final int[] distances;

        /**
         * Creates javelin types
         *
         * @param ids The javelin IDs
         * @param dropRate The drop rate
         * @param pullback The pullback graphic
         * @param projectile The projectile ID
         * @param distances The distances
         */
        private JavelinType(int ids[], double dropRate, Graphic pullback, int projectile, int[] distances) {
            this.ids = ids;
            this.dropRate = dropRate;
            this.distances = distances;
            this.pullback = pullback;
            this.projectile = projectile;
        }

        /**
         * Gets the IDS
         *
         * @return The IDs
         */
        public int[] getIds() {
            return ids;
        }

        /**
         * Gets a type
         *
         * @param index The index
         * @return the id
         */
        public int getId(int index) {
            return ids[index];
        }

        /**
         * Gets the javelin's percentage chance to disappear once fired
         *
         * @return The javelin's percentage chance to disappear once fired.
         */
        public double getDropRate() {
            return dropRate;
        }

        /**
         * Gets a distance required to be near the victim.
         *
         * @param index The combat style index.
         * @return The distance required to be near the victim
         */
        public int getDistance(int index) {
            return distances[index];
        }

        /**
         * Gets the javelin's pullback graphic.
         *
         * @return The javelin's pullback graphic.
         */
        public Graphic getPullbackGraphic() {
            return pullback;
        }

        /**
         * Gets the javelin's projectile id.
         *
         * @return The javelin's projectile id.
         */
        public int getProjectileId() {
            return projectile;
        }

    }
}
