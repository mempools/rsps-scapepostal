package org.hyperion.rs2.model.combat.impl;

import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.ActorCooldowns.CooldownFlags;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.combat.CombatAction;
import org.hyperion.rs2.model.combat.CombatHandler;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.tickable.Tickable;

public class Melee extends AbstractCombat {

    /**
     * The instance.
     */
    private static final Melee INSTANCE = new Melee();

    /**
     * Gets the instance.
     *
     * @return The instance.
     */
    public static CombatAction getAction() {
        return INSTANCE;
    }

    @Override
    public CooldownFlags getCooldownFlags() {
        return CooldownFlags.MELEE_SWING;
    }

    @Override
    public int distance(Actor source) {
        Item weapon = source.getEquipment().get(Equipment.SLOT_WEAPON);
        if (weapon != null && weapon.getDefinition().getName().toLowerCase().contains("halberd")) {
            source.getWalkingQueue().reset();
            return 2;
        }
        return 1;
    }

    @Override
    public boolean canAttack(Actor source, Actor target) {
        return super.canAttack(source, target);
    }

    @Override
    public void attack(final Actor source, final Actor target) {
        final int damage = damage(source, target);
        Item weapon = source.getEquipment().get(Equipment.SLOT_WEAPON);
        if ((weapon != null && weapon.getEquipmentDefinition() != null) && !source.isNPC()) {
            int attackAnimationIndex = source.getCombatSession().getCombatStyle().getId();
            if (attackAnimationIndex > 3) {
                attackAnimationIndex -= 4;
            }
            source.playAnimation(weapon.getEquipmentDefinition().getAnimation().getAttackAnimation(attackAnimationIndex));
        } else {
            source.playAnimation(source.getAttackAnimation());
        }
        World.getWorld().submit(new Tickable(CombatHandler.getCombatHitDelay(source)) {
            @Override
            public void execute() {
                inflictDamage(source, target, damage);
                target.getActiveCombatAction().defend(source, target);
                this.stop();
            }
        });
        target.getActiveCombatAction().defend(source, target);
    }

    @Override
    public void specialAttack(final Actor source, final Actor target) {
        World.getWorld().submit(new Tickable(1) {
            @Override
            public void execute() {
                int dmg = damage(source, target);
                int specialHit = random.nextInt(dmg < 1 ? 1 : dmg + 1);
                special(source, target, specialHit);
                this.stop();
            }
        });
    }

    @Override
    public void inflictDamage(Actor source, Actor target, int damage) {
        super.inflictDamage(source, target, damage);
    }

    @Override
    public void defend(Actor source, Actor target) {
        super.defend(source, target);
    }

    @Override
    public int damage(Actor source, Actor target) {
        return super.damage(source, target);
    }

    @Override
    public void special(Actor source, Actor target, int damage) {
        super.special(source, target, damage);
    }

    @Override
    public void addExperience(Actor source, int damage) {
        /*if (source.isPlayer()) {
         super.addExperience(source, damage);
         for (int i = 0; i < source.getCombatSession().getCombatStyle().getSkills().length; i++) {
         double exp = source.getCombatSession().getCombatStyle().getExperience(i);
         source.getSkills().addExperience(source.getCombatSession().getCombatStyle().getSkill(i), (exp * damage));
         }
         }     */
    }

    @Override
    public int getSpeed(Actor source, Actor target) {
        return super.getSpeed(source, target);
    }
}
