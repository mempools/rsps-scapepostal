package org.hyperion.rs2.model.combat.impl;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.action.impl.AttackAction;
import org.hyperion.rs2.model.*;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Damage.Hit;
import org.hyperion.rs2.model.Damage.HitType;
import org.hyperion.rs2.model.combat.CombatAction;
import org.hyperion.rs2.model.combat.Formulae;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.tickable.Tickable;

import java.util.Random;
import org.hyperion.rs2.model.ActorCooldowns.CooldownFlags;

/**
 * Credits to the Rs2-Server team for the structure(and everything else)!<br> A
 * super class that supercedes all the other combat actions.
 */
public abstract class AbstractCombat implements CombatAction {

    /**
     * A random
     */
    public Random random = new Random();

    @Override
    public boolean canAttack(Actor source, Actor target) {
        // PvP combat, PvE not supported (yet)
        if (target.isDead() || source.isDead()) {
            return false;
        }
        if (source.isPlayer() && target.isPlayer()) {
            // attackable zones, etc
        }
        /*
         * Friendly fire on gangs
         */
        if (source.getGang() != null && target.getGang() != null) {
            /*
             * If the gang leader is the same as the target's gang leader
             */
            if (source.getGang().getLeader().equals(target.getGang().getLeader())) {
                source.getActionSender().sendMessage("You cannot attack your own gang!");
                return false;
            }
        }
        return true;
    }

    @Override
    public void start(final Actor source, Actor target) {
        if (!canAttack(source, target)) {
            return;
        }
        if (!source.getEntityCooldowns().get(getCooldownFlags())) {
            source.getCombatSession().setInCombat(true);
            source.setAggressorState(true);

            target.getCollectiveCombatSession().setLastHitTimer(10000);
            source.getCollectiveCombatSession().setLastHitTimer(10000);

            source.getCollectiveCombatSession().setLastOpponent(target);
            target.getCollectiveCombatSession().setLastOpponent(source);

            source.setInteractingActor(target);

            source.getAttributes().set(Attributes.KILLED_BY, target);
            target.getAttributes().set(Attributes.KILLED_BY, source);

            //source.face(target.getLocation());
            source.canAnimate(false);
            World.getWorld().submit(new Tickable(1) {
                @Override
                public void execute() {
                    source.canAnimate(true);
                    this.stop();
                }
            });
            if (!source.getCombatSession().isSpecialOn()) {
                attack(source, target);
            } else {
                if (!source.getEntityCooldowns().get(CooldownFlags.SPECIAL_ATTACK)) {
                    specialAttack(source, target);
                    source.getEntityCooldowns().flag(CooldownFlags.SPECIAL_ATTACK, getSpeed(source, target), source);
                } else {
                    source.getActionSender().sendMessage("You can't special right now!");
                }
            }
            source.getEntityCooldowns().flag(getCooldownFlags(), getSpeed(source, target), source);
        }
    }

    @Override
    public void defend(Actor source, Actor target) {
        if ((target.isPlayer() && ((Player) target).getSettings().isAutoRetaliating()) || target.isNPC()) {
            //target.face(source.getLocation());
            target.getActionQueue().addAction(new AttackAction(target, source));
        }
        if (target.canAnimate()) {
            Animation defend = Animation.create(404);
            Item shield = target.getEquipment().get(Equipment.SLOT_SHIELD);
            Item weapon = target.getEquipment().get(Equipment.SLOT_WEAPON);
            String shieldName = shield != null ? shield.getDefinition().getName() : "";
            if (shieldName.contains("shield") || shieldName.contains("ket-xil")) {
                defend = shield.getEquipmentDefinition().getAnimation().getDefendAnimation();
            } else if (weapon != null) {
                defend = weapon.getEquipmentDefinition().getAnimation().getDefendAnimation();
            } else if (shield != null) {
                defend = shield.getEquipmentDefinition().getAnimation().getDefendAnimation();
            } else {
                defend = target.getDefendAnimation();
            }
            target.playAnimation(defend);
        }
    }

    @Override
    public void inflictDamage(Actor source, Actor target, int verdict) {
        HitType hit;

        /*
         * If the damage verdict is over the target's health, just set it at the target's health
         */
        if (verdict >= target.getSkills().getLevel(Skills.HITPOINTS)) {
            verdict = target.getSkills().getLevel(Skills.HITPOINTS);
        }

        if (verdict == 0) {
            hit = HitType.NO_DAMAGE;
        } else {
            hit = HitType.NORMAL_DAMAGE;
        }

        target.inflictDamage(new Hit(verdict, hit));
        addExperience(source, verdict);
    }

    @Override
    public int damage(Actor source, Actor target) {
        int damage = 1;
        if (source.isPlayer()) {
            Player player = (Player) source;
            int potBonus = 0;
            int prayerBonus = 0;
            int styleBonus = 0;

            double effectiveStrength = ((player.getSkills().getLevel(
                    Skills.STRENGTH) + potBonus)
                    + prayerBonus + potBonus)
                    + styleBonus;
            double baseDamage = 1.3 + (effectiveStrength / 10)
                    + (player.getSkills().getBonus(10) / 80)
                    + ((effectiveStrength * player.getSkills().getBonus(10)) / 640);

            damage = (int) (Math.random() * (Math.ceil((baseDamage)) + 1));
            return damage;
        }
        return damage;
    }

    @Override
    public void special(final Actor source, final Actor target, int damageAmount) {
        final Damage damage = new Damage();
        Item weapon = source.getEquipment().get(Equipment.SLOT_WEAPON);
        source.getCombatSession().inverseSpecial();
        if (!(source.getCombatSession().getSpecialAmount() >= weapon.getEquipmentDefinition().getSpecialConsumption())) {
            source.getActionSender().sendMessage("You do not have enough special energy.");
            return;
        }
        switch (weapon.getId()) {
            /*
             * Dragon dagger
             */
            case 5698:
                source.playAnimation(Animation.create(1062));
                source.playGraphics(Graphic.create(252, 0, 100));
                break;
        }
        /*
         * Primary damage
         */
        damage.setPrimaryHit(new Hit(Formulae.calculateMeleeMaxHit(source, true) + damageAmount, null));
        inflictDamage(source, target, damage.getPrimaryDamage());
        /*
         * Secondary damage
         */
        if (weapon.getEquipmentDefinition().getSpecialHits() > 1) {
            damage.setSecondaryHit(new Hit(Formulae.calculateMeleeMaxHit(source, false) + damageAmount, null));
            World.getWorld().submit(new Tickable((int) 0.5) {
                @Override
                public void execute() {
                    inflictDamage(source, target, damage.getSecondaryDamage());
                    this.stop();
                }
            });
        }
        source.getCombatSession().decreaseSpecialAmount(weapon.getEquipmentDefinition().getSpecialConsumption());
    }

    @Override
    public void addExperience(Actor source, int damage) {
        //source.getSkills().addExperience(Skills.HITPOINTS, (damage * 0.3) * Constants.EXP_BONUS);
    }

    @Override
    public int getSpeed(Actor source, Actor target) {
        int speed = source.getCombatCooldownDelay();
        if (target.getInteractingActor() != source) {
            speed += 0;
        }
        return speed;
    }
}
