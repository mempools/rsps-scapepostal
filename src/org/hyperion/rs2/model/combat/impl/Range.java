package org.hyperion.rs2.model.combat.impl;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.ActorCooldowns.CooldownFlags;
import org.hyperion.rs2.model.*;
import org.hyperion.rs2.model.combat.CombatAction;
import org.hyperion.rs2.model.combat.CombatStyle;
import org.hyperion.rs2.model.combat.Formulae;
import org.hyperion.rs2.model.combat.Poison.PoisonType;
import org.hyperion.rs2.model.combat.RangeData.*;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.definition.EquipmentDefinition;
import org.hyperion.rs2.tickable.Tickable;

/**
 * Created by IntelliJ IDEA. User: Date: 12/26/11 | 12:54 PM
 */
public class Range extends AbstractCombat {

    /**
     * The instance.
     */
    private static final Range INSTANCE = new Range();

    /**
     * Gets the instance.
     *
     * @return The instance.
     */
    public static CombatAction getAction() {
        return INSTANCE;
    }

    @Override
    public CooldownFlags getCooldownFlags() {
        return CooldownFlags.RANGED_SHOT;
    }

    @Override
    public int distance(Actor source) {
        int dist = 1;

        Item weapon = source.getEquipment().get(Equipment.SLOT_WEAPON);
        int weaponId = weapon != null ? weapon.getId() : -1;
        BowType bow = EquipmentDefinition.forId(weaponId).getBowType();
        KnifeType knife = KnifeType.forId(weaponId);
        DartType dart = DartType.forId(weaponId);
        ThrownaxeType thrownaxe = ThrownaxeType.forId(weaponId);
        JavelinType javelin = JavelinType.forId(weaponId);
        if (bow != null) {
            dist = bow.getDistance(source.getCombatSession().getCombatStyle().getId() - (source.getCombatSession().getCombatStyle() == CombatStyle.DEFENSIVE ? 1 : 0));
        } else if (knife != null) {
            dist = knife.getDistance(source.getCombatSession().getCombatStyle().getId() - (source.getCombatSession().getCombatStyle() == CombatStyle.DEFENSIVE ? 1 : 0));
        } else if (dart != null) {
            dist = dart.getDistance(source.getCombatSession().getCombatStyle().getId() - (source.getCombatSession().getCombatStyle() == CombatStyle.DEFENSIVE ? 1 : 0));
        } else if (thrownaxe != null) {
            dist = thrownaxe.getDistance(source.getCombatSession().getCombatStyle().getId() - (source.getCombatSession().getCombatStyle() == CombatStyle.DEFENSIVE ? 1 : 0));
        } else if (javelin != null) {
            dist = javelin.getDistance(source.getCombatSession().getCombatStyle().getId() - (source.getCombatSession().getCombatStyle() == CombatStyle.DEFENSIVE ? 1 : 0));
        } else if (weapon.getId() == 6522) {
            dist = source.getCombatSession().getCombatStyle() == CombatStyle.DEFENSIVE ? 9 : 7;
        }
        return dist;
    }

    @Override
    public boolean canAttack(Actor source, Actor target) {
        if (!super.canAttack(source, target)) {
            return false;
        }
        Item weapon = source.getEquipment().get(Equipment.SLOT_WEAPON);
        int weaponId = weapon != null ? weapon.getId() : -1;

        Item arrow = source.getEquipment().get(Equipment.SLOT_ARROWS);
        int arrowId = arrow != null ? arrow.getId() : -1;

        BowType bow = EquipmentDefinition.forId(weaponId).getBowType();
        ArrowType arrowType = EquipmentDefinition.forId(arrowId).getArrowType();
        if (bow != null && bow != BowType.CRYSTAL_BOW) {
            if (source.getEquipment().get(Equipment.SLOT_ARROWS) == null || arrowType == null) {
                source.getActionSender().sendMessage("There are no " + (bow == BowType.KARILS_XBOW ? "bolts" : "arrows") + " left in your quiver.");
                return false;
            }
            boolean rangeArrows = true;
            for (int i = 0; i < bow.getValidArrows().length; i++) {
                if (arrowId == bow.getValidArrow(i).getProjectileId()) {
                    rangeArrows = true;
                    break;
                }
            }
            if (!rangeArrows) {
                source.getActionSender().sendMessage("You can't use " + source.getEquipment().get(Equipment.SLOT_ARROWS).getDefinition().getName()
                        + "s with a " + source.getEquipment().get(Equipment.SLOT_WEAPON).getDefinition().getName() + ".");
                return false;
            }
        }
        return true;
    }

    @Override
    public void attack(final Actor source, final Actor target) {
        Item weapon = source.getEquipment().get(Equipment.SLOT_WEAPON);
        int weaponId = weapon != null ? weapon.getId() : -1;

        Item arrow = source.getEquipment().get(Equipment.SLOT_ARROWS);
        BowType bow = EquipmentDefinition.forId(weaponId).getBowType();
        ArrowType arrows = EquipmentDefinition.forId(arrow != null ? arrow.getId() : -1).getArrowType();
        KnifeType knife = KnifeType.forId(weaponId);
        DartType dart = DartType.forId(weaponId);
        ThrownaxeType thrownaxe = ThrownaxeType.forId(weaponId);
        JavelinType javelin = JavelinType.forId(weaponId);

        /**
         * TODO: add projectile speeds within a range
         */
        int hitDelay = 2;
        int initialHeight = 43;
        int endHeight = 31;
        int showDelay = 55;
        int clientSpeed = 22;
        int slope = 16;
        int radius = 50;
        double dropRate = 20;
        final Item ammunition;

        Graphic drawback = null;
        int projectile = -1;

        if (knife != null || dart != null || thrownaxe != null || javelin != null || weapon.getId() == 6522) {
            if (source.getEquipment() != null) {
                source.getSkills().setBonus(12, source.getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition().getBonus(12));//we are using a range weapon, so we must set to use the weapon's ranged strength
            }
            if (knife != null) {
                drawback = knife.getPullbackGraphic();
                projectile = knife.getProjectileId();
                dropRate = knife.getDropRate();
            } else if (dart != null) {
                drawback = dart.getPullbackGraphic();
                projectile = dart.getProjectileId();
                dropRate = dart.getDropRate();
            } else if (thrownaxe != null) {
                drawback = thrownaxe.getPullbackGraphic();
                projectile = thrownaxe.getProjectileId();
                dropRate = thrownaxe.getDropRate();
            } else if (javelin != null) {
                drawback = javelin.getPullbackGraphic();
                projectile = javelin.getProjectileId();
                dropRate = javelin.getDropRate();
            } else {
                drawback = null;
                projectile = 442;
                //dropRate = 0.45;
            }
            ammunition = source.getEquipment().get(Equipment.SLOT_WEAPON);
            hitDelay = 2;
        } else {
            if (source.getEquipment() != null) {
//                source.getSkills().setBonus(12, source.getEquipment().get(Equipment.SLOT_ARROWS).getEquipmentDefinition().getBonus(12));//we are using arrows, so we must set to use the arrows' ranged strength
            }
            drawback = bow == BowType.CRYSTAL_BOW ? Graphic.create(250, 0, 100) : arrows.getPullbackGraphic();
            projectile = bow == BowType.CRYSTAL_BOW ? 249 : arrows.getProjectileId();
            dropRate = bow == BowType.CRYSTAL_BOW ? 1.1 : arrows.getDropRate();
            ammunition = source.getEquipment().get(Equipment.SLOT_ARROWS);
            hitDelay = 2;
        }

        if (source.getActionSender() != null) {
            source.getSkills().sendBonuses();
        }

        /**
         * The damage amount
         */
        final int damage = damage(source, target);

        if ((bow == null || bow != BowType.CRYSTAL_BOW)) {
            source.getEquipment().remove(new Item(ammunition.getId(), 1));
        }

        if (Formulae.fullKaril(source)) {
            int karil = random.nextInt(8);
            if (karil == 3) {
                target.getSkills().decreaseLevelToMinimum(Skills.AGILITY, target.getSkills().getLevelForExperience(Skills.AGILITY) / 4);
                target.playGraphics(Graphic.create(401, 0, 100));
            }
        }

        if (drawback != null) {
            source.playGraphics(drawback);
        }
        int attackAnimationIndex = source.getCombatSession().getCombatStyle().getId();
        if (attackAnimationIndex > 3) {
            attackAnimationIndex -= 4;
        }
//        source.playAnimation(
//                (source.getEquipment().get(Equipment.SLOT_WEAPON) != null && source.getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition() != null)
//                ? source.getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition().getAnimation().getAttackAnimation(attackAnimationIndex)
//                : source.getAttackAnimation());
//        
        /**
         * If the special isn't activated
         */
        source.playProjectile(Projectile.create(projectile, initialHeight, endHeight, showDelay, 22, slope, radius), source, target);
        World.getWorld().submit(new Tickable(hitDelay) {
            @Override
            public void execute() {
                inflictDamage(source, target, damage);
                this.stop();
            }
        });
        /**
         * Apply poison damage
         */
        if (target.getCombatSession().getPoisonDamage() < 1 && random.nextInt(11) == 3 && target.getCombatSession().canBePoisoned()) {
            if (arrows != null && source.getEquipment().get(Equipment.SLOT_ARROWS).getEquipmentDefinition() != null) {
                if (source.getEquipment().get(Equipment.SLOT_ARROWS).getEquipmentDefinition().getPoisonType() != PoisonType.NONE) {
                    target.getCombatSession().setPoisonDamage(source.getEquipment().get(Equipment.SLOT_ARROWS).getEquipmentDefinition().getPoisonType().getRangeDamage(), source);
                    if (target.getActionSender() != null) {
                        target.getActionSender().sendMessage("You have been poisoned!");
                    }
                }
            } else if ((knife != null || dart != null || thrownaxe != null || javelin != null) && source.getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition() != null) {
                if (source.getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition().getPoisonType() != PoisonType.NONE) {
                    target.getCombatSession().setPoisonDamage(source.getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition().getPoisonType().getRangeDamage(), source);
                    if (target.getActionSender() != null) {
                        target.getActionSender().sendMessage("You have been poisoned!");
                    }
                }
            }
        }
    }

    @Override
    public void inflictDamage(Actor source, Actor target, int damage) {
        super.inflictDamage(source, target, damage);
        double r = random.nextDouble();
    }

    @Override
    public int damage(Actor source, Actor target) {
        //Range calculations
        return super.damage(source, target);
    }

    @Override
    public void special(final Actor source, final Actor target, int damage) {
        Item weapon = source.getEquipment().get(Equipment.SLOT_WEAPON);
        int weaponId = weapon != null ? weapon.getId() : -1;

        Item arrow = source.getEquipment().get(Equipment.SLOT_ARROWS);
        BowType bow = EquipmentDefinition.forId(weaponId).getBowType();
        ArrowType arrows = EquipmentDefinition.forId(arrow != null ? arrow.getId() : -1).getArrowType();
        KnifeType knife = KnifeType.forId(weaponId);
        DartType dart = DartType.forId(weaponId);
        ThrownaxeType thrownaxe = ThrownaxeType.forId(weaponId);
        JavelinType javelin = JavelinType.forId(weaponId);

        /**
         * TODO: add projectile speeds within a range
         */
        int hitDelay = 2;
        int initialHeight = 43;
        int endHeight = 31;
        int showDelay = 55;
        int clientSpeed = 22;
        int slope = 16;
        int radius = 50;
        double dropRate = 20;
        final Item ammunition;

        Graphic drawback = null;
        int projectile = -1;

        if (knife != null || dart != null || thrownaxe != null || javelin != null || weapon.getId() == 6522) {
            if (source.getEquipment() != null) {
                source.getSkills().setBonus(12, source.getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition().getBonus(12));//we are using a range weapon, so we must set to use the weapon's ranged strength
            }
            if (knife != null) {
                drawback = knife.getPullbackGraphic();
                projectile = knife.getProjectileId();
                dropRate = knife.getDropRate();
            } else if (dart != null) {
                drawback = dart.getPullbackGraphic();
                projectile = dart.getProjectileId();
                dropRate = dart.getDropRate();
            } else if (thrownaxe != null) {
                drawback = thrownaxe.getPullbackGraphic();
                projectile = thrownaxe.getProjectileId();
                dropRate = thrownaxe.getDropRate();
            } else if (javelin != null) {
                drawback = javelin.getPullbackGraphic();
                projectile = javelin.getProjectileId();
                dropRate = javelin.getDropRate();
            } else {
                drawback = null;
                projectile = 442;
                //dropRate = 0.45;
            }
            ammunition = source.getEquipment().get(Equipment.SLOT_WEAPON);
            hitDelay = 2;
        } else {
            if (source.getEquipment() != null) {
//                source.getSkills().setBonus(12, source.getEquipment().get(Equipment.SLOT_ARROWS).getEquipmentDefinition().getBonus(12));//we are using arrows, so we must set to use the arrows' ranged strength
            }
            drawback = bow == BowType.CRYSTAL_BOW ? Graphic.create(250, 0, 100) : arrows.getPullbackGraphic();
            projectile = bow == BowType.CRYSTAL_BOW ? 249 : arrows.getProjectileId();
            dropRate = bow == BowType.CRYSTAL_BOW ? 1.1 : arrows.getDropRate();
            ammunition = source.getEquipment().get(Equipment.SLOT_ARROWS);
            hitDelay = 2;
        }
        source.playProjectile(Projectile.create(projectile, initialHeight, endHeight, showDelay, 22, slope, radius), source, target);
        World.getWorld().submit(new Tickable(hitDelay) {
            @Override
            public void execute() {
                int dmg = damage(source, target);
                special(source, target, dmg);
                this.stop();
            }
        });
    }

    @Override
    public void addExperience(Actor source, int damage) {
        super.addExperience(source, damage);
        source.getSkills().addExperience(Skills.RANGE, damage * 0.6);
    }

    @Override
    public int getSpeed(Actor source, Actor target) {
        return super.getSpeed(source, target);
    }

    @Override
    public void specialAttack(Actor source, Actor target) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
