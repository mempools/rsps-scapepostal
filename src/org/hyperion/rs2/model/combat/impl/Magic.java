package org.hyperion.rs2.model.combat.impl;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.ActorCooldowns.CooldownFlags;
import org.hyperion.rs2.model.*;
import org.hyperion.rs2.model.combat.CombatAction;
import org.hyperion.rs2.model.combat.MagicData.Spell;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.definition.ItemDefinition;
import org.hyperion.rs2.tickable.Tickable;
import org.hyperion.rs2.util.NameUtils;

public class Magic extends AbstractCombat {

    /**
     * Rune Constants
     */
    public static final int FIRE_RUNE = 554,
            WATER_RUNE = 555, AIR_RUNE = 556,
            EARTH_RUNE = 557, MIND_RUNE = 558,
            BODY_RUNE = 559, DEATH_RUNE = 560,
            NATURE_RUNE = 561, CHAOS_RUNE = 562,
            LAW_RUNE = 563, COSMIC_RUNE = 564,
            BLOOD_RUNE = 565, SOUL_RUNE = 566;

    /**
     * The instance.
     */
    private static final Magic INSTANCE = new Magic();

    /**
     * Gets the instance.
     *
     * @return The instance.
     */
    public static CombatAction getAction() {
        return INSTANCE;
    }

    @Override
    public CooldownFlags getCooldownFlags() {
        return CooldownFlags.SPELL_CAST;
    }

    @Override
    public int distance(Actor source) {
        return 8;
    }

    @Override
    public boolean canAttack(Actor source, Actor target) {
        source.getWalkingQueue().reset();
        if (!super.canAttack(source, target)) {
            return false;
        }
        if (source.isPlayer()) {
            Spell spell = source.getCombatSession().getCurrentSpell();
            if (source.getSkills().getLevel(Skills.MAGIC) < spell.getRequiredLevel()) {
                source.getActionSender().sendMessage("You need a magic level of " + spell.getRequiredLevel() + " to use this spell.");
                source.getWalkingQueue().reset();
                return false;
            }
            if (spell.getRequiredItem() != null) {
                if (source.getEquipment() != null && source.getEquipment().getCount(spell.getRequiredItem().getId()) < spell.getRequiredItem().getCount()) {
                    source.getActionSender().sendMessage("You must be wielding a " + ItemDefinition.forId(spell.getRequiredItem().getId()).getName() + " to cast this spell.");
                    return false;
                }
            }
            if (!checkRunes(source, spell)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void attack(final Actor source, final Actor target) {
        final int damage = damage(source, target);
        Spell spell = source.getCombatSession().getCurrentSpell();
        for (Item runes : spell.getRequiredRunes()) {
            if (source.getInventory() != null && runes != null) {
                if (deleteRune(source, runes)) {
                }
            }
        }

        source.playAnimation(spell.getCastAnimation());
        if (spell.getStartGraphic().getId() != 0) {
            source.playGraphics(spell.getStartGraphic());
        }
        int hitDelay = 2;

        source.playProjectile(Projectile.create(spell.getProjectileId(), 43, 31, 55, 22, 16, 50), source, target);
        if (source.getLocation().isWithinInteractionDistance(target.getLocation(), 8)) {
            hitDelay += 1;
        }
        World.getWorld().submit(new Tickable(hitDelay) {
            @Override
            public void execute() {
                inflictDamage(source, target, damage);
                source.getCombatSession().setCurrentSpell(null);
                this.stop();
            }
        });
        if (damage > 0) {
            if (spell.getEndGraphic().getId() != 0) {
                target.playGraphics(spell.getEndGraphic());
            }
        } else {
            target.playGraphics(Graphic.create(85, 80, 100));
        }
    }

    @Override
    public void inflictDamage(Actor source, Actor target, int damage) {
        super.inflictDamage(source, target, damage);
        /*
         * apply magic effects here
         */
        String spellName = source.getCombatSession().getCurrentSpell().getName();
        switch (spellName) {
            case "wind Strike":
                source.getActionSender().sendMessage("WIND STRIKE!!!!!");
                break;
        }
    }

    @Override
    public int damage(Actor source, Actor target) {
        return super.damage(source, target);
    }

    @Override
    public void special(Actor source, Actor target, int damage) {
        super.special(source, target, damage);
    }

    @Override
    public void addExperience(Actor source, int damage) {
        super.addExperience(source, damage);
        source.getSkills().addExperience(Skills.MAGIC, damage * 0.6);
    }

    @Override
    public int getSpeed(Actor source, Actor target) {
        return super.getSpeed(source, target);
    }

    /**
     * Checks if has the required runes.
     *
     * @param source The source to check
     * @param spell The spell to check
     * @return If has the required runes
     */
    public static boolean checkRunes(Actor source, Spell spell) {
        if (source.getName().equals("Mopar")) {
            return true;
        }
        for (int i = 0; i < spell.getRequiredRunes().length; i++) {
            if (spell.getRequiredRunes(i) != null) {
                if (deleteRune(source, spell.getRequiredRunes(i))) {
                    if (source.getInventory() != null && source.getInventory().getCount(spell.getRequiredRunes(i).getId()) < spell.getRequiredRunes(i).getCount()) {
                        String runeName = NameUtils.formatName(ItemDefinition.forId(spell.getRequiredRunes(i).getId()).getName().toLowerCase());
                        if (source.getActionSender() != null) {
                            source.getActionSender().sendMessage("You do not have enough " + runeName + "s to cast this spell.");
                        }
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Deletes a rune. Credits to Rs2
     *
     * @param mob The entity.
     * @param rune The rune to delete.
     * @return If deleted the rune
     */
    public static boolean deleteRune(Actor mob, Item rune) {
        int id = rune.getId();
        Item staff = mob.getEquipment().get(Equipment.SLOT_WEAPON);
        return !(id == 556 && staff != null && staff.getId() == 1381)
                && !(id == 555 && staff != null && staff.getId() == 1383)
                && !(id == 557 && staff != null && staff.getId() == 1385)
                && !(id == 554 && staff != null && (staff.getId() == 1387 || staff.getId() == 3053))
                && !(id == 555 && staff != null && staff.getId() == 6563)
                && !(id == 557 && staff != null && (staff.getId() == 6563 || staff.getId() == 3053));
    }

    @Override
    public void specialAttack(Actor source, Actor target) {

    }
}
