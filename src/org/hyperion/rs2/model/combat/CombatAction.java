package org.hyperion.rs2.model.combat;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.ActorCooldowns.CooldownFlags;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public interface CombatAction {

    /**
     * Gets the cool down flags for the combat action.
     *
     * @return The cool down flag.
     */
    public CooldownFlags getCooldownFlags();

    /**
     * The distance required to attack
     *
     * @param source The source to check
     * @return The required distance
     */
    public int distance(Actor source);

    /**
     * If the Player or NPC can attack
     *
     * @param source The source (source)
     * @param target The target (target)
     * @return if the source can attack or not
     */
    public boolean canAttack(Actor source, Actor target);

    /**
     * Starts the combat
     *
     * @param source The source
     * @param target The target
     */
    public void start(Actor source, Actor target);

    /**
     * Performs attacking
     *
     * @param source The source
     * @param target The target
     */
    public void attack(Actor source, Actor target);

    /**
     * Performs a special attack
     *
     * @param source The source
     * @param target The target
     */
    public void specialAttack(Actor source, Actor target);

    /**
     * Performs defending
     *
     * @param source The source
     * @param target The target
     */
    public void defend(Actor source, Actor target);

    /**
     * Inflicts the damage.
     *
     * @param source The source (source)
     * @param target The target (target)
     * @param damage The damage amount.
     */
    public void inflictDamage(Actor source, Actor target, int damage);

    /**
     * Gets the damage amount
     *
     * @param source The source
     * @param target The target
     * @return the total damage
     */
    public int damage(Actor source, Actor target);

    /**
     * Gets the special attack effects
     *
     * @param source The source
     * @param target The target
     * @param damage The damage amount
     */
    public void special(Actor source, Actor target, int damage);

    /**
     * Gets the extra speed delay
     *
     * @param source The source
     * @param target The target
     * @return The extra speed delay
     */
    public int getSpeed(Actor source, Actor target);

    /**
     * Adds experience points
     *
     * @param source The source
     * @param damage The damage amount
     */
    public void addExperience(Actor source, int damage);
}
