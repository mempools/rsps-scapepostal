package org.hyperion.rs2.model.combat;

import java.util.HashMap;
import java.util.Map;

/**
 * Used for defense calculation, EG: White mace vs Low crush defense.
 *
 * @author Michael Bull
 */
public enum AttackType {

    STAB(0),
    SLASH(1),
    CRUSH(2),
    MAGIC(3),
    RANGE(4),
    NONE(5);
    /**
     * A map of attack types.
     */
    private static final Map<Integer, AttackType> attackTypes = new HashMap<>();

    /**
     * Gets a attack type by its ID.
     *
     * @param attackType The attack type id.
     * @return The attack type, or <code>null</code> if the id is not a attack
     * type.
     */
    public static AttackType forId(int attackType) {
        return attackTypes.get(attackType);
    }

    /**
     * Populates the attack type map.
     */
    static {
        for (AttackType attackType : AttackType.values()) {
            attackTypes.put(attackType.id, attackType);
        }
    }

    /**
     * The attack type's id.
     */
    private int id;

    /**
     * Creates the attack type
     *
     * @param id The attack type id
     */
    private AttackType(int id) {
        this.id = id;
    }

    /**
     * Gets the attack types id.
     *
     * @return The attack types id.
     */
    public int getId() {
        return id;
    }
}
