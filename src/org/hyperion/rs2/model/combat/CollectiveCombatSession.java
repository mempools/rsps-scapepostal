package org.hyperion.rs2.model.combat;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Damage;

import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Represents an instance of combat, where Entity is an assailant and Integer is
 * the sum of their damage done. This is mapped to every victim in combat, and
 * used to determine drops.
 *
 * @author Brett Russell
 */
public class CollectiveCombatSession {

    /**
     * The time stamp
     */
    private long stamp;
    /**
     * The damage map
     */
    private Map<Actor, Damage> damageMap;
    /**
     * The participants names
     */
    private Set<Actor> names = null;//damageMap.keySet();//TODO: fix this
    /**
     * The Active state
     */
    private boolean isActive;
    /**
     * The last opponent
     */
    private Actor lastOpponent;
    /**
     * The last hit timer
     */
    private long lastHitTimer;

    /**
     * The collective combat session
     *
     * @param character The entity
     */
    public CollectiveCombatSession(Actor character) {
        Date date = new Date();
        this.stamp = date.getTime();
        this.isActive = true;
    }

    /**
     * Gets the timestamp for this object (when the session began).
     *
     * @return The timestamp.
     */
    public long getStamp() {
        return stamp;
    }

    /**
     * Gets the entity with the highest damage count this session.
     *
     * @return The entity with the highest damage count.
     */
    public Actor getTopDamage() {
        /*  Entity top = null;
         int damageDone = 0;
         int currentHighest = 0;

         Iterator<Entity> itr = names.iterator();

         while (itr.hasNext()) {
         Entity currentEntity = itr.next();
         damageDone = damageMap.get(currentEntity).getPrimaryDamage();
         if (damageDone > currentHighest) {
         currentHighest = damageDone;
         top = currentEntity;
         }
         }
         return top;*/
        return null;
    }

    /**
     * Returns the Map of this session's participants. If you would want it,
     * that is...
     *
     * @return A Map of the participants and their damage done.
     */
    public Map<Actor, Damage> getDamageCharts() {
        return damageMap;
    }

    /**
     * Adds a participant to this session.
     *
     * @param participant The participant to add.
     */
    public void addParticipant(Actor participant) {
        damageMap.put(participant, null);
    }

    /**
     * Remove a participant.
     *
     * @param participant The participant to remove.
     */
    public void removeParticipant(Actor participant) {
        damageMap.remove(participant);
    }

    /**
     * Sets this sessions active state.
     *
     * @param b A <code>boolean</code> value representing the state.
     */
    public void setState(boolean b) {
        this.isActive = b;
    }

    /**
     * Determine the active state of this session.
     *
     * @return The active state as a <code>boolean</code> value.
     */
    public boolean isActive() {
        return this.isActive;
    }

    /**
     * Gets the last opponent
     *
     * @return lastOpponent The last opponent
     */
    public Actor getLastOpponent() {
        return lastOpponent;
    }

    /**
     * Sets the last opponent
     *
     * @param lastOpponent The last opponent to set
     */
    public void setLastOpponent(Actor lastOpponent) {
        this.lastOpponent = lastOpponent;
    }

    /**
     * @return the lastHitTimer
     */
    public long getLastHitTimer() {
        return lastHitTimer;
    }

    /**
     * @param lastHitTimer the lastHitTimer to set
     */
    public void setLastHitTimer(long lastHitTimer) {
        this.lastHitTimer = lastHitTimer + System.currentTimeMillis();
    }
}
