package org.hyperion.rs2.model.combat;

import org.hyperion.rs2.action.impl.AttackAction;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.combat.impl.Magic;
import org.hyperion.rs2.model.combat.impl.Melee;
import org.hyperion.rs2.model.combat.impl.Range;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.player.Player;

/**
 * Handles the combat system.
 *
 * @author Brett
 */
public class CombatHandler {

    /**
     * Gets the attacker's weapon hit delay (ticks)
     *
     * @param character The player whose weapon we are getting the speed value.
     * @return A <code>short</code>-type value of the weapon hit delay.
     */
    public static int getCombatHitDelay(Actor character) {
        if (character.isNPC()) {
            return 1;
        }
        return (character.getEquipment() != null && character.getEquipment().get(3) != null) ? character.getEquipment().get(3).getEquipmentDefinition().getDelay() : 1;
    }

    /**
     * Handles aggressive monsters
     *
     * @param npc The NPC to handle
     * @return Weather the NPC can attack
     */
    public static boolean handleAgressiveMonsters(NPC npc) {
        for (Player player : World.getWorld().getPlayers()) {
            if (player == null || npc == null) {
                continue;
            }
            if (npc.getDefinition().isAggressive()) {
                if (player.getSkills().getCombatLevel() >= (npc.getDefinition().getLevel() * 2)
                        || npc.isDead() || player.isDead() || npc.getInteractingActor() != null) {
                    return false;
                }
                if (npc.getLocation().isWithinDistance(player.getLocation())) {
                    npc.setInteractingActor(player);
                    npc.getActionQueue().addAction(new AttackAction(npc, player));
                    return true;
                } else {
                    npc.setInteractingActor(null);
                    return false;
                }
            }
        }
        return false;
    }
}
