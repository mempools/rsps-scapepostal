package org.hyperion.rs2.model.combat;

import org.hyperion.rs2.model.Skills;

import java.util.HashMap;
import java.util.Map;

public enum CombatStyle {

    ACCURATE(0, new int[]{Skills.ATTACK, Skills.HITPOINTS}, new double[]{4, 1.33}),
    AGGRESSIVE_1(1, new int[]{Skills.STRENGTH}, new double[]{4}),
    AGGRESSIVE_2(2, new int[]{Skills.STRENGTH}, new double[]{4}),
    DEFENSIVE(3, new int[]{Skills.DEFENCE}, new double[]{4, 1.33}),
    CONTROLLED_1(4, new int[]{Skills.ATTACK, Skills.STRENGTH, Skills.DEFENCE}, new double[]{1.33, 1.33, 1.33}),
    CONTROLLED_2(5, new int[]{Skills.ATTACK, Skills.STRENGTH, Skills.DEFENCE}, new double[]{1.33, 1.33, 1.33}),
    CONTROLLED_3(6, new int[]{Skills.ATTACK, Skills.STRENGTH, Skills.DEFENCE}, new double[]{1.33, 1.33, 1.33}),
    AUTOCAST(7, new int[]{Skills.MAGIC}, new double[]{2}),
    DEFENSIVE_AUTOCAST(8, new int[]{Skills.MAGIC, Skills.DEFENCE}, new double[]{1.33, 1});
    /**
     * A map of combat styles.
     */
    private static final Map<Integer, CombatStyle> combatStyles = new HashMap<>();

    /**
     * Gets a combat style by its ID.
     *
     * @param combatStyle The combat style id.
     * @return The combat style, or <code>null</code> if the id is not a combat
     * style.
     */
    public static CombatStyle forId(int combatStyle) {
        return combatStyles.get(combatStyle);
    }

    /**
     * Populates the combat style map.
     */
    static {
        for (CombatStyle combatStyle : CombatStyle.values()) {
            combatStyles.put(combatStyle.getId(), combatStyle);
        }
    }

    /**
     * The combat style's id.
     */
    private final int id;
    /**
     * The skills this combat style adds experience to.
     */
    private final int[] skills;
    /**
     * The amounts of experience this combat style adds.
     */
    private final double[] experiences;

    private CombatStyle(int id, int[] skills, double[] experiences) {
        this.id = id;
        this.skills = skills;
        this.experiences = experiences;
    }

    /**
     * Gets the combat styles id.
     *
     * @return The combat styles id.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the skills this attack type adds experience to.
     *
     * @return The skills this attack type adds experience to.
     */
    public int[] getSkills() {
        return skills;
    }

    /**
     * Gets a skill this attack type adds experience to by its index.
     *
     * @param index The skill index.
     * @return The skill this attack type adds experience to by its index.
     */
    public int getSkill(int index) {
        return skills[index];
    }

    /**
     * Gets the experience amounts this attack type adds.
     *
     * @return The experience amounts this attack type adds.
     */
    public double[] getExperiences() {
        return experiences;
    }

    /**
     * Gets an amount of experience this attack type adds by its index.
     *
     * @param index The experience index.
     * @return The amount of experience this attack type adds by its index.
     */
    public double getExperience(int index) {
        return experiences[index];
    }
}
