package org.hyperion.rs2.model;

import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.action.ActionQueue;
import org.hyperion.rs2.action.impl.DistanceAction;
import org.hyperion.rs2.content.gang.Gang;
import org.hyperion.rs2.content.skills.Prayers;
import org.hyperion.rs2.model.Damage.Hit;
import org.hyperion.rs2.model.UpdateFlags.UpdateFlag;
import org.hyperion.rs2.model.combat.CollectiveCombatSession;
import org.hyperion.rs2.model.combat.CombatAction;
import org.hyperion.rs2.model.combat.CombatSession;
import org.hyperion.rs2.model.container.Container;
import org.hyperion.rs2.model.container.Container.Type;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.container.Inventory;
import org.hyperion.rs2.net.ActionSender;

import java.util.LinkedList;
import java.util.List;
import org.hyperion.rs2.model.combat.impl.Magic;
import org.hyperion.rs2.model.combat.impl.Melee;
import org.hyperion.rs2.model.combat.impl.Range;
import org.hyperion.rs2.packet.context.DeathContext.DeathStage;
import org.hyperion.rs2.packet.context.ForceWalkContext;
import org.hyperion.rs2.tickable.Tickable;
import org.hyperion.rs2.tickable.impl.DeathTick;

/**
 * Represents an {@link Actor} in the game world; {@link Player} or an
 * {@link NPC}.
 *
 * @author Graham Edgecombe
 */
public abstract class Actor extends Entity {

    /**
     * The default, i.e. spawn, location.
     */
    public static final Location DEFAULT_LOCATION = Location.create(3223, 3398, 0);
    /**
     * The update flags.
     */
    private final UpdateFlags updateFlags = new UpdateFlags();
    /**
     * The character's cool downs.
     */
    private final ActorCooldowns cooldowns = new ActorCooldowns();
    /**
     * The list of local players.
     */
    private final List<Player> localPlayers = new LinkedList<>();
    /**
     * The list of local NPCs.
     */
    private final List<NPC> localNpcs = new LinkedList<>();
    /**
     * The character's skill levels.
     */
    private final Skills skills = new Skills(this);
    /**
     * The character's inventory.
     */
    private final Container inventory = new Container(Type.STANDARD, Inventory.SIZE);
    /**
     * The player's equipment.
     */
    private final Container equipment = new Container(Type.STANDARD, Equipment.SIZE);
    /**
     * A queue of actions.
     */
    private final ActionQueue actionQueue = new ActionQueue();
    /**
     * The walking queue.
     */
    private final WalkingQueue walkingQueue = new WalkingQueue(this);
    /**
     * The sprites i.e. walk directions.
     */
    private final Sprites sprites = new Sprites();
    /**
     * The index in the <code>ActorList</code>.
     */
    private int index;
    /**
     * The character's gang.
     */
    private Gang gang = null;
    /**
     * The attributes.
     */
    private final Attributes attributes = new Attributes();
    /**
     * The character's first stored hit for updates.
     */
    private final transient Damage damage = new Damage();
    /**
     * The character's combat session.
     */
    private final CombatSession combatSession = new CombatSession(this);
    /**
     * The character's collective combat session
     */
    private final CollectiveCombatSession collectiveCombatSession = new CollectiveCombatSession(this);
    /**
     * The character's prayer state
     */
    private final Prayers prayers = new Prayers(this);
    /**
     * The character's head icon
     */
    private HeadIcon headIcon = new HeadIcon();
    /**
     * The character's direction.
     */
    private Direction direction = Direction.NORTH;
    /**
     * The character's state of life.
     */
    private boolean isDead;
    /**
     * The animate state.
     */
    private boolean canAnimate;
    /**
     * The teleportation target.
     */
    private Location teleportTarget = null;
    /**
     * The teleporting flag.
     */
    private boolean teleporting = false;
    /**
     * Map region changing flag.
     */
    private boolean mapRegionChanging = false;
    /**
     * The current animation.
     */
    private Animation currentAnimation;
    /**
     * The current graphic.
     */
    private Graphic currentGraphic;
    /**
     * The interacting actor.
     */
    private Actor interactingActor;
    /**
     * The face location.
     */
    private Location face;
    /**
     * The forced chat message.
     */
    private String forcedChatMessage;
    /**
     * The character's transform id.
     */
    private int transformId = -1;
    /**
     * character's combat aggressor state.
     */
    private boolean isAggressor;
    /**
     * The force walk context.
     */
    private ForceWalkContext forceWalk;

    /**
     * Creates the entity.
     */
    public Actor() {
        setLocation(DEFAULT_LOCATION);
        setLastKnownRegion(getLocation());
    }

    /**
     * Gets the action queue.
     *
     * @return The action queue.
     */
    public ActionQueue getActionQueue() {
        return actionQueue;
    }

    /**
     * Gets the inventory.
     *
     * @return The inventory.
     */
    public Container getInventory() {
        return inventory;
    }

    /**
     * Gets the player's equipment.
     *
     * @return The player's equipment.
     */
    public Container getEquipment() {
        return equipment;
    }

    /**
     * Gets the character's aggressor state.
     *
     * @return <code>true</code> if aggressive, <code>false</code> if not.
     */
    public boolean getAggressorState() {
        return isAggressor;
    }

    /**
     * Sets the aggressor state for this entity.
     *
     * @param b The aggressor state
     */
    public void setAggressorState(boolean b) {
        isAggressor = b;
    }

    /**
     * Creates the force chat mask.
     *
     * @param message The message to force chat
     */
    public void forceChat(String message) {
        forcedChatMessage = message;
        updateFlags.flag(UpdateFlag.FORCED_CHAT);
    }

    /**
     * Gets the forced chat message
     *
     * @return forcedChatMessage The forced chat message
     */
    public String getForcedChatMessage() {
        return forcedChatMessage;
    }

    /**
     * Gets the player NPC id
     *
     * @return The player NPC id
     */
    public int getTransformId() {
        return transformId;
    }

    /**
     * Sets the transform flag
     *
     * @param id The NPC to set
     */
    public void setTransformId(int id) {
        this.transformId = id;
        getUpdateFlags().flag(isPlayer() ? UpdateFlag.APPEARANCE : UpdateFlag.TRANSFORM);
    }

    /**
     * Gets the player transform flag
     *
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean isTransformed() {
        return getTransformId() > -1;
    }

    /**
     * Gets the animate state.
     *
     * @return <code>true</code> if can animate, <code>false</code> if not.
     */
    public boolean canAnimate() {
        return canAnimate;
    }

    /**
     * Sets the animate state,
     *
     * @param canAnimate The animate state
     * @return <code>true</code> the character can animate, <code>false</code>
     * can not.
     */
    public boolean canAnimate(boolean canAnimate) {
        return this.canAnimate = canAnimate;
    }

    /**
     * Sets the character's head icon
     *
     * @param icon The icon to set
     * @return The head icon
     */
    public HeadIcon setHeadIcon(HeadIcon icon) {
        return this.headIcon = icon;
    }

    /**
     * Sets the character's direction
     *
     * @param direction The direction to set
     * @return The direction.
     */
    public Direction setDirection(Direction direction) {
        return this.direction = direction;
    }

    /**
     * Is the character dead?
     *
     * @return isDead flag
     */
    public boolean isDead() {
        return isDead;
    }

    /**
     * Set the character's state of life.
     *
     * @param isDead Boolean
     */
    public void setDead(boolean isDead) {
        this.isDead = isDead;
    }

    /**
     * Gets the head icon
     *
     * @return headIcon The head icon
     */
    public HeadIcon getHeadIcon() {
        return headIcon;
    }

    /**
     * Gets the character's direction
     *
     * @return The direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Makes this character face a location.
     *
     * @param location The location to face.
     */
    public void face(Location location) {
        this.face = location;
        this.updateFlags.flag(UpdateFlag.FACE_COORDINATE);
    }

    /**
     * Checks if this character is facing a location.
     *
     * @return <code>true</code> the character is facing, <code>false</code> can
     * not.
     */
    public boolean isFacing() {
        return face != null;
    }

    /**
     * Resets the facing location.
     */
    public void resetFace() {
        this.face = null;
        this.updateFlags.flag(UpdateFlag.FACE_COORDINATE);
    }

    /**
     * Gets the face location.
     *
     * @return The face location, or <code>null</code> if the character is not
     * facing.
     */
    public Location getFaceLocation() {
        return face;
    }

    /**
     * Resets the force walk.
     */
    public void resetForceWalk() {
        this.forceWalk = null;
    }

    /**
     * Sets the force walk context
     *
     * @param forceWalk The force walk context to set
     */
    public void setForceWalk(final ForceWalkContext forceWalk) {
        this.forceWalk = forceWalk;
        World.getWorld().submit(new Tickable(forceWalk.getTick()) {
            @Override
            public void execute() {
                setTeleportTarget(getLocation().transform(forceWalk.getSource().getX(), forceWalk.getSource().getY(), 0));
                this.stop();
            }
        });
    }

    /**
     * Plays a force movement.
     *
     * @param ticks The ticks
     * @param animation The animation to play
     * @param forceWalk The force walk
     */
    public void playForceMovement(int ticks, final Animation animation, final ForceWalkContext forceWalk) {
        if (this.forceWalk != null) {
            World.getWorld().submit(new Tickable(ticks) {
                @Override
                public void execute() {
                    playAnimation(animation);
                    setForceWalk(forceWalk);
                    getUpdateFlags().flag(UpdateFlag.FORCE_MOVEMENT);
                    this.stop();
                }
            });
        }
    }

    /**
     * Gets the force walk context
     *
     * @return The force walk context.
     */
    public ForceWalkContext getForceWalk() {
        return forceWalk;
    }

    /**
     * Checks if this character is interacting with another entity.
     *
     * @return <code>true</code> the character is interacting,
     * <code>false</code> if not.
     */
    public boolean isInteracting() {
        return interactingActor != null;
    }

    /**
     * Resets the interacting actor.
     */
    public void resetInteractingActor() {
        if (this.getInteractingActor() != null) {
            if (this.getInteractingActor() == this) {
                this.setInteractingActor(null);
            }
        }
        this.interactingActor = null;
        //this.updateFlags.flag(UpdateFlag.FACE_ENTITY);//TODO: fix for 377
    }

    /**
     * Gets the interacting entity.
     *
     * @return The character to interact with.
     */
    public Actor getInteractingActor() {
        return interactingActor;
    }

    /**
     * Sets the interacting entity.
     *
     * @param entity The new character to interact with.
     */
    public void setInteractingActor(Actor entity) {
        this.interactingActor = entity;
        //this.updateFlags.flag(UpdateFlag.FACE_ENTITY);//TODO: fix for 377
    }

    /**
     * Gets the current animation.
     *
     * @return The current animation;
     */
    public Animation getCurrentAnimation() {
        return currentAnimation;
    }

    /**
     * Gets the current graphic.
     *
     * @return The current graphic.
     */
    public Graphic getCurrentGraphic() {
        return currentGraphic;
    }

    /**
     * Resets attributes after an update cycle.
     */
    public void reset() {
        this.currentAnimation = null;
        this.currentGraphic = null;
    }

    /**
     * Animates the entity.
     *
     * @param animation The animation.
     */
    public void playAnimation(Animation animation) {
        this.currentAnimation = animation;
        this.getUpdateFlags().flag(UpdateFlag.ANIMATION);
    }

    /**
     * Plays graphics.
     *
     * @param graphic The graphics.
     */
    public void playGraphics(Graphic graphic) {
        this.currentGraphic = graphic;
        this.getUpdateFlags().flag(UpdateFlag.GRAPHICS);
    }

    /**
     * Adds a Distance <code>Action</code> to the queue.. Checks if the
     * <code>Entity</code> is within the specified location and interaction
     * distance.
     *
     * @param distance The distance.
     * @param character The entity location
     * @param other The other entity.
     * @param action The action.
     */
    public void addDistanceAction(Actor character, Actor other, Action action, int distance) {
        addDistanceAction(character, other.getLocation(), action, distance);
    }

    /**
     * Adds a Distance <code>Action</code> to the queue.. Checks if the
     * <code>Entity</code> is within the specified location and interaction
     * distance.
     *
     * @param character The character
     * @param distance The distance.
     * @param other The other location.
     * @param action The action.
     */
    public void addDistanceAction(Actor character, Location other, Action action, int distance) {
        actionQueue.clearAllActions();
        if (character.getLocation().isWithinInteractionDistance(other, distance)) {
            action.execute();
        } else {
            actionQueue.addAction(new DistanceAction(character, other, distance, action));
        }
    }

    /**
     * Gets the walking queue.
     *
     * @return The walking queue.
     */
    public WalkingQueue getWalkingQueue() {
        return walkingQueue;
    }

    /**
     * Checks if the map region has changed in this cycle.
     *
     * @return <code>true</code> if the map region is changing,
     * <code>false</code> if not.
     */
    public boolean isMapRegionChanging() {
        return mapRegionChanging;
    }

    /**
     * Sets the map region changing flag.
     *
     * @param mapRegionChanging The map region changing flag.
     */
    public void setMapRegionChanging(boolean mapRegionChanging) {
        this.mapRegionChanging = mapRegionChanging;
    }

    /**
     * Checks if this character has a target to teleport to.
     *
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean hasTeleportTarget() {
        return teleportTarget != null;
    }

    /**
     * Gets the teleport target.
     *
     * @return The teleport target.
     */
    public Location getTeleportTarget() {
        return teleportTarget;
    }

    /**
     * Sets the teleport target.
     *
     * @param teleportTarget The target location.
     */
    public void setTeleportTarget(Location teleportTarget) {
        this.teleportTarget = teleportTarget;
    }

    /**
     * Resets the teleport target.
     */
    public void resetTeleportTarget() {
        this.teleportTarget = null;
    }

    /**
     * Gets the sprites.
     *
     * @return The sprites.
     */
    public Sprites getSprites() {
        return sprites;
    }

    /**
     * Checks if this player is teleporting.
     *
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean isTeleporting() {
        return teleporting;
    }

    /**
     * Sets the teleporting flag.
     *
     * @param teleporting The teleporting flag.
     */
    public void setTeleporting(boolean teleporting) {
        this.teleporting = teleporting;
    }

    /**
     * Gets the list of local players.
     *
     * @return The list of local players.
     */
    public List<Player> getLocalPlayers() {
        return localPlayers;
    }

    /**
     * Gets the list of local NPCs.
     *
     * @return The list of local NPCs.
     */
    public List<NPC> getLocalNPCs() {
        return localNpcs;
    }

    /**
     * Gets the character's index.
     *
     * @return The index.
     */
    public int getIndex() {
        return index;
    }

    /**
     * Sets the character's index.
     *
     * @param index The index.
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * Heals the entity
     *
     * @param amount The amount to heal
     */
    public void heal(int amount) {
        final int current = skills.getLevel(Skills.HITPOINTS);
        if (current + amount > skills.getLevelForExperience(Skills.HITPOINTS)) {
            skills.setLevel(Skills.HITPOINTS, skills.getLevelForExperience(Skills.HITPOINTS));
        } else {
            skills.setLevel(Skills.HITPOINTS, (current + amount));
        }
    }

    /**
     * Gets the update flags.
     *
     * @return The update flags.
     */
    public UpdateFlags getUpdateFlags() {
        return updateFlags;
    }

    /**
     * Gets the cool-down flags.
     *
     * @return The cool-down flags.
     */
    public ActorCooldowns getEntityCooldowns() {
        return cooldowns;
    }

    /**
     * Get this character's hit1.
     *
     * @return The character's hits as <code>Hit</code> type.
     */
    public Damage getDamage() {
        return damage;
    }

    /**
     * Gets the combat session
     *
     * @return combatSession The combat session
     */
    public CombatSession getCombatSession() {
        return combatSession;
    }

    /**
     * Gets the collective combat session
     *
     * @return collectiveCombatSession The collective combat session
     */
    public CollectiveCombatSession getCollectiveCombatSession() {
        return collectiveCombatSession;
    }

    /**
     * Gets the prayer state
     *
     * @return prayers The prayer state
     */
    public Prayers getPrayers() {
        return prayers;
    }

    /**
     * Gets the character's attributes
     *
     * @return attributes The attributes
     */
    public Attributes getAttributes() {
        return attributes;
    }

    /**
     * Gets the player's skills.
     *
     * @return The player's skills.
     */
    public Skills getSkills() {
        return skills;
    }

    /**
     * Gets the gang.
     *
     * @return gang The gang
     */
    public Gang getGang() {
        return gang;
    }

    /**
     * Sets the gang.
     *
     * @param gang The gang to set
     * @return The {@link Gang}.
     */
    public Gang setGang(Gang gang) {
        if (gang != null) {
            gang.addMember(gang, this);
        }
        return this.gang = gang;
    }

    /**
     * Deal a hit to the <code>Entity</code> and manages update flags and HP
     * modification when a hit occurs.
     *
     * @param hit The type of damage we are inflicting.
     */
    public void inflictDamage(Hit hit) {
        if (!getUpdateFlags().get(UpdateFlag.PRIMARY_HIT)) {
            getDamage().setPrimaryHit(hit);
            getUpdateFlags().flag(UpdateFlag.PRIMARY_HIT);
        } else {
            if (!getUpdateFlags().get(UpdateFlag.SECONDARY_HIT)) {
                getDamage().setSecondaryHit(hit);
                getUpdateFlags().flag(UpdateFlag.SECONDARY_HIT);
            }
        }
        skills.decreaseLevel(Skills.HITPOINTS, hit.getDamage());
        this.getCombatSession().setInCombat(true);
        this.setAggressorState(false);
        if (skills.getLevel(Skills.HITPOINTS) <= 0) {
            if (!this.isDead()) {
                playAnimation(getDeathAnimation());
                if (isPlayer()) {
                    getActionSender().sendMessage("Oh dear, you are dead!");
                }
                World.getWorld().getScriptContext().getDeathEventListener().notify(this, DeathStage.BEFORE);
                World.getWorld().submit(new DeathTick(this));
            }
            this.setDead(true);
        }
    }

    /**
     * Gets the active combat action
     *
     * @return The active combat action
     */
    public CombatAction getActiveCombatAction() {
        if (getDefaultCombatAction() != null) {
            return getDefaultCombatAction();
        }
        if (getCombatSession().getCurrentSpell() != null) {
            return Magic.getAction();
        }
        try {
            Item item = getEquipment().get(Equipment.SLOT_WEAPON);
            String name = item.getDefinition().getName();

            if (name.contains("bow") || name.contains("c'bow") || name.contains("long bow")
                    || name.contains("knife") || name.contains("thrown")) {
                return Range.getAction();
            }
        } catch (Exception e) {
            return Melee.getAction();
        }
        return Melee.getAction();
    }

    /**
     * Gets an actor as a Player
     *
     * @return The player
     */
    public Player asPlayer() {
        return (Player) this;
    }

    /**
     * Gets an actor as an NPC
     *
     * @return The NPC
     */
    public NPC asNPC() {
        return (NPC) this;
    }

    /**
     * Drops loot for an Actor.
     *
     * @param killer The killer
     */
    public abstract void dropLoot(Actor killer);

    /**
     * Gets the default combat action for this <code>Actor</code>.
     *
     * @return The default combat action
     */
    public abstract CombatAction getDefaultCombatAction();

    /**
     * Gets the combat cool down delay, the time for it to takes to attack again
     *
     * @return The combat cool down delay
     */
    public abstract int getCombatCooldownDelay();

    /**
     * Gets the character's name.
     *
     * @return The character's name.
     */
    public abstract String getName();

    /**
     * Gets the action sender
     *
     * @return The ActionSender for chaining
     */
    public abstract ActionSender getActionSender();

    /**
     * Gets the character's attack animation.
     *
     * @return The character's attack animation.
     */
    public abstract Animation getAttackAnimation();

    /**
     * Gets the character's defend animation.
     *
     * @return The character's defend animation.
     */
    public abstract Animation getDefendAnimation();

    /**
     * Gets the character's death animation.
     *
     * @return The character's death animation.
     */
    public abstract Animation getDeathAnimation();

    /**
     * Gets the walking area
     *
     * @return The walking area
     */
    public abstract Area getWalkingArea();

}
