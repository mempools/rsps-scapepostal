package org.hyperion.rs2.model;

/**
 * Holds information about a single player's look.
 *
 * @author Graham Edgecombe
 */
public class Appearance {

    /**
     * The gender.
     */
    private int gender;
    /**
     * The chest model.
     */
    private int chest;
    /**
     * The arms model.
     */
    private int arms;
    /**
     * The legs model.
     */
    private int legs;
    /**
     * The head model.
     */
    private int head;
    /**
     * The hands model.
     */
    private int hands;
    /**
     * The feet model.
     */
    private int feet;
    /**
     * The beard model.
     */
    private int beard;
    /**
     * The hair color.
     */
    private int hairColour;
    /**
     * The torso color.
     */
    private int torsoColour;
    /**
     * The legs color.
     */
    private int legColour;
    /**
     * The feet color.
     */
    private int feetColour;
    /**
     * The skin color.
     */
    private int skinColour;
    /**
     * The design state
     */
    private boolean hasDesigned;

    /**
     * Creates the default player appearance.
     */
    public Appearance() {
        gender = 0;
        head = 0;
        chest = 14;
        arms = 18;
        hands = 26;
        legs = 34;
        feet = 36;
        beard = 42;
        hairColour = 0;
        torsoColour = 5;
        legColour = 8;
        feetColour = 3;
        skinColour = 7;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        int[] look = this.getLook();
        for (int i : look) {
            s.append(i).append(":");
        }
        return new String(s.deleteCharAt(s.length() - 1));
    }

    /**
     * Gets the look array, which is an array with 13 elements describing the
     * look of a player.
     *
     * @return The look array.
     */
    public int[] getLook() {
        return new int[]{
            gender,
            head,
            chest,
            arms,
            hands,
            legs,
            feet,
            beard,
            hairColour,
            torsoColour,
            legColour,
            feetColour,
            skinColour,};
    }

    /**
     * Sets the look array.
     *
     * @param look The look array.
     * @throws IllegalArgumentException if the array length is not 13.
     */
    public void setLook(int[] look) {
        if (look.length != 13) {
            throw new IllegalArgumentException("Array length must be 13.");
        }
        gender = look[0];
        head = look[1];
        chest = look[2];
        arms = look[3];
        hands = look[4];
        legs = look[5];
        feet = look[6];
        beard = look[7];
        hairColour = look[8];
        torsoColour = look[9];
        legColour = look[10];
        feetColour = look[11];
        skinColour = look[12];
    }

    /**
     * Sets the design state
     *
     * @return hasDesigned The design state
     */
    public boolean hasDesigned(boolean hasDesigned) {
        return this.hasDesigned = hasDesigned;
    }

    /**
     * Gets the hair colour.
     *
     * @return The hair colour.
     */
    public int getHairColour() {
        return hairColour;
    }

    /**
     * Gets the torso colour.
     *
     * @return The torso colour.
     */
    public int getTorsoColour() {
        return torsoColour;
    }

    /**
     * Gets the leg colour.
     *
     * @return The leg colour.
     */
    public int getLegColour() {
        return legColour;
    }

    /**
     * Gets the feet colour.
     *
     * @return The feet colour.
     */
    public int getFeetColour() {
        return feetColour;
    }

    /**
     * Gets the skin colour.
     *
     * @return The skin colour.
     */
    public int getSkinColour() {
        return skinColour;
    }

    /**
     * Gets the gender.
     *
     * @return The gender.
     */
    public int getGender() {
        return gender;
    }

    /**
     * Gets the chest model.
     *
     * @return The chest model.
     */
    public int getChest() {
        return chest;
    }

    /**
     * Gets the arms model.
     *
     * @return The arms model.
     */
    public int getArms() {
        return arms;
    }

    /**
     * Gets the head model.
     *
     * @return The head model.
     */
    public int getHead() {
        return head;
    }

    /**
     * Gets the hands model.
     *
     * @return The hands model.
     */
    public int getHands() {
        return hands;
    }

    /**
     * Gets the legs model.
     *
     * @return The legs model.
     */
    public int getLegs() {
        return legs;
    }

    /**
     * Gets the feet model.
     *
     * @return The feet model.
     */
    public int getFeet() {
        return feet;
    }

    /**
     * Gets the beard model.
     *
     * @return The beard model.
     */
    public int getBeard() {
        return beard;
    }

    /**
     * Gets the design state
     *
     * @return hasDesigned The design state
     */
    public boolean hasDesigned() {
        return hasDesigned;
    }
}
