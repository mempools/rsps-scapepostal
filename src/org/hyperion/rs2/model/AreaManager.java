package org.hyperion.rs2.model;

import org.hyperion.util.xStream;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Global - 6/26/13 9:41 PM
 */
public class AreaManager {

    /**
     * The logger
     */
    private static final Logger logger = Logger.getLogger(AreaManager.class.getName());

    /**
     * The list of areas
     */
    private static List<Area> areas = new ArrayList<>();

    /**
     * Adds all information to the area map
     *
     * @throws java.io.IOException
     */
    public static void load() throws IOException {
        logger.info("Loading areas...");
        File file = new File("./data/areas.xml");
        if (file.exists()) {
            areas = xStream.readXML(file);
            logger.info("Loaded " + areas.size() + " areas");
        } else {
            logger.info("Area data not found");
        }
    }

    /**
     * Get areas by it's name
     *
     * @param name The name of the area
     * @return The area list by it's specified name
     */
    public static List<Area> getArea(String name) {
        List<Area> areaList = new ArrayList<>();
        for (Area area : areas) {
            if (area.getName().equalsIgnoreCase(name)) {
                areaList.add(area);
            }
        }
        return areaList;
    }

    /**
     * Checks if the location is within the area
     *
     * @param location The location to check
     * @param name The name of the area
     * @return If location is within the area
     */
    public static boolean isWithinArea(Location location, String name) {
        for (Area area : getArea(name)) {
            if (location.getZ() == area.getSecondPoint().getZ()
                    && location.getZ() == area.getFirstPoint().getZ()) {
                if (location.getX() >= area.getSecondPoint().getX()
                        && location.getX() <= area.getFirstPoint().getX()
                        && location.getY() >= area.getSecondPoint().getY()
                        && location.getY() <= area.getFirstPoint().getY()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if the location is within the area, ignores the Z coordinate
     *
     * @param location The location to check
     * @param name The name of the area
     * @return If the location is within the area
     */
    public static boolean isWithinAreaNoZ(Location location, String name) {
        for (Area area : getArea(name)) {
            if (location.getX() >= area.getSecondPoint().getX()
                    && location.getX() <= area.getFirstPoint().getX()
                    && location.getY() >= area.getSecondPoint().getY()
                    && location.getY() <= area.getFirstPoint().getY()) {
                return true;
            }
        }
        return false;
    }
}
