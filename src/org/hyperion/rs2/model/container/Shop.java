package org.hyperion.rs2.model.container;

import org.hyperion.Constants;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.container.Container.Type;
import org.hyperion.rs2.model.container.impl.InterfaceContainerListener;
import org.hyperion.rs2.model.definition.ItemDefinition;
import org.hyperion.util.xStream;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Shopping utility class.
 *
 * @author Martin
 */
public class Shop {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(Shop.class.getName());

    /**
     * The shop size.
     */
    public static final int SIZE = 40;

    /**
     * The player inventory interface.
     */
    public static final int PLAYER_INVENTORY_INTERFACE = 3823;

    /**
     * The shop inventory interface.
     */
    public static final int SHOP_INVENTORY_INTERFACE = 3900;

    /**
     * A List of Shops.
     */
    private static final List<ShopData> shops = new LinkedList<>();

    /**
     * The map for of NPC's working with the shop.
     */
    private static final Map<Integer, Integer> shopNpcs = new HashMap<>();

    /**
     * Loads the shop data
     *
     * @throws IOException File wasn't found
     */
    public static void parse() throws IOException {
        logger.info("Loading Shops...");
        xStream.alias("shop", ShopData.class);
        List<ShopData> loadShopList = xStream.readXML(new File("./data/shops.xml"));
        for (ShopData shop : loadShopList) {
            shops.add(shop);
            for (int i : shop.getNPCs()) {
                shopNpcs.put(i, shops.indexOf(shop));
            }
        }
        logger.info("Loaded " + shops.size() + " Shops.");
    }

    /**
     * Gets the shop by id
     *
     * @param id The shop id to get
     * @return The ShopData.
     */
    public static ShopData forId(int id) {
        return shops.get(id);
    }

    /**
     * Gets a shop by an NPC id
     *
     * @param npcId The NPC id
     * @return THe NPC Id.
     */
    public static int forNpcId(int npcId) {
        if (shopNpcs.containsKey(npcId)) {
            return shopNpcs.get(npcId);
        } else {
            return -1;
        }
    }

    /**
     * Opens the shop
     *
     * @param player The player
     * @param shopId The NPC id
     */
    public static void open(Player player, int shopId) {
        if (shopId != -1) {
            return;
        }
        player.getInterfaceState().interfaceClosed();
        player.getInterfaceState().shopOpened(shopId);

        ShopData shop = forId(player.getInterfaceState().getCurrentShop());
        player.getActionSender().sendString(3901, shop.getName());

        player.getInterfaceState().addListener(shop.getCurrentStock(), new InterfaceContainerListener(player, SHOP_INVENTORY_INTERFACE));
        player.getInterfaceState().addListener(player.getInventory(), new InterfaceContainerListener(player, PLAYER_INVENTORY_INTERFACE));
    }

    /**
     * Sells an item
     *
     * @param player The player
     * @param slot The slot in the player's inventory
     * @param id The item id
     * @param amount The item amount
     */
    public static void sellItem(Player player, int slot, int id, int amount) {
        ShopData shop = forId(player.getInterfaceState().getCurrentShop());
        Item item = player.getInventory().get(slot);
        if (item == null || item.getId() != id) {
            return;
        }

        int transferAmount = player.getInventory().getCount(id);
        if (amount >= transferAmount) {
            amount = transferAmount;
        } else if (transferAmount == 0) {
            return;
        }
        boolean couldBuy = false;
        switch (shop.getType()) {
            case SPECIALIST:
                if (shop.getCurrentStock().contains(item.getId())) {
                    couldBuy = true;
                }
                break;
            case GENERAL:
                couldBuy = true;
                break;
        }
        if (item.getId() == shop.getCurrency()) {
            couldBuy = false;
        }
        if (couldBuy) {
            int shopSlot = shop.getCurrentStock().contains(item.getId()) ? shop.getCurrentStock().getSlotById(item.getId()) : shop.getCurrentStock().freeSlot();
            if (shopSlot == -1) {
                player.getActionSender().sendMessage("This shop is full!");
            } else {
                if (shop.getCurrentStock().get(shopSlot) != null) {
                    if (shop.getCurrentStock().get(shopSlot).getCount() + amount < 1
                            || shop.getCurrentStock().get(shopSlot).getCount() + amount > Constants.MAX_ITEMS) {
                        player.getActionSender().sendMessage("This shop is full!");
                        return;
                    }
                }
                if (player.getInventory().add(new Item(shop.getCurrency(), amount * shop.getSellValue(item)))) {
                    player.getInventory().remove(new Item(item.getId(), amount));
                    shop.getCurrentStock().add(new Item(item.getId(), amount));
                }
            }
        } else {
            player.getActionSender().sendMessage("This shop doesn't accept this item");
        }
    }

    /**
     * Buys an item
     *
     * @param player The player
     * @param slot The slot in the player's inventory
     * @param id The item id
     * @param amount The item amount
     */
    public static void buyItem(Player player, int slot, int id, int amount) {
        ShopData shop = forId(player.getInterfaceState().getCurrentShop());
        Item item = shop.getCurrentStock().get(slot);
        if (item == null || item.getId() != id) {
            return;
        }
        int transferAmount = player.getInventory().freeSlots();
        if (amount >= transferAmount && !ItemDefinition.forId(item.getId()).isStackable()) {
            amount = transferAmount;
        } else if (transferAmount == 0) {
            return;
        }
        if (shop.getCurrentStock().get(slot).getCount() > 0) {
            if (amount >= shop.getCurrentStock().get(slot).getCount()) {
                amount = shop.getCurrentStock().get(slot).getCount();
            }
            if (!shop.canBuy(player, item, amount)) {
                player.getActionSender().sendMessage("You don't have enough " + ItemDefinition.forId(shop.getCurrency()).getName().toLowerCase() + ".");
            } else {
                if (player.getInventory().add(new Item(item.getId(), amount))) {
                    if (player.getInventory().contains(shop.getCurrency())) {
                        player.getInventory().remove(new Item(shop.getCurrency(), amount * shop.getPriceValue(new Item(item.getId(), amount))));
                    }
                    if (shop.getCurrentStock().contains(item.getId())) {
                        shop.getCurrentStock().removeZero(new Item(item.getId(), amount));
                    } else {
                        shop.getCurrentStock().remove(new Item(item.getId(), amount));
                    }
                }
            }
        }
    }

    /**
     * Gets the price of an item in the shop
     *
     * @param player The player
     * @param slot The item slot in the shop
     * @param id The item id
     */
    public static void itemPrice(Player player, int slot, int id) {
        ShopData shop = forId(player.getInterfaceState().getCurrentShop());
        Item item = shop.getCurrentStock().get(slot);
        if (item == null || item.getId() != id) {
            return;
        }
        player.getActionSender().sendMessage(ItemDefinition.forId(item.getId()).getName() + ": currently costs " + shop.getPriceValue(item) + " " + ItemDefinition.forId(shop.getCurrency()).getName().toLowerCase() + ".");
    }

    /**
     * Gets the item value
     *
     * @param player The player
     * @param slot The item slot
     * @param id The item id
     */
    public static void getItemValue(Player player, int slot, int id) {
        ShopData shop = forId(player.getInterfaceState().getCurrentShop());
        Item item = player.getInventory().get(slot);
        if (item == null || item.getId() != id) {
            return;
        }
        boolean sendMessage = false;
        switch (shop.getType()) {
            case GENERAL:
                sendMessage = true;
                break;
            case SPECIALIST:
                if (shop.getCurrentStock().contains(item.getId())) {
                    sendMessage = true;
                }
                break;
        }

        int finalPrice = shop.getSellValue(item);
        String shopMessage = "";
        if (finalPrice >= 1000 && finalPrice < 1000000) {
            shopMessage = "(" + (finalPrice / 1000) + "K).";
        } else if (finalPrice >= 1000000) {
            shopMessage = "(" + (finalPrice / 1000000) + " million).";
        }
        if (sendMessage) {
            player.getActionSender().sendMessage(ItemDefinition.forId(item.getId()).getName() + ": shop will buy for " + finalPrice + " " + ItemDefinition.forId(shop.getCurrency()).getName().toLowerCase() + " " + shopMessage);
        } else {
            player.getActionSender().sendMessage("This shop doesn't accept this item");
        }
    }

    /**
     * The shop data.
     */
    public static class ShopData {

        /**
         * The shop type.
         */
        public ShopType type;
        /**
         * The name of the shop.
         */
        public String name;
        /**
         * The shop's currency.
         */
        private final int currency;
        /**
         * The stock of the shop.
         */
        private final Container currentStock = new Container(Type.ALWAYS_STACK, Shop.SIZE);
        /**
         * The shop's NPCs.
         */
        private final List<Integer> npcs = new ArrayList<>();

        /**
         * Constructs the shop
         *
         * @param type The shop type
         * @param name The shop name
         * @param currency The shop currency
         */
        public ShopData(ShopType type, String name, int currency) {
            this.type = type;
            this.name = name;
            this.currency = currency;
        }

        /**
         * If the player could buy the item with the given currency
         *
         * @param player The player
         * @param item The item
         * @param amount The amount
         * @return If can buy or not
         */
        public boolean canBuy(Player player, Item item, int amount) {
            ShopData shop = forId(player.getInterfaceState().getCurrentShop());
            int finalAmt = getPriceValue(item);
            if (finalAmt == -1) {
                return false;
            }
            finalAmt *= amount;
            return player.getInventory().getCount(shop.getCurrency()) >= finalAmt;
        }

        /**
         * Gets the price value
         *
         * @param item The item
         * @return The price
         */
        public int getPriceValue(Item item) {
            switch (getType()) {
                case GENERAL:
                    return (int) (ItemDefinition.forId(item.getId()).getValue() * 0.8);
                case SPECIALIST:
                    return (int) ItemDefinition.forId(item.getId()).getValue();
            }
            return 1;
        }

        /**
         * Gets the sell price
         *
         * @param item The item
         * @return The sell value
         */
        public int getSellValue(Item item) {
            switch (getType()) {
                case GENERAL:
                    return (int) (ItemDefinition.forId(item.getId()).getValue() * 0.4);
                case SPECIALIST:
                    return (int) (ItemDefinition.forId(item.getId()).getValue() * 0.6);
            }
            return 1;
        }

        /**
         * Gets the shop type
         *
         * @return type The shop type
         */
        public ShopType getType() {
            return type;
        }

        /**
         * Gets the shop name
         *
         * @return name The shop name
         */
        public String getName() {
            return name;
        }

        /**
         * Gets the currency
         *
         * @return currency The currency
         */
        public int getCurrency() {
            return currency;
        }

        /**
         * Gets the shop's current stock
         *
         * @return currentStock The shop's current stock
         */
        public Container getCurrentStock() {
            return currentStock;
        }

        /**
         * Gets the shop NPC id's
         *
         * @return NPCs The shop NPC id's
         */
        public List<Integer> getNPCs() {
            return npcs;
        }
    }

    /**
     * The shop type.
     */
    public static enum ShopType {

        /**
         * A general store that will buy anything for less and sell for more.
         */
        GENERAL,
        /**
         * A specialist store that buys items that is in it's stock.
         */
        SPECIALIST
    }
}
