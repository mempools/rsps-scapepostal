package org.hyperion.rs2.model.container;

import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.container.impl.InterfaceContainerListener;
import org.hyperion.rs2.model.definition.ItemDefinition;

/**
 * Banking utility class.
 *
 * @author Graham Edgecombe
 */
public class Bank {

    /**
     * The bank size.
     */
    public static final int SIZE = 352;
    /**
     * The player inventory interface.
     */
    public static final int PLAYER_INVENTORY_INTERFACE = 5064;
    /**
     * The bank inventory interface.
     */
    public static final int BANK_INVENTORY_INTERFACE = 5382;
    /**
     * The bank to open
     */
    private static Container bank = null;

    /**
     * The bank type
     */
    public static enum BankType {
        /*
         * The gang bank.
         */

        GANG,
        /*
         * The player's bank.
         */
        DEFAULT
    }

    /**
     * Opens the bank for the specified player.
     *
     * @param player The player to open the bank for.
     * @param b The bank to open
     */
    public static void open(Player player, Container b) {
        bank = b;
        bank.shift();
        player.getActionSender().sendInventoryInterface(5292, 5063);
        player.getInterfaceState().addListener(bank, new InterfaceContainerListener(player, BANK_INVENTORY_INTERFACE));
        player.getInterfaceState().addListener(player.getInventory(), new InterfaceContainerListener(player, PLAYER_INVENTORY_INTERFACE));
    }

    /**
     * Withdraws an item.
     *
     * @param player The player.
     * @param slot The slot in the player's inventory.
     * @param id The item id.
     * @param amount The amount of the item to deposit.
     */
    public static void withdraw(Player player, int slot, int id, int amount) {
        if (bank == null) {
            return;
        }
        Item item = bank.get(slot);
        if (item == null) {
            return; // invalid packet, or client out of sync
        }
        if (item.getId() != id) {
            return; // invalid packet, or client out of sync
        }
        int transferAmount = item.getCount();
        if (transferAmount >= amount) {
            transferAmount = amount;
        } else if (transferAmount == 0) {
            return; // invalid packet, or client out of sync
        }
        int newId = item.getId(); // TODO deal with withdraw as notes!
        if (player.getSettings().isWithdrawingAsNotes()) {
            if (item.getDefinition().isNoteable()) {
                newId = item.getDefinition().getNotedId();
            }
        }
        ItemDefinition def = ItemDefinition.forId(newId);
        if (def.isStackable()) {
            if (player.getInventory().freeSlots() <= 0 && player.getInventory().getById(newId) == null) {
                player.getActionSender().sendMessage("You don't have enough inventory space to withdraw that many."); // this is the real message
            }
        } else {
            int free = player.getInventory().freeSlots();
            if (transferAmount > free) {
                player.getActionSender().sendMessage("You don't have enough inventory space to withdraw that many."); // this is the real message
                transferAmount = free;
            }
        }
        // now add it to inv
        if (player.getInventory().add(new Item(newId, transferAmount))) {
            // all items in the bank are stacked, makes it very easy!
            int newAmount = item.getCount() - transferAmount;
            if (newAmount <= 0) {
                bank.set(slot, null);
            } else {
                bank.set(slot, new Item(item.getId(), newAmount));
            }
        } else {
            player.getActionSender().sendMessage("You don't have enough inventory space to withdraw that many."); // this is the real message
        }
    }

    /**
     * Deposits an item.
     *
     * @param player The player.
     * @param slot The slot in the player's inventory.
     * @param id The item id.
     * @param amount The amount of the item to deposit.
     */
    public static void deposit(Player player, int slot, int id, int amount) {
        if (bank == null) {
            return;
        }
        boolean inventoryFiringEvents = player.getInventory().isFiringEvents();
        player.getInventory().setFiringEvents(false);
        try {
            Item item = player.getInventory().get(slot);
            if (item == null) {
                return; // invalid packet, or client out of sync
            }
            if (item.getId() != id) {
                return; // invalid packet, or client out of sync
            }
            int transferAmount = player.getInventory().getCount(id);
            if (transferAmount >= amount) {
                transferAmount = amount;
            } else if (transferAmount == 0) {
                return; // invalid packet, or client out of sync
            }
            boolean noted = item.getDefinition().isNoted();
            if (item.getDefinition().isStackable() || noted) {
                int bankedId = noted ? item.getDefinition().getNormalId() : item.getId();
                if (bank.freeSlots() < 1 && bank.getById(bankedId) == null) {
                    player.getActionSender().sendMessage("You don't have enough space in your bank account."); // this is the real message
                }
                // we only need to remove from one stack
                int newInventoryAmount = item.getCount() - transferAmount;
                Item newItem;
                if (newInventoryAmount <= 0) {
                    newItem = null;
                } else {
                    newItem = new Item(item.getId(), newInventoryAmount);
                }
                if (!bank.add(new Item(bankedId, transferAmount))) {
                    player.getActionSender().sendMessage("You don't have enough space in your bank account."); // this is the real message
                } else {
                    player.getInventory().set(slot, newItem);
                    player.getInventory().fireItemsChanged();
                    bank.fireItemsChanged();
                }
            } else {
                if (bank.freeSlots() < transferAmount) {
                    player.getActionSender().sendMessage("You don't have enough space in your bank account."); // this is the real message
                }
                if (!bank.add(new Item(item.getId(), transferAmount))) {
                    player.getActionSender().sendMessage("You don't have enough space in your bank account."); // this is the real message
                } else {
                    // we need to remove multiple items
                    for (int i = 0; i < transferAmount; i++) {
                        /* if(i == 0) {
                         player.getInventory().set(slot, null);
                         } else { */
                        player.getInventory().set(player.getInventory().getSlotById(item.getId()), null);
                        // }
                    }
                    player.getInventory().fireItemsChanged();
                }
            }
        } finally {
            player.getInventory().setFiringEvents(inventoryFiringEvents);
        }
    }
}
