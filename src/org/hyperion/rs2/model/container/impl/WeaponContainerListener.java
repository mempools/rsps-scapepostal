package org.hyperion.rs2.model.container.impl;

import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.combat.AttackType;
import org.hyperion.rs2.model.combat.CombatStyle;
import org.hyperion.rs2.model.container.Container;
import org.hyperion.rs2.model.container.ContainerListener;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.definition.EquipmentDefinition.WeaponStyle;

/**
 * A listener which updates the weapon tab.
 *
 * @author Graham Edgecombe
 */
public class WeaponContainerListener implements ContainerListener {

    /**
     * The player.
     */
    private final Player player;

    /**
     * Creates the listener.
     *
     * @param player The player.
     */
    public WeaponContainerListener(Player player) {
        this.player = player;
    }

    @Override
    public void itemChanged(Container container, int slot) {
        if (slot == Equipment.SLOT_WEAPON) {
            sendWeapon();
        }
    }

    @Override
    public void itemsChanged(Container container, int[] slots) {
        for (int slot : slots) {
            if (slot == Equipment.SLOT_WEAPON) {
                sendWeapon();
                return;
            }
        }
    }

    @Override
    public void itemsChanged(Container container) {
        sendWeapon();
    }

    /**
     * Sends weapon information.
     */
    private void sendWeapon() {
        Item weapon = player.getEquipment().get(Equipment.SLOT_WEAPON);
        int id = -1;
        String name;
        if (weapon == null) {
            name = "Unarmed";
        } else {
            name = weapon.getDefinition().getName();
            id = weapon.getId();
        }
        String genericName = filterWeaponName(name).trim();
        sendWeapon(id, name, genericName);
        sendSpecialBar();
        if (weapon != null && weapon.getEquipmentDefinition() != null) {
            player.getEquipmentAnimations().setRunAnimation(weapon.getEquipmentDefinition().getAnimation().getRunAnimation());
            player.getEquipmentAnimations().setStandAnimation(weapon.getEquipmentDefinition().getAnimation().getStandAnimation());
            player.getEquipmentAnimations().setWalkAnimation(weapon.getEquipmentDefinition().getAnimation().getWalkAnimation());
            player.getEquipmentAnimations().setStandTurnAnimation(weapon.getEquipmentDefinition().getAnimation().getStandTurnAnimation());
            player.getEquipmentAnimations().setTurn180Animation(weapon.getEquipmentDefinition().getAnimation().getTurn180Animation());
            player.getEquipmentAnimations().setTurn90CWAnimation(weapon.getEquipmentDefinition().getAnimation().getTurn90CW());
            player.getEquipmentAnimations().setTurn90CCWAnimation(weapon.getEquipmentDefinition().getAnimation().getTurn90CCWAnimation());
        }
    }

    /**
     * Sends the special bar.
     */
    public void sendSpecialBar() {
        Item weapon = player.getEquipment().get(Equipment.SLOT_WEAPON);
        if (weapon == null || weapon.getEquipmentDefinition() == null
                || !weapon.getEquipmentDefinition().containsWeaponStyle(WeaponStyle.SPECIAL)) {
            player.getActionSender().sendInterfaceConfig(7599, true);
            player.getActionSender().sendInterfaceConfig(7649, true);
            player.getActionSender().sendInterfaceConfig(7549, true);
            player.getActionSender().sendInterfaceConfig(7574, true);
            player.getActionSender().sendInterfaceConfig(7674, true);
            player.getActionSender().sendInterfaceConfig(7599, true);
            player.getActionSender().sendInterfaceConfig(7499, true);
            player.getActionSender().sendInterfaceConfig(7624, true);
            player.getActionSender().sendInterfaceConfig(7800, true);
            player.getActionSender().sendInterfaceConfig(8493, true);
            player.getActionSender().sendInterfaceConfig(7474, true);
            player.getActionSender().sendInterfaceConfig(7549, true);
            player.getActionSender().sendInterfaceConfig(7699, true);
            player.getActionSender().sendInterfaceConfig(12323, true);
            return;
        }
        player.getActionSender().sendInterfaceConfig(7599, false);
        player.getActionSender().sendInterfaceConfig(7649, false);
        player.getActionSender().sendInterfaceConfig(7549, false);
        player.getActionSender().sendInterfaceConfig(7574, false);
        player.getActionSender().sendInterfaceConfig(7674, false);
        player.getActionSender().sendInterfaceConfig(7599, false);
        player.getActionSender().sendInterfaceConfig(7499, false);
        player.getActionSender().sendInterfaceConfig(7624, false);
        player.getActionSender().sendInterfaceConfig(7800, false);
        player.getActionSender().sendInterfaceConfig(8493, false);
        player.getActionSender().sendInterfaceConfig(7474, false);
        player.getActionSender().sendInterfaceConfig(7549, false);
        player.getActionSender().sendInterfaceConfig(7699, false);
        player.getActionSender().sendInterfaceConfig(12323, false);
        player.getActionSender().updateSpecial();
    }

    /**
     * Sends weapon information.
     *
     * @param id The id.
     * @param name The name.
     * @param genericName The filtered name.
     */
    private void sendWeapon(int id, String name, String genericName) {
        if (name.equals("Unarmed")) {
            player.getActionSender().sendSidebarInterface(0, 5855);
            player.getActionSender().sendString(5857, name);
        } else if (name.endsWith("whip")) {
            player.getActionSender().sendSidebarInterface(0, 12290);
            player.getActionSender().sendInterfaceModel(12291, 200, id);
            player.getActionSender().sendString(12293, name);
        } else if (name.endsWith("Scythe")) {
            player.getActionSender().sendSidebarInterface(0, 776);
            player.getActionSender().sendInterfaceModel(777, 200, id);
            player.getActionSender().sendString(779, name);
        } else if (name.endsWith("bow") || name.startsWith("Crystal bow")
                || name.startsWith("Toktz-xil-ul")) {
            player.getActionSender().sendSidebarInterface(0, 1764);
            player.getActionSender().sendInterfaceModel(1765, 200, id);
            player.getActionSender().sendString(1767, name);
        } else if (name.startsWith("Staff") || name.endsWith("staff")) {
            player.getActionSender().sendSidebarInterface(0, 328);
            player.getActionSender().sendInterfaceModel(329, 200, id);
            player.getActionSender().sendString(331, name);
        } else if (genericName.startsWith("dart")) {
            player.getActionSender().sendSidebarInterface(0, 4446);
            player.getActionSender().sendInterfaceModel(4447, 200, id);
            player.getActionSender().sendString(4449, name);
        } else if (genericName.startsWith("dagger")) {
            player.getActionSender().sendSidebarInterface(0, 2276);
            player.getActionSender().sendInterfaceModel(2277, 200, id);
            player.getActionSender().sendString(2279, name);
        } else if (genericName.startsWith("pickaxe")) {
            player.getActionSender().sendSidebarInterface(0, 5570);
            player.getActionSender().sendInterfaceModel(5571, 200, id);
            player.getActionSender().sendString(5573, name);
        } else if (genericName.startsWith("axe")
                || genericName.startsWith("battleaxe")) {
            player.getActionSender().sendSidebarInterface(0, 1698);
            player.getActionSender().sendInterfaceModel(1699, 200, id);
            player.getActionSender().sendString(1701, name);
        } else if (genericName.startsWith("Axe")
                || genericName.startsWith("Battleaxe")) {
            player.getActionSender().sendSidebarInterface(0, 1698);
            player.getActionSender().sendInterfaceModel(1699, 200, id);
            player.getActionSender().sendString(1701, name);
        } else if (genericName.startsWith("halberd")) {
            player.getActionSender().sendSidebarInterface(0, 8460);
            player.getActionSender().sendInterfaceModel(8461, 200, id);
            player.getActionSender().sendString(8463, name);
        } else {
            player.getActionSender().sendSidebarInterface(0, 2423);
            player.getActionSender().sendInterfaceModel(2424, 200, id);
            player.getActionSender().sendString(2426, name);
        }

        if (name.equals("unarmed")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case DEFENSIVE:
                case CONTROLLED_3:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 2);
                    break;
            }
            player.getActionSender().sendString(5857, genericName);
            player.getActionSender().sendSidebarInterface(0, 5855);
        } else if (name.endsWith("whip") || name.contains("mouse")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                case CONTROLLED_3:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.CONTROLLED_2);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 2);
                    break;
            }
            player.getActionSender().sendInterfaceModel(12291, 200, id);
            player.getActionSender().sendString(12293, genericName);
            player.getActionSender().sendSidebarInterface(0, 12290);
        } else if (name.endsWith("Scythe")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case CONTROLLED_3:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_2);
                    player.getActionSender().sendConfig(43, 2);
                    break;
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 3);
                    break;
            }
            player.getActionSender().sendInterfaceModel(777, 200, id);
            player.getActionSender().sendString(779, genericName);
            player.getActionSender().sendSidebarInterface(0, 776);
        } else if ((name.contains("bow") || name.equals("seercull"))
                && !name.contains("karil") && !name.contains("c'bow")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.RANGE);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.RANGE);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case DEFENSIVE:
                case CONTROLLED_3:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.RANGE);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 2);
                    break;
            }
            player.getActionSender().sendInterfaceModel(1765, 200, id);
            player.getActionSender().sendString(1767, genericName);
            player.getActionSender().sendSidebarInterface(0, 1764);
        } else if (name.contains("karil") || name.contains("c'bow")
                || name.contains("cross")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.RANGE);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.RANGE);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case DEFENSIVE:
                case CONTROLLED_3:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.RANGE);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 2);
                    break;
            }
            player.getActionSender().sendInterfaceModel(1750, 200, id);
            player.getActionSender().sendString(1752, genericName);
            player.getActionSender().sendSidebarInterface(0, 1749);
        } else if (genericName.contains("scimitar")
                || name.equals("excalibur")
                || name.endsWith("light")
                || (genericName.contains("sword")
                && !genericName.contains("2h")
                && !genericName.contains("god") && !genericName.contains("saradomin"))) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case CONTROLLED_3:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.CONTROLLED_1);
                    player.getActionSender().sendConfig(43, 2);
                    break;
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 3);
                    break;
            }
            player.getActionSender().sendInterfaceModel(2424, 200, id);
            player.getActionSender().sendString(2426, genericName);
            player.getActionSender().sendSidebarInterface(0, 2423);
        } else if (name.contains("staff") || name.contains("wand")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case DEFENSIVE:
                case CONTROLLED_3:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 2);
                    break;
            }
            player.getActionSender().sendInterfaceModel(329, 200, id);
            player.getActionSender().sendString(331, genericName);
            player.getActionSender().sendSidebarInterface(0, 328);
        } else if (genericName.startsWith("dart")
                || genericName.endsWith("knife")
                || genericName.endsWith("thrownaxe")
                || genericName.endsWith("javelin")
                || name.equals("toktz-xil-ul")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.RANGE);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.RANGE);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case DEFENSIVE:
                case CONTROLLED_3:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.RANGE);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 2);
                    break;
            }
            player.getActionSender().sendInterfaceModel(4447, 200, id);
            player.getActionSender().sendString(4449, genericName);
            player.getActionSender().sendSidebarInterface(0, 4446);
        } else if (genericName.contains("mace") || name.endsWith("flail")
                || name.endsWith("anchor")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case CONTROLLED_3:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_2);
                    player.getActionSender().sendConfig(43, 2);
                    break;
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 3);
                    break;
            }
            player.getActionSender().sendInterfaceModel(3797, 200, id);
            player.getActionSender().sendString(3799, genericName);
            player.getActionSender().sendSidebarInterface(0, 3796);
        } else if (genericName.startsWith("dagger")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case CONTROLLED_3:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_2);
                    player.getActionSender().sendConfig(43, 2);
                    break;
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 3);
                    break;
            }
            player.getActionSender().sendInterfaceModel(2277, 200, id);
            player.getActionSender().sendString(2279, genericName);
            player.getActionSender().sendSidebarInterface(0, 2276);
        } else if (genericName.startsWith("pickaxe")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case CONTROLLED_3:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_2);
                    player.getActionSender().sendConfig(43, 2);
                    break;
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 3);
                    break;
            }
            player.getActionSender().sendInterfaceModel(5571, 200, id);
            player.getActionSender().sendString(5573, genericName);
            player.getActionSender().sendSidebarInterface(0, 5570);
        } else if (genericName.startsWith("maul")
                || genericName.endsWith("warhammer")
                || name.endsWith("hammers") || name.equals("tzhaar-ket-om")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case DEFENSIVE:
                case CONTROLLED_3:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 2);
                    break;
            }
            player.getActionSender().sendInterfaceModel(426, 200, id);
            player.getActionSender().sendString(428, genericName);
            player.getActionSender().sendSidebarInterface(0, 425);
        } else if (genericName.endsWith("2h")
                || genericName.endsWith("godsword")
                || name.equals("saradomin sword")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case CONTROLLED_3:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_2);
                    player.getActionSender().sendConfig(43, 2);
                    break;
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 3);
                    break;
            }
            player.getActionSender().sendInterfaceModel(4706, 200, id);
            player.getActionSender().sendString(4708, genericName);
            player.getActionSender().sendSidebarInterface(0, 4705);
        } else if (genericName.contains("axe")
                || genericName.contains("battleaxe")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_1);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case CONTROLLED_3:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.AGGRESSIVE_2);
                    player.getActionSender().sendConfig(43, 2);
                    break;
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 3);
                    break;
            }
            player.getActionSender().sendInterfaceModel(1699, 200, id);
            player.getActionSender().sendString(1701, genericName);
            player.getActionSender().sendSidebarInterface(0, 1698);
        } else if (genericName.startsWith("halberd")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.CONTROLLED_1);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.CONTROLLED_2);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case DEFENSIVE:
                case CONTROLLED_3:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.CONTROLLED_3);
                    player.getActionSender().sendConfig(43, 2);
                    break;
            }
            player.getActionSender().sendInterfaceModel(8461, 200, id);
            player.getActionSender().sendString(8463, genericName);
            player.getActionSender().sendSidebarInterface(0, 8460);
        } else if (genericName.contains("spear")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.CONTROLLED_1);
                    player.getActionSender().sendConfig(43, 0);
                    break;
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                    player.getCombatSession().setAttackType(AttackType.SLASH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.CONTROLLED_2);
                    player.getActionSender().sendConfig(43, 1);
                    break;
                case AGGRESSIVE_2:
                case CONTROLLED_3:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession()
                            .setCombatStyle(CombatStyle.CONTROLLED_3);
                    player.getActionSender().sendConfig(43, 2);
                    break;
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.STAB);
                    player.getCombatSession().setCombatStyle(CombatStyle.DEFENSIVE);
                    player.getActionSender().sendConfig(43, 3);
                    break;
            }
            player.getActionSender().sendInterfaceModel(4680, 200, id);
            player.getActionSender().sendString(4682, genericName);
            player.getActionSender().sendSidebarInterface(0, 4679);
        } else if (genericName.equals("bow-sword")) {
            switch (player.getCombatSession().getCombatStyle()) {
                case ACCURATE:
                case CONTROLLED_1:
                case AGGRESSIVE_1:
                case CONTROLLED_2:
                case AGGRESSIVE_2:
                case CONTROLLED_3:
                case DEFENSIVE:
                case AUTOCAST:
                    player.getCombatSession().setAttackType(AttackType.CRUSH);
                    player.getCombatSession().setCombatStyle(CombatStyle.ACCURATE);
                    player.getActionSender().sendConfig(43, 0);
                    break;
            }
        }
    }

    /**
     * Filters a weapon name.
     *
     * @param name The original name.
     * @return The filtered name.
     */
    private String filterWeaponName(String name) {
        final String[] filtered = new String[]{
            "Iron", "Steel", "Scythe", "Black", "Mithril", "Adamant",
            "Rune", "Granite", "Dragon", "Crystal", "Bronze"
        };
        for (String filter : filtered) {
            name = name.replaceAll(filter, "");
        }
        return name;
    }
}
