package org.hyperion.rs2.model.container;

import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.Player.Rights;
import org.hyperion.rs2.model.request.RequestManager.RequestState;
import org.hyperion.rs2.model.container.impl.InterfaceContainerListener;
import org.hyperion.rs2.model.definition.ItemDefinition;

import java.text.NumberFormat;

/**
 * @author rune-server's official server team
 */
public class Trade {

    /**
     * The trade size.
     */
    public static final int SIZE = 28;
    /**
     * The player inventory interface.
     */
    public static final int PLAYER_INVENTORY_INTERFACE = 3322;
    /**
     * The trade inventory interface.
     */
    public static final int TRADE_INVENTORY_INTERFACE = 3415;
    /**
     * The items traded
     */
    private static Item[] sentItems = null;
    /**
     * The received items
     */
    private static Item[] receivedItems = null;
    /**
     * The un-tradable items
     */
    public static int[] untradableItems = {};

    /**
     * Opens the trade for the specified player.
     *
     * @param player The player to open the trade for.
     * @param partner The other player trading.
     */
    public static void open(Player player, Player partner) {
        player.getActionSender().sendString(3431, "");
        player.getActionSender().sendString(3535, "Are you sure you want to make this trade?");
        player.getActionSender().sendString(3417, "Trading with: " + partner.getName());
        player.getActionSender().sendInventoryInterface(3323, 3321);

        partner.getActionSender().sendString(3431, "");
        partner.getActionSender().sendString(3535, "Are you sure you want to make this trade?");
        partner.getActionSender().sendString(3417, "Trading with: " + player.getName());
        partner.getActionSender().sendInventoryInterface(3323, 3321);

        player.getInterfaceState().addListener(player.getTrade(), new InterfaceContainerListener(player, TRADE_INVENTORY_INTERFACE));
        player.getInterfaceState().addListener(partner.getTrade(), new InterfaceContainerListener(player, 3416));
        player.getInterfaceState().addListener(player.getInventory(), new InterfaceContainerListener(player, PLAYER_INVENTORY_INTERFACE));

        partner.getInterfaceState().addListener(partner.getTrade(), new InterfaceContainerListener(partner, TRADE_INVENTORY_INTERFACE));
        partner.getInterfaceState().addListener(player.getTrade(), new InterfaceContainerListener(partner, 3416));
        partner.getInterfaceState().addListener(partner.getInventory(), new InterfaceContainerListener(partner, PLAYER_INVENTORY_INTERFACE));
    }

    /**
     * Offers an item.
     *
     * @param player The player.
     * @param slot The slot in the player's inventory.
     * @param id The item id.
     * @param amount The amount of the item to offer.
     */
    public static void offerItem(Player player, int slot, int id, int amount) {
        if (player.getInterfaceState().getCurrentInterface() != 3323) {
            return;
        }
        Item item = player.getInventory().get(slot);
        if (item == null) {
            return; // invalid packet, or client out of sync
        }
        if (item.getId() != id) {
            return; // invalid packet, or client out of sync
        }
        /*
         * Un-tradable items
         */
        for (int untradable : untradableItems) {
            if (item.getId() == untradable && (player.getRights() == Rights.PLAYER
                    || player.getRights() == Rights.MODERATOR)) {
                player.getActionSender().sendMessage("You cannot trade this item.");
                return;
            }
        }
        Player partner = player.getRequestManager().getAcquaintance();
        if (partner == null) {
            return;
        }
        player.getActionSender().sendString(3431, "");
        partner.getActionSender().sendString(3431, "");
        player.getRequestManager().setState(RequestState.PARTICIPATING);
        partner.getRequestManager().setState(RequestState.PARTICIPATING);
        boolean inventoryFiringEvents = player.getInventory().isFiringEvents();
        player.getInventory().setFiringEvents(false);
        try {
            int transferAmount = player.getInventory().getCount(id);
            if (transferAmount >= amount) {
                transferAmount = amount;
            } else if (transferAmount == 0) {
                return; // invalid packet, or client out of sync
            }

            if (player.getTrade().add(new Item(item.getId(), transferAmount), -1)) {
                player.getInventory().remove(new Item(item.getId(), transferAmount));
            }
            player.getInventory().fireItemsChanged();
        } finally {
            player.getInventory().setFiringEvents(inventoryFiringEvents);
        }
    }

    /**
     * Removes an offered an item.
     *
     * @param player The player.
     * @param slot The slot in the player's inventory.
     * @param id The item id.
     * @param amount The amount of the item to offer.
     */
    public static void removeItem(Player player, int slot, int id, int amount) {
        if (player.getInterfaceState().getCurrentInterface() != 3323) {
            return;
        }
        Player partner = player.getRequestManager().getAcquaintance();
        if (partner == null) {
            return;
        }
        player.getActionSender().sendString(3431, "");
        partner.getActionSender().sendString(3431, "");
        player.getRequestManager().setState(RequestState.PARTICIPATING);
        partner.getRequestManager().setState(RequestState.PARTICIPATING);
        boolean inventoryFiringEvents = player.getInventory().isFiringEvents();
        player.getInventory().setFiringEvents(false);
        try {

            Item item = player.getTrade().get(slot);
            if (item == null || item.getId() != id) {
                //cheat detection
                return; // invalid packet, or client out of sync
            }
            int transferAmount = player.getTrade().getCount(id);
            if (transferAmount >= amount) {
                transferAmount = amount;
            } else if (transferAmount == 0) {
                return; // invalid packet, or client out of sync
            }

            if (player.getInventory().add(new Item(item.getId(), transferAmount), -1)) {
                player.getTrade().remove(new Item(item.getId(), transferAmount));
            }
            player.getInventory().fireItemsChanged();
        } finally {
            player.getInventory().setFiringEvents(inventoryFiringEvents);
        }
    }

    /**
     * Sends the second trade screen
     *
     * @param player The player
     * @param partner The partner
     */
    public static void secondScreen(Player player, Player partner) {
        if (partner == null) {
            return;
        }
        String tradeString = "Absolutely nothing!";
        String tradeStringAmt = "";
        int amt = 0;
        for (int i = 0; i < SIZE; i++) {
            if (player.getTrade().get(i) != null) {
                if (player.getTrade().get(i).getCount() >= 1000 && player.getTrade().get(i).getCount() < 1000000) {
                    tradeStringAmt = "@cya@" + (player.getTrade().get(i).getCount() / 1000) + "K @whi@(" + NumberFormat.getInstance().format(player.getTrade().get(i).getCount()) + ")";
                } else if (player.getTrade().get(i).getCount() >= 1000000) {
                    tradeStringAmt = "@gre@" + (player.getTrade().get(i).getCount() / 1000000) + " million @whi@(" + NumberFormat.getInstance().format(player.getTrade().get(i).getCount()) + ")";
                } else {
                    tradeStringAmt = "" + NumberFormat.getInstance().format(player.getTrade().get(i).getCount());
                }

                if (amt == 0) {
                    tradeString = ItemDefinition.forId(player.getTrade().get(i).getId()).getName();
                } else {
                    tradeString = tradeString + "\\n" + ItemDefinition.forId(player.getTrade().get(i).getId()).getName();
                }
                if (ItemDefinition.forId(player.getTrade().get(i).getId()).isStackable()) {
                    tradeString = tradeString + " x " + tradeStringAmt;
                }
                amt++;
            }
        }

        player.getActionSender().sendString(3557, tradeString);
        tradeString = "Absolutely nothing!";
        tradeStringAmt = "";
        amt = 0;

        for (int i = 0; i < SIZE; i++) {
            if (partner.getTrade().get(i) != null) {
                if (partner.getTrade().get(i).getCount() >= 1000 && partner.getTrade().get(i).getCount() < 1000000) {
                    tradeStringAmt = "@cya@" + (partner.getTrade().get(i).getCount() / 1000) + "K @whi@(" + NumberFormat.getInstance().format(partner.getTrade().get(i).getCount()) + ")";
                } else if (partner.getTrade().get(i).getCount() >= 1000000) {
                    tradeStringAmt = "@gre@" + (partner.getTrade().get(i).getCount() / 1000000) + " million @whi@(" + NumberFormat.getInstance().format(partner.getTrade().get(i).getCount()) + ")";
                } else {
                    tradeStringAmt = "" + NumberFormat.getInstance().format(partner.getTrade().get(i).getCount());
                }

                if (amt == 0) {
                    tradeString = ItemDefinition.forId(partner.getTrade().get(i).getId()).getName();
                } else {
                    tradeString = tradeString + "\\n" + ItemDefinition.forId(partner.getTrade().get(i).getId()).getName();
                }
                if (ItemDefinition.forId(partner.getTrade().get(i).getId()).isStackable()) {
                    tradeString = tradeString + " x " + tradeStringAmt;
                }

                amt++;
            }
        }
        player.getActionSender().sendString(3558, tradeString);
        player.getActionSender().sendInventoryInterface(3443, 3213);
    }

    /**
     * Sends the accept trade
     *
     * @param player The player
     * @param partner The partner
     * @param screenStage The screen state
     */
    public static void acceptTrade(Player player, Player partner, int screenStage) {
        switch (screenStage) {
            case 1:
                if (player.getInventory().freeSlots() < partner.getTrade().size()) {
                    player.getActionSender().sendMessage("You do not have enough free inventory slots.");
                    player.getActionSender().sendString(3431, "");
                    partner.getActionSender().sendString(3431, "");
                    player.getRequestManager().setState(RequestState.PARTICIPATING);
                    partner.getRequestManager().setState(RequestState.PARTICIPATING);
                } else if (partner.getInventory().freeSlots() < player.getTrade().size()) {
                    player.getActionSender().sendMessage("The other player does not have enough free inventory slots.");
                    player.getActionSender().sendString(3431, "");
                    partner.getActionSender().sendString(3431, "");
                    player.getRequestManager().setState(RequestState.PARTICIPATING);
                    partner.getRequestManager().setState(RequestState.PARTICIPATING);
                } else if (partner.getRequestManager().getState() == RequestState.CONFIRM_1) {
                    secondScreen(player, partner);
                    secondScreen(partner, player);
                } else {
                    player.getActionSender().sendString(3431, "Waiting for other player...");
                    partner.getActionSender().sendString(3431, "Other player has accepted");
                    player.getRequestManager().setState(RequestState.CONFIRM_1);
                }
                break;
            case 2:
                if (partner.getRequestManager().getState() == RequestState.CONFIRM_2) {
                    receivedItems = partner.getTrade().toArray();
                    sentItems = player.getTrade().toArray();
                    player.getRequestManager().finishRequest();
                } else {
                    player.getRequestManager().setState(RequestState.CONFIRM_2);
                    player.getActionSender().sendString(3535, "Waiting for other player...");
                    partner.getActionSender().sendString(3535, "Other player has accepted.");
                }
                break;
        }
    }
}
