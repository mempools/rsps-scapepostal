package org.hyperion.rs2.model;

import org.hyperion.Constants;
import org.hyperion.rs2.WorldConstants;
import org.hyperion.rs2.model.UpdateFlags.UpdateFlag;

/**
 * Represents a player's skill and experience levels.
 *
 * @author Graham Edgecombe
 */
public class Skills {

    /**
     * The number of skills.
     */
    public static final int SKILL_COUNT = 21;
    /**
     * The largest allowed experience.
     */
    public static final double MAXIMUM_EXP = 200000000;
    /**
     * The skill names.
     */
    public static final String[] SKILL_NAME = {"Attack", "Defence",
        "Strength", "Hitpoints", "Range", "Prayer",
        "Magic", "Cooking", "Woodcutting", "Fletching",
        "Fishing", "Firemaking", "Crafting", "Smithing",
        "Mining", "Herblore", "Agility", "Thieving",
        "Slayer", "Farming", "Runecrafting"};
    /**
     * The bonus names.
     */
    public static final String[] BONUS_NAME = {"Stab", "Slash",
        "Crush", "Magic", "Range", "Stab", "Slash", "Crush",
        "Magic", "Range", "Strength", "Prayer", "Ranged Strength"
    };
    /**
     * Constants for the skill numbers.
     */
    public static final int ATTACK = 0, DEFENCE = 1, STRENGTH = 2,
            HITPOINTS = 3, RANGE = 4, PRAYER = 5, MAGIC = 6,
            COOKING = 7, WOODCUTTING = 8, FLETCHING = 9,
            FISHING = 10, FIREMAKING = 11, CRAFTING = 12,
            SMITHING = 13, MINING = 14, HERBLORE = 15,
            AGILITY = 16, THIEVING = 17, SLAYER = 18,
            FARMING = 19, RUNECRAFTING = 20;
    /**
     * The entity object.
     */
    private Actor character;
    /**
     * The levels array.
     */
    private int[] levels = new int[SKILL_COUNT];
    /**
     * The experience array.
     */
    private double[] exps = new double[SKILL_COUNT];
    /**
     * The bonus array.
     */
    private int[] bonuses = new int[13];

    /**
     * Creates a skills object.
     *
     * @param character The player whose skills this object represents.
     */
    public Skills(Actor character) {
        this.character = character;
        for (int i = 0; i < SKILL_COUNT; i++) {
            levels[i] = 1;
            exps[i] = 0;
        }
        levels[3] = 10;
        exps[3] = 1184;
    }

    /**
     * Gets the total level.
     *
     * @return The total level.
     */
    public int getTotalLevel() {
        int total = 0;
        for (int i = 0; i < levels.length; i++) {
            total += getLevelForExperience(i);
        }
        return total;
    }

    /**
     * Gets the experience for a requested level.
     *
     * @param lvl The level we're getting the experience amount for.
     * @return Minimum experience required for that level.
     */
    /**
     * Gets the combat level.
     *
     * @return The combat level.
     */
    public int getCombatLevel() {
        final int attack = getLevelForExperience(0);
        final int defence = getLevelForExperience(1);
        final int strength = getLevelForExperience(2);
        final int hp = getLevelForExperience(3);
        final int prayer = getLevelForExperience(5);
        final int ranged = getLevelForExperience(4);
        final int magic = getLevelForExperience(6);
        int combatLevel = 3;
        combatLevel = (int) ((defence + hp + Math.floor(prayer / 2)) * 0.2535) + 1;
        final double melee = (attack + strength) * 0.325;
        final double ranger = Math.floor(ranged * 1.5) * 0.325;
        final double mage = Math.floor(magic * 1.5) * 0.325;
        if (melee >= ranger && melee >= mage) {
            combatLevel += melee;
        } else if (ranger >= melee && ranger >= mage) {
            combatLevel += ranger;
        } else if (mage >= melee && mage >= ranger) {
            combatLevel += mage;
        }
        if (combatLevel <= 126) {
            return combatLevel;
        } else {
            return 126;
        }
    }

    /**
     * Gets the health as a percentage
     *
     * @return percentage The entity's health percentage
     */
    public int getHealthPercentage() {
        double currentHealth = character.getSkills().getLevel(Skills.HITPOINTS);
        double maxHealth = character.getSkills().getLevelForExperience(Skills.HITPOINTS);
        double calc = (double) currentHealth / (double) maxHealth;
        return (int) Math.round(calc * 100);
    }

    /**
     * Sets a skill.
     *
     * @param skill The skill id.
     * @param level The level.
     * @param exp The experience.
     */
    public void setSkill(int skill, int level, double exp) {
        levels[skill] = level;
        exps[skill] = exp;
        if (character.isPlayer()) {
            character.getActionSender().sendSkill(skill);
        }
    }

    /**
     * Sets a level.
     *
     * @param skill The skill id.
     * @param level The level.
     */
    public void setLevel(int skill, int level) {
        levels[skill] = level;
        if (character.isPlayer()) {
            character.getActionSender().sendSkill(skill);
        }
    }

    /**
     * Sets experience.
     *
     * @param skill The skill id.
     * @param exp The experience.
     */
    public void setExperience(int skill, double exp) {
        int oldLvl = getLevelForExperience(skill);
        exps[skill] = exp;
        if (character.isPlayer()) {
            character.getActionSender().sendSkill(skill);
            int newLvl = getLevelForExperience(skill);
            if (oldLvl != newLvl) {
                character.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
            }
        }
    }

    /**
     * Increases a level.
     *
     * @param skill The skill to increment.
     * @param amount The amount to increase
     */
    public void increaseLevel(int skill, int amount) {
        if (levels[skill] == 0) {
            amount = 0;
        }
        if (amount > levels[skill]) {
            amount = levels[skill];
        }
        levels[skill] = levels[skill] + amount;
        if (character.isPlayer()) {
            character.getActionSender().sendSkill(skill);
        }
    }

    /**
     * Detracts a given level a given amount.
     *
     * @param skill The level to detract.
     * @param amount The amount to detract from the level.
     */
    public void decreaseLevel(int skill, int amount) {
        if (levels[skill] == 0) {
            amount = 0;
        }
        if (amount > levels[skill]) {
            amount = levels[skill];
        }
        levels[skill] = levels[skill] - amount;
        if (character.isPlayer()) {
            character.getActionSender().sendSkill(skill);
        }
    }

    /**
     * Normalizes a level (adjusts it until it is at its normal value).
     *
     * @param skill The skill to normalize.
     */
    public void normalizeLevel(int skill) {
        int norm = getLevelForExperience(skill);
        if (levels[skill] > norm) {
            levels[skill]--;
            character.getActionSender().sendSkill(skill);
        } else if (levels[skill] < norm) {
            levels[skill]++;
            character.getActionSender().sendSkill(skill);
        }
    }

    /**
     * Gets a level.
     *
     * @param skill The skill id.
     * @return The level.
     */
    public int getLevel(int skill) {
        return levels[skill];
    }

    /**
     * Gets a level by experience.
     *
     * @param skill The skill id.
     * @return The level.
     */
    public int getLevelForExperience(int skill) {
        double exp = exps[skill];
        int points = 0;
        int output = 0;

        for (int lvl = 1; lvl <= 99; lvl++) {
            points += Math.floor(lvl + 300.0 * Math.pow(2.0, lvl / 7.0));
            output = (int) Math.floor(points / 4);
            if (output >= exp) {
                return lvl;
            }
        }
        return 99;
    }

    /**
     * Gets a experience from the level.
     *
     * @param level The level.
     * @return The experience.
     */
    public int getXPForLevel(int level) {
        int points = 0;
        int output = 0;
        for (int lvl = 1; lvl <= level; lvl++) {
            points += Math.floor(lvl + 300.0 * Math.pow(2.0, lvl / 7.0));
            if (lvl >= level) {
                return output;
            }
            output = (int) Math.floor(points / 4);
        }
        return 0;
    }
    /* Gets a experience from the level.
     *
     * @param level The level.
     * @return The experience.
     */

    public double getExperienceForLevel(int level) {
        double points = 0;
        double output = 0;
        for (int lvl = 1; lvl <= level; lvl++) {
            points += Math.floor(lvl + 300.0 * Math.pow(2.0, lvl / 7.0));
            if (lvl >= level) {
                return output;
            }
            output = (int) Math.floor(points / 4);
        }
        return 0;
    }

    /**
     * Gets experience.
     *
     * @param skill The skill id.
     * @return The experience.
     */
    public double getExperience(int skill) {
        return exps[skill];
    }

    /**
     * Adds experience.
     *
     * @param skill The skill.
     * @param exp The experience to add.
     */
    public void addExperience(int skill, double exp) {
        int oldLevel = levels[skill];
        exps[skill] += exp * WorldConstants.EXP_BONUS;
        if (exps[skill] > MAXIMUM_EXP) {
            exps[skill] = MAXIMUM_EXP;
        }
        int newLevel = getLevelForExperience(skill);
        int levelDiff = newLevel - oldLevel;
        if (levelDiff > 0) {
            levels[skill] += levelDiff;
            character.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
        }
        if (character.isPlayer()) {
            character.getActionSender().sendSkill(skill);
        }
    }

    /**
     * Sets one of the entity's bonuses.
     *
     * @param index The bonus index.
     * @param amount The bonus to set.
     */
    public void setBonus(int index, int amount) {
        bonuses[index] = amount;
    }

    /**
     * Sets one of the entity's bonuses.
     *
     * @param bonuses The bonus to set.
     */
    public void setBonuses(int[] bonuses) {
        this.bonuses = bonuses;
    }

    /**
     * Resets the entity's bonuses.
     */
    public void resetBonuses() {
        bonuses = new int[13];
    }

    /**
     * Gets the entity's bonuses.
     *
     * @return The entity's bonuses.
     */
    public int[] getBonuses() {
        return bonuses;
    }

    /**
     * Gets a bonus by its index.
     *
     * @param index The bonus index.
     * @return The bonus.
     */
    public int getBonus(int index) {
        return bonuses[index];
    }

    /**
     * Calculates the bonuses
     */
    public void sendBonuses() {
        resetBonuses();
        for (Item item : character.getEquipment().toArray()) {
            if (item != null && item.getEquipmentDefinition() != null) {
                for (int i = 0; i < item.getEquipmentDefinition().getBonuses().length; i++) {
                    setBonus(i, getBonus(i) + item.getEquipmentDefinition().getBonus(i));
                }
            }
        }
        for (int index = 0; index < getBonuses().length; index++) {
            sendBonus(index);
        }
    }

    /**
     * Sends a bonus by the bonus id
     *
     * @param index The bonus id
     * @return The action sender instance, for chaining.
     */
    public Skills sendBonus(int index) {
        int offset = 0;
        String send = "";
        if (getBonus(index) >= 0) {
            send = BONUS_NAME[index] + ": +" + getBonus(index);
        } else {
            send = BONUS_NAME[index] + ": " + getBonus(index);
        }
        if (index == 10) {
            offset = 1;
        }
        if (index != 12) {
            if (index == 10) {
                send += "/" + (getBonus(12) >= 0 ? "+" + getBonus(12) : getBonus(12));
            }
            character.getActionSender().sendString(1675 + index + offset, send);
        }
        return this;
    }

    /**
     * Resets all the statistics to their default level.
     */
    public void resetStats() {
        for (int i = 0; i < levels.length; i++) {
            levels[i] = getLevelForExperience(i);
        }
        // = getLevelForExperience(Skills.PRAYER);
        setLevel(Skills.PRAYER, getLevelForExperience(Skills.PRAYER));
        if (character.getActionSender() != null) {
            character.getActionSender().sendSkills();
        }
    }

    /**
     * Checks if a level is below it's normal level for experience. This is used
     * for consumables, e.g. 130/99hp, then eating a manta ray and it returning
     * to 99/99hp.
     *
     * @param skill The skill id.
     * @return If the level is below it's normal level.
     */
    public boolean isLevelBelowOriginal(int skill) {
        return levels[skill] < getLevelForExperience(skill);
    }

    /**
     * Checks if a level is below it's normal level for experience + a certain
     * modification. This is used for consumables, e.g. 130/99 str, then
     * drinking a super strength potion and it returning to 118/99.
     *
     * @param skill The skill id.
     * @param modification The modification amount.
     * @return If the level is below it's normal level + a certain modification.
     */
    public boolean isLevelBelowOriginalModification(int skill, int modification) {
        return levels[skill] < (getLevelForExperience(skill) + modification);
    }

    /**
     * Increases a level to its level for experience, depending on the
     * modification amount.
     *
     * @param skill The skill id.
     * @param modification The modification amount.
     */
    public void increaseLevelToMaximum(int skill, int modification) {
        if (isLevelBelowOriginal(skill)) {
            setLevel(skill, levels[skill] + modification >= getLevelForExperience(skill) ? getLevelForExperience(skill) : levels[skill] + modification);
        }
    }

    /**
     * Increases a level to its level for experience + modification amount.
     *
     * @param skill The skill id.
     * @param modification The modification amount.
     */
    public void increaseLevelToMaximumModification(int skill, int modification) {
        if (isLevelBelowOriginalModification(skill, modification)) {
            setLevel(skill, levels[skill] + modification >= (getLevelForExperience(skill) + modification) ? (getLevelForExperience(skill) + modification) : levels[skill] + modification);
        }
    }

    /**
     * Decreases a level to its minimum.
     *
     * @param skill The skill id.
     * @param modification The modification amount.
     */
    public void decreaseLevelToMinimum(int skill, int modification) {
        if (levels[skill] > 1) {
            setLevel(skill, levels[skill] - modification <= 1 ? 1 : levels[skill] - modification);
        }
    }

    /**
     * Decreases a level to 0.
     *
     * @param skill The skill id.
     * @param modification The modification amount.
     */
    public void decreaseLevelToZero(int skill, int modification) {
        if (levels[skill] > 0) {
            setLevel(skill, levels[skill] - modification <= 0 ? 0 : levels[skill] - modification);
        }
    }
}
