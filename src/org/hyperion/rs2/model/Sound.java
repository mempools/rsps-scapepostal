package org.hyperion.rs2.model;

public class Sound {

    /**
     * The id of this sound.
     */
    private final int id;

    /**
     * The volume this sound is played at.
     */
    private byte volume = 1;

    /**
     * The delay before this sound is played.
     */
    private int delay = 0;

    /**
     * Sound constructor
     *
     * @param id The sound id
     * @param volume The volume
     * @param delay The delay
     */
    public Sound(int id, byte volume, int delay) {
        this.id = id;
        this.volume = volume;
        this.delay = delay;
    }

    /**
     * Creates a sound
     *
     * @param id The id
     * @param delay The delay
     * @return The {@link Sound}.
     */
    public static Sound create(int id, int delay) {
        return new Sound(id, (byte) 1, delay);
    }

    /**
     * Creates a sound
     *
     * @param id The id
     * @param volume The volume
     * @param delay The delay
     * @return The {@link Sound}.
     */
    public static Sound create(int id, byte volume, int delay) {
        return new Sound(id, volume, delay);
    }

    /**
     * Gets the sound id
     *
     * @return id The sound id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the volume
     *
     * @return volume The volume
     */
    public byte getVolume() {
        return volume;
    }

    /**
     * Gets the delay
     *
     * @return delay The delay
     */
    public int getDelay() {
        return delay;
    }
}
