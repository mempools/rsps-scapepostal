package org.hyperion.rs2.model;

/**
 * Represents a single animation request.
 *
 * @author Graham Edgecombe
 */
public class Animation {

    /**
     * Different animation constants.
     */
    public final static Animation DEFAULT = create(-1);
    public final static Animation YES_EMOTE = create(855);
    public final static Animation NO_EMOTE = create(856);
    public final static Animation THINKING = create(857);
    public final static Animation BOW = create(858);
    public final static Animation ANGRY = create(859);
    public final static Animation CRY = create(860);
    public final static Animation LAUGH = create(861);
    public final static Animation CHEER = create(862);
    public final static Animation WAVE = create(863);
    public final static Animation BECKON = create(864);
    public final static Animation CLAP = create(865);
    public final static Animation DANCE = create(866);
    public final static Animation PANIC = create(2105);
    public final static Animation JIG = create(2106);
    public final static Animation SPIN = create(2107);
    public final static Animation HEADBANG = create(2108);
    public final static Animation JOYJUMP = create(2109);
    public final static Animation RASPBERRY = create(2110);
    public final static Animation YAWN = create(2111);
    public final static Animation SALUTE = create(2112);
    public final static Animation SHRUG = create(2113);
    public final static Animation BLOW_KISS = create(1368);
    public final static Animation GLASS_WALL = create(1128);
    public final static Animation LEAN = create(1129);
    public final static Animation CLIMB_ROPE = create(1130);
    public final static Animation GLASS_BOX = create(1131);
    public final static Animation GOBLIN_BOW = create(2127);
    public final static Animation GOBLIN_DANCE = create(2128);
    public static final Animation DEATH = create(2304);

    /**
     * Dialogue Animations.
     */
    public final static Animation FACE_HAPPY = create(588);
    public final static Animation FACE_CALM_1 = create(589);
    public final static Animation FACE_CALM_2 = create(590);
    public final static Animation FACE_DEFAULT = create(591);
    public final static Animation FACE_EVIL = create(592);
    public final static Animation FACE_EVIL_CONTINUED = create(593);
    public final static Animation FACE_DELIGHTED_EVIL = create(594);
    public final static Animation FACE_ANNOYED = create(595);
    public final static Animation FACE_DISTRESSED = create(596);
    public final static Animation FACE_DISTRESSED_CONTINUED = create(597);
    public final static Animation FACE_ALMOST_CRYING = create(598);
    public final static Animation FACE_BOWS_HEAD_WHILE_SAD = create(599);
    public final static Animation FACE_DRUNK_TO_LEFT = create(600);
    public final static Animation FACE_DRUNK_TO_RIGHT = create(601);
    public final static Animation FACE_DISINTERESTED = create(602);
    public final static Animation FACE_SLEEPY = create(603);
    public final static Animation FACE_PLAIN_EVIL = create(604);
    public final static Animation FACE_LAUGH_1 = create(605);
    public final static Animation FACE_LAUGH_2 = create(606);
    public final static Animation FACE_LAUGH_3 = create(607);
    public final static Animation FACE_LAUGH_4 = create(608);
    public final static Animation FACE_EVIL_LAUGH = create(609);
    public final static Animation FACE_SAD = create(610);
    public final static Animation FACE_MORE_SAD = create(611);
    public final static Animation FACE_ON_ONE_HAND = create(612);
    public final static Animation FACE_NEARLYC_RYING = create(613);
    public final static Animation FACE_ANGER_1 = create(614);
    public final static Animation FACE_ANGER_2 = create(615);
    public final static Animation FACE_ANGER_3 = create(616);
    public final static Animation FACE_ANGER_4 = create(617);

    /**
     * Creates an animation with no delay.
     *
     * @param id The id.
     * @return The new animation object.
     */
    public static Animation create(int id) {
        return create(id, 0);
    }

    /**
     * Creates an animation.
     *
     * @param id The id.
     * @param delay The delay.
     * @return The new animation object.
     */
    public static Animation create(int id, int delay) {
        return new Animation(id, delay);
    }

    /**
     * The id.
     */
    private final int id;
    /**
     * The delay.
     */
    private final int delay;

    /**
     * Creates an animation.
     *
     * @param id The id.
     * @param delay The delay.
     */
    private Animation(int id, int delay) {
        this.id = id;
        this.delay = delay;
    }

    @Override
    public String toString() {
        return "[id=" + id + " delay=" + delay + "]";
    }

    /**
     * Gets the id.
     *
     * @return The id.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the delay.
     *
     * @return The delay.
     */
    public int getDelay() {
        return delay;
    }
}
