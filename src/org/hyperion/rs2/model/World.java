package org.hyperion.rs2.model;

import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.PlayerDetails;
import org.hyperion.rs2.model.definition.NPCDefinition;
import org.hyperion.rs2.net.ChannelStorage;
import org.hyperion.cache.npc.ActorDefinition;
import org.hyperion.cache.obj.ObjectManager;
import org.hyperion.database.ConnectionPool;
import org.hyperion.database.DatabaseConnection;
import org.hyperion.database.mysql.MySQLDatabaseConfiguration;
import org.hyperion.Constants;
import org.hyperion.rs2.GameEngine;
import org.hyperion.rs2.GenericWorldLoader;
import org.hyperion.rs2.WorldLoader;
import org.hyperion.rs2.WorldLoader.LoginResult;
import org.hyperion.rs2.content.menu.MenuManager;
import org.hyperion.rs2.content.mission.MissionManager;
import org.hyperion.event.Event;
import org.hyperion.event.EventManager;
import org.hyperion.event.impl.UpdateEvent;
import org.hyperion.loginserver.LoginServerConnector;
import org.hyperion.loginserver.LoginServerWorldLoader;
import org.hyperion.rs2.model.definition.ItemDefinition;
import org.hyperion.rs2.model.region.RegionManager;
import org.hyperion.rs2.net.PacketBuilder;
import org.hyperion.rs2.net.PacketDecoder;
import org.hyperion.rs2.net.PacketHandler;
import org.hyperion.rs2.net.PacketManager;
import org.hyperion.rs2.task.Task;
import org.hyperion.rs2.task.impl.ChannelLoginTask;
import org.hyperion.rs2.tickable.Tickable;
import org.hyperion.rs2.tickable.TickableManager;
import org.hyperion.rs2.tickable.impl.RestoreSkillTick;
import org.hyperion.rs2.util.EntityList;
import org.hyperion.rs2.util.NameUtils;
import org.hyperion.rs2.util.TextUtils;
import org.hyperion.script.ScriptContext;
import org.hyperion.script.ScriptManager;
import org.hyperion.util.BlockingExecutorService;
import org.hyperion.util.configuration.ConfigurationNode;
import org.hyperion.util.configuration.ConfigurationParser;
import org.hyperion.util.xStream;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import org.hyperion.tickable.impl.CleanupTickable;

/**
 * Holds data global to the game world.
 *
 * @author Graham Edgecombe
 */
public class World {

    /**
     * Logging class.
     */
    private static final Logger logger = Logger.getLogger(World.class.getName());

    /**
     * World instance.
     */
    private static final World world = new World();

    /**
     * Gets the world instance.
     *
     * @return The world instance.
     */
    public static World getWorld() {
        return world;
    }

    /**
     * An executor service which handles background loading tasks.
     */
    private final BlockingExecutorService backgroundLoader = new BlockingExecutorService(Executors.newSingleThreadExecutor());

    /**
     * The game engine.
     */
    private GameEngine engine;

    /**
     * The configuration node.
     */
    private ConfigurationNode mainNode;

    /**
     * The script manager.
     */
    private ScriptManager scriptManager;

    /**
     * The script context.
     */
    private ScriptContext scriptContext;

    /**
     * The event manager.
     */
    private EventManager eventManager;

    /**
     * The tick manager.
     */
    private TickableManager tickManager;

    /**
     * The current loader implementation.
     */
    private WorldLoader loader;

    /**
     * A list of connected players.
     */
    private final EntityList<Player> players = new EntityList<>(Constants.MAX_PLAYERS);

    /**
     * A list of active NPCs.
     */
    private final EntityList<NPC> npcs = new EntityList<>(Constants.MAX_NPCS);

    /**
     * The game object manager.
     */
    private ObjectManager objectManager;

    /**
     * The login server connector.
     */
    private LoginServerConnector connector;
    /**
     * The region manager.
     */
    private final RegionManager regionManager = new RegionManager();

    /**
     * The player storage map.
     */
    private final ChannelStorage<Player> playerStorage = new ChannelStorage<>();

    /**
     * The Game Connection pool.
     */
    private ConnectionPool<? extends DatabaseConnection> gameConnection;

    /**
     * The Forum Connection pool.
     */
    private ConnectionPool<? extends DatabaseConnection> forumConnection;

    /**
     * This world's world Id.
     */
    private int nodeId;

    /**
     * Creates the world.
     */
    public World() {
    }

    /**
     * Begins background loading tasks.
     */
    public void init() {
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                //Runtime.getRuntime().addShutdownHook(new ShutDownHookEvent());
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                objectManager = new ObjectManager();
                objectManager.load();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                ItemDefinition.init();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                NPCDefinition.init();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                ActorDefinition.load();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                xStream.load(world);
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                MissionManager.load();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                MenuManager.load();
                return null;
            }
        });
    }

    /**
     * Gets the login server connector.
     *
     * @return The login server connector.
     */
    public LoginServerConnector getLoginServerConnector() {
        return connector;
    }

    /**
     * Gets the background loader.
     *
     * @return The background loader.
     */
    public BlockingExecutorService getBackgroundLoader() {
        return backgroundLoader;
    }

    /**
     * Gets the region manager.
     *
     * @return The region manager.
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Initializes the world: loading configuration and registering global
     * events.
     *
     * @param engine The engine processing this world's tasks.
     * @throws IOException if an I/O error occurs loading configuration.
     * @throws ClassNotFoundException if a class loaded through reflection was
     * not found.
     * @throws IllegalAccessException if a class could not be accessed.
     * @throws InstantiationException if a class could not be created.
     * @throws IllegalStateException if the world is already initialized.
     */
    public void init(GameEngine engine) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        if (this.engine != null) {
            throw new IllegalStateException("The world has already been initialised.");
        } else {
            this.engine = engine;
            this.eventManager = new EventManager(engine);
            this.tickManager = new TickableManager();
            this.scriptContext = new ScriptContext(this);
            this.scriptManager = new ScriptManager(scriptContext);
            this.registerGlobalEvents();
            this.loadConfiguration();
        }
    }

    /**
     * Loads server configuration.
     *
     * @throws IOException if an I/O error occurs.
     * @throws ClassNotFoundException if a class loaded through reflection was
     * not found.
     * @throws IllegalAccessException if a class could not be accessed.
     * @throws InstantiationException if a class could not be created.
     */
    private void loadConfiguration() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (FileInputStream fis = new FileInputStream(Constants.CONFIGURATION_FILE)) {
            ConfigurationParser configurationParser = new ConfigurationParser(fis);
            mainNode = configurationParser.parse();

            ConfigurationNode serverNode = mainNode.nodeFor("server");

            if (mainNode.has("gameDatabase")) {
                ConfigurationNode settings = mainNode.nodeFor("gameDatabase");
                if (settings.getBoolean("enabled")) {
                    MySQLDatabaseConfiguration config = new MySQLDatabaseConfiguration();
                    config.setHost(settings.getString("host"));
                    config.setPort(Integer.parseInt(settings.getString("port")));
                    config.setDatabase(settings.getString("database"));
                    config.setUsername(settings.getString("username"));
                    config.setPassword(settings.getString("password"));
                    gameConnection = new ConnectionPool<>(config);
                    logger.info("[NOTICE]: Using game database");
                }
            }
            if (mainNode.has("forumDatabase")) {
                ConfigurationNode settings = mainNode.nodeFor("forumDatabase");
                if (settings.getBoolean("enabled")) {
                    MySQLDatabaseConfiguration config = new MySQLDatabaseConfiguration();
                    config.setHost(settings.getString("host"));
                    config.setPort(Integer.parseInt(settings.getString("port")));
                    config.setDatabase(settings.getString("database"));
                    config.setUsername(settings.getString("username"));
                    config.setPassword(settings.getString("password"));
                    forumConnection = new ConnectionPool<>(config);
                    logger.info("[NOTICE]: Using forum database");
                }
            }

            if (serverNode.has("worldLoader")) {
                String worldLoaderClass = serverNode.getString("worldLoader");
                Class<?> classLoader = Class.forName(worldLoaderClass);
                this.loader = (WorldLoader) classLoader.newInstance();
                logger.info("WorldLoader set to : " + worldLoaderClass);
            } else {
                this.loader = new GenericWorldLoader();
                logger.info("WorldLoader set to default");
            }

            ConfigurationNode loginNode = serverNode.nodeFor("loginServer");
            if (loader instanceof LoginServerWorldLoader) {
                String host = loginNode.getString("host");
                connector = new LoginServerConnector(host);
                nodeId = loginNode.getInteger("nodeid");
                connector.connect(loginNode.getString("password"), nodeId);
            }

            if (mainNode.has("packets")) {
                Map<String, Object> packets = mainNode.nodeFor("packets").nodeFor("packet").getChildren();
                for (Map.Entry<String, Object> handler : packets.entrySet()) {
                    /*
                     * The handler
                     */
                    Class<?> handlerClass = Class.forName(((ConfigurationNode) handler.getValue()).getString("handler"));
                    PacketHandler handlerInstance = (PacketHandler) handlerClass.newInstance();
                    /*
                     * The decoder
                     */
                    Class<?> decoderClass = Class.forName(((ConfigurationNode) handler.getValue()).getString("decoder"));
                    PacketDecoder decoderInstance = (PacketDecoder) decoderClass.newInstance();

                    Integer[] idArray = TextUtils.parseIntArray(handler.getKey(), ",");
                    for (int id : idArray) {
                        PacketManager.getPacketManager().bind(handlerInstance, decoderInstance, id);
                    }
                    logger.fine("Bounded " + handlerClass.getName() + " to opcodes : " + Arrays.toString(idArray));
                }
            }
        }
    }

    /**
     * Registers global events such as updating.
     */
    private void registerGlobalEvents() {
        submit(new UpdateEvent());
        submit(new CleanupTickable());
        submit(new RestoreSkillTick());
    }

    /**
     * Submits a new event.
     *
     * @param event The event to submit.
     */
    public void submit(Event event) {
        this.eventManager.submit(event);
    }

    /**
     * Submits a new tickable.
     *
     * @param tickable The tickable to submit.
     */
    public void submit(final Tickable tickable) {
        submit(new Task() {
            @Override
            public void execute(GameEngine context) {
                // DO NOT REMOVE THIS CODE, IT PREVENTS CONCURRENT MODIFICATION
                // PROBLEMS!
                World.this.tickManager.submit(tickable);
            }
        });
    }

    /**
     * Submits a new task.
     *
     * @param task The task to submit.
     */
    public void submit(Task task) {
        this.engine.pushTask(task);
    }

    /**
     * Gets the object map.
     *
     * @return The object map.
     */
    public ObjectManager getObjectMap() {
        return objectManager;
    }

    /**
     * Gets the world loader.
     *
     * @return The world loader.
     */
    public WorldLoader getWorldLoader() {
        return loader;
    }

    /**
     * Gets the game engine.
     *
     * @return The game engine.
     */
    public GameEngine getEngine() {
        return engine;
    }

    /**
     * Gets the configuration node.
     *
     * @return The configuration node.
     */
    public ConfigurationNode getNode() {
        return mainNode;
    }

    /**
     * Gets the ruby script environment
     *
     * @return The ruby script environment
     */
    public ScriptManager getScriptManager() {
        return scriptManager;
    }

    /**
     * Gets the script context
     *
     * @return scriptContext The script context
     */
    public ScriptContext getScriptContext() {
        return scriptContext;
    }

    /**
     * Gets the tick manager.
     *
     * @return The tick manager.
     */
    public TickableManager getTickManager() {
        return tickManager;
    }

    /**
     * Get the channel storage map
     *
     * @return The channel storage map
     */
    public ChannelStorage<Player> getChannelStorage() {
        return playerStorage;
    }

    /**
     * Gets the player list.
     *
     * @return The player list.
     */
    public EntityList<Player> getPlayers() {
        return players;
    }

    /**
     * Gets the NPC list.
     *
     * @return The NPC list.
     */
    public EntityList<NPC> getNPCs() {
        return npcs;
    }

    /**
     * Get the active game connection pool
     *
     * @return The game connection pool
     */
    public ConnectionPool<? extends DatabaseConnection> getGameConnection() {
        return gameConnection;
    }

    /**
     * Get the forum connection pool
     *
     * @return The forum connection pool
     */
    public ConnectionPool<? extends DatabaseConnection> getForumConnection() {
        return forumConnection;
    }

    /**
     * Gets the world Id.
     *
     * @return nodeId The world Id.
     */
    public int getWorldId() {
        return nodeId;
    }

    /**
     * Loads a player's game in the work service.
     *
     * @param pd The player's details.
     */
    public void load(final PlayerDetails pd) {
        engine.submitWork(new Runnable() {
            @Override
            public void run() {
                LoginResult lr = loader.checkLogin(pd);
                int code = lr.getResponseCode();
                if (!NameUtils.isValidName(pd.getName())) {
                    code = 11;
                }
                if (code != LoginResult.RESPONSE_OK) {
                    PacketBuilder bldr = new PacketBuilder();
                    bldr.put((byte) code);
                    if (code == 21) {
                        bldr.put((byte) 30);
                    }
                    pd.getSession().write(bldr.toPacket()).addListener(new ChannelFutureListener() {

                        @Override
                        public void operationComplete(ChannelFuture arg0)
                                throws Exception {
                            arg0.getChannel().close();
                        }
                    });
                } else {
                    playerStorage.set(lr.getPlayer().getSession(), lr.getPlayer());

                    loader.loadPlayer(lr.getPlayer());

                    engine.pushTask(new ChannelLoginTask(lr.getPlayer()));
                }
            }
        });
    }

    /**
     * Registers a new NPC.
     *
     * @param npc The NPC to register.
     */
    public void register(final NPC npc) {
        npcs.add(npc);
    }

    /**
     * Unregisters an old NPC.
     *
     * @param npc The NPC to unregister.
     */
    public void unregister(NPC npc) {
        npcs.remove(npc);
        npc.destroy();
    }

    /**
     * Registers a new player.
     *
     * @param player The player to register.
     */
    public void register(final Player player) {
        int returnCode = 2;
        if (isPlayerOnline(player.getName())) {
            returnCode = 5;
        } else {
            if (!players.add(player)) {
                returnCode = 7;
                logger.warning("Could not register player : " + player
                        + " [world full]");
            }
        }
        final int responseCode = returnCode;
        PacketBuilder bldr = new PacketBuilder();
        bldr.put((byte) returnCode);
        bldr.put((byte) player.getRights().toInteger());
        bldr.put((byte) 0);
        player.getSession().write(bldr.toPacket()).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) {
                if (responseCode != LoginResult.RESPONSE_OK) {
                    player.getSession().close();
                } else {
                    player.getActionSender().sendLogin();
                    getScriptContext().getLoginListener().submit(player);
                }
            }
        });
        if (returnCode == LoginResult.RESPONSE_OK) {
            logger.fine("Registered player : " + player + " [online=" + players.size() + "]");
        }
    }

    /**
     * Un-registers a player, and saves their game.
     *
     * @param player The player to unregister.
     */
    public void unregister(final Player player) {
        if (player.isAi()) {
            player.getActionQueue().cancelQueuedActions();
            player.destroy();
            return;
        }
        getScriptContext().getLogoutListener().submit(player);
        player.getActionQueue().cancelQueuedActions();
        player.destroy();
        player.getSession().close();
        players.remove(player);
        logger.info("Unregistered player : " + player + " [online=" + players.size() + "]");
        engine.submitWork(new Runnable() {
            @Override
            public void run() {
                loader.savePlayer(player);
                if (World.getWorld().getLoginServerConnector() != null) {
                    World.getWorld().getLoginServerConnector().disconnected(player.getName());
                }
            }
        });
    }

    /**
     * Checks if a player is online.
     *
     * @param name The player's name.
     * @return <code>true</code> if they are online, <code>false</code> if not.
     */
    public boolean isPlayerOnline(String name) {
        name = NameUtils.formatName(name);
        for (Player player : players) {
            if (player.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Format all player names into a string for display on the website
     *
     * @return The player name string
     */
    public String getPlayersOnline() {
        StringBuilder resp = new StringBuilder();
        for (Player player : players) {
            resp.append(NameUtils.formatNameForProtocol(player.getName())).append(",");
        }
        resp.append(players.size());
        return resp.toString();
    }

    /**
     * Gets a player by the name
     *
     * @param name The player's name to get
     * @return The player
     */
    public Player getPlayer(String name) {
        name = NameUtils.formatName(name);
        for (Player player : players) {
            if (player.getName().equalsIgnoreCase(name)) {
                return player;
            }
        }
        return null;
    }

    /**
     * Gets a player by the name long
     *
     * @param name The name long to get
     * @return The player.
     */
    public Player getPlayer(long name) {
        for (Player player : players) {
            if (player.getNameAsLong() == name) {
                return player;
            }
        }
        return null;
    }

    /**
     * Handles an exception in any of the pools.
     *
     * @param t The exception.
     */
    public void handleError(Throwable t) {
        logger.severe("An error occurred in an executor service! The server will be halted immediately.");
        t.printStackTrace();
        System.exit(1);
    }
}
