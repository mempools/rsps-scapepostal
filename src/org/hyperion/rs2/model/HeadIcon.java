package org.hyperion.rs2.model;

/**
 * An enumeration containing head icon id's
 *
 * @author Nikki -- with edits
 */
public class HeadIcon {

    /**
     * The prayer icons
     */
    private PrayerIcon prayerIcon;

    /**
     * The skull icons
     */
    private SkullIcon skullIcon;

    /**
     * The hint icons
     */
    private HintIcon hintIcon;

    /**
     * Constructor
     */
    public HeadIcon() {
        this.prayerIcon = PrayerIcon.NONE;
        this.skullIcon = SkullIcon.NONE;
        this.hintIcon = HintIcon.NONE;
    }

    /**
     * Gets the prayer icon
     *
     * @return prayerIcon The prayer icon
     */
    public PrayerIcon getPrayerIcon() {
        return prayerIcon;
    }

    /**
     * Gets the skull icon
     *
     * @return skullIcon The skull icon
     */
    public SkullIcon getSkullIcon() {
        return skullIcon;
    }

    /**
     * Gets the hint icon
     *
     * @return hintIcon The hint icon
     */
    public HintIcon getHintIcon() {
        return hintIcon;
    }

    /**
     * Sets the prayerIcon
     *
     * @param prayerIcon The prayer icon to set
     * @return The {@link PrayerIcon}.
     */
    public PrayerIcon setPrayerIcon(PrayerIcon prayerIcon) {
        return this.prayerIcon = prayerIcon;
    }

    /**
     * Sets the skull icon
     *
     * @param skullIcon The skull icon to set
     * @return The {@link SkullIcon}.
     */
    public SkullIcon setSkullIcon(SkullIcon skullIcon) {
        return this.skullIcon = skullIcon;
    }

    /**
     * Sets the hint icon
     *
     * @param hintIcon The skull icon to set
     * @return The {@link HintIcon}.
     */
    public HintIcon setHintIcon(HintIcon hintIcon) {
        return this.hintIcon = hintIcon;
    }

    /**
     * An enumeration containing prayer icons.
     */
    public enum PrayerIcon {

        NONE(-1),
        /**
         * Prayer head icons
         */
        PROTECT_MELEE(0),
        PROTECT_MISSLES(1),
        PROTECT_MAGIC(2),
        RETRIBUTION(3),
        SMITE(4),
        REDEMPTION(5);
        /**
         * The head icon id
         */
        private final int prayerIconId;

        /**
         * Create a head icon with the specified id
         *
         * @param headIconId The id to set
         */
        private PrayerIcon(int headIconId) {
            this.prayerIconId = headIconId;
        }

        /**
         * Get the head icon's id
         *
         * @return The id associated with the head icon
         */
        public int toInteger() {
            return prayerIconId;
        }
    }

    /**
     * An enumeration containing skull icons.
     */
    public enum SkullIcon {

        NONE(-1),
        SKULL(1);
        /**
         * The skull icon id
         */
        private final int skullIcon;

        /**
         * Create a skull icon with the specified id
         *
         * @param skullIcon The id to set
         */
        private SkullIcon(int skullIcon) {
            this.skullIcon = skullIcon;
        }

        /**
         * Get the head icon's id
         *
         * @return The id associated with the head icon
         */
        public int toInteger() {
            return skullIcon;
        }
    }

    /**
     * An enumeration containing hint icons.
     */
    public enum HintIcon {

        NONE(-1),
        /**
         * Hint
         */
        ARROW(1);
        /**
         * The hint icon id
         */
        private int hintIcon = -1;

        /**
         * Create a head icon with the specified id
         *
         * @param headIconId The id to set
         */
        private HintIcon(int headIconId) {
            this.hintIcon = headIconId;
        }

        /**
         * Get the head icon's id
         *
         * @return The id associated with the head icon
         */
        public int toInteger() {
            return hintIcon;
        }
    }
}
