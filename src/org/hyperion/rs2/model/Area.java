package org.hyperion.rs2.model;

/**
 * Represents a squared area in the game
 *
 * @author Global - 6/26/13 7:33 PM
 */
public class Area {

    /**
     * The area name
     */
    private final String name;
    /**
     * The top left point
     */
    private final Location firstPoint;
    /**
     * The bottom right point
     */
    private final Location secondPoint;

    /**
     * Creates the area
     *
     * @param name The name of the area
     * @param firstPoint The first point
     * @param secondPoint The second point
     */
    public Area(String name, Location firstPoint, Location secondPoint) {
        this.name = name;
        this.firstPoint = firstPoint;
        this.secondPoint = secondPoint;
    }

    /**
     * Creates the area
     *
     * @param firstPoint The first point
     * @param secondPoint The second point
     */
    public Area(Location firstPoint, Location secondPoint) {
        this.name = null;
        this.firstPoint = firstPoint;
        this.secondPoint = secondPoint;
    }

    /**
     * Creates an Area.
     *
     * @param name The name of the area
     * @param firstPoint The first point
     * @param secondPoint The second point
     * @return The {@link Area}.
     */
    public Area create(String name, Location firstPoint, Location secondPoint) {
        return new Area(name, firstPoint, secondPoint);
    }

    /**
     * Gets the area name
     *
     * @return The area name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the first point
     *
     * @return The first point
     */
    public Location getFirstPoint() {
        return firstPoint;
    }

    /**
     * Gets the second point
     *
     * @return The second point
     */
    public Location getSecondPoint() {
        return secondPoint;
    }

    /**
     * Checks if the target is within the <code>Area</code>.
     *
     * @param target The target to check
     * @return If the target is in the area
     */
    public boolean contains(Location target) {
        return target.getX() >= secondPoint.getX() && target.getY() >= secondPoint.getY()
                && target.getX() <= firstPoint.getX() && target.getY() <= firstPoint.getY();
    }
}
