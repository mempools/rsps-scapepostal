package org.hyperion.rs2.model.region;

import org.hyperion.rs2.model.Location;

public class MapTile {

    /**
     * The flag constants.
     */
    public static final int TILE_FLAGS_CLIPPING_BIT = 1;
    public static final int TILE_FLAGS_BRIDGE_BIT = 2;
    public static final int TILE_FLAGS_ROOF_REMOVAL_BIT = 4;
    public static final int TILE_FLAGS_BLACK_MINIMAP_BIT = 8;
    /**
     * The over lay.
     */
    private int overlay = 0;
    /**
     * The under lay.
     */
    private int underlay = 0;
    /**
     * The flag.
     */
    private int flags = 0;
    /**
     * The height.
     */
    private int height = -1;
    /**
     * The location.
     */
    private Location location = null;
    /**
     * The shape.
     */
    private int shape = 0;

    /**
     * Gets the overlay
     *
     * @return overlay The overlay
     */
    public int getOverlay() {
        return overlay;
    }

    /**
     * Gets the underlay
     *
     * @return underlay The underlay
     */
    public int getUnderlay() {
        return underlay;
    }

    /**
     * Gets the flags
     *
     * @return flags The flags
     */
    public int getFlags() {
        return flags;
    }

    /**
     * Gets the shape
     *
     * @return shape The shape
     */
    public int getShape() {
        return shape;
    }

    /**
     * Gets the height
     *
     * @return height The height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Gets the location
     *
     * @return location The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Sets the overlay
     *
     * @param overlay The overlay to set
     */
    public void setOverlay(int overlay) {
        this.overlay = overlay;
    }

    /**
     * Sets the underlay
     *
     * @param underlay The underlay to set
     */
    public void setUnderlay(int underlay) {
        this.underlay = underlay;
    }

    /**
     * Sets the flags
     *
     * @param flags The flags to set
     */
    public void setFlags(int flags) {
        this.flags = flags;
    }

    /**
     * Sets the shape
     *
     * @param shape The shape to set
     */
    public void setShape(int shape) {
        this.shape = shape;
    }

    /**
     * Sets the height
     *
     * @param height The height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Sets the location
     *
     * @param location The location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }
}
