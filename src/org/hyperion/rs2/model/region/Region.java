package org.hyperion.rs2.model.region;

import org.hyperion.rs2.model.GroundItem;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.*;

import java.util.*;

/**
 * Represents a single region.
 *
 * @author Graham Edgecombe
 */
public class Region {

    /**
     * The region coordinates.
     */
    private final RegionCoordinates coordinate;
    /**
     * A list of players in this region.
     */
    private final List<Player> players = new LinkedList<>();
    /**
     * A list of NPCs in this region.
     */
    private final List<NPC> npcs = new LinkedList<>();
    /**
     * A list of Entities in this region.
     */
    private final List<Actor> entities = new LinkedList<>();
    /**
     * A list of objects in this region.
     */
    private final List<GameObject> objects = new LinkedList<>();
    /**
     * A list of items in this region.
     */
    private final List<GroundItem> items = new LinkedList<>();
    /**
     * A list of tiles in this region, only initiated when the region is called
     * upon.
     */
    private Map<Location, Tile> tiles;
    /**
     * A list of map tiles in this region.
     */
    private final Map<Location, MapTile> mapTiles = new HashMap<>();

    /**
     * Creates a region.
     *
     * @param coordinate The coordinate.
     */
    public Region(RegionCoordinates coordinate) {
        this.coordinate = coordinate;
    }

    /**
     * Gets the region coordinates.
     *
     * @return The region coordinates.
     */
    public RegionCoordinates getCoordinates() {
        return coordinate;
    }

    /**
     * Gets the list of players.
     *
     * @return The list of players.
     */
    public Collection<Player> getPlayers() {
        synchronized (this) {
            return Collections.unmodifiableCollection(new LinkedList<>(players));
        }
    }

    /**
     * Gets the list of NPCs.
     *
     * @return The list of NPCs.
     */
    public Collection<NPC> getNpcs() {
        synchronized (this) {
            return Collections.unmodifiableCollection(new LinkedList<>(npcs));
        }
    }

    /**
     * Gets the list of entities.
     *
     * @return The list of entities.
     */
    public Collection<Actor> getEntities() {
        synchronized (this) {
            return Collections.unmodifiableCollection(new LinkedList<>(entities));
        }
    }

    /**
     * Gets the list of objects.
     *
     * @return The list of objects.
     */
    public Collection<GameObject> getGameObjects() {
        return objects;
    }

    /**
     * Gets the list of GroundItems in this region.
     *
     * @return The list of GroundItems in this region.
     */
    public Collection<GroundItem> getGroundItems() {
        synchronized (this) {
            return Collections.unmodifiableCollection(new LinkedList<>(items));
        }
    }

    /**
     * Gets the tiles
     *
     * @return tiles The tiles
     */
    public Map<Location, Tile> getTiles() {
        return tiles;
    }

    /**
     * Gets the map tiles
     *
     * @return mapTiles The map tiles
     */
    public Map<Location, MapTile> getMapTiles() {
        return mapTiles;
    }

    /**
     * Adds a new player.
     *
     * @param player The player to add.
     */
    public void addPlayer(Player player) {
        synchronized (this) {
            players.add(player);
        }
    }

    /**
     * Removes an old player.
     *
     * @param player The player to remove.
     */
    public void removePlayer(Player player) {
        synchronized (this) {
            players.remove(player);
        }
    }

    /**
     * Adds a new NPC.
     *
     * @param npc The NPC to add.
     */
    public void addNpc(NPC npc) {
        synchronized (this) {
            npcs.add(npc);
        }
    }

    /**
     * Removes an old NPC.
     *
     * @param npc The NPC to remove.
     */
    public void removeNpc(NPC npc) {
        synchronized (this) {
            npcs.remove(npc);
        }
    }

    /**
     * Adds a new GroundItem.
     *
     * @param item The GroundItem to add.
     */
    public void addItem(GroundItem item) {
        synchronized (this) {
            items.add(item);
        }
    }

    /**
     * Removes an old GroundItem.
     *
     * @param item The GroundItem to remove.
     */
    public void removeItem(GroundItem item) {
        synchronized (this) {
            items.remove(item);
        }
    }

    /**
     * Adds an object
     *
     * @param object The object to add
     */
    public void addObject(GameObject object) {
        synchronized (this) {
            objects.add(object);
        }
    }

    /**
     * Removes an object
     *
     * @param object The object to remove
     */
    public void removeObject(GameObject object) {
        synchronized (this) {
            objects.remove(object);
        }
    }

    /**
     * Gets a tile
     *
     * @param location The location of the tile.
     * @return The tile.
     */
    public MapTile getMapTile(Location location) {
        if (mapTiles.isEmpty()) {
            World.getWorld().getObjectMap().loadArea(
                    (coordinate.getX() * RegionManager.REGION_SIZE) / 64,
                    (coordinate.getY() * RegionManager.REGION_SIZE) / 64);
        }
        return mapTiles.get(location);
    }

    /**
     * Sets a tile.
     *
     * @param tile The tile.
     * @param location The location.
     */
    public void setTile(Tile tile, Location location) {
        if (tiles == null) {
            tiles = new HashMap<>();
            populateTiles(location.getZ());
        }
        tiles.put(location, tile);
    }

    /**
     * Sets a tile.
     *
     * @param tile The tile.
     * @param location The location.
     */
    public void setMapTile(MapTile tile, Location location) {
        mapTiles.put(location, tile);
    }

    /**
     * Gets a tile
     *
     * @param location The location of the tile.
     * @return The tile.
     */
    public Tile getTile(Location location) {
        if (tiles == null) {
            tiles = new HashMap<>();
            populateTiles(location.getZ());
        }
        return tiles.get(location) == null ? Tile.SOLID_TILE : tiles.get(location);
    }

    /**
     * Populates the tile map on a specified plane.
     *
     * @param plane The plane.
     */
    public void populateTiles(int plane) {
        if (mapTiles.size() == 0) {
            World.getWorld().getObjectMap().loadArea(
                    (coordinate.getX() * RegionManager.REGION_SIZE) / 64,
                    (coordinate.getY() * RegionManager.REGION_SIZE) / 64);
        }
        for (MapTile mt : getMapTiles().values()) {
            if (mt.getLocation().getZ() == plane && (mt.getFlags() & MapTile.TILE_FLAGS_CLIPPING_BIT) != 0) {
                if ((getMapTile(Location.create(mt.getLocation().getX(), mt.getLocation().getY(), 1)).getFlags() & MapTile.TILE_FLAGS_BRIDGE_BIT) != 0) {
                    if (mt.getLocation().getZ() != 3 && (getMapTile(mt.getLocation().transform(0, 0, 1)).getFlags() & MapTile.TILE_FLAGS_CLIPPING_BIT) != 0) {
                        setTile(Tile.SOLID_TILE, mt.getLocation());
                    } else {
                        setTile(Tile.EMPTY_TILE, mt.getLocation());
                    }
                } else {
                    setTile(Tile.SOLID_TILE, mt.getLocation());
                }

            } else {
                setTile(Tile.EMPTY_TILE, mt.getLocation());
            }
        }
        for (GameObject obj : objects) {
            if ((!obj.getDefinition().isSolid()) || obj.getDefinition().isWalkable()) {
                continue;
            }
            Location loc = obj.getLocation();
            if (loc.getZ() != plane) {
                continue;
            }

            int sizeX = obj.getDefinition().getWidth();
            int sizeY = obj.getDefinition().getHeight();
            // position in the tile map
            if (obj.getRotation() == 1 || obj.getRotation() == 3) {
                // switch sizes if rotated
                int temp = sizeX;
                sizeX = sizeY;
                sizeY = temp;
            }

            if (obj.getType() >= 0 && obj.getType() <= 3) {
                // walls

                //int finalRotation = (obj.getType() + obj.getRotation()) % 4;
                int finalRotation = obj.getRotation();
                // finalRotation - 0 = west, 1 = north, 2 = east, 3 = south
                Tile t = getTile(obj.getLocation());
                int flags = t.getTraversalMask();
                // clear flags
                if (finalRotation == 0) {
                    flags &= ~Tile.WEST_TRAVERSAL_PERMITTED;
                } else if (finalRotation == 1) {
                    flags &= ~Tile.NORTH_TRAVERSAL_PERMITTED;
                } else if (finalRotation == 2) {
                    flags &= ~Tile.EAST_TRAVERSAL_PERMITTED;
                } else {
                    flags &= ~Tile.SOUTH_TRAVERSAL_PERMITTED;
                }
                if (flags != t.getTraversalMask()) {
                    setTile(new Tile(flags), obj.getLocation());
                }
            } else if (obj.getType() == 9 || obj.getType() == 10 || obj.getType() == 11) {
                // world objects

                for (int offX = 0; offX < sizeX; offX++) {
                    for (int offY = 0; offY < sizeY; offY++) {
                        int x = offX + obj.getLocation().getX();
                        int y = offY + obj.getLocation().getY();
                        setTile(Tile.SOLID_TILE, Location.create(x, y, plane));
                    }
                }
            } else if (obj.getType() == 22) {
                // floor decoration
                if (obj.getDefinition().hasActions()) {
                    setTile(Tile.SOLID_TILE, obj.getLocation());
                }
            } else {
                // 4-8 are wall decorations and 12-21 are roofs
                // we can ignore those
            }
        }
    }

    /**
     * Gets the regions surrounding a location.
     *
     * @return The regions surrounding the location.
     */
    public Region[] getSurroundingRegions() {
        Region[] surrounding = new Region[9];
        surrounding[0] = this;
        surrounding[1] = World.getWorld().getRegionManager().getRegion(this.getCoordinates().getX() - 1, this.getCoordinates().getY() - 1);
        surrounding[2] = World.getWorld().getRegionManager().getRegion(this.getCoordinates().getX() + 1, this.getCoordinates().getY() + 1);
        surrounding[3] = World.getWorld().getRegionManager().getRegion(this.getCoordinates().getX() - 1, this.getCoordinates().getY());
        surrounding[4] = World.getWorld().getRegionManager().getRegion(this.getCoordinates().getX(), this.getCoordinates().getY() - 1);
        surrounding[5] = World.getWorld().getRegionManager().getRegion(this.getCoordinates().getX() + 1, this.getCoordinates().getY());
        surrounding[6] = World.getWorld().getRegionManager().getRegion(this.getCoordinates().getX(), this.getCoordinates().getY() + 1);
        surrounding[7] = World.getWorld().getRegionManager().getRegion(this.getCoordinates().getX() - 1, this.getCoordinates().getY() + 1);
        surrounding[8] = World.getWorld().getRegionManager().getRegion(this.getCoordinates().getX() + 1, this.getCoordinates().getY() - 1);
        return surrounding;
    }
}
