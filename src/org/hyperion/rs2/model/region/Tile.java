package org.hyperion.rs2.model.region;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hyperion.rs2.model.GameObject;
import org.hyperion.rs2.model.GroundItem;

/**
 * Represents a tile.
 *
 * @author Graham Edgecombe
 */
public class Tile {

    /**
     * Tile direction constants.
     */
    public static int NORTH = 6, WEST = 3, EAST = 4, SOUTH = 5, CENTER = 6;

    /**
     * Constant values used by the bitmask.
     */
    public static final int NORTH_TRAVERSAL_PERMITTED = 1,
            EAST_TRAVERSAL_PERMITTED = 2,
            SOUTH_TRAVERSAL_PERMITTED = 4,
            WEST_TRAVERSAL_PERMITTED = 8;

    /**
     * A tile in which traversal in no directions is permitted.
     */
    public static final Tile SOLID_TILE = new Tile(0);

    /**
     * A tile in which traversal in any direction is permitted.
     */
    public static final Tile EMPTY_TILE = new Tile(NORTH_TRAVERSAL_PERMITTED
            | EAST_TRAVERSAL_PERMITTED
            | SOUTH_TRAVERSAL_PERMITTED
            | WEST_TRAVERSAL_PERMITTED);

    /**
     * A bitmask which determines which directions can be traversed.
     */
    private final int traversalMask;
    /**
     * A list of items in this region.
     */
    private final List<GroundItem> items = new ArrayList<>();
    /**
     * The game object on this tile.
     */
    private final GameObject[] gameObjects = new GameObject[23];

    /**
     * Creates the tile with a traversal mask.
     *
     * @param traversalMask The traversal mask to set.
     */
    public Tile(int traversalMask) {
        this.traversalMask = traversalMask;
    }

    /**
     * Gets the traversal bitmask.
     *
     * @return The traversal bitmask.
     */
    public int getTraversalMask() {
        return traversalMask;
    }

    /**
     * Checks if northern traversal is permitted.
     *
     * @return True if so, false if not.
     */
    public boolean isNorthernTraversalPermitted() {
        return (traversalMask & NORTH_TRAVERSAL_PERMITTED) > 0;
    }

    /**
     * Checks if eastern traversal is permitted.
     *
     * @return True if so, false if not.
     */
    public boolean isEasternTraversalPermitted() {
        return (traversalMask & EAST_TRAVERSAL_PERMITTED) > 0;
    }

    /**
     * Checks if southern traversal is permitted.
     *
     * @return True if so, false if not.
     */
    public boolean isSouthernTraversalPermitted() {
        return (traversalMask & SOUTH_TRAVERSAL_PERMITTED) > 0;
    }

    /**
     * Checks if western traversal is permitted.
     *
     * @return True if so, false if not.
     */
    public boolean isWesternTraversalPermitted() {
        return (traversalMask & WEST_TRAVERSAL_PERMITTED) > 0;
    }

    /**
     * Gets the list of items.
     *
     * @return The list of items.
     */
    public Collection<GroundItem> getGroundItems() {
        return items;
    }

    /**
     * Gets the game object by type
     *
     * @param type The game object type
     * @return the GameObject
     */
    public GameObject getGameObject(int type) {
        return gameObjects[type];
    }

    /**
     * Sets the game object
     *
     * @param type The type
     * @param gameObject the GameObject
     */
    public void setGameObject(int type, GameObject gameObject) {
        gameObjects[type] = gameObject;
    }
}
