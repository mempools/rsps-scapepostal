package org.hyperion.rs2.action;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.tickable.Tickable;

/**
 * An <code>Event</code> used for handling game actions.
 *
 * @author blakeman8192
 * @author Graham Edgecombe
 */
public abstract class Action extends Tickable {

    /**
     * A queue policy determines when the clients should queue up actions.
     *
     * @author Graham Edgecombe
     */
    public enum QueuePolicy {

        /**
         * This indicates actions will always be queued.
         */
        ALWAYS,
        /**
         * This indicates actions will never be queued.
         */
        NEVER,
        /**
         * This indicates actions will be cleared, then queued.
         */
        FORCE
    }

    /**
     * A queue policy determines whether the action can occur while walking.
     *
     * @author Graham Edgecombe
     * @author Brett Russell
     */
    public enum WalkablePolicy {

        /**
         * This indicates actions may occur while walking.
         */
        WALKABLE,
        /**
         * This indicates actions cannot occur while walking.
         */
        NON_WALKABLE,
        /**
         * This indicates actions can continue while following.
         */
        FOLLOW
    }

    /**
     * The <code>Actor</code> associated with this ActionEvent.
     */
    private final Actor actor;

    /**
     * Creates a new ActionEvent.
     *
     * @param actor The actor.
     * @param delay The initial delay.
     */
    public Action(Actor actor, int delay) {
        super(delay);
        this.actor = actor;
    }

    /**
     * Gets the entity.
     *
     * @return The entity.
     */
    public Actor getActor() {
        return actor;
    }

    /**
     * Gets the queue policy of this action.
     *
     * @return The queue policy of this action.
     */
    public abstract QueuePolicy getQueuePolicy();

    /**
     * Gets the WalkablePolicy of this action.
     *
     * @return The walkable policy of this action.
     */
    public abstract WalkablePolicy getWalkablePolicy();

    @Override
    public void stop() {
        super.stop();
        actor.getActionQueue().processNextAction();
    }
}
