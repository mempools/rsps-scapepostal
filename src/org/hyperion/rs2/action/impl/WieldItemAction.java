package org.hyperion.rs2.action.impl;

import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.container.Equipment;
import org.hyperion.rs2.model.container.Equipment.EquipmentType;
import org.hyperion.rs2.model.definition.EquipmentDefinition.Skill;

public class WieldItemAction extends Action {

    /**
     * The item's id.
     */
    private final int id;

    /**
     * The item's slot.
     */
    private final int slot;

    /**
     * The wield item action
     *
     * @param player The player
     * @param id The item Id
     * @param slot The slot
     * @param ticks The ticks
     */
    public WieldItemAction(Actor player, int id, int slot, int ticks) {
        super(player, ticks);
        this.id = id;
        this.slot = slot;
    }

    @Override
    public QueuePolicy getQueuePolicy() {
        return QueuePolicy.ALWAYS;
    }

    @Override
    public WalkablePolicy getWalkablePolicy() {
        return WalkablePolicy.WALKABLE;
    }

    @Override
    public void execute() {
        this.stop();
        Item item = getActor().getInventory() != null ? getActor().getInventory().get(slot) : null;
        if (item == null || item.getId() != id) {
            return;
        }
//        if (!getActor().canAnimate()) {
//            System.out.println("THIS");
//            return;
//        }
        if (item.getEquipmentDefinition() == null || item.getEquipmentDefinition().getType() == null) {
            if (getActor().getActionSender() != null) {
                getActor().getActionSender().sendMessage("You can't wear that.");
            }
            return;
        }
        EquipmentType type = Equipment.getType(item);

        if (item.getEquipmentDefinition() != null && item.getEquipmentDefinition().getSkillRequirements() != null) {
            for (Skill skill : item.getEquipmentDefinition().getSkillRequirements().keySet()) {
                if (getActor().getSkills().getLevelForExperience(skill.getId()) < item.getEquipmentDefinition().getSkillRequirement(skill.getId())) {
                    if (getActor().getActionSender() != null) {
                        String level = "a ";
                        if (Skills.SKILL_NAME[skill.getId()].toLowerCase().startsWith("a")) {
                            level = "an ";
                        }
                        level += Skills.SKILL_NAME[skill.getId()].toLowerCase();
                        getActor().getActionSender().sendMessage("You need to have " + level + " level of " + item.getEquipmentDefinition().getSkillRequirement(skill.getId()) + ".");
                    }
                    return;
                }
            }
        }
        long itemCount = item.getCount();
        long equipCount = getActor().getEquipment().getCount(item.getId());
        long totalCount = (itemCount + equipCount);
        if (totalCount > Integer.MAX_VALUE) {
            getActor().getActionSender().sendMessage("Not enough equipment space.");
            return;
        }
        boolean inventoryFiringEvents = getActor().getInventory().isFiringEvents();
        getActor().getInventory().setFiringEvents(false);
        try {
            if (type.getSlot() == Equipment.SLOT_WEAPON || type.getSlot() == Equipment.SLOT_SHIELD) {
                if (type == EquipmentType.WEAPON_2H) {
                    if (getActor().getEquipment().get(Equipment.SLOT_WEAPON) != null && getActor().getEquipment().get(Equipment.SLOT_SHIELD) != null) {
                        if (getActor().getInventory().freeSlots() < 1) {
                            if (getActor().getActionSender() != null) {
                                getActor().getActionSender().sendMessage("Not enough space in your inventory.");
                            }
                            return;
                        }
                        getActor().getInventory().remove(item, slot);
                        if (getActor().getEquipment().get(Equipment.SLOT_WEAPON) != null) {
                            getActor().getInventory().add(getActor().getEquipment().get(Equipment.SLOT_WEAPON), slot);
                            getActor().getEquipment().set(Equipment.SLOT_WEAPON, null);
                        }
                        if (getActor().getEquipment().get(Equipment.SLOT_SHIELD) != null) {
                            getActor().getInventory().add(getActor().getEquipment().get(Equipment.SLOT_SHIELD), slot - 1);
                            getActor().getEquipment().set(Equipment.SLOT_SHIELD, null);
                        }
                        getActor().getEquipment().set(type.getSlot(), item);
                        if (getActor().getActionSender() != null) {
                            getActor().getSkills().sendBonuses();
                        }
                        getActor().getInventory().fireItemsChanged();
                        return;
                    } else if (getActor().getEquipment().get(Equipment.SLOT_SHIELD) != null && getActor().getEquipment().get(Equipment.SLOT_WEAPON) == null) {
                        getActor().getInventory().remove(item, slot);
                        getActor().getEquipment().set(Equipment.SLOT_WEAPON, item);
                        getActor().getInventory().add(getActor().getEquipment().get(Equipment.SLOT_SHIELD), slot);
                        getActor().getEquipment().set(Equipment.SLOT_SHIELD, null);
                        if (getActor().getActionSender() != null) {
                            getActor().getSkills().sendBonuses();
                        }
                        getActor().getInventory().fireItemsChanged();
                        return;
                    }
                }
                if (type.getSlot() == Equipment.SLOT_SHIELD && getActor().getEquipment().get(Equipment.SLOT_WEAPON) != null
                        && getActor().getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition() != null
                        && getActor().getEquipment().get(Equipment.SLOT_WEAPON).getEquipmentDefinition().getType() == EquipmentType.WEAPON_2H) {
                    getActor().getInventory().remove(item, slot);
                    getActor().getInventory().add(getActor().getEquipment().get(Equipment.SLOT_WEAPON), slot);
                    getActor().getEquipment().set(Equipment.SLOT_WEAPON, null);
                    getActor().getEquipment().set(Equipment.SLOT_SHIELD, item);
                    if (getActor().getActionSender() != null) {
                        getActor().getSkills().sendBonuses();
                    }
                    getActor().getInventory().fireItemsChanged();
                    return;
                }
            }
            getActor().getInventory().remove(item, slot);
            if (getActor().getEquipment().get(type.getSlot()) != null) {
                if (getActor().getEquipment().get(type.getSlot()).getId() == item.getId()
                        && item.getDefinition().isStackable()) {
                    item = new Item(item.getId(), getActor().getEquipment().get(type.getSlot()).getCount() + item.getCount());
                } else {
                    if (getActor().getEquipment().get(type.getSlot()).getEquipmentDefinition() != null) {
                        for (int i = 0; i < getActor().getEquipment().get(type.getSlot()).getEquipmentDefinition().getBonuses().length; i++) {
                            getActor().getSkills().setBonus(i, getActor().getSkills().getBonus(i) - getActor().getEquipment().get(type.getSlot()).getEquipmentDefinition().getBonus(i));
                        }
                    }
                    getActor().getInventory().add(getActor().getEquipment().get(type.getSlot()), slot);
                }
            }
            getActor().getEquipment().set(type.getSlot(), item);
            if (item.getEquipmentDefinition() != null) {
                for (int i = 0; i < item.getEquipmentDefinition().getBonuses().length; i++) {
                    getActor().getSkills().setBonus(i, getActor().getSkills().getBonus(i) + item.getEquipmentDefinition().getBonus(i));
                }
            }
            if (getActor().getActionSender() != null) {
                getActor().getSkills().sendBonuses();
            }
            getActor().getInventory().fireItemsChanged();
        } finally {
            getActor().getInventory().setFiringEvents(inventoryFiringEvents);
        }
    }
}
