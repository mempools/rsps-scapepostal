package org.hyperion.rs2.action.impl;

import org.hyperion.rs2.action.impl.MiningAction.Node;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.definition.ItemDefinition;

public class ProspectingAction extends InspectAction {

    /**
     * The node type.
     */
    private final Node node;
    /**
     * The delay.
     */
    private static final int DELAY = 5;//3000

    /**
     * Constructor.
     *
     * @param character The character
     * @param location The location
     * @param node The node
     */
    public ProspectingAction(Actor character, Location location, Node node) {
        super(character, location);
        this.node = node;
    }

    @Override
    public int getInspectDelay() {
        return DELAY;
    }

    @Override
    public void init() {
        final Actor character = getActor();
        character.getActionSender().sendMessage("You examine the rock for ores...");
    }

    @Override
    public void giveRewards(Actor character) {
        character.getActionSender().sendMessage("This rock contains "
                + ItemDefinition.forId(node.getOreId()).getName().toLowerCase().replaceAll("ore", "").trim() + ".");
    }
}
