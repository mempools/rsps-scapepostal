package org.hyperion.rs2.action.impl;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.model.*;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.definition.ItemDefinition;

/**
 * <p>
 * A harvesting action is a resource-gathering action, which includes, but is
 * not limited to, woodcutting and mining.</p>
 * <p/>
 * <p>
 * This class implements code related to all harvesting-type skills, such as
 * dealing with the action itself, looping, expiring the object (i.e. changing
 * rocks to the gray rock and trees to the stump), checking requirements and
 * giving out the harvested resources.</p>
 * <p/>
 * <p>
 * The individual woodcutting and mining classes implement things specific to
 * these individual skills such as random events.</p>
 *
 * @author Graham Edgecombe
 */
public abstract class HarvestingAction extends Action {

    /**
     * The location.
     */
    private final Location location;

    /**
     * Creates the harvesting action for the specified entity.
     *
     * @param character The entity to create the action for.
     * @param location The location
     */
    public HarvestingAction(Actor character, Location location) {
        super(character, 0);
        this.location = location;
    }

    @Override
    public QueuePolicy getQueuePolicy() {
        return QueuePolicy.NEVER;
    }

    @Override
    public WalkablePolicy getWalkablePolicy() {
        return WalkablePolicy.NON_WALKABLE;
    }

    /**
     * Called when the action is initialized.
     */
    public abstract void init();

    /**
     * Gets the harvest delay.
     *
     * @return The delay between consecutive harvests.
     */
    public abstract int getHarvestDelay();

    /**
     * Gets the number of cycles.
     *
     * @return The number of cycles.
     */
    public abstract int getCycles();

    /**
     * Gets the success factor.
     *
     * @return The success factor.
     */
    public abstract double getFactor();

    /**
     * Gets the harvested item.
     *
     * @return The harvested item.
     */
    public abstract Item getHarvestedItem();

    /**
     * Gets the experience.
     *
     * @return The experience.
     */
    public abstract double getExperience();

    /**
     * Gets the skill.
     *
     * @return The skill.
     */
    public abstract int getSkill();

    /**
     * Gets the animation.
     *
     * @return The animation.
     */
    public abstract Animation getAnimation();

    /**
     * Gets reward type.
     *
     * @return <code>true/false</code> Whether items are rewarded periodically
     * during the action.
     */
    public abstract boolean getPeriodicRewards();

    /**
     * The total number of cycles.
     */
    private int totalCycles;
    /**
     * The number of remaining cycles.
     */
    private int cycles;

    /**
     * Grants the entity his or her reward.
     *
     * @param character The entity object.
     * @param reward The item reward object.
     */
    private void giveRewards(Actor character, Item reward) {
        character.getInventory().add(reward);
        ItemDefinition def = reward.getDefinition();
        character.getActionSender().sendMessage("You get some " + def.getName() + ".");
        character.getSkills().addExperience(getSkill(), getExperience());
    }

    @Override
    public void execute() {
        final Actor character = (Player) getActor();
        if (this.getTickDelay() == 0) {
            this.setTickDelay(getHarvestDelay());
            init();
            if (this.isRunning()) {
                character.playAnimation(getAnimation());
                character.face(location);
            }
            this.cycles = getCycles();
            this.totalCycles = cycles;
        } else {
            cycles--;
            Item item = getHarvestedItem();
            if (character.getInventory().hasRoomFor(item)) {
                if (totalCycles == 1 || Math.random() > getFactor()) {
                    if (getPeriodicRewards()) {
                        giveRewards(character, item);
                    }
                }
            } else {
                stop();
                character.getActionSender().sendMessage("There is not enough space in your inventory.");
                return;
            }
            if (cycles == 0) {
                // TODO replace with expired object!
                if (!getPeriodicRewards()) {
                    giveRewards(character, item);
                }
                stop();
            } else {
                character.playAnimation(getAnimation());
                character.face(location);
            }
        }
    }
}
