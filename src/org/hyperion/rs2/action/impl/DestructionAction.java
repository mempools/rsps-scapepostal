package org.hyperion.rs2.action.impl;

import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.model.Actor;

/**
 * <p>
 * A destruction action is one where an item is used and lost forever, such as
 * bone burying (for the prayer skill).</p>
 * <p/>
 * <p>
 * The destruction action class handles functionality common to all
 * destruction-type skills.</p>
 * <p/>
 * <p>
 * The individual skills handle specific functionality.</p>
 *
 * @author Graham Edgecombe
 */
public abstract class DestructionAction extends Action {

    /**
     * Creates the destruction action for the specified entity.
     *
     * @param character The entity to create the action for.
     */
    public DestructionAction(Actor character) {
        super(character, 0);
    }

    @Override
    public QueuePolicy getQueuePolicy() {
        return QueuePolicy.NEVER;
    }

    @Override
    public WalkablePolicy getWalkablePolicy() {
        return WalkablePolicy.WALKABLE;
    }

    /**
     * Gets the destruction delay.
     *
     * @return The delay between consecutive destructions.
     */
    public abstract int getDestructionDelay();

    /**
     * Rewards the entity
     *
     * @param character The specified entity.
     */
    public abstract void giveRewards(Actor character);

    /**
     * Called when the action is initialized.
     */
    public abstract void init();

    /**
     * Executes the Action.
     */
    @Override
    public void execute() {
        if (this.getTickDelay() == 0) {
            this.setTickDelay(getDestructionDelay());
            init();
        } else {
            giveRewards(getActor());
            stop();
        }
    }
}
