package org.hyperion.rs2.action.impl;

import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.combat.CombatAction;
import org.hyperion.rs2.model.combat.impl.Magic;

/**
 * Handles an action for an attacking an entity.
 *
 * @author Brett
 */
public class AttackAction extends Action {

    /**
     * The victim of this attack action.
     */
    private final Actor victim;

    /**
     * Constructor method for this action.
     *
     * @param character The attacker.
     * @param victim The attacked.
     */
    public AttackAction(Actor character, Actor victim) {
        super(character, 0);
        this.victim = victim;
    }

    @Override
    public QueuePolicy getQueuePolicy() {
        return QueuePolicy.FORCE;
    }

    @Override
    public WalkablePolicy getWalkablePolicy() {
        return WalkablePolicy.FOLLOW;
    }

    @Override
    public void execute() {
        final Actor character = getActor();
        if (victim.isDead() || victim == null) {
            this.stop();
            return;
        }

        final CombatAction action = character.getActiveCombatAction();
        if (!action.canAttack(character, victim)
                || action == Magic.getAction() && character.getCombatSession().getCurrentSpell() == null) {
            this.stop();
            return;
        }

        if (character.isNPC()) {
            if (!character.getLocation().isWithinDistance(character, victim, character.getActiveCombatAction().distance(character) + 5)) {
                character.resetInteractingActor();
                this.stop();
                return;
            }
            //TODO: add clipped following
        }

        int requiredDistance = character.getActiveCombatAction().distance(character);
        int movementDistance = 0;

        if (character.getSprites().getPrimarySprite() != -1) {
            movementDistance = 3;
        } else if (character.getSprites().getSecondarySprite() != -1) {
            movementDistance = 1;
        }

        int distance = character.getLocation().distanceToEntity(character, victim);
        if (distance > requiredDistance + movementDistance) {
            return;
        }

        if (distance <= requiredDistance) { //only reset walking queue when they are exactly in distance, and not still moving
            character.getWalkingQueue().reset();
        }
        if (victim != null) {
            init(character);
        } else {
            this.stop();
        }
    }

    /**
     * Initiates the attack.
     */
    private void init(Actor attacker) {
        final CombatAction action = attacker.getActiveCombatAction();
        if (victim.isAi()) {
            this.stop();
            return;
        }
        action.start(attacker, victim);
    }
}
