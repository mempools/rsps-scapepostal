package org.hyperion.rs2.action.impl;

import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Location;

public class DistanceAction extends Action {

    /**
     * The action.
     */
    private final Action action;
    /**
     * The required location.
     */
    private final Location location;
    /**
     * The distance required to be near the location.
     */
    private final int distance;

    /**
     * Creates the action.
     *
     * @param character The entity.
     * @param location The required location.
     * @param distance The distance
     * @param action The action.
     */
    public DistanceAction(Actor character, Location location, int distance, Action action) {
        super(character, 0);
        this.distance = distance;
        this.action = action;
        this.location = location;
    }

    @Override
    public QueuePolicy getQueuePolicy() {
        return QueuePolicy.FORCE;
    }

    @Override
    public WalkablePolicy getWalkablePolicy() {
        return WalkablePolicy.WALKABLE;
    }

    @Override
    public void execute() {
        if (getActor().getLocation().isWithinInteractionDistance(location, distance)) {
            getActor().getActionQueue().addAction(action);
            this.stop();
        } else {
            this.setTickDelay(1);
        }
    }
}
