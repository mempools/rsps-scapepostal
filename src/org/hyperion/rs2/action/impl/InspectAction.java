package org.hyperion.rs2.action.impl;

import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Location;

public abstract class InspectAction extends Action {

    /**
     * The location.
     */
    private final Location location;

    /**
     * Constructor.
     *
     * @param character The character
     * @param location Tee location
     */
    public InspectAction(Actor character, Location location) {
        super(character, 0);
        this.location = location;
    }

    @Override
    public QueuePolicy getQueuePolicy() {
        return QueuePolicy.NEVER;
    }

    @Override
    public WalkablePolicy getWalkablePolicy() {
        return WalkablePolicy.NON_WALKABLE;
    }

    /**
     * Initialization method.
     */
    public abstract void init();

    /**
     * Inspection time consumption.
     */
    public abstract int getInspectDelay();

    /**
     * Rewards to give the entity.
     *
     * @param character
     */
    public abstract void giveRewards(Actor character);

    @Override
    public void execute() {
        final Actor character = getActor();
        if (this.getTickDelay() == 0) {
            this.setTickDelay(getInspectDelay());
            init();
            if (this.isRunning()) {
                character.face(location);
            }
        } else {
            giveRewards(character);
            stop();
        }
    }
}
