package org.hyperion.rs2.action.impl;

import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.model.GroundItem;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.player.Player;

/**
 * Executed when the player clicks on a GroundItem. Responsible for walking to
 * and picking it up.
 *
 * @author Bloodraider
 */
public final class PickupItemAction extends Action {

    /**
     * The target ground item.
     */
    private final GroundItem item;

    /**
     * Creates the pick up item action
     *
     * @param player The player
     * @param item The item
     */
    public PickupItemAction(Player player, GroundItem item) {
        super(player, 0);
        this.item = item;
    }

    @Override
    public QueuePolicy getQueuePolicy() {
        return QueuePolicy.NEVER;
    }

    @Override
    public WalkablePolicy getWalkablePolicy() {
        return WalkablePolicy.WALKABLE;
    }

    @Override
    public void execute() {
        Player player = (Player) getActor();
        Location playerLocation = player.getLocation();
        Location groundLocation = item.getLocation();

        if (!item.isAvailable()) {
            player.getWalkingQueue().reset();
            this.stop();
        } else if (playerLocation.getX() == groundLocation.getX()
                && playerLocation.getY() == groundLocation.getY()) {
            if (item.isAvailable()) {
                if (player.getInventory().hasRoomFor(item.getItem())) {
                    player.getInventory().add(item.getItem());
                    item.remove();
                } else {
                    player.getActionSender().sendMessage("You do not have enough room for that!");
                }
            }
            this.stop();
        }
    }
}
