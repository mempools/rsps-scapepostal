package org.hyperion.rs2.action.impl;

import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.content.dialogue.Dialogue;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.player.Player;

/**
 * <p>
 * A dialogue action is when a {@link Player} interacts with an {@link NPC}.
 * </p>
 *
 * @author Global <http://rune-server.org/members/global>
 */
public class DialogueAction extends Action {

    /**
     * The player.
     */
    private final Player player;

    /**
     * The NPC.
     */
    private final NPC npc;

    /**
     * The dialogue.
     */
    private final Dialogue dialogue;

    /**
     * The dialogue action constructor
     *
     * @param source The player
     * @param npc The NPC
     * @param dialogue The dialogue
     */
    public DialogueAction(Player source, NPC npc, Dialogue dialogue) {
        super(source, 0);
        this.player = source;
        this.npc = npc;
        this.dialogue = dialogue;
    }

    @Override
    public QueuePolicy getQueuePolicy() {
        return QueuePolicy.ALWAYS;
    }

    @Override
    public WalkablePolicy getWalkablePolicy() {
        return WalkablePolicy.NON_WALKABLE;
    }

    @Override
    public void execute() {
        if (player.isDead() || npc.isDead() || dialogue == null) {
            this.stop();
            return;
        }
        player.setInteractingActor(npc);
        player.getInterfaceState().setDialogue(dialogue);
        player.getInterfaceState().getDialogue().openDialogue(player, 1);
        this.stop();
    }

}
