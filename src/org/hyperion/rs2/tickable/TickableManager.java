package org.hyperion.rs2.tickable;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * A class that manages <code>Tickable</code>s for a specific
 * <code>GameEngine</code>.
 *
 * @author Michael Bull
 */
public class TickableManager {

    /**
     * The list of tickables.
     */
    private final List<Tickable> tickables = new LinkedList<>();

    /**
     * The paused tickables.
     */
    private final Map<Tickable, Thread> pausedTickables = new HashMap<>();

    /**
     * @return The tickables.
     */
    public List<Tickable> getTickables() {
        return tickables;
    }

    /**
     * Submits a new tickable to the <code>GameEngine</code>.
     *
     * @param tickable The tickable to submit.
     */
    public void submit(final Tickable tickable) {
        tickables.add(tickable);
    }
}
