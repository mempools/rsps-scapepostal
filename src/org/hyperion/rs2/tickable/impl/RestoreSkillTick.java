package org.hyperion.rs2.tickable.impl;

import org.hyperion.rs2.model.World;
import org.hyperion.rs2.task.impl.RestoreSkillTask;
import org.hyperion.rs2.tickable.Tickable;

public class RestoreSkillTick extends Tickable {

    /**
     * The tick.
     */
    private final static int TICK = 200;

    public RestoreSkillTick() {
        super(TICK);
    }

    @Override
    public void execute() {
        World.getWorld().submit(new RestoreSkillTask());
    }
}
