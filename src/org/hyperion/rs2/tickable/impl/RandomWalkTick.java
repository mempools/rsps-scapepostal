package org.hyperion.rs2.tickable.impl;

import java.util.Random;
import org.hyperion.rs2.model.Area;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.region.TileMap;
import org.hyperion.rs2.model.region.TileMapBuilder;
import org.hyperion.rs2.pf.DumbPathFinder;
import org.hyperion.rs2.pf.Path;
import org.hyperion.rs2.pf.PathFinder;
import org.hyperion.rs2.pf.Point;
import org.hyperion.rs2.tickable.Tickable;

public class RandomWalkTick extends Tickable {

    /**
     * The character.
     */
    private final Actor character;

    /**
     * The random.
     */
    private final Random random = new Random();

    /**
     * Creates the random walk tick
     *
     * @param character The character
     * @param ticks The ticks
     */
    public RandomWalkTick(Actor character, int ticks) {
        super(ticks);
        this.character = character;
    }

    @Override
    public void execute() {
        if (character.isNPC()) {
            NPC npc = (NPC) character;
            Area area = npc.getSpawnData().getWalkArea();
            if (npc.canMove()) {
                if (!npc.isInteracting() && !npc.isDead() && npc.canMove() && random.nextInt(3) == 1) {
                    int x = npc.getLocation().getX() + random.nextInt(2) - random.nextInt(2);
                    int y = npc.getLocation().getY() + random.nextInt(2) - random.nextInt(2);
                    boolean canWalk = true;
                    if (npc.getWidth() > 1 || npc.getHeight() > 1) {
                        /**
                         * Checks of any of the NPCs tiles are outside of the
                         * designated area.
                         */
                        for (int offX = 0; offX < npc.getWidth(); offX++) {
                            for (int offY = 0; offY < npc.getHeight(); offY++) {
                                int offsetX = x + offX;
                                int offsetY = y + offX;

                                if (offsetX < area.getFirstPoint().getX() && offsetX > area.getFirstPoint().getX()
                                        && offsetY < area.getSecondPoint().getY() && offsetY > area.getSecondPoint().getY()) {
                                    canWalk = false;
                                }
                            }
                        }
                    }
//                    if (canWalk && x >= area.getFirstPoint().getX() && x <= area.getSecondPoint().getX()
//                            && y >= area.getFirstPoint().getY() && y <= area.getSecondPoint().getY()) {
////                        npc.getWalkingQueue().addStep(x, y);
//                        npc.getWalkingQueue().findDumbPath(x, y);
////                        npc.getWalkingQueue().finish();
//                    }
//                    
                    if (canWalk && area.contains(Location.create(x, y))) {
                        npc.getWalkingQueue().findDumbPath(x, y);
                    }
                }
            }
        } else {
            /**
             * Character is a player (or A.I.), do a path find
             */
        }
    }
}
