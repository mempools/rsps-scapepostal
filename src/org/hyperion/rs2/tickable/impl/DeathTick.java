package org.hyperion.rs2.tickable.impl;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.Attributes;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.npc.NPCSpawnManager;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.region.Region;
import org.hyperion.rs2.packet.context.DeathContext.DeathStage;
import org.hyperion.rs2.tickable.Tickable;
import org.hyperion.script.listener.DeathEventListener;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class DeathTick extends Tickable {

    /**
     * The death event listener.
     */
    private final DeathEventListener listener = World.getWorld().getScriptContext().getDeathEventListener();

    /**
     * The victim.
     */
    private Actor victim = null;

    /**
     * The killer.
     */
    private Actor killer = null;

    /**
     * Creates the death event
     *
     * @param victim The victim
     */
    public DeathTick(Actor victim) {
        super(0);
        this.victim = victim;
        this.killer = (Actor) victim.getAttributes().get(Attributes.KILLED_BY);
    }

    @Override
    public void execute() {
        victim.setLastLocation(victim.getLocation());
        World.getWorld().submit(new Tickable(6) {
            @Override
            public void execute() {
                if (victim.isPlayer()) {
                    victim.getSkills().setLevel(Skills.HITPOINTS, victim.getSkills().getLevelForExperience(Skills.HITPOINTS));
                    victim.setTeleportTarget(Actor.DEFAULT_LOCATION);
                    victim.dropLoot(killer);
                    listener.notify(victim, DeathStage.ACTION);
                } else if (victim.isNPC()) {
                    final NPC npc = (NPC) victim;
                    victim.dropLoot(killer);
                    listener.notify(npc, DeathStage.ACTION);
                    Region region = World.getWorld().getRegionManager().getRegionByLocation(npc.getLocation());
                    region.removeNpc(npc);
                    World.getWorld().submit(new Tickable(npc.getSpawnData().getRespawnTick()) {

                        @Override
                        public void execute() {
                            NPCSpawnManager.spawn(npc.getSpawnData());
                            this.stop();
                        }
                    });
                    World.getWorld().unregister(npc);
                }

                World.getWorld().submit(new Tickable(1) {
                    @Override
                    public void execute() {
                        for (Player players : World.getWorld().getPlayers()) {
                            if (players.getLocation().isWithinDistance(players.getLocation())) {
                                players.getActionSender().resetAllAnimation();
                            }
                        }
                        this.stop();
                    }
                });
                listener.notify(victim, DeathStage.AFTER);
                victim.setDead(false);
                victim.resetInteractingActor();
                if (killer != null) {
                    victim.getAttributes().remove(Attributes.KILLED_BY);
                    killer.resetInteractingActor();
                    killer.getAttributes().remove(Attributes.KILLED_BY);
                }
                this.stop();
            }
        });
        this.stop();
    }

}
