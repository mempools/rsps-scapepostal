package org.hyperion.rs2.tickable.impl;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.ActorCooldowns.CooldownFlags;
import org.hyperion.rs2.tickable.Tickable;

/**
 * This event handles the expiry of a cooldown.
 *
 * @author Brett Russell
 */
@SuppressWarnings("unused")
public class CooldownTick extends Tickable {

    /**
     * The actor.
     */
    private final Actor character;
    /**
     * The cooldown flag.
     */
    private final CooldownFlags cooldown;

    /**
     * Creates a cool down event for a single CooldownFlag.
     *
     * @param character The entity for whom we are expiring a cool down.
     * @param cooldown The cooldown flag.
     * @param duration The length of the cool down.
     */
    public CooldownTick(Actor character, CooldownFlags cooldown, int duration) {
        super(duration);
        this.character = character;
        this.cooldown = cooldown;
    }

    @Override
    public void execute() {
        character.getEntityCooldowns().set(cooldown, false);
        this.stop();
    }
}
