package org.hyperion.rs2.tickable.impl;

import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.tickable.Tickable;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class RestoreEnergyTick extends Tickable {

    /**
     * The entity
     */
    private final Actor character;

    /**
     * Creates a energy restore tick
     *
     * @param character The entity
     * @param restoreRate The restore rate
     */
    public RestoreEnergyTick(Actor character, int restoreRate) {
        super(restoreRate);
        this.character = character;
    }

    @Override
    public void execute() {
        if ((character.getWalkingQueue().isRunningToggled()
                || character.getWalkingQueue().isRunning())
                && character.getSprites().getSecondarySprite() != -1) {
            this.stop();
        } else if (character.getWalkingQueue().getRunningEnergy() < 100) {
            character.getWalkingQueue().setRunningEnergy(character.getWalkingQueue().getRunningEnergy() + 0.5);
            character.getActionSender().sendEnergy();
        }
        this.stop();
    }
}
