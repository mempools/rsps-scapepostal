package org.hyperion.rs2.content.menu;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.script.listener.event.ActionButtonEvent;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public interface Menu {

    /**
     * Title constants
     */
    public static final int QUEST_TAB_TITLE = 640;
    public static final int MENU_TITLE = 663;

    /**
     * Gets the menu button
     *
     * @return The button
     */
    public int getButton();

    /**
     * Gets the title
     *
     * @return The title
     */
    public String getTitle();

    /**
     * Sends the menu content
     *
     * @param player The player
     */
    public void sendContent(Player player);

    /**
     * Handles the action button
     *
     * @return The button hook
     */
    public ActionButtonEvent getButtonHook();
}
