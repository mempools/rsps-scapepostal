package org.hyperion.rs2.content.menu;

import org.hyperion.rs2.content.menu.impl.DefaultMenu;
import org.hyperion.rs2.content.menu.impl.MissionMenu;
import org.hyperion.rs2.content.menu.impl.OptionMenu;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.script.listener.event.ActionButtonEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.hyperion.rs2.content.menu.impl.InformationMenu;
import org.hyperion.rs2.packet.context.ActionButtonContext;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public class MenuManager {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(MenuManager.class.getName());

    /**
     * Button constants.
     */
    public static final int DEFAULT_MENU = 7332;
    public static final int OPTION_MENU = 7333;

    /**
     * The map holding all menus.
     */
    private static final Map<Integer, Menu> menus = new HashMap<>();

    /**
     * Gets the map of menus.
     *
     * @return menus The menus.
     */
    public static Map<Integer, Menu> getMenus() {
        return menus;
    }

    /**
     * Adds a menu to the map.
     *
     * @param menu The menu to add
     */
    public static void add(Menu menu) {
        menus.put(menu.getButton(), menu);
    }

    /**
     * Gets the menu by page
     *
     * @param button The page of the menu to getMenu
     * @return The menu
     */
    public static Menu getMenu(int button) {
        if (menus.containsKey(button)) {
            return menus.get(button);
        } else {
            return new DefaultMenu();
        }
    }

    /**
     * Loads the menus.
     */
    public static void load() {
        add(new DefaultMenu());//page 0
        add(new OptionMenu());//page 1
        add(new MissionMenu());//page 2 ...etc
        add(new InformationMenu());

        //Add the global button hook!
        World.getWorld().getScriptContext().getActionButtonListener().add(getButtonHook());
        //Now add all the other menu button hooks!
        for (Menu menu : menus.values()) {
            World.getWorld().getScriptContext().getActionButtonListener().add(menu.getButtonHook());
        }
        logger.info("Loaded " + menus.size() + " Menus");
    }

    /**
     * Updates the menu.
     *
     * @param player The player to handle
     */
    public static void updateMenu(Player player) {
        if (player.getMenu() != null) {
            player.openMenu(player.getMenu());
            player.getMenu().sendContent(player);
        }
    }

    /**
     * Handles the menu
     *
     * @return The game hook
     */
    private static ActionButtonEvent getButtonHook() {
        return new ActionButtonEvent() {
            @Override
            public void handleButton(Player player, ActionButtonContext context) {
                int button = context.getButton();
                /**
                 * The main menu button will always supersede all the others.
                 */
                if (button == DEFAULT_MENU || button == OPTION_MENU) {
                    Menu menu = MenuManager.getMenu(button);
                    if (menu != null) {
                        player.openMenu(menu);
                    }
                }
            }
        };
    }
}
