package org.hyperion.rs2.content.menu.impl;

import org.hyperion.Constants;
import org.hyperion.rs2.WorldConstants;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.packet.context.ActionButtonContext;
import org.hyperion.script.listener.event.ActionButtonEvent;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class InformationMenu extends AbstractMenu {

    @Override
    public int getButton() {
        return 7383;
    }

    @Override
    public String getTitle() {
        return "@gre@*@red@ Information/Tips";
    }

    @Override
    public void sendContent(Player player) {
        super.sendContent(player);
        addLine(true, "@gre@* @red@About@gre@ *@red@");
        addLine("@red@This page is for providing");
        addLine("@red@information and tips.", " ");
        addLine(true, "@gre@* @red@Commands@gre@ *@red@");
        addLine("Commands can be used");
        addLine("with quotes. e.g.: @whi@::mute");
        addLine("@whi@\"player\" \"long reason\"");
        addLine("HELLO I AM TESTING THE WRAPPING FUNCTION. IT IS WORKING");
        player.getActionSender().sendMenuString(WorldConstants.FREE_QUESTS, 5, getDescription());

    }

    @Override
    public ActionButtonEvent getButtonHook() {
        return new ActionButtonEvent() {
            @Override
            public void handleButton(Player player, ActionButtonContext button) {
                if (player.getInterfaceState().isInMenu(InformationMenu.this)) {
                    player.getActionSender().sendMessage("ur in the menu!!");
                }
            }
        };
    }

}
