package org.hyperion.rs2.content.menu.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.hyperion.rs2.content.menu.Menu;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.util.TextUtils;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public abstract class AbstractMenu implements Menu {

    /**
     * The menu description.
     */
    private final List<String> description = new ArrayList<>();

    @Override
    public void sendContent(Player player) {
        player.getActionSender().sendCleanQuestTab(0);
        player.getActionSender().sendString(Menu.MENU_TITLE, getTitle());
        player.getActionSender().sendString(7332, "Main Menu");
        player.getActionSender().sendString(7333, "Menus");
        //clear the menu description
        description.clear();
    }

    /**
     * Adds a description line
     *
     * @param ignoreWrap Weather to wrap the text or not
     * @param desc The line to add a description.
     */
    public void addLine(boolean ignoreWrap, String... desc) {
        for (String line : desc) {
            String[] lines = TextUtils.wrapText(ignoreWrap, line, 30);
            description.addAll(Arrays.asList(lines));
        }
    }

    /**
     * Adds a description line & wraps
     *
     * @param desc The description line
     */
    public void addLine(String... desc) {
        for (String line : desc) {
            String[] lines = TextUtils.wrapText(false, line, 30);
            description.addAll(Arrays.asList(lines));
        }
    }

    /**
     * Gets the description lines.
     *
     * @return The menu lines
     */
    public String[] getDescription() {
        String[] array = new String[description.size()];
        description.toArray(array);
        return array;
    }

}
