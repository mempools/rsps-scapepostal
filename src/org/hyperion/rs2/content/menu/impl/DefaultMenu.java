package org.hyperion.rs2.content.menu.impl;

import org.hyperion.Constants;
import org.hyperion.rs2.WorldConstants;
import org.hyperion.rs2.content.gang.Gang;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.packet.context.ActionButtonContext;
import org.hyperion.script.listener.event.ActionButtonEvent;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public class DefaultMenu extends AbstractMenu {

    @Override
    public int getButton() {
        return 7332;
    }

    @Override
    public String getTitle() {
        return "Default Menu";
    }

    @Override
    public void sendContent(Player player) {
        super.sendContent(player);
        Gang gang = player.getGang();
        addLine("Welcome to " + WorldConstants.SERVER_NAME + "");
        if (gang != null) {
            addLine(true, "     @red@[@gre@GANG@red@]");
            addLine("@red@Gang: @gre@" + gang.getName());
            addLine(true, "@red@Leader: @gre@"
                    + (gang.getLeader().equals(player.getName()) ? "YOU" : gang.getLeader()));
            int tillNextRank = gang.getRank().getNextRank().getRequiredLevel() - gang.getRespectPoints();
            addLine(true, "@red@Respect Points:"
                    + " @gre@" + gang.getRespectPoints() + "@red@(@gre@" + tillNextRank + "@red@)");
            addLine(true, "@red@Rank: @gre@" + gang.getRank().name());
        } else {
            addLine("@red@You aren't in a gang!");
            addLine("@red@You could create one at");
            addLine("@red@Ardgune Castle, or join");
            addLine("@red@a gang (invite only)");
            addLine("@red@-----");
        }
        player.getActionSender().sendMenuString(WorldConstants.FREE_QUESTS, 5, getDescription());
    }

    @Override
    public ActionButtonEvent getButtonHook() {
        return new ActionButtonEvent() {
            @Override
            public void handleButton(Player player, ActionButtonContext context) {
                int button = context.getButton();
                if (player.getInterfaceState().isInMenu(DefaultMenu.this)) {
                    //do buttons
                }
            }
        };
    }
}
