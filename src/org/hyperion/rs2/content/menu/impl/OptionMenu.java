/*
 *  Copyright (c) 2013 Global <http://rune-server.org/members/global/>
 * 
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 *  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 *  AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 *  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 *  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 *  OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 *  PERFORMANCE OF THIS SOFTWARE.
 */
package org.hyperion.rs2.content.menu.impl;

import org.hyperion.rs2.content.menu.Menu;
import org.hyperion.rs2.content.menu.MenuManager;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.packet.context.ActionButtonContext;
import org.hyperion.script.listener.event.ActionButtonEvent;

/**
 * @author Global <http://rune-server.org/members/global/>
 */
public class OptionMenu extends AbstractMenu {

    @Override
    public int getButton() {
        return 7333;
    }

    @Override
    public String getTitle() {
        return "Menus";
    }

    @Override
    public void sendContent(Player player) {
        super.sendContent(player);
        player.getActionSender().sendString(Menu.MENU_TITLE, "Menus");
        for (Menu menu : MenuManager.getMenus().values()) {
            player.getActionSender().sendString(menu.getButton(), menu.getTitle());
        }
    }

    @Override
    public ActionButtonEvent getButtonHook() {
        return new ActionButtonEvent() {
            @Override
            public void handleButton(Player player, ActionButtonContext context) {
                if (player.getInterfaceState().isInMenu(OptionMenu.this)) {
                    int button = context.getButton();
                    Menu menu = MenuManager.getMenu(button);
                    if (menu != null) {
                        if (button == menu.getButton()) {
                            player.openMenu(menu);
                        }
                    }
                }
            }
        };
    }
}
