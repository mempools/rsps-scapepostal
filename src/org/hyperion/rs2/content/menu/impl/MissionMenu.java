package org.hyperion.rs2.content.menu.impl;

import org.hyperion.rs2.content.mission.Mission;
import org.hyperion.rs2.content.mission.MissionManager;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.packet.context.ActionButtonContext;
import org.hyperion.script.listener.event.ActionButtonEvent;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public class MissionMenu extends AbstractMenu {

    @Override
    public int getButton() {
        return 7336;
    }

    @Override
    public String getTitle() {
        return "@gre@*@red@ Missions";
    }

    @Override
    public void sendContent(Player player) {
        super.sendContent(player);
        for (Mission mission : MissionManager.getMissions().values()) {
            String missionName = mission.getName();
            String line = missionName;

            if (player.getMissionStorage().hasStarted(mission)) {
                line = "@yel@" + missionName;
            }
            if (player.getMissionStorage().hasFinished(mission)) {
                line = "@gre@" + missionName;
            }

            player.getActionSender().sendString(mission.getButton(), line);
        }
    }

    @Override
    public ActionButtonEvent getButtonHook() {
        return new ActionButtonEvent() {
            @Override
            public void handleButton(Player player, ActionButtonContext context) {
                if (player.getInterfaceState().isInMenu(MissionMenu.this)) {
                    Mission mission = MissionManager.get(context.getButton());
                    if (mission != null) {
                        int stage = player.getMissionStorage().getMissionStage(mission);
                        mission.showBrief(player, stage);
                    }
                }
            }
        };
    }
}
