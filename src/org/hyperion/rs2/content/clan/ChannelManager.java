package org.hyperion.rs2.content.clan;

import org.hyperion.rs2.model.player.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public class ChannelManager {

    /**
     * The map of channels/rooms.
     */
    private static final Map<String, GameChannel> channels = new HashMap<>();

    /**
     * The default chat rooms.
     */
    static {
        /**
         * The global channel.
         */
        add(new GameChannel(null, "game"));
    }

    /**
     * Adds a channel
     *
     * @param channel The channel to add
     */
    public static void add(GameChannel channel) {
        channels.put(channel.getName(), channel);
    }

    /**
     * Removes a channel by name
     *
     * @param channel The channel to remove
     */
    public static void remove(String channel) {
        channels.remove(channel);
    }

    /**
     * Gets a channel by name
     *
     * @param channel The name of the channel
     * @return The channel
     */
    public static GameChannel get(String channel) {
        return channels.get(channel);
    }

    /**
     * Checks if a channel exists
     *
     * @param channel The channel to chick
     * @return If it exists or not
     */
    public static boolean exists(String channel) {
        return channels.containsKey(channel);
    }

    /**
     * Joins a channel
     *
     * @param player The player that's joining
     * @param name The channel name
     */
    public static void join(Player player, String name) {
        GameChannel channel = ChannelManager.get(name);
        /**
         * If the clan chat exists
         */
        if (ChannelManager.exists(name)) {
            /**
             * If player is already in a clan chat, remove the player when
             * joining a new clan chat
             *
             */
            if (player.getGameChannel() != null) {
                if (player.getGameChannel().getOwner() == player) {
                    player.getGameChannel().destroy();
                } else {
                    player.getGameChannel().remove(player);
                }
            }
            /**
             * Add the player to the clan chat
             */
            channel.add(player);
            /**
             * Set the player's clan chat
             */
            player.setGameChannel(channel);
        } else {
            player.getActionSender().sendMessage("Clan Chat '" + name + "' doesn't exist!");
        }
    }

    /**
     * Sends a channel command with '/'
     *
     * @param player The player
     * @param message The command
     * @return If a command was sent <code>@true</code> Send channel command {
     * <code>@false</code> Don't send the command
     */
    public static boolean sendChannelCommand(Player player, String message) {
        String[] args = message.split(" ");
        final char COMMAND_PREFIX = 'c';
        String command = COMMAND_PREFIX + args[0];
        switch (command) {

            /**
             * Shows command list.
             */
            case "help":
                player.getActionSender().sendMessage(
                        "@blu@****@bla@",
                        "/cjoin [channel name] - joins a channel",
                        "/cleave - leaves the channel",
                        "/ccreate [name] - creates a channel");
                break;
            /**
             * Creates a room.
             */
            case "create":
                if (args.length == 2) {
                    String roomName = args[1];
                    if (roomName.length() >= 4) {
                        GameChannel clan = new GameChannel(player, roomName);
                        if (!ChannelManager.exists(clan.getName())) {
                            ChannelManager.add(clan);
                            ChannelManager.join(player, clan.getName());
                        } else {
                            player.getActionSender().sendMessage("Channel already exists");
                        }
                    } else {
                        player.getActionSender().sendMessage("Channel name has to have 4 or more characters");
                    }
                } else {
                    player.getActionSender().sendMessage("Syntax: /ccreate [name]");
                }
                return true;

            /**
             * Joins a clan chat room.
             */
            case "join":
                if (args.length == 2) {
                    String channel = args[1];
                    if (player.getGameChannel() != ChannelManager.get(channel)) {
                        ChannelManager.join(player, channel);
                    } else {
                        player.getActionSender().sendMessage("You're already in this channel!");
                    }
                } else {
                    player.getActionSender().sendMessage("Syntax: /cjoin [name]");
                }
                return true;
            /**
             * Leaves clan chat room.
             */
            case "leave":
                if (player.getGameChannel() != null) {
                    /**
                     * If the player is the clan chat owner.
                     */
                    if (player.getGameChannel().getOwner() == player) {
                        player.getGameChannel().destroy();
                    } else {
                        player.getGameChannel().remove(player);
                    }
                }
                return true;
        }
        return false;
    }
}
