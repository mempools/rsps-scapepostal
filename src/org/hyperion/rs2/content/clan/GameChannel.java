package org.hyperion.rs2.content.clan;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.Player.Rights;
import org.hyperion.rs2.util.NameUtils;

import java.util.ArrayList;
import java.util.List;
import org.hyperion.rs2.model.GroundItem;
import org.hyperion.rs2.model.Item;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.definition.ItemDefinition;
import org.hyperion.rs2.packet.decoders.CommandDecoder;
import org.hyperion.rs2.util.TextUtils;

/**
 * The game channel is a clan chat system, handles player talking channels.
 *
 * @author global <http://rune-server.org/members/global/>
 */
public class GameChannel {

    /**
     * The owner of the clan.
     */
    private final Player owner;

    /**
     * The name of the clan.
     */
    private final String name;

    /**
     * The people in the clan chat.
     */
    private final List<Player> users = new ArrayList<>();

    /**
     * The loot share flag.
     */
    private boolean isLootSharing;

    /**
     * Creates the clan chat / channel
     *
     * @param owner The owner of the room
     * @param name The name of the room
     */
    public GameChannel(Player owner, String name) {
        this.owner = owner;
        this.name = name;
        this.isLootSharing = false;
    }

    /**
     * Gets the owner of the clan
     *
     * @return owner The owner of the clan
     */
    public Player getOwner() {
        return owner;
    }

    /**
     * Gets the name of the clan
     *
     * @return name The clan name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the users in the clan
     *
     * @return users The users in the clan
     */
    public List<Player> getUsers() {
        return users;
    }

    /**
     * Gets the loot sharing flag
     *
     * @return Weather loot sharing is enabled or not
     */
    public boolean isLootSharing() {
        return isLootSharing;
    }

    /**
     * Adds a player to the clan
     *
     * @param player The player to add
     */
    public void add(Player player) {
        if (!users.contains(player)) {
            player.getActionSender().sendMessage("You're now talking in " + name);
            users.add(player);
            sendClanMessage("<col=red>" + player.getName() + " has joined the channel");
        } else {
            player.getActionSender().sendMessage("[NOTICE]: You're already in this clan!");
        }
    }

    /**
     * Removes a player from the room
     *
     * @param player The player to remove
     */
    public void remove(Player player) {
        if (users.contains(player)) {
            player.getActionSender().sendMessage("You disconnected from " + name);
            users.remove(player);
            player.setGameChannel(null);
            sendClanMessage(player.getName() + " has left the room");
        }
    }

    /**
     * Gets the message format
     *
     * @param player The player
     * @return format The format
     */
    private String getFormat(Player player) {
        if (player.getGang() != null) {
            return player.getGang().getRank().getTitle() + ":";
        } else {
            Rights rights = player.getRights();
            String[] ranks = {"", "<Mod>", "<Admin>"};
            return ranks[rights.toInteger()] + ":";
        }
    }

    /**
     * Sends a command to the clan chat
     *
     * @param player The player
     * @param message The message
     */
    private boolean sendCommands(Player player, String message) {
        String[] args = CommandDecoder.parse(message);
        String command = args[0];
        switch (command) {
            case "help":
                player.getActionSender().sendMessage(
                        "@blu@****@bla@",
                        "/chelp - global channel commands",
                        "/lootshare - enables/disables lootshare",
                        "/kick [player name] - kicks a player from the channel");
                return true;
        }

        if (player == getOwner() || player.getRights().toInteger() > 1) {
            switch (command) {
                case "lootshare":
                    boolean enabled = !isLootSharing;
                    this.isLootSharing = enabled;
                    sendClanMessage("@blu@LOOT SHARING@bla@ has been " + (enabled ? "@gre@Enabled" : "@red@Disabled") + "@bla@.");
                    return true;
                /**
                 * Kick command.
                 */
                case "kick":
                    if (args.length == 2) {
                        Player victim = getUser(args[1]);
                        if (victim == getOwner()) {
                            return false;
                        }
                        if (victim != null) {
                            victim.getActionSender().sendMessage("You've just been kicked off the channel.");
                            sendClanMessage(victim.getName() + " has been kicked from the channel.");
                            remove(victim);
                        } else {
                            player.getActionSender().sendMessage("User doesn't exist");
                        }
                    } else {
                        player.getActionSender().sendMessage("Syntax: ckick [player name]");
                    }
                    return true;
            }
        }
        return false;

    }

    /**
     * Sends a message in the room
     *
     * @param player The player who sent the message
     * @param message The message
     */
    public void sendMessage(Player player, String message) {
        if (!sendCommands(player, message)) {
            for (Player user : getUsers()) {
                user.getActionSender().sendMessage(getFormat(player) + message);
            }
        }
    }

    /**
     * Sends a message to all users w/ no format
     *
     * @param message The message to send
     */
    public void sendClanMessage(String message) {
        for (Player user : getUsers()) {
            user.getActionSender().sendMessage("<@blu@" + name + "@bla@ MESSAGE>: " + message);
        }
    }

    /**
     * Gets a user by their name
     *
     * @param name The name of the user to get
     * @return The player
     */
    public Player getUser(String name) {
        name = NameUtils.formatName(name);
        for (Player user : getUsers()) {
            if (user.getName().equalsIgnoreCase(name)) {
                return user;
            }
        }
        return null;
    }

    /**
     * Handles the loot sharing.
     *
     * @param groundItem The ground item - the loot.
     */
    public void handleLootShare(GroundItem groundItem) {
        for (Player player : users) {
            if (groundItem == null || groundItem.getOwner() == player.getName()
                    || !World.getWorld().isPlayerOnline(player.getName())) {
                return;
            }
            Item item = groundItem.getItem();
            int splitAmount = ItemDefinition.forId(item.getId()).getValue() / users.size();
            String itemName = ItemDefinition.forId(item.getId()).getName();
            if (player.getInventory().hasRoomFor(item)) {
                player.getInventory().add(item);
            } else {
                player.getActionSender().sendMessage("You do not have enough room in your inventory!");
                GroundItem.create(player, item, player.getLocation());
            }
            player.getActionSender().sendMessage("[LOOTSHARE]: You received: " + splitAmount + " "
                    + TextUtils.getIndefiniteArticle(itemName) + ".");
        }
    }

    /**
     * Destroys the room.
     */
    public void destroy() {
        synchronized (this) {
            for (Player user : users) {
                user.getActionSender().sendMessage(name + " has been deleted.");
                user.setGameChannel(null);
            }
        }
        synchronized (users) {
            users.removeAll(getUsers());
            ChannelManager.remove(getName());
        }
    }
}
