package org.hyperion.rs2.content.chat;

public enum FriendStatus {

    /**
     * Offline state
     */
    OFFLINE,
    /**
     * Online status
     */
    ONLINE
}
