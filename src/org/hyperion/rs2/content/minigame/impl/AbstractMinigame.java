package org.hyperion.rs2.content.minigame.impl;

import org.hyperion.rs2.content.minigame.Minigame;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.tickable.Tickable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author SPEEDO <http://rune-server.org/members/Speedo/>
 */
public abstract class AbstractMinigame<E extends Enum<E>> {

    /**
     * The name of the minigame
     */
    private final String name;
    /**
     * The game's tick
     */
    private final int tick;
    /**
     * Started flag
     */
    private boolean started = false;
    /**
     * The list of players
     */
    private final Map<E, ArrayList<Player>> players = new HashMap<>();
    /**
     * The minigame listeners
     */
    private ArrayList<Minigame> minigameListeners = new ArrayList<>();

    /**
     * Initializes the minigame
     *
     * @param game The game to initiate
     */
    public AbstractMinigame(String name) {
        this(name, 1);
    }

    /**
     * Initializes the minigame
     *
     * @param name The minigame name
     * @param tick The tick
     */
    public AbstractMinigame(String name, int tick) {
        this.name = name;
        this.tick = tick;
    }

    /**
     * Adds a minigame to the listener
     *
     * @param minigame The minigame to add
     */
    public void addListener(Minigame minigame) {
        minigameListeners.add(minigame);
    }

    /**
     * Adds a player to the minigame
     *
     * @param team The team
     * @param player The player to add
     */
    public boolean addPlayer(E team, Player player) {
        if (!players.get(team).contains(player)) {
            players.get(team).add(player);
            for (Minigame minigame : minigameListeners) {
                minigame.addPlayer(player);
            }
            return true;
        }
        return false;
    }

    /**
     * Removes a player from a team
     *
     * @param team The team
     * @param player The player to remove
     */
    public boolean removePlayer(E team, Player player) {
        if (players.get(team).contains(player)) {
            players.get(team).remove(player);
            for (Minigame listener : minigameListeners) {
                listener.removePlayer(player);
            }
            return true;
        }
        return false;
    }

    /**
     * Begins the minigame
     */
    public void start() {
        if (!started) {
            World.getWorld().submit(new Tickable(tick) {
                @Override
                public void execute() {
                    tick();
                }
            });
            for (Minigame listener : minigameListeners) {
                listener.start();
            }
            started = true;
        }
    }

    /**
     * The tick
     */
    public abstract void tick();

    /**
     * Gets the minigame's name
     *
     * @return The minigame's name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the tick
     *
     * @return The tick
     */
    public int getTick() {
        return tick;
    }

    /**
     * If the minigame has started
     *
     * @return The started flag
     */
    public boolean hasStarted() {
        return started;
    }

    /**
     * Gets the list of players in the minigame
     *
     * @return The list of active players in the minigame
     */
    public Map<E, ArrayList<Player>> getPlayers() {
        return players;
    }

    /**
     * Gets the list of player in a team
     *
     * @param team The team to check
     * @return The list of players in the team
     */
    public ArrayList<Player> getPlayers(E team) {
        return players.get(team);
    }
}
