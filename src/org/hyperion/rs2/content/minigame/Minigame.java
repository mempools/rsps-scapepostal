package org.hyperion.rs2.content.minigame;

import org.hyperion.rs2.model.player.Player;

/**
 * @author SPEEDO <http://rune-server.org/members/Speedo/>
 */
public interface Minigame {

    /**
     * The start of the minigame
     */
    public void start();

    /**
     * The end of the minigame
     */
    public void end();

    /**
     * When a player is added
     *
     * @param player The player to add
     */
    public void addPlayer(Player player);

    /**
     * When a player is removed
     *
     * @param player The player to remove
     */
    public void removePlayer(Player player);
}
