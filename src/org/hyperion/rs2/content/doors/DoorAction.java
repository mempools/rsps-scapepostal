package org.hyperion.rs2.content.doors;

import org.hyperion.rs2.action.Action;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.definition.GameObjectDefinition;

/**
 * @author Global <http://rune-server.org/members/global/>
 */
public class DoorAction extends Action {

    /**
     * The player
     */
    private final Player player;

    /**
     * The door
     */
    private final int id;

    /**
     * The location
     */
    private final Location location;

    /**
     * Creates the door action
     *
     * @param player The player
     * @param id The door
     * @param location The door location
     */
    public DoorAction(Player player, int id, Location location) {
        super(player, 0);
        this.player = player;
        this.id = id;
        this.location = location;
    }

    @Override
    public QueuePolicy getQueuePolicy() {
        return QueuePolicy.FORCE;
    }

    @Override
    public WalkablePolicy getWalkablePolicy() {
        return WalkablePolicy.NON_WALKABLE;
    }

    @Override
    public void execute() {
        Door.openDoor(id, location);
        this.stop();
    }

    /**
     * Checks if the object is a door
     *
     * @param id The object id
     * @return If the object is a door or not
     */
    public static boolean isDoor(int id) {
        GameObjectDefinition definition = GameObjectDefinition.forId(id);
        return definition != null && (definition.getName().equals("Door") || definition.getName().equals("Large door"));
    }
}
