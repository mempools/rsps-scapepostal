package org.hyperion.rs2.content.doors;

import org.hyperion.rs2.model.GameObject;
import org.hyperion.rs2.model.Location;
import org.hyperion.cache.obj.ObjectManager;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.definition.GameObjectDefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Killamess Basic door manipulation.
 */
public class Door {

    /**
     * The logger
     */
    private static final Logger logger = Logger.getLogger(Door.class.getName());

    /**
     * The list of doors
     */
    private static final List<Door> doors = new ArrayList<>();

    /**
     * Gets the door list
     *
     * @return The door list
     */
    public List<Door> getDoors() {
        return doors;
    }

    /**
     * Parses the door data
     *
     * @param door The door game object
     */
    public static void parse(GameObject door) {
        GameObjectDefinition definition = door.getDefinition();
        if (definition != null && (definition.getName().equals("Door") || definition.getName().equals("Large door"))) {
            doors.add(new Door(definition.getId(),
                    door.getLocation().getX(), door.getLocation().getY(), door.getLocation().getZ(), door.getRotation(),
                    door.getType(), isOpen(definition.getId()), isDoubleDoor(door)));
        }
    }

    /**
     * Checks if the door is already open
     *
     * @param id The id of the door
     * @return If the door is open or not
     */
    private static int isOpen(int id) {
        final int[] openDoors = {
            1504, 1514, 1517, 1520, 1531,
            1534, 2033, 2035, 2037, 2998,
            3271, 4468, 4697, 6101, 6103,
            6105, 6107, 6109, 6111, 6113,
            6115, 6976, 6978, 8696, 8819,
            10261, 10263, 10265, 11708, 11710,
            11712, 11715, 11994, 12445, 13002, 1520, 1517
        };
        for (int openDoor : openDoors) {
            if (openDoor == id) {
                return 1;
            }
        }
        return 0;
    }

    /**
     * Checks if the door is a double door
     *
     * @param object The door object to check
     * @return If the door is a double door
     */
    private static boolean isDoubleDoor(GameObject object) {
        GameObjectDefinition def = object.getDefinition();
        GameObject nextTo = World.getWorld().getRegionManager().getGameObject(object.getLocation(), object.getDefinition().getId());
        /*
         * TODO: Check if the door is next to another door
         */
        return false;
    }

    /**
     * Gets a door by it's id and location
     *
     * @param id The door id
     * @param location The door location
     * @return The door
     */
    public static Door getDoor(int id, Location location) {
        return getDoor(id, location.getX(), location.getY(), location.getZ());
    }

    /**
     * Gets a door by it's location
     *
     * @param id The door id
     * @param x The X coordinate
     * @param y The Y coordinate
     * @param z The Z coordinate
     * @return The door
     */
    public static Door getDoor(int id, int x, int y, int z) {
        for (Door door : doors) {
            if (door.id == id) {
                if (door.doorX == x && door.doorY == y && door.doorZ == z) {
                    return door;
                }
            }
        }
        return null;
    }

    /**
     * Opens a door by it's Id and location
     *
     * @param id The door id
     * @param location The door location
     */
    public static void openDoor(int id, Location location) {
        Door door = getDoor(id, location);

        if (door == null) {
            return;
        }
        if (door.isDouble) {
            if (door.id > 12000) {
                return; //nearly all of these are not opened
            }
            if (door.open == 0) {
                if (door.originalFace == 0) {
                    Door lowerDoor = getDoor(id - 3, door.doorX, door.doorY - 1, door.doorZ);
                    Door upperDoor = getDoor(id + 3, door.doorX, door.doorY + 1, door.doorZ);
                    if (lowerDoor != null) {
                        changeDoor(lowerDoor, false);
                        changeDoor(door, true);
                    } else if (upperDoor != null) {
                        changeDoor(door, false);
                        changeDoor(upperDoor, true);
                    }
                } else if (door.originalFace == 1) {
                    Door westDoor = getDoor(id - 3, door.doorX - 1, door.doorY, door.doorZ);
                    Door eastDoor = getDoor(id + 3, door.doorX + 1, door.doorY, door.doorZ);
                    if (westDoor != null) {
                        changeDoor(westDoor, false);
                        changeDoor(door, true);
                    } else if (eastDoor != null) {
                        changeDoor(door, false);
                        changeDoor(eastDoor, true);
                    }
                } else if (door.originalFace == 2) {
                    Door lowerDoor = getDoor(id - 3, door.doorX, door.doorY + 1, door.doorZ);
                    Door upperDoor = getDoor(id + 3, door.doorX, door.doorY - 1, door.doorZ);
                    if (lowerDoor != null) {
                        changeDoor(lowerDoor, false);
                        changeDoor(door, true);
                    } else if (upperDoor != null) {
                        changeDoor(door, false);
                        changeDoor(upperDoor, true);
                    }
                } else if (door.originalFace == 3) {
                    Door westDoor = getDoor(id + 3, door.doorX - 1, door.doorY, door.doorZ);
                    Door eastDoor = getDoor(id - 3, door.doorX + 1, door.doorY, door.doorZ);
                    if (westDoor != null) {
                        changeDoor(westDoor, false);
                        changeDoor(door, true);
                    } else if (eastDoor != null) {
                        changeDoor(door, false);
                        changeDoor(eastDoor, true);
                    }
                }
            } else if (door.open == 1) {
                if (door.originalFace == 0) {
                    Door westDoor = getDoor(id - 3, door.doorX - 1, door.doorY, door.doorZ);
                    Door upperDoor = getDoor(id + 3, door.doorX + 1, door.doorY, door.doorZ);
                    if (westDoor != null) {
                        changeDoor(westDoor, false);
                        changeDoor(door, true);
                    } else if (upperDoor != null) {
                        changeDoor(door, false);
                        changeDoor(upperDoor, true);
                    }
                } else if (door.originalFace == 1) {
                    Door northDoor = getDoor(id - 3, door.doorX, door.doorY + 1, door.doorZ);
                    Door southDoor = getDoor(id + 3, door.doorX, door.doorY - 1, door.doorZ);
                    if (northDoor != null) {
                        changeDoor(northDoor, false);
                        changeDoor(door, true);
                    } else if (southDoor != null) {
                        changeDoor(door, false);
                        changeDoor(southDoor, true);
                    }
                } else if (door.originalFace == 2) {
                    Door westDoor = getDoor(id - 3, door.doorX - 1, door.doorY, door.doorZ);
                    Door eastDoor = getDoor(id + 3, door.doorX, door.doorY - 1, door.doorZ);
                    if (westDoor != null) {
                        changeDoor(westDoor, false);
                        changeDoor(door, true);
                    } else if (eastDoor != null) {
                        changeDoor(door, false);
                        changeDoor(eastDoor, true);
                    }
                } else if (door.originalFace == 3) {
                    Door northDoor = getDoor(id - 3, door.doorX, door.doorY + 1, door.doorZ);
                    Door southDoor = getDoor(id + 3, door.doorX, door.doorY - 1, door.doorZ);
                    if (northDoor != null) {
                        changeDoor(northDoor, false);
                        changeDoor(door, true);
                    } else if (southDoor != null) {
                        changeDoor(door, false);
                        changeDoor(southDoor, true);
                    }
                }
            }
        } else {
            /*
             * Single doors
             */
            int xAdjustment = 0, yAdjustment = 0;
            if (door.type == 0) {
                if (door.open == 0) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        xAdjustment = -1;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        yAdjustment = 1;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        xAdjustment = 1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        yAdjustment = -1;
                    }
                } else if (door.open == 1) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        yAdjustment = 1;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        xAdjustment = 1;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        yAdjustment = -1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        xAdjustment = -1;
                    }
                }
            } else if (door.type == 9) {
                if (door.open == 0) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        xAdjustment = 1;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        xAdjustment = 1;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        xAdjustment = -1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        xAdjustment = -1;
                    }
                } else if (door.open == 1) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        xAdjustment = 1;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        xAdjustment = 1;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        xAdjustment = -1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        xAdjustment = -1;
                    }
                }
            }
            if (xAdjustment != 0 || yAdjustment != 0) {
                Location loc = Location.create(door.doorX, door.doorY, door.doorZ);
                GameObject obj = new GameObject(GameObjectDefinition.forId(id), loc, door.type, 0);
                ObjectManager.removeGlobalObject(obj);
            }
            if (door.doorX == door.originalX && door.doorY == door.originalY) {
                door.doorX += xAdjustment;
                door.doorY += yAdjustment;
            } else {
                Location loc = Location.create(door.doorX, door.doorY, door.doorZ);
                GameObject obj = new GameObject(GameObjectDefinition.forId(id), loc, door.type, 0);
                ObjectManager.removeGlobalObject(obj);
                door.doorX = door.originalX;
                door.doorY = door.originalY;
            }
            if (door.id == door.originalId) {
                if (door.open == 0) {
                    door.id += 1;
                } else if (door.open == 1) {
                    door.id -= 1;
                }
            } else if (door.id != door.originalId) {
                if (door.open == 0) {
                    door.id -= 1;
                } else if (door.open == 1) {
                    door.id += 1;
                }
            }
            Location loc = Location.create(door.doorX, door.doorY, door.doorZ);
            GameObject obj = new GameObject(GameObjectDefinition.forId(door.id), loc, door.type, getNextFace(door, false));
            ObjectManager.createGlobalObject(obj);
        }
    }

    /**
     * Gets the next door facing direction
     *
     * @param door The door
     * @return The facing direction
     */
    private static int getNextFace(Door door, boolean right) {
        int face = door.originalFace;
        if (door.isDouble) {
            if (right) {
                if (door.open == 0) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        face = 1;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        face = 2;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        face = 3;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        face = 2;
                    } else if (door.originalFace != door.currentFace) {
                        face = door.originalFace;
                    }
                } else if (door.open == 1) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        face = 3;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        face = 0;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        face = 1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        face = 2;
                    } else if (door.originalFace != door.currentFace) {
                        face = door.originalFace;
                    }
                }
            } else {
                if (door.open == 0) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        face = 3;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        face = 0;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        face = 1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        face = 0;
                    } else if (door.originalFace != door.currentFace) {
                        face = door.originalFace;
                    }
                } else if (door.open == 1) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        face = 1;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        face = 2;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        face = 1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        face = 2;
                    } else if (door.originalFace != door.currentFace) {
                        face = door.originalFace;
                    }
                }
            }
        } else {
            if (door.type == 0) {
                if (door.open == 0) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        face = 1;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        face = 2;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        face = 3;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        face = 0;
                    } else if (door.originalFace != door.currentFace) {
                        face = door.originalFace;
                    }
                } else if (door.open == 1) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        face = 3;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        face = 0;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        face = 1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        face = 2;
                    } else if (door.originalFace != door.currentFace) {
                        face = door.originalFace;
                    }
                }
            } else if (door.type == 9) {
                if (door.open == 0) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        face = 3;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        face = 2;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        face = 1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        face = 0;
                    } else if (door.originalFace != door.currentFace) {
                        face = door.originalFace;
                    }
                } else if (door.open == 1) {
                    if (door.originalFace == 0 && door.currentFace == 0) {
                        face = 3;
                    } else if (door.originalFace == 1 && door.currentFace == 1) {
                        face = 0;
                    } else if (door.originalFace == 2 && door.currentFace == 2) {
                        face = 1;
                    } else if (door.originalFace == 3 && door.currentFace == 3) {
                        face = 2;
                    } else if (door.originalFace != door.currentFace) {
                        face = door.originalFace;
                    }
                }
            }
        }
        door.currentFace = face;
        return face;
    }

    /**
     * Changes the door, for double doors
     *
     * @param door The door
     * @param right If the door is on the right
     */
    public static void changeDoor(Door door, boolean right) {
        int xAdjustment = 0, yAdjustment = 0;
        GameObject obj = null;
        if (right) {
            if (door.open == 0) {
                if (door.originalFace == 0 && door.currentFace == 0) {
                    xAdjustment = -1;
                } else if (door.originalFace == 1 && door.currentFace == 1) {
                    yAdjustment = 1;
                } else if (door.originalFace == 2 && door.currentFace == 2) {
                    xAdjustment = +1;
                } else if (door.originalFace == 3 && door.currentFace == 3) {
                    yAdjustment = -1;
                }
            } else if (door.open == 1) {
                if (door.originalFace == 0 && door.currentFace == 0) {
                    xAdjustment = 1;
                } else if (door.originalFace == 1 && door.currentFace == 1) {
                    xAdjustment = -1;
                } else if (door.originalFace == 2 && door.currentFace == 2) {
                    yAdjustment = -1;
                } else if (door.originalFace == 3 && door.currentFace == 3) {
                    xAdjustment = -1;
                }
            }
            if (xAdjustment != 0 || yAdjustment != 0) {
                obj = new GameObject(GameObjectDefinition.forId(door.id), Location.create(door.doorX, door.doorY, door.doorZ), 0, 0);
                ObjectManager.removeGlobalObject(obj);
            }
            if (door.doorX == door.originalX && door.doorY == door.originalY) {
                door.doorX += xAdjustment;
                door.doorY += yAdjustment;
            } else {
                obj = new GameObject(GameObjectDefinition.forId(door.id), Location.create(door.doorX, door.doorY, door.doorZ), 0, 0);
                ObjectManager.removeGlobalObject(obj);
                door.doorX = door.originalX;
                door.doorY = door.originalY;
            }
            if (door.id == door.originalId) {
                if (door.open == 0) {
                    door.id += 1;
                } else if (door.open == 1) {
                    door.id -= 1;
                }
            } else if (door.id != door.originalId) {
                if (door.open == 0) {
                    door.id = door.originalId;
                } else if (door.open == 1) {
                    door.id = door.originalId;
                }
            }
            obj = new GameObject(GameObjectDefinition.forId(door.id), Location.create(door.doorX, door.doorY, door.doorZ), 0, getNextFace(door, true));
        } else {
            if (door.open == 0) {
                if (door.originalFace == 0 && door.currentFace == 0) {
                    xAdjustment = -1;
                } else if (door.originalFace == 1 && door.currentFace == 1) {
                    yAdjustment = 1;
                } else if (door.originalFace == 2 && door.currentFace == 2) {
                    xAdjustment = +1;
                } else if (door.originalFace == 3 && door.currentFace == 3) {
                    yAdjustment = -1;
                }
            } else if (door.open == 1) {
                if (door.originalFace == 0 && door.currentFace == 0) {
                    yAdjustment = -1;
                } else if (door.originalFace == 1 && door.currentFace == 1) {
                    xAdjustment = -1;
                } else if (door.originalFace == 2 && door.currentFace == 2) {
                    xAdjustment = -1;
                } else if (door.originalFace == 3 && door.currentFace == 3) {
                    xAdjustment = -1;
                }
            }
            if (xAdjustment != 0 || yAdjustment != 0) {
                obj = new GameObject(GameObjectDefinition.forId(door.id), Location.create(door.doorX, door.doorY, door.doorZ), 0, 0);
                ObjectManager.removeGlobalObject(obj);
            }
            if (door.doorX == door.originalX && door.doorY == door.originalY) {
                door.doorX += xAdjustment;
                door.doorY += yAdjustment;
            } else {
                obj = new GameObject(GameObjectDefinition.forId(door.id), Location.create(door.doorX, door.doorY, door.doorZ), 0, 0);
                ObjectManager.removeGlobalObject(obj);
                door.doorX = door.originalX;
                door.doorY = door.originalY;
            }
            if (door.id == door.originalId) {
                if (door.open == 0) {
                    door.id += 1;
                } else if (door.open == 1) {
                    door.id -= 1;
                }
            } else if (door.id != door.originalId) {
                if (door.open == 0) {
                    door.id = door.originalId;
                } else if (door.open == 1) {
                    door.id = door.originalId;
                }
            }
            obj = new GameObject(GameObjectDefinition.forId(door.id), Location.create(door.doorX, door.doorY, door.doorZ), 0, getNextFace(door, false));
        }
        ObjectManager.createGlobalObject(obj);
    }

    /**
     * The door id
     */
    private int id;

    /**
     * The door X coordinate
     */
    private int doorX;

    /**
     * The door Y coordinate
     */
    private int doorY;

    /**
     * The door Z coordinate
     */
    private int doorZ;

    /**
     * The original door id
     */
    private int originalId;

    /**
     * The original X coordinate
     */
    private int originalX;

    /**
     * The original Y coordinate
     */
    private int originalY;

    /**
     * The original door face direction
     */
    private int originalFace;

    /**
     * The current door face direction
     */
    private int currentFace;

    /**
     * The type
     */
    private int type;

    /**
     * The open state
     */
    private int open;

    /**
     * If the door is a double door
     */
    private boolean isDouble;

    /**
     * Creates the door
     *
     * @param door The door id
     * @param x The door X coordinate
     * @param y The door Y coordinate
     * @param z The door Z coordinate
     * @param face The door face direction
     * @param type The door type
     * @param open The door open state
     * @param isDouble If double door
     */
    public Door(int door, int x, int y, int z, int face, int type, int open, boolean isDouble) {
        this.id = door;
        this.originalId = door;
        this.doorX = x;
        this.doorY = y;
        this.originalX = x;
        this.originalY = y;
        this.doorZ = z;
        this.originalFace = face;
        this.currentFace = face;
        this.type = type;
        this.open = open;
        this.isDouble = isDouble;
    }

    /**
     * Gets the door id
     *
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the original door id
     *
     * @return The original door id
     */
    public int getOriginalId() {
        return originalId;
    }

    /**
     * Gets the door location
     *
     * @return The door location
     */
    public Location getLocation() {
        return Location.create(doorX, doorY, doorZ);
    }

    /**
     * Gets the original door location
     *
     * @return The original door location
     */
    public Location getOriginalLocation() {
        return Location.create(originalX, originalY);
    }

    /**
     * Gets the original facing direction
     *
     * @return The original facing direction
     */
    public int getOriginalFace() {
        return originalFace;
    }

    /**
     * Gets the current facing direction
     *
     * @return The current facing direction
     */
    public int getCurrentFace() {
        return currentFace;
    }

    /**
     * Gets the type
     *
     * @return The type
     */
    public int getType() {
        return type;
    }

    /**
     * Gets the door state
     *
     * @return The door state
     */
    public boolean isOpen() {
        return open == 1;
    }

    /**
     * If the door is a double door
     *
     * @return If double or not
     */
    public boolean isDouble() {
        return isDouble;
    }
}
