package org.hyperion.rs2.content.journal.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.hyperion.rs2.content.journal.Journal;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.util.TextUtils;

public abstract class AbstractJournal extends Journal {

    /**
     * The abstract journal
     *
     * @param player
     */
    public AbstractJournal(Player player) {
        super(player);
    }

    /**
     * The page description.
     */
    private final List<String> description = new ArrayList<>();

    /**
     * Sets the page description
     *
     * @param page The page
     * @param desc The page description lines
     * @return The description in an array
     */
    public String[] pageDescription(int page, String... desc) {
        for (String line : desc) {
            String[] lines = TextUtils.wrapText(false, line, 24);
            description.addAll(Arrays.asList(lines));
        }
        String[] array = new String[description.size()];
        description.toArray(array);
        return array;
    }

    /**
     * Gets the page description
     *
     * @return The page description
     */
    public List<String> getDescription() {
        return description;
    }

}
