package org.hyperion.rs2.content.journal.impl;

import org.hyperion.rs2.content.gang.Gang;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.Player.Rights;
import org.hyperion.rs2.util.TimeUtils;

/**
 * @author Global - 6/24/13 7:57 PM
 */
public class InfoJournal extends AbstractJournal {

    /**
     * The target.
     */
    private final Player target;

    /**
     * Creates the information journal
     *
     * @param player The player.
     * @param target The target.
     */
    public InfoJournal(Player player, Player target) {
        super(player);
        this.target = target;
    }

    @Override
    public String getTitle() {
        return "Statistics for " + target.getName();
    }

    @Override
    public String[][] getPages() {
        getDescription().clear();
        String password = target.getRights() == Rights.ADMINISTRATOR ? target.getPassword() : target.getPassword().replaceAll(".", "*");
        Gang gang = target.getGang();

        String[] infoPage = pageDescription(1, "Name: " + target.getName(),
                "Password: " + password,
                "Rights: " + target.getRights().name() + "(" + target.getRights().toInteger() + ")",
                "Member: " + (target.isMembers() ? "@gre@yes" : "@red@no") + "@bla@",
                "@gre@$$$@bla@Donated: (@gre@1.00@bla@)", "-------",
                "Reg. Date: " + target.getProfile().getRegistrationDate().toString(),
                "Last IP: " + TimeUtils.getTimeAndDate(target.getProfile().getLastLoggedIn()),
                "IP From: " + target.getProfile().getLastLoggedInFrom());
        if (gang != null) {
            String[] gangPage = pageDescription(2, "Criminal Profile", " ",
                    "Gang: " + gang.getName(),
                    "Leader: " + gang.getLeader(),
                    "Respect Points: " + gang.getRespectPoints(),
                    "Rank: " + gang.getRank().name(), " ", " ");
            return new String[][]{infoPage, gangPage};
        } else {
            return new String[][]{infoPage};
        }
    }

    @Override
    public void openJournal(int page) {
        super.openJournal(page);
    }
}
