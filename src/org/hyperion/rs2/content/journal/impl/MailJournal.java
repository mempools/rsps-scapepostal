package org.hyperion.rs2.content.journal.impl;

import org.hyperion.rs2.model.player.MailMessage;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.Profile;

/**
 * @author Global <http://rune-server.org/members/global>
 */
public class MailJournal extends AbstractJournal {

    /**
     * The player's profile.
     */
    private final Profile profile;

    /**
     * The player's profile.
     */
    /**
     * Creates the mail journal
     *
     * @param player The player.
     */
    public MailJournal(Player player) {
        super(player);
        this.profile = player.getProfile();
    }

    @Override
    public String getTitle() {
        if (!profile.getInbox().isEmpty()) {
            String suffix = profile.getUnreadMessages() == 1 ? "" : "s";
            return profile.getUnreadMessages() == 0
                    ? "No new messages" : "You have @red@" + profile.getUnreadMessages() + "@ora@ unread message" + suffix + "!";
        } else {
            return "No new mail in your inbox.";
        }
    }

    @Override
    public String[][] getPages() {
        getDescription().clear();
        String[] firstPage = {""};
        if (!profile.getInbox().isEmpty()) {
            MailMessage firstMail = profile.getInbox().get(0);
            firstPage = pageDescription(1, "Sender: " + firstMail.getCreator(),
                    "Subject: " + firstMail.getSubject(), "", "Message: " + firstMail.getMessage()
            );
            return new String[][]{firstPage};
        } else {
            firstPage = pageDescription(1, "You have no mail.");
        }

        return new String[][]{firstPage};
    }

}
