package org.hyperion.rs2.content.journal;

import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.player.Player;

/**
 * @author Robgob69
 */
public abstract class Journal {

    /**
     * The player
     */
    private Player player;

    /**
     * The journal
     *
     * @param player The player
     */
    public Journal(Player player) {
        this.player = player;
    }

    /**
     * The current page.
     */
    private int page;

    /**
     * Gets the player
     *
     * @return player The player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets the page number.
     *
     * @return The page.
     */
    public int getPage() {
        return page;
    }

    /**
     * Sets the page.
     *
     * @param page The page to set
     * @return The page
     */
    public int setPage(int page) {
        return this.page = page;
    }

    /**
     * Gets the max page number.
     *
     * @return The max page number.
     */
    public int getMaxPage() {
        return getPages().length - 1;
    }

    /**
     * Gets the title
     *
     * @return The journal title
     */
    public abstract String getTitle();

    /**
     * Gets the page content
     * <p/>
     * String[page][content]
     *
     * @return The pages
     */
    public abstract String[][] getPages();

    /**
     * Opens the journal
     *
     * @param page The page number
     */
    public void openJournal(int page) {
        page = page - 1;
        if (getPages() == null || page == -1) {
            return;
        }
        /**
         * Clean out the journal
         */
        for (int i = 0; i < 22; i++) {
            getPlayer().getActionSender().sendString(843 + i, "");
        }

        setPage(page);
        getPlayer().playAnimation(Animation.create(1350));
        getPlayer().getActionSender().sendString(903, getTitle());
        for (int i = 0; i < getPages()[page].length; i++) {
            getPlayer().getActionSender().sendString(843 + i, getPages()[page][i]);
        }
        getPlayer().getActionSender().sendString(14165, "> " + (getPage() + 1) + " <");
        getPlayer().getActionSender().sendString(14166, "");
        getPlayer().getActionSender().sendInterface(837, false);
    }
}
