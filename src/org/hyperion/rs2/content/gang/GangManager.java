package org.hyperion.rs2.content.gang;

import org.hyperion.rs2.model.player.Player;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public class GangManager {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(GangManager.class.getName());

    /**
     * The map of gangs.
     */
    private static final Map<String, Gang> gangs = new HashMap<>();

    static {
        /**
         * TODO: Add NPC Gangs.
         */
    }

    /**
     * Loads gang data
     *
     * @throws SQLException Error with the query
     */
    public static void load() throws SQLException {
//        Connection sql = game.getConnection();
//        PreparedStatement prep = sql.prepareStatement("SELECT * FROM gang_data");
//        prep.executeQuery();
//
//        ResultSet result = prep.getResultSet();
//        while (result.next()) {
//            Gang gang = new Gang(result.getString("leader"), result.getString("name"));
//            add(gang);
//        }
    }

    /**
     * Gets the map of all gangs
     *
     * @return gangs The gangs
     */
    public static Map<String, Gang> getGangs() {
        return gangs;
    }

    /**
     * Adds a gang
     *
     * @param gang The gang to add
     */
    public static void add(Gang gang) {
        gangs.put(gang.getName(), gang);
    }

    /**
     * Gets a gang by it's name
     *
     * @param gang The gang to get
     * @return gang The gang
     */
    public static Gang get(String gang) {
        if (gang == null) {
            return null;
        }
        return gangs.get(gang);
    }

    /**
     * Removes a gang
     *
     * @param gang The gang to remove
     */
    public static void remove(String gang) {
        gangs.remove(gang);
    }

    /**
     * Creates a gang.
     *
     * @param player The leader
     * @param name The name of the gang
     */
    public static void create(Player player, String name) {
        if (name.length() <= 5) {
            player.getActionSender().sendMessage("The gang name has to have 5 or more characters.");
            return;
        } else if (name.length() >= 16) {
            player.getActionSender().sendMessage(
                    "Your gang name has to be under 16 characters!");
            return;
        }
        /**
         * If the gang exists.
         */
        if (!checkGang(name, player)) {
            Gang gang = new Gang(player.getName(), name);
            /**
             * If player is already in a gang, remove the player when creating a
             * gang.
             */
            if (player.getGang() != null) {
                player.getGang().disband(player);
            }

            player.setGang(gang);
            saveGang(gang);
            add(gang);
            player.getActionSender().sendMessage("You successfully created a gang!");
        }
    }

    /**
     * Saves a gang to the database.
     *
     * @param gang The gang to save
     * @return If the save was successful or not.
     */
    private static boolean saveGang(final Gang gang) {
//        Connection sql = game.getConnection();
//        String set = "`name` = '" + gang.getName() + "'," + "`leader` = '" + gang.getLeader() + "'";
//        try (PreparedStatement prep = sql.prepareStatement("INSERT INTO `gang_data` (`name`, `leader`) VALUES (?, ?) ON DUPLICATE key UPDATE " + set + ";")) {
//            prep.setString(1, gang.getName());
//            prep.setString(2, gang.getLeader());
//
//            prep.executeUpdate();
//            prep.close();
//        } catch (SQLException e) {
//            logger.log(Level.SEVERE, "Error saving gang", e);
//        }
        return true;
    }

    /**
     * Removes a gang from the database.
     *
     * @param gang The gang to remove
     * @return Whether the gang was removed
     */
    public static boolean removeGang(final Gang gang) {
//        Connection sql = game.getConnection();
//        try (PreparedStatement prep = sql.prepareStatement("DELETE FROM gang_data WHERE name = ?")) {
//            prep.setString(1, gang.getName());
//            prep.executeUpdate();
//            remove(gang.getName());
//        } catch (SQLException e) {
//            logger.log(Level.SEVERE, "Error removing gang", e);
//        }
        return true;
    }

    /**
     * Checks if a gang exists.
     *
     * @param gang The gang to check
     * @return <code>true</code> If the gang exists. <code>false</code> if the
     * gang does not exist.
     * @throws SQLException Error with the query
     */
    private static boolean checkGang(final String gang, final Player player) {
//        Connection sql = game.getConnection();
//        try (PreparedStatement prep = sql.prepareStatement("SELECT * FROM gang_data WHERE name = ?")) {
//            prep.setString(1, gang);
//            prep.executeQuery();
//
//            ResultSet result = prep.getResultSet();
//            if (result.next()) {
//                String gangName = result.getString(1);
//                if (gangName.equals(gang)) {
//                    player.getActionSender().sendMessage("That name is already taken!");
//                    return true;
//                }
//            }
//            result.close();
//            prep.close();
//        } catch (SQLException e) {
//            logger.log(Level.SEVERE, "Error checking gang", e);
//        }
        return false;
    }
}
