package org.hyperion.rs2.content.gang.impl;

import org.hyperion.rs2.content.gang.Gang;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.definition.NPCDefinition;

import java.util.ArrayList;
import java.util.List;

/**
 * Im just messing around here! nothing serious! i plan on making NPC Gangs too
 * for missions and other stuff, but this is nothing serious right now, im just
 * messing around~
 */
public class GangThugs extends Gang {

    private static NPC leader = new NPC(NPCDefinition.forId(1), null);
    private List<Actor> members = new ArrayList<>();

    public GangThugs() {
        super(leader.getName(), "Thugs");
        //members.add(leader);
        //forgot the warrior npc id
    }

    @Override
    public String getLeader() {
        return leader.getName();
    }

}
