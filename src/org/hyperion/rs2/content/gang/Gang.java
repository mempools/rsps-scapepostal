package org.hyperion.rs2.content.gang;

import org.hyperion.rs2.model.GroundItem;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.Actor;
import org.hyperion.rs2.model.*;
import org.hyperion.rs2.model.UpdateFlags.UpdateFlag;
import org.hyperion.rs2.model.container.Bank;
import org.hyperion.rs2.model.container.Container;
import org.hyperion.rs2.model.container.Container.Type;
import org.hyperion.rs2.model.definition.ItemDefinition;
import org.hyperion.rs2.util.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author global <http://rune-server.org/members/global/> rofl
 */
public class Gang {

    /**
     * The ranks
     */
    public enum GangRank {

        GOON(1, 0, "[GOON]"),
        GANGSTER(2, 130, "[@gre@Gangster@bla@]"),
        WISE_GUY(3, 260, "[@blu@Wise Guy@bla@]"),
        MADE_MAN(4, 460, "[@red@Made Man@bla@]"),
        DON(5, 700, "[@red@DON@bla@");

        /**
         * The respect bonus
         */
        private final int respectBonus;
        /**
         * The required level till next level
         */
        private final int requiredLevel;
        /**
         * The rank title
         */
        private final String title;

        /**
         * The gang ranks
         *
         * @param respectBonus The respect bonus
         * @param requiredLevel The required respect level
         * @param title The title
         */
        private GangRank(int respectBonus, int requiredLevel, String title) {
            this.respectBonus = respectBonus;
            this.requiredLevel = requiredLevel;
            this.title = title;
        }

        /**
         * Gets the respect bonus
         *
         * @return respectBonus The respect bonus
         */
        public int getRespectBonus() {
            return respectBonus;
        }

        /**
         * Gets the required respect level
         *
         * @return requiredLevel The required respect level
         */
        public int getRequiredLevel() {
            return requiredLevel;
        }

        /**
         * Gets the rank title
         *
         * @return title The rank title
         */
        public String getTitle() {
            return title;
        }

        /**
         * Gets the next rank
         *
         * @return nextRank The next gang rank
         */
        public GangRank getNextRank() {
            switch (this) {
                case GOON:
                    return GANGSTER;
                case GANGSTER:
                    return WISE_GUY;
                case WISE_GUY:
                    return MADE_MAN;
                case MADE_MAN:
                    return DON;
            }
            return null;
        }
    }

    /**
     * The leader of the gang.
     */
    private final String leader;
    /**
     * The name of the gang
     */
    private final String name;
    /**
     * The rank.
     */
    private GangRank rank = null;
    /**
     * The entity's respect points.
     */
    private int respectPoints = 0;
    /**
     * The gang's respect points.
     */
    private int gangRespectPoints = 0;
    /**
     * The gang's bank.
     */
    private final Container bank = new Container(Type.ALWAYS_STACK, Bank.SIZE);
    /**
     * The map of members in the gang.
     */
    private final List<Actor> members = new ArrayList<>();

    /**
     * Creates the gang.
     *
     * @param leader The leader
     * @param name The title
     */
    public Gang(String leader, String name) {
        this.leader = leader;
        this.name = name;
    }

    /**
     * Adds a gang member
     *
     * @param gang The gang
     * @param character The entity to add
     * @return If successfully added the member
     */
    public boolean addMember(Gang gang, Actor character) {
        if (!members.contains(character)) {
            members.add(character);
            return true;
        }
        return false;
    }

    /**
     * Removes a gang member
     *
     * @param gang The gang
     * @param character The entity to remove
     * @return If successfully removed the member
     */
    public boolean removeMember(Gang gang, Actor character) {
        if (members.contains(character)) {
            members.remove(character);
            return true;
        }
        return false;
    }

    /**
     * Gets the leader of the gang
     *
     * @return leader The leader
     */
    public String getLeader() {
        return leader;
    }

    /**
     * Gets the title of the gang
     *
     * @return title The title
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the gang rank
     *
     * @return rank The rank
     */
    public GangRank getRank() {
        return rank;
    }

    /**
     * Gets the amount of respect points
     *
     * @return respectPoints The amount of respect points
     */
    public int getRespectPoints() {
        return respectPoints;
    }

    /**
     * Sets the amount of respect points
     *
     * @param respectPoints The amount to set
     * @return The respect points
     */
    public int setRespectPoints(int respectPoints) {
        return this.respectPoints = respectPoints;
    }

    /**
     * Sets the gang rank
     *
     * @param rank The rank to set
     * @return The gang rank
     */
    public GangRank setRank(GangRank rank) {
        return this.rank = rank;
    }

    /**
     * Gets the list of members
     *
     * @return members The members in the gang
     */
    public List<Actor> getMembers() {
        return members;
    }

    /**
     * Gets the gang's bank.
     *
     * @return bank The gang's bank.
     */
    public Container getBank() {
        return bank;
    }

    /**
     * Gets the total of all gang member's respect points
     *
     * @return The respect points
     */
    public int getGangRespectPoints() {
//        Connection sql = Database.getDatabase("game").getConnection();
//        try (PreparedStatement prep = sql.prepareStatement("SELECT * FROM player WHERE gang = ?")) {
//            prep.setString(1, getName());
//            ResultSet result = prep.executeQuery();
//            while (result.next()) {
//                gangRespectPoints += result.getInt("respectPoints");
//            }
//            return gangRespectPoints;
//        } catch (SQLException ex) {
//            Logger.getLogger(Gang.class.getName()).log(Level.SEVERE, "Error loading gang respect points", ex);
//        }
        return gangRespectPoints;
    }

    /**
     * Increases the respect points
     *
     * @param character The entity
     * @param amount The amount
     * @return If increased points
     */
    public boolean increaseRespect(Actor character, int amount) {
        if (!character.isPlayer()) {
            return false;
        }
        this.respectPoints += amount;
        character.getActionSender().sendMessage("You gained " + amount + " respect points!",
                "You now have " + getRespectPoints() + " respect points.");
        if (getRespectPoints() >= getRank().getNextRank().requiredLevel) {
            setRank(getRank().getNextRank());
        }
        return true;
    }

    /**
     * Decreases the respect points
     *
     * @param character The entity
     * @param amount The amount
     * @return If decreased points
     */
    public boolean decreaseRespct(Actor character, int amount) {
        if (!character.isPlayer()) {
            return false;
        }
        if (this.respectPoints <= 0) {
            this.respectPoints = 0;
        }
        this.respectPoints -= amount;
        return true;
    }

    /**
     * Increases the gang respect points
     *
     * @param amount The amount
     * @return If increased points
     */
    public boolean increaseGangRespect(int amount) {
        this.gangRespectPoints += amount;
        return true;
    }

    /**
     * Decreases the gang respect points
     *
     * @param amount The amount
     * @return If decreased points
     */
    public boolean decreaseGangRespect(int amount) {
        this.gangRespectPoints -= amount;
        return true;
    }

    /**
     * Splits an <code>Item</code> drop, then gives it to all the gang members.
     * TODO: TEST!
     *
     * @param item The item to split
     */
    public void splitDrop(Item item) {
        for (Actor actor : members) {
            if (actor.isPlayer() && World.getWorld().isPlayerOnline(actor.getName())) {
                Player player = (Player) actor;
                int splitAmount = ItemDefinition.forId(item.getId()).getValue() / members.size();
                String itemName = ItemDefinition.forId(item.getId()).getName();
                if (player.getInventory().hasRoomFor(item)) {
                    player.getInventory().add(item);
                } else {
                    player.getActionSender().sendMessage("You do not have enough room in your inventory!");
                    GroundItem.create(player, item, player.getLocation());
                }
                player.getActionSender().sendMessage("[LOOTSHARE]: You received: " + splitAmount + " "
                        + TextUtils.getIndefiniteArticle(itemName) + ".");
            }
        }
    }

    /**
     * Removes a member from the gang
     *
     * @param player The player to disband
     */
    public void disband(Actor player) {
        if (getLeader().equals(player.getName())) {
            synchronized (this) {
                for (Actor character : members) {
                    character.getActionSender().sendMessage("Your gang has been disbanded!");
                    this.respectPoints = 0;
                    this.rank = null;
                    character.setGang(null);
                }
            }
            synchronized (getMembers()) {
                members.clear();
                GangManager.removeGang(this);
            }
        } else {
            /**
             * Removes a gang member.
             */
            members.remove(player);
            setRank(null);
            player.setGang(null);
            player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
        }
    }
}
