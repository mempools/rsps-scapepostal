package org.hyperion.rs2.content.mission.impl;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.packet.context.ActionButtonContext;
import org.hyperion.script.listener.event.ActionButtonEvent;
import org.hyperion.script.listener.event.LoginEvent;
import org.hyperion.script.listener.event.LogoutEvent;

/**
 * @author Global - 6/29/13 2:00 AM
 */
public class HitmanMission extends AbstractMission {

    @Override
    public int getButton() {
        return 7383;
    }

    @Override
    public String getName() {
        return "Contract Killer pt. 1";
    }

    @Override
    public int getFinalStage() {
        return 15;
    }

    @Override
    public void showBrief(Player player, int stage) {
        String[] req = {(player.getSkills().getCombatLevel() >= 30 ? "30 Combat" : "@str@30 Combat")};
        switch (stage) {
            case 0:
                player.getActionSender().sendScrollInterface(getName(), "@red@Requirements@bla@", "",
                        req[0], "", "",
                        "I can start this quest by talking to the @mag@bum@bla@ at @red@Varrock@bla@.");
                break;
            case 1:
                player.getActionSender().sendScrollInterface(getName(), "@red@Requirements@bla@", "",
                        req[0], "", "",
                        "@str@I can start this quest by talking to the @mag@bum@bla@ at @red@Varrock@bla@.",
                        "The @mag@bum@bla@ told me the target was located @red@down the street@bla@");
                break;
        }
        //player.getActionSender().sendScrollInterface(getName(), message);
    }

    @Override
    public boolean getRequirements(Player player) {
        return player.getSkills().getCombatLevel() >= 30;
    }

    @Override
    public void giveRewards(Player player) {
        super.giveRewards(player);
    }

    @Override
    public void start(Player player) {
        super.start(player);
        player.getMissionStorage().setMissionStage(this, 1);
    }

    @Override
    public void end(Player player) {
        super.end(player);
    }

    @Override
    public void quit(Player player) {
        super.quit(player);
    }

    @Override
    public void handleLogin(Player player) {

    }

    @Override
    public void handleLogout(Player player) {

    }

    @Override
    public void handleButtons(Player player, ActionButtonContext context) {
        player.getActionSender().sendMessage("MISSION BUTTON !");
    }

}
