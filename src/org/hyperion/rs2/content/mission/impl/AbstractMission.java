package org.hyperion.rs2.content.mission.impl;

import org.hyperion.rs2.content.menu.impl.MissionMenu;
import org.hyperion.rs2.content.mission.Mission;
import org.hyperion.rs2.content.mission.MissionStorage;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.packet.context.ActionButtonContext;
import org.hyperion.script.ScriptContext;
import org.hyperion.script.listener.event.ActionButtonEvent;
import org.hyperion.script.listener.event.LoginEvent;
import org.hyperion.script.listener.event.LogoutEvent;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public abstract class AbstractMission implements Mission {

    /**
     * The script context.
     */
    private final ScriptContext scriptContext = World.getWorld().getScriptContext();

    /**
     * The login hook.
     */
    private final LoginEvent loginHook = new LoginEvent() {
        @Override
        public void handle(Player player) {
            handleLogin(player);
        }
    };

    /**
     * The logout hook.
     */
    private final LogoutEvent logoutHook = new LogoutEvent() {
        @Override
        public void handle(Player player) {
            handleLogout(player);
        }
    };
    /**
     * The action button hook.
     */
    private final ActionButtonEvent actionButtonHook = new ActionButtonEvent() {
        @Override
        public void handleButton(Player player, ActionButtonContext button) {
            handleButtons(player, button);
        }
    };

    @Override
    public boolean getRequirements(Player player) {
        return false;
    }

    @Override
    public void showBrief(Player player, int stage) {

    }

    @Override
    public void giveRewards(Player player) {

    }

    @Override
    public void start(Player player) {
        //TODO: look at this
//        if (player.getMission() == this) {
//            player.getActionSender().sendMessage("You can't do more than"
//                    + " 1 mission until you complete your current mission.");
//            //return;
//        }
        //add the mission hooks
        addHooks();
    }

    @Override
    public void end(Player player) {
        //remove hooks
        removeHooks();
    }

    @Override
    public void quit(Player player) {
        player.getActionSender().sendMessage("You failed the mission.");
        player.getMissionStorage().setMissionStage(player.getMission(), 0);
        player.setMission(null);

        //remove hooks
        removeHooks();

        if (player.getGang() != null) {
            player.getGang().decreaseRespct(player, 20);
        }
    }

    /**
     * Adds the mission hooks.
     */
    public void addHooks() {
        scriptContext.getLoginListener().add(loginHook);
        scriptContext.getLogoutListener().add(logoutHook);
        scriptContext.getActionButtonListener().add(actionButtonHook);
    }

    /**
     * Removes the mission hooks.
     */
    public void removeHooks() {
        scriptContext.getLoginListener().remove(loginHook);
        scriptContext.getLogoutListener().remove(logoutHook);
        scriptContext.getActionButtonListener().remove(actionButtonHook);
    }

    /**
     * Continues the mission, like a player logged off and back in
     *
     * @param player The player
     */
    public void continueMission(Player player) {
        MissionStorage missionStorage = player.getMissionStorage();
        Mission mission = player.getMission();

        /**
         * Player is currently in a mission.
         */
        if (mission != null) {
            /**
             * Mission already started, so add the hooks...
             */
            if (missionStorage.hasStarted(mission)) {
                // add hooks
            }
        }
    }
}
