package org.hyperion.rs2.content.mission;

import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.packet.context.ActionButtonContext;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public interface Mission {

    /**
     * The mission button.
     *
     * @return The mission button
     */
    public int getButton();

    /**
     * The mission name.
     *
     * @return The mission name
     */
    public String getName();

    /**
     * The final stage of the mission.
     *
     * @return The final stage Id
     */
    public int getFinalStage();

    /**
     * The mission requirements.
     *
     * @param player The player
     * @return If has the requirements
     */
    public boolean getRequirements(Player player);

    /**
     * Gets the mission progress
     *
     * @param player The player
     * @param stage The mission stage
     */
    public void showBrief(Player player, int stage);

    /**
     * Starts the mission
     *
     * @param player The player
     */
    public void start(Player player);

    /**
     * Ends the mission
     *
     * @param player The player
     */
    public void end(Player player);

    /**
     * Quits the mission
     *
     * @param player The player
     */
    public void quit(Player player);

    /**
     * Gives the rewards
     *
     * @param player The player
     */
    public void giveRewards(Player player);

    /**
     * Handles the login actions.
     *
     * @param player The player
     */
    public void handleLogin(Player player);

    /**
     * Handles the logout actions.
     *
     * @param player The player
     */
    public void handleLogout(Player player);

    /**
     * Handles action buttons listener
     *
     * @param player The player
     * @param context The action button context
     */
    public abstract void handleButtons(Player player, ActionButtonContext context);
}
