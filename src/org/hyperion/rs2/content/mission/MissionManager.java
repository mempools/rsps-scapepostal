package org.hyperion.rs2.content.mission;

import org.hyperion.rs2.content.mission.impl.HitmanMission;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public class MissionManager {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(MissionManager.class.getName());

    /**
     * The amount of missions available.
     */
    public static final int AMOUNT_OF_MISSIONS = 136;

    /**
     * The mapping holding all mission data.
     */
    private static final Map<Integer, Mission> missions = new HashMap<>();

    /**
     * Gets the missions
     *
     * @return missions The missions
     */
    public static Map<Integer, Mission> getMissions() {
        return missions;
    }

    /**
     * Adds a mission
     *
     * @param mission The mission to add
     * @return The added mission
     */
    private static Mission add(Mission mission) {
        return missions.put(mission.getButton(), mission);
    }

    /**
     * Loads the quests.
     */
    public static void load() {
        // missions.put(15, new DwarfCannonMission());
        //START OF THE MISSION SHOULD BE 7334
        add(new HitmanMission());
        logger.info("Loaded " + missions.size() + " missions");
    }

    /**
     * Grabs a mission from the mapping.
     *
     * @param buttonId The mission button id.
     * @return The {@code Mission} which id matches the id given, or
     * {@code null} if the mission didn't exist.
     */
    public static Mission get(int buttonId) {
        return missions.get(buttonId);
    }
}
