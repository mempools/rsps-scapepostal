package org.hyperion.rs2.content.mission;

/**
 * @author Emperor
 */
public class MissionStorage {

    /**
     * The current mission states.
     */
    private final int[] MISSION_STATES = new int[7335 + 137];

    /**
     * The mission points.
     */
    private int missionPoints;

    /**
     * Gets the mission points
     *
     * @return missionPoints The mission points
     */
    public int getMissionPoints() {
        return missionPoints;
    }

    /**
     * Sets the mission points
     *
     * @param set The amount to set.
     * @return missionPoints The mission points
     */
    public int setMissionPoints(int set) {
        return this.missionPoints = set;
    }

    /**
     * Increases / adds mission points
     *
     * @param amt The amount to add.
     * @return missionPoints The mission points
     */
    public int addMissionPoints(int amt) {
        return missionPoints += amt;
    }

    /**
     * Decreases / subtracts mission points
     *
     * @param amt The amount to decrease
     * @return missionPoints The mission points
     */
    public int removeMissionPoints(int amt) {
        return missionPoints -= amt;
    }

    /**
     * Gets the mission state by mission.
     *
     * @param missionId The mission to check.
     * @return The mission state (0 at default).
     */
    public int getMissionStage(Mission missionId) {
        return MISSION_STATES[missionId.getButton()];
    }

    /**
     * Gets the mission state.
     *
     * @param missionId The mission id to check.
     * @return The mission state (0 at default).
     */
    public int getMissionStage(int missionId) {
        return MISSION_STATES[missionId];
    }

    /**
     * Checks if the player has started a certain mission.
     *
     * @param missionId The mission's id.
     * @return {@code True} if the player has started the mission, {@code false}
     * if not.
     */
    public boolean hasStarted(Mission missionId) {
        Mission mission = MissionManager.get(missionId.getButton());
        return MISSION_STATES[missionId.getButton()] > 0 && MISSION_STATES[missionId.getButton()] < mission.getFinalStage();
    }

    /**
     * Checks if the player has finished a certain mission.
     *
     * @param missionId The mission id.
     * @return {@code True} if so, {@code false} if not.
     */
    public boolean hasFinished(Mission missionId) {
        Mission mission = MissionManager.get(missionId.getButton());
        return mission != null && mission.getFinalStage() <= MISSION_STATES[missionId.getButton()];
    }

    /**
     * Sets a current mission state.
     *
     * @param missionId The mission's id.
     * @param state The state to set.
     */
    public void setMissionStage(Mission missionId, int state) {
        MISSION_STATES[missionId.getButton()] = state;
    }

    /**
     * Sets a current mission state.
     *
     * @param missionId The mission id.
     * @param state The state to set.
     */
    public void setMissionStage(int missionId, int state) {
        MISSION_STATES[missionId] = state;
    }
}
