package org.hyperion.rs2.content.dialogue;

/**
 * The dialogue type.
 */
public enum DialogueType {

    /**
     * NPC dialogue.
     */
    NPC,
    /**
     * Player dialogue.
     */
    PLAYER,
    /**
     * Options dialogue.
     */
    OPTION,
    /**
     * Message dialogue.
     */
    STATEMENT,
    /**
     * Item message dialogue.
     */
    MESSAGE_MODEL,
    /**
     * Information box.
     */
    INFO;
}
