package org.hyperion.rs2.content.dialogue;

import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.player.Player;

/**
 * @author global <http://rune-server.org/members/global/>
 */
public abstract class Dialogue {

    /**
     * Opens the dialogue
     *
     * @param player The Player associated with the dialogue
     * @param npc The NPC associated with the dialogue
     * @param dialogue The dialogue id.
     */
    public abstract void handle(Player player, NPC npc, int dialogue);

    /**
     * Opens a player dialogue
     *
     * @param player The player.
     * @param dialogueId The dialogueId to show
     */
    public void openDialogue(final Player player, int dialogueId) {
        if (dialogueId == -1) {
            return;
        }
        for (int i = 0; i < 6; i++) {
            player.getInterfaceState().setNextDialogueId(i, -1);
        }
        player.getInterfaceState().setCurrentDialogue(dialogueId);

        NPC npc = (NPC) player.getInteractingActor();
        player.setInteractingActor(npc);
        switch (dialogueId) {
            /*
             * Closes the dialogue ; resets it
             */
            case 0:
                player.getActionSender().removeAllWindows();
                npc.resetInteractingActor();
                npc.resetFace();
                break;
        }
        if (player.getInterfaceState().getDialogue() != null) {
            player.getInterfaceState().getDialogue().handle(player, npc, dialogueId);
        }
    }

    /**
     * Sends the next dialogue
     *
     * @param player The player
     * @param index The dialogue index
     */
    public void advanceDialogue(Player player, int index) {
        int dialogueId = player.getInterfaceState().getNextDialogueId(index);
        if (dialogueId == -1 || dialogueId == 0) {
            player.getActionSender().removeAllWindows();
            return;
        }
        openDialogue(player, dialogueId);
    }
}
