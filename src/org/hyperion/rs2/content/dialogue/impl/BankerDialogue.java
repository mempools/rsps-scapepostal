package org.hyperion.rs2.content.dialogue.impl;

import org.hyperion.rs2.content.dialogue.Dialogue;
import org.hyperion.rs2.content.dialogue.DialogueType;
import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.container.Bank;

/**
 * @author Global <http://rune-server.org/members/global/>
 */
public class BankerDialogue extends Dialogue {

    @Override
    public void handle(Player player, NPC npc, int id) {
        switch (id) {
            case 1:
                player.getActionSender().sendDialogue(npc.getDefinition().getName(), DialogueType.NPC, npc.getDefinition().getId(), Animation.FACE_DEFAULT,
                        "Good day. How may I help you?");
                player.getInterfaceState().setNextDialogueId(0, 2);
                break;
            case 2:
                player.getActionSender().sendDialogue("Select an Option", DialogueType.OPTION, -1, Animation.FACE_DEFAULT,
                        "I'd like to access my bank account, please.",
                        "I'd like to change my PIN please.",
                        "What is this place?");
                player.getInterfaceState().setNextDialogueId(0, 3);
                player.getInterfaceState().setNextDialogueId(1, 4);
                player.getInterfaceState().setNextDialogueId(2, 6);
                break;
            case 3:
                player.getActionSender().removeAllWindows();
                Bank.open(player, player.getBank());
                break;
            case 4:
                player.getActionSender().sendDialogue(player.getName(), DialogueType.PLAYER, -1, Animation.FACE_DEFAULT,
                        "I'd like to set/change my PIN please.");
                player.getInterfaceState().setNextDialogueId(0, 5);
                break;
            case 5:
                player.getActionSender().sendDialogue(npc.getDefinition().getName(), DialogueType.NPC, npc.getDefinition().getId(), Animation.FACE_DEFAULT,
                        "We currently do not offer bank PINs, sorry.");
                player.getInterfaceState().setNextDialogueId(0, 0);
                break;
            case 6:
                player.getActionSender().sendDialogue(player.getName(), DialogueType.PLAYER, -1, Animation.FACE_DEFAULT,
                        "What is this place?");
                player.getInterfaceState().setNextDialogueId(0, 7);
                break;
            case 7:
                player.getActionSender().sendDialogue(npc.getDefinition().getName(), DialogueType.NPC, npc.getDefinition().getId(), Animation.FACE_DEFAULT,
                        "This is a branch of the Bank of RuneScape. We have",
                        "branches in many towns.");
                player.getInterfaceState().setNextDialogueId(0, 8);
                break;
            case 8:
                player.getActionSender().sendDialogue("Select an Option", DialogueType.OPTION, -1, Animation.FACE_DEFAULT,
                        "And what do you do?",
                        "Didn't you used to be called the Bank of Varrock?");
                player.getInterfaceState().setNextDialogueId(0, 9);
                player.getInterfaceState().setNextDialogueId(1, 11);
                break;
            case 9:
                player.getActionSender().sendDialogue(player.getName(), DialogueType.PLAYER, -1, Animation.FACE_DEFAULT,
                        "And what do you do?");
                player.getInterfaceState().setNextDialogueId(0, 10);
                break;
            case 10:
                player.getActionSender().sendDialogue(npc.getDefinition().getName(), DialogueType.NPC, npc.getDefinition().getId(), Animation.FACE_DEFAULT,
                        "We will look after your items and money for you.",
                        "Leave your valuables with us if you want to keep them",
                        "safe.");
                player.getInterfaceState().setNextDialogueId(0, 0);
                break;
            case 11:
                player.getActionSender().sendDialogue(player.getName(), DialogueType.PLAYER, -1, Animation.FACE_DEFAULT,
                        "Didn't you used to be called the Bank of Varrock?");
                player.getInterfaceState().setNextDialogueId(0, 12);
                break;
            case 12:
                player.getActionSender().sendDialogue(npc.getDefinition().getName(), DialogueType.NPC, npc.getDefinition().getId(), Animation.FACE_DEFAULT,
                        "Yes we did, but people kept on coming into our",
                        "branches outside of Varrock and telling us that our",
                        "signs were wrong. They acted as if we didn't know",
                        "what town we were in or something.");
                player.getInterfaceState().setNextDialogueId(0, 0);
                break;
        }
    }
}
