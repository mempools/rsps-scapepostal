package org.hyperion.rs2;

import org.hyperion.loginserver.PlayerData;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.player.PlayerDetails;

/**
 * An interface which describes the methods for loading persistent world
 * information such as players.
 *
 * @author Graham Edgecombe
 */
public interface WorldLoader {

    /**
     * Represents the result of a login request.
     *
     * @author Graham Edgecombe
     */
    public static class LoginResult {

        /**
         * Response code constants.
         */
        public static final int RESPONSE_UNEXPECTED = 1;
        public static final int RESPONSE_OK = 2;
        public static final int RESPONSE_INVALID_INFORMATION = 3;
        public static final int RESPONSE_BANNED = 4;
        public static final int RESPONSE_ALREADY_LOGGED = 5;
        public static final int RESPONSE_WORLD_UPDATE = 6;
        public static final int RESPONSE_FULL_WORLD = 7;
        public static final int RESPONSE_LOGIN_SERVER_OFFLINE = 8;
        public static final int RESPONSE_LOGIN_LIMIT = 9;
        public static final int RESPONSE_BAD_SESSION = 10;
        public static final int RESPONSE_CHANGE_PASSWORD = 11;
        public static final int RESPONSE_MEMBERS_ACCOUNT = 12;
        public static final int RESPONSE_INCOMPLETE_LOGIN = 13;
        public static final int RESPONSE_SERVER_UPDATE = 14;
        public static final int RESPONSE_LOGIN_WAIT = 15;
        public static final int RESPONSE_LOGIN_WAIT_ADDRESS = 16;
        public static final int RESPONSE_IN_MEMBERS_WORLD = 17;
        public static final int RESPONSE_LOCKED = 18;
        public static final int RESPONSE_CLOSED_BETA = 19;
        public static final int RESPONSE_INVALID_LOGIN_SERVER = 20;
        public static final int RESPONSE_WAIT_TRANSFER = 21;
        public static final int RESPONSE_MALFORMED_LOGIN = 22;
        public static final int RESPONSE_IDLE_LOGIN_SERVER = 23;
        public static final int RESPONSE_PROFILE_ERROR = 24;
        public static final int RESPONSE_UNEXPECTED_LOGIN_SERVER = 25;
        public static final int RESPONSE_COMPUTER_BLOCKED = 26;
        public static final int RESPONSE_SERVICE_UNAVAILABLE = 27;

        /**
         * The response code.
         */
        private final int responseCode;

        /**
         * The player object, or <code>null</code> if the login failed.
         */
        private Player player;

        /**
         * The player data.
         */
        private PlayerData playerData;

        /**
         * Creates a login result that failed.
         *
         * @param responseCode The return code.
         */
        public LoginResult(int responseCode) {
            this.responseCode = responseCode;
            this.player = null;
        }

        /**
         * Creates a login result that succeeded.
         *
         * @param responseCode The return code.
         * @param player The player object.
         */
        public LoginResult(int responseCode, Player player) {
            this.responseCode = responseCode;
            this.player = player;
        }

        /**
         * Creates a login result that succeeded.
         *
         * @param code The return code.
         * @param playerData The player object data.
         */
        public LoginResult(int code, PlayerData playerData) {
            this.responseCode = code;
            this.playerData = playerData;
        }

        /**
         * Gets the return code.
         *
         * @return The return code.
         */
        public int getResponseCode() {
            return responseCode;
        }

        /**
         * Gets the player.
         *
         * @return The player.
         */
        public Player getPlayer() {
            return player;
        }

        /**
         * Gets the player data
         *
         * @return The player data
         */
        public PlayerData getPlayerData() {
            return playerData;
        }

    }

    /**
     * Checks if a set of login details are correct. If correct, creates but
     * does not load, the player object.
     *
     * @param pd The login details.
     * @return The login result.
     */
    public LoginResult checkLogin(PlayerDetails pd);

    /**
     * Loads player information.
     *
     * @param player The player object.
     * @return <code>true</code> on success, <code>false</code> on failure.
     */
    public boolean loadPlayer(Player player);

    /**
     * Saves player information.
     *
     * @param player The player object.
     * @return <code>true</code> on success, <code>false</code> on failure.
     */
    public boolean savePlayer(Player player);

}
