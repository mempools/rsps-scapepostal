package org.hyperion.rs2.task.impl;

import org.hyperion.rs2.GameEngine;
import org.hyperion.rs2.packet.context.ChatMessageContext;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.UpdateFlags.UpdateFlag;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.net.PacketManager;
import org.hyperion.rs2.task.Task;

import java.util.Queue;

/**
 * A task which is executed before an <code>UpdateTask</code>. It is similar to
 * the call to <code>process()</code> but you should use <code>Event</code>s
 * instead of putting timers in this class.
 *
 * @author Graham Edgecombe
 */
public class PlayerTickTask implements Task {

    /**
     * The player.
     */
    private final Player player;

    /**
     * Creates a tick task for a player.
     *
     * @param player The player to create the tick task for.
     */
    public PlayerTickTask(Player player) {
        this.player = player;
    }

    @Override
    public void execute(GameEngine context) {
        /**
         * Chat message queue
         */
        Queue<ChatMessageContext> messages = player.getChatMessageQueue();
        if (messages.size() > 0) {
            player.getUpdateFlags().flag(UpdateFlag.CHAT);
            ChatMessageContext message = player.getChatMessageQueue().poll();
            player.setCurrentChatMessage(message);
        } else {
            player.setCurrentChatMessage(null);
        }
        /**
         * Packet queue
         */
        Queue<Packet> packets = player.getPacketQueue();
        Packet packet = null;
        while ((packet = packets.poll()) != null) {
            PacketManager.getPacketManager().handle(player, packet);
        }
        player.getWalkingQueue().processNextMovement();
    }
}
