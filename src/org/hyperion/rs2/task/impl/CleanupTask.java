package org.hyperion.rs2.task.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.logging.Logger;
import org.hyperion.rs2.GameEngine;
import org.hyperion.rs2.task.Task;
import org.hyperion.util.ServerUtils;

/**
 * Performs garbage collection and finalization.
 *
 * @author Graham Edgecombe
 */
public class CleanupTask implements Task {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(CleanupTask.class.getName());

    /**
     * The decimal format.
     */
    private final DecimalFormat decimalFormat = new DecimalFormat("0.0#%");

    /**
     * The memory format.
     */
    private final NumberFormat memoryFormat = NumberFormat.getInstance();

    @Override
    public void execute(GameEngine context) {
        context.submitWork(new Runnable() {
            @Override
            public void run() {
                System.gc();
                System.runFinalization();
            }
        });
        StringBuilder bldr = new StringBuilder();
        long freeMemory = ServerUtils.getFreeMemory();
        long usedMemory = ServerUtils.getUsedMemory();
        long currentMemory = ServerUtils.getCurrentMemory();
        bldr.append("Memory Usage: ");
        bldr.append(memoryFormat.format(freeMemory));
        bldr.append(" / ");
        bldr.append(memoryFormat.format(currentMemory));
        bldr.append(" MB, ");
        bldr.append(decimalFormat.format(((double) usedMemory / (double) currentMemory)));
        logger.info(bldr.toString());
    }
}
