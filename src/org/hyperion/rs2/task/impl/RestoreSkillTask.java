package org.hyperion.rs2.task.impl;

import org.hyperion.rs2.GameEngine;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.task.Task;

/**
 * Performs restoring of a player's skills.
 *
 * @author Korsakoff
 */
public class RestoreSkillTask implements Task {

    @Override
    public void execute(GameEngine context) {
        for (Player player : World.getWorld().getPlayers()) {
            for (int i = 0; i < Skills.SKILL_COUNT; i++) {
                if (i == Skills.HITPOINTS || i == Skills.PRAYER) {
                    continue;
                }
                player.getSkills().normalizeLevel(i);
            }
        }
    }
}
