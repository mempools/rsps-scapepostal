package org.hyperion.rs2.task.impl;

import org.hyperion.rs2.GameEngine;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.task.Task;
import org.jboss.netty.channel.Channel;

import java.util.logging.Logger;

/**
 * A task that is executed when a session is closed.
 *
 * @author Graham Edgecombe
 */
public class ChannelClosedTask implements Task {

    /**
     * Logger instance.
     */
    private static final Logger logger = Logger.getLogger(ChannelClosedTask.class.getName());

    /**
     * The session that closed.
     */
    private final Channel channel;

    /**
     * Creates the session closed task.
     *
     * @param channel The session.
     */
    public ChannelClosedTask(Channel channel) {
        this.channel = channel;
    }

    @Override
    public void execute(GameEngine context) {
        logger.fine("Channel closed : " + channel.getRemoteAddress());
        if (World.getWorld().getChannelStorage().contains(channel)) {
            Player p = World.getWorld().getChannelStorage().get(channel);
            World.getWorld().unregister(p);
        }
    }

}
