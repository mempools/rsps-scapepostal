package org.hyperion.rs2.task.impl;

import org.hyperion.Constants;
import org.hyperion.rs2.GameEngine;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.net.Packet;
import org.hyperion.rs2.task.Task;
import org.jboss.netty.channel.Channel;

/**
 * A task that is executed when a session receives a message.
 *
 * @author Graham Edgecombe
 * @author Nikki
 */
public class ChannelMessageTask implements Task {

    /**
     * The session.
     */
    private final Channel channel;

    /**
     * The packet.
     */
    private final Packet message;

    /**
     * Creates the session message task.
     *
     * @param channel The session.
     * @param message The packet.
     */
    public ChannelMessageTask(Channel channel, Packet message) {
        this.channel = channel;
        this.message = message;
    }

    @Override
    public void execute(GameEngine context) {
        Player player = World.getWorld().getChannelStorage().get(channel);
        if (player != null) {
            if (player.getPacketQueue().size() < Constants.PLAYER_MAX_PACKETS) {
                player.getPacketQueue().add(message);
            } else {
                // TODO anything when the size is too large?
            }
        }
    }

}
