package org.hyperion.rs2.task.impl;

import java.util.Iterator;

import org.hyperion.rs2.GameEngine;
import org.hyperion.rs2.model.Entity;
import org.hyperion.rs2.model.Location;
import org.hyperion.rs2.model.npc.NPC;
import org.hyperion.rs2.model.player.Player;
import org.hyperion.rs2.model.Skills;
import org.hyperion.rs2.model.UpdateFlags;
import org.hyperion.rs2.model.World;
import org.hyperion.rs2.model.UpdateFlags.UpdateFlag;
import org.hyperion.rs2.net.Packet.Type;
import org.hyperion.rs2.net.PacketBuilder;
import org.hyperion.rs2.task.Task;

/**
 * A task which creates and sends the NPC update block.
 *
 * @author Graham Edgecombe
 *
 */
public class NPCUpdateTask implements Task {

    /**
     * The player.
     */
    private final Player player;

    /**
     * Creates an NPC update task.
     *
     * @param player The player.
     */
    public NPCUpdateTask(Player player) {
        this.player = player;
    }

    @Override
    public void execute(GameEngine context) {
        /*
         * The update block holds the update masks and data, and is written
         * after the main block.
         */
        PacketBuilder updateBlock = new PacketBuilder();

        /*
         * The main packet holds information about adding, moving and removing
         * NPCs.
         */
        PacketBuilder packet = new PacketBuilder(71, Type.VARIABLE_SHORT);
        packet.startBitAccess();

        /*
         * Write the current size of the npc list.
         */
        packet.putBits(8, player.getLocalNPCs().size());

        /*
         * Iterate through the local npc list.
         */
        for (Iterator<NPC> it$ = player.getLocalNPCs().iterator(); it$.hasNext();) {
            /*
             * Get the next NPC.
             */
            NPC npc = it$.next();

            /*
             * If the NPC should still be in our list.
             */
            if (World.getWorld().getNPCs().contains(npc)
                    && !npc.isTeleporting()
                    && npc.getLocation().isWithinDistance(player.getLocation())) {
                /*
                 * Update the movement.
                 */
                updateNPCMovement(packet, npc);

                /*
                 * Check if an update is required, and if so, send the update.
                 */
                if (npc.getUpdateFlags().isUpdateRequired()) {
                    updateNPC(updateBlock, npc);
                }
            } else {
                /*
                 * Otherwise, remove the NPC from the list.
                 */
                it$.remove();

                /*
                 * Tell the client to remove the NPC from the list.
                 */
                packet.putBits(1, 1);
                packet.putBits(2, 3);
            }
        }

        /*
         * Loop through all NPCs in the world.
         */
        for (NPC npc : World.getWorld().getRegionManager().getLocalNpcs(player)) {
            /*
             * Check if there is room left in the local list.
             */
            if (player.getLocalNPCs().size() >= 255) {
                /*
                 * There is no more room left in the local list. We cannot add
                 * more NPCs, so we just ignore the extra ones. They will be
                 * added as other NPCs get removed.
                 */
                break;
            }

            /*
             * If they should not be added ignore them.
             */
            if (player.getLocalNPCs().contains(npc)) {
                continue;
            }

            /*
             * Add the npc to the local list if it is within distance.
             */
            player.getLocalNPCs().add(npc);

            /*
             * Add the npc in the packet.
             */
            addNewNPC(packet, npc);

            /*
             * Check if an update is required.
             */
            if (npc.getUpdateFlags().isUpdateRequired()) {

                /*
                 * If so, update the npc.
                 */
                updateNPC(updateBlock, npc);

            }
        }

        /*
         * Check if the update block isn't empty.
         */
        if (!updateBlock.isEmpty()) {
            /*
             * If so, put a flag indicating that an update block follows.
             */
            packet.putBits(14, 16383);
            packet.finishBitAccess();

            /*
             * And append the update block.
             */
            packet.put(updateBlock.toPacket().getPayload());
        } else {
            /*
             * Terminate the packet normally.
             */
            packet.finishBitAccess();
        }

        /*
         * Write the packet.
         */
        player.write(packet.toPacket());
    }

    /**
     * Adds a new NPC.
     *
     * @param packet The main packet.
     * @param npc The npc to add.
     */
    private void addNewNPC(PacketBuilder packet, NPC npc) {
        /*
         * Write the NPC's index.
         */
        packet.putBits(14, npc.getIndex());

        /*
         * Calculate the x and y offsets.
         */
        int yPos = npc.getLocation().getY() - player.getLocation().getY();
        int xPos = npc.getLocation().getX() - player.getLocation().getX();

        packet.putBits(1, npc.getUpdateFlags().isUpdateRequired() ? 1 : 0);

        /*
         * And write them.
         */
        packet.putBits(5, yPos);
        packet.putBits(5, xPos);

        /*
         * TODO unsure, but probably discards the client-side walk queue.
         */
        packet.putBits(1, 0);

        /*
         * We now write the NPC type id.
         */
        packet.putBits(13, npc.getDefinition().getId());
    }

    /**
     * Update an NPC's movement.
     *
     * @param packet The main packet.
     * @param npc The npc.
     */
    private void updateNPCMovement(PacketBuilder packet, NPC npc) {
        /*
         * Check if the NPC is running.
         */
        if (npc.getSprites().getSecondarySprite() == -1) {
            /*
             * They are not, so check if they are walking.
             */
            if (npc.getSprites().getPrimarySprite() == -1) {
                /*
                 * They are not walking, check if the NPC needs an update.
                 */
                if (npc.getUpdateFlags().isUpdateRequired()) {
                    /*
                     * Indicate an update is required.
                     */
                    packet.putBits(1, 1);

                    /*
                     * Indicate we didn't move.
                     */
                    packet.putBits(2, 0);
                } else {
                    /*
                     * Indicate no update or movement is required.
                     */
                    packet.putBits(1, 0);
                }
            } else {
                /*
                 * They are walking, so indicate an update is required.
                 */
                packet.putBits(1, 1);

                /*
                 * Indicate the NPC is walking 1 tile.
                 */
                packet.putBits(2, 1);

                /*
                 * And write the direction.
                 */
                packet.putBits(3, npc.getSprites().getPrimarySprite());

                /*
                 * And write the update flag.
                 */
                packet.putBits(1, npc.getUpdateFlags().isUpdateRequired() ? 1 : 0);
            }
        } else {
            /*
             * They are running, so indicate an update is required.
             */
            packet.putBits(1, 1);

            /*
             * Indicate the NPC is running 2 tiles.
             */
            packet.putBits(2, 2);

            /*
             * And write the directions.
             */
            packet.putBits(3, npc.getSprites().getPrimarySprite());
            packet.putBits(3, npc.getSprites().getSecondarySprite());

            /*
             * And write the update flag.
             */
            packet.putBits(1, npc.getUpdateFlags().isUpdateRequired() ? 1 : 0);
        }
    }

    /**
     * Update an NPC.
     *
     * @param packet The update block.
     * @param npc The NPC.
     */
    private void updateNPC(PacketBuilder packet, NPC npc) {
        /*
         * Calculate the mask.
         */
        int mask = 0;
        final UpdateFlags flags = npc.getUpdateFlags();

        if (flags.get(UpdateFlag.ANIMATION)) {
            mask |= 0x2;
        }
        if (flags.get(UpdateFlag.PRIMARY_HIT)) {
            mask |= 0x10;
        }
        if (flags.get(UpdateFlag.GRAPHICS)) {
            mask |= 0x4;
        }
        if (flags.get(UpdateFlag.FACE_ENTITY)) {
            mask |= 0x40;
        }
        if (flags.get(UpdateFlag.FORCED_CHAT)) {
            mask |= 0x20;
        }
        if (flags.get(UpdateFlag.SECONDARY_HIT)) {
            // mask |= 0x10;
        }
        if (flags.get(UpdateFlag.TRANSFORM)) {
            mask |= 0x1;
        }
        if (flags.get(UpdateFlag.FACE_COORDINATE)) {
            mask |= 0x8;
        }

        /*
         * And write the mask.
         */
        if (mask > 0xff) {
            mask |= 0x20;
            packet.put((byte) (mask & 0xff));
            packet.put((byte) (mask >> 8));
        } else {
            packet.put((byte) mask);
        }

        if (flags.get(UpdateFlag.ANIMATION)) {
            packet.putShort(npc.getCurrentAnimation().getId());
            packet.putByteS((byte) npc.getCurrentAnimation().getDelay());
        }
        if (flags.get(UpdateFlag.PRIMARY_HIT)) {
            packet.putByteS((byte) npc.getDamage().getPrimaryDamage());
            packet.putByteS((byte) npc.getDamage().getPrimaryHitType());
            packet.put((byte) npc.getSkills().getLevel(Skills.HITPOINTS));
            packet.putByteC((byte) npc.getSkills().getLevelForExperience(Skills.HITPOINTS));
        }
        if (flags.get(UpdateFlag.SECONDARY_HIT)) {
//            packet.putByteS((byte) npc.getDamage().getSecondaryDamage());
//            packet.putByteS((byte) npc.getDamage().getSecondaryHitType());
//            packet.put((byte) npc.getSkills().getLevel(Skills.HITPOINTS));
//            packet.putByteC((byte) npc.getSkills().getLevelForExperience(Skills.HITPOINTS));
        }
        if (flags.get(UpdateFlag.GRAPHICS)) {
            packet.putShort(npc.getCurrentGraphic().getId());
            packet.putInt2(npc.getCurrentGraphic().getDelay());
        }
        if (flags.get(UpdateFlag.FACE_ENTITY)) {
            //TODO: fix
            Entity entity = npc.getInteractingActor();
            packet.putLEShort(entity == null ? -1 : entity.getClientIndex());
        }
        if (flags.get(UpdateFlag.FORCED_CHAT)) {
            packet.putRS2String(npc.getForcedChatMessage());
        }
        if (flags.get(UpdateFlag.TRANSFORM)) {
            packet.putByteA((byte) npc.getTransformId());
        }
        if (flags.get(UpdateFlag.FACE_COORDINATE)) {
            Location loc = npc.getFaceLocation();
            int direction = npc.getDirection().toInteger();
            if (loc == null) {
                packet.putLEShortA(loc.getX());
                packet.putLEShort(loc.getY());
            } else {
                packet.putLEShortA(loc.getX() * 2 + direction);
                packet.putLEShort(loc.getY() * 2 + direction);
            }
        }
    }

}
