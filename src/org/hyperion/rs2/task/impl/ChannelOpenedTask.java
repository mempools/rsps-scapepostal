package org.hyperion.rs2.task.impl;

import org.hyperion.rs2.GameEngine;
import org.hyperion.rs2.task.Task;
import org.jboss.netty.channel.Channel;

import java.util.logging.Logger;

/**
 * A task that is executed when a session is opened.
 *
 * @author Graham Edgecombe
 */
public class ChannelOpenedTask implements Task {

    /**
     * The logger class.
     */
    private static final Logger logger = Logger.getLogger(ChannelOpenedTask.class.getName());

    /**
     * The session.
     */
    private final Channel channel;

    /**
     * Creates the session opened task.
     *
     * @param channel The session.
     */
    public ChannelOpenedTask(Channel channel) {
        this.channel = channel;
    }

    @Override
    public void execute(GameEngine context) {
        logger.fine("Session opened : " + channel.getRemoteAddress());
    }
}
