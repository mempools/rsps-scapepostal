package org.hyperion.rs2.task;

import org.hyperion.rs2.GameEngine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * A task which executes a group of tasks in a guaranteed sequence.
 *
 * @author Graham Edgecombe
 */
public class ConsecutiveTask implements Task {

    /**
     * The tasks.
     */
    private final Collection<Task> tasks;

    /**
     * Creates the consecutive task.
     *
     * @param tasks The child tasks to handle.
     */
    public ConsecutiveTask(Task... tasks) {
        List<Task> taskList = new ArrayList<>();
        for (Task task : tasks) {
            taskList.add(task);
        }
        this.tasks = Collections.unmodifiableCollection(taskList);
    }

    @Override
    public void execute(GameEngine context) {
        for (Task task : tasks) {
            task.execute(context);
        }
    }
}
