package org.hyperion.util.tools;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hyperion.rs2.model.Animation;
import org.hyperion.rs2.model.definition.NPCDefinition;

/**
 * @author black flag <http://rune-server.org/members/black+flag/>
 */
public class NpcAnimConvert {

    private static final Logger logger = Logger.getLogger(NpcAnimConvert.class.getName());

    public static Animation getAttack2(int id) {
        return NPCDefinition.forId(id).getAttackAnimation();
    }

    /**
     * Loads Shop Files.
     */
    public static void main(String[] args) throws Exception {
        load();
    }

    public static void load() {
        try {
            NPCDefinition.init();
            NpcConvertData data = new NpcConvertData();
            XStream xStream = new XStream(new DomDriver());
            xStream.alias("npcAnimation", NpcConvertData.class);
            String line = "";
            String[] token3 = null;
            BufferedReader characterfile;
            int loaded = 0;
            try {
                logger.log(Level.INFO, "Loading dump...");
                characterfile = new BufferedReader(new FileReader("./data/npcanim.txt"));
                FileOutputStream d = new FileOutputStream(new File("./data/npcA.xml"));
                d.write("<list>".getBytes("UTF-8"));
                while ((line = characterfile.readLine()) != null) {
                    line = line.trim();
                    String tokens[];
                    if (line.contains("//")) {
                        continue;
                    }
                    line = line.replace("ID: ", "");
                    line = line.replace("Name: ", "");
                    line = line.replace("Walk: ", "");
                    tokens = line.replaceAll("\t", "\t").split("\t");

                    int end = line.indexOf(':');
                    if (end > -1) {
                        int id = Integer.parseInt(tokens[0]);
                        NPCDefinition def = NPCDefinition.forId(id);
                        int walk = Integer.parseInt(tokens[2]);
                        int stand = Integer.parseInt(tokens[3].replace("Stand: ", ""));
                        data.setId(id);
                        data.setWalkAnimation(Animation.create(walk));
                        data.setStandAnimation(Animation.create(stand));
                        if (def != null) {
                            data.setAttackAnimation(def.getAttackAnimation());
                            data.setDeathAnimation(def.getDeathAnimation());
                        }
                        data.setBlockAnimation(Animation.create(getDefend(id)));

                        d.write(xStream.toXML(data).getBytes("UTF-8"));
                        //System.out.println("ID: " + id + " NAME:" + name+ "WALK:"+walk+"STAND:"+stand);
                    }

                    // System.out.println(xStream.toXML(data));
                }
                // xStream.toXML(ncd, new FileOutputStream(new File("./data/npcConvert.xml")));
                d.write("</list>".getBytes("UTF-8"));
                d.close();
                characterfile.close();
                logger.info("Successfully loaded.");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            Logger.getLogger(NpcAnimConvert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void dump(int npc) throws FileNotFoundException, UnsupportedEncodingException, IOException {
        FileOutputStream d = new FileOutputStream(new File("./data/npcA.xml"));
        d.write("<list>".getBytes("UTF-8"));

        d.write("</list>".getBytes("UTF-8"));
        d.close();
    }

    public static class NpcConvertData {

        public int id;
        public Animation walkAnimation;
        public Animation standAnimation;
        public Animation attackAnimation;
        public Animation blockAnimation;
        public Animation deathAnimation;

        public NpcConvertData() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Animation getWalkAnimation() {
            return walkAnimation;
        }

        public void setWalkAnimation(Animation walkAnimation) {
            this.walkAnimation = walkAnimation;
        }

        public Animation getStandAnimation() {
            return standAnimation;
        }

        public void setStandAnimation(Animation standAnimation) {
            this.standAnimation = standAnimation;
        }

        public Animation getAttackAnimation() {
            return attackAnimation;
        }

        public void setAttackAnimation(Animation attackAnimation) {
            this.attackAnimation = attackAnimation;
        }

        public Animation getBlockAnimation() {
            return blockAnimation;
        }

        public void setBlockAnimation(Animation blockAnimation) {
            this.blockAnimation = blockAnimation;
        }

        public Animation getDeathAnimation() {
            return deathAnimation;
        }

        public void setDeathAnimation(Animation deathAnimation) {
            this.deathAnimation = deathAnimation;
        }
    }

    public static int getDeath(int npc) {
        if (npc == 3761) {
            return 3883;
        }
        if (npc == 3760) {
            return 3883;
        }
        if (npc == 3741) {
            return 3903;
        }
        if (npc == 3776) {
            return 3894;
        }
        if (npc == 3751 || npc == 3750 || npc == 3749 || npc == 3748 || npc == 3747) {
            return 3910;
        }
        if (npc == 3771) {
            return 3922;
        }

        switch (npc) {
            case 6279:
            case 6280:
            case 6281:
            case 6282:
                return 6182;
            case 5529:
                return 5784;
            case 6116: // Seagull
                return 6812;
            case 5247:
                return 5412;
            case 5233:
            case 5232:
            case 5231:
            case 5230:
                return 5397;
            case 1019:
                return 1031;
            case 1020:
                return 1037;
            case 1021:
                return 1041;
            case 1022:
                return 1048;
            case 5219:
            case 5218:
            case 5217:
            case 5216:
            case 5215:
            case 5214:
            case 5213:
                return 5098; // Penance
            case 4353:
            case 4354:
            case 4355:
                return 4233;
            case 113:
                return 131;
            case 114:
                return 361;
            case 3058:
                return 2938;
            case 3057:
                return 2945;
            case 3063:
                return 2938;
            case 131:
                return 5671;
            case 1608:
                return 1513;

            case 1676:
                return 1628;
            case 1677:
                return 1618;
            case 1678:
                return 1614;

            case 4404:
            case 4405:
            case 4406:
                return 4265;
            case 914:
                return 196;
            case 3065:
                return 2728;
            case 1620:
            case 1621:
                return 1563;
            case 3066:
                return 2925;
            case 1265:
                return 1314;
            case 118:
                return 102;
            case 2263:
                return 2183;
            case 2598:
            case 2599:
            case 2600:
            case 2601:
            case 2602:
            case 2603:
            case 2606://tzhaar-xil
            case 2591:
            case 2604://tzhar-hur
                return 2607;
            case 3347:
            case 3346:
                return 3327;
            case 1192:
                return 1246;
            case 1624:
            case 1625:
                return 1558;
            case 2892:
                return 2865;
            case 127:
                return 188;
            case 119:
                return 102;
            case 2881: // supreme
            case 2882: // prime
            case 2883: // rex
                return 2856;
            case 1590://metal dragon
            case 1591:
            case 1592:
                return 92;
            case 2783://dark beast
                return 2733;
            case 3452:// penance queen
                return 5412;
            case 2889:
                return 2862;
            case 1354://dagnnoth mother
            case 1341:
                return 1342;
            case 3200:
                return 3147;
            case 2457:
                return 2367;

            case 66:
            case 67:
            case 168:
            case 169:
            case 162:
            case 68://gnomes
                return 196;
            case 160:
            case 161:
                return 196;
            case 163:
            case 164:
                return 196;
            case 1153:
            case 1154:
            case 1155:
            case 1156:
            case 1157:
            case 1158: // kalphite
                return 6230;
            case 1160: // kalphite
                return 6234;

            case 438:
            case 439:
            case 440:
            case 441:
            case 442: // Tree spirit
            case 443:
                return 97;
            case 391:
            case 392:
            case 393:
            case 394:
            case 395://river troll
            case 396:
                return 287;
            case 413:
            case 414:
            case 415:
            case 416:
            case 417://rock golem
            case 418:
                return 156;

            case 2745:
                return 2654;
            case 2743:
                return 2646;

            case 2734:
            case 2627://tzhaar 
                return 2620;
            case 2630:
            case 2629:
            case 2736:
            case 2738:
                return 2627;
            case 2631:
            case 2632:
                return 2630;
            case 2741:
                return 2638;

            case 908:
                return 131;
            case 909:
                return 146;
            case 911:
                return 67;

            case 60:
            case 59:
            case 61:
            case 63:
            case 64:
            case 134:
            case 2035: //spider
            case 62:
            case 1009:
                return 5329;

            case 6006: //wolfman
                return 6537;

            case 1612://banshee
                return 1524;

            case 1648:
            case 1649:
            case 1650:
            case 1651:
            case 1652:
            case 1653:
            case 1654:
            case 1655:
            case 1656:
            case 1657://crawling hand
                return 1590;

            case 1604:
            case 1605:
            case 1606:
            case 1607://aberrant specter
                return 1508;

            case 1618:
            case 1619://bloodveld
                return 1553;

            case 1643:
            case 1644:
            case 1645:
            case 1646:
            case 1647://infernal mage
                return 2304;

            case 1613://nechryael
                return 1530;

            case 1610:
            case 1611://gargoyle
                return 1518;

            case 1615://abyssal demon
                return 1538;

            case 3406:
            case 1633: // pyrefiend
                return 1580;

            case 1456://monkey archer
                return 1397;

            case 122://hobgoblin
                return 167;

            case 1125://dad
                return 287;

            case 1096:
            case 1097:
            case 1098:
            case 1942:
            case 1108:
            case 1109:
            case 1110:
            case 1111:
            case 1112:
            case 1116:
            case 1117:
            case 1101://troll 
            case 1106:
            case 1095:
                return 287;

            case 103:
            case 104:
            case 749:
            case 655:
            case 491: //ghost
                return 126;

            case 49://hellhound
            case 142:
            case 95:
                return 6576;

            case 74:
            case 75:
            case 76:
                return 5569;

            case 73:
            case 751: //zombie
            case 77:
                return 5569;

            case 1770:
            case 1771:
            case 1772:
            case 1773:
            case 2678:
            case 2679:
            case 1774:
            case 1775:
            case 1776:// goblins			
                return 313;

            case 50: //kbd
                return 92;

            case 708: //imp
            case 3062:
                return 172;

            case 81: //cow
            case 397:
                return 5851;

            case 86:
            case 87:
                return 141;
            case 47://rat
                return 2707;

            case 2620: //tzhaar
            case 2610:
            case 2619:
                return 2607;

            case 1560: //troll
            case 1566:
                return 287;

            case 89:
                return 6377;
            case 133: //unicorns
                return 292;

            case 2033: //rat
            case 748:
                return 141;

            case 2031: // bloodvel
                return 2073;

            case 101: //goblin
                return 313;

            case 105:
            case 1195:
            case 1196: //bear
                return 4930;

            case 128: //snake
            case 1479:
                return 278;

            case 132: //monkey
                return 223;

            case 108://scorpion
            case 1477:
                return 6256;
            case 144:
            case 107:
                return 6256;

            case 90:
            case 91:
            case 5359:
            case 5384:
            case 92:
            case 93: //skeleton
                return 5491;

            case 82:
            case 3064:
            case 4694:
            case 4695:
            case 4696:
            case 4697:
            case 83:
            case 752:
            case 84:
            case 4702:
            case 4703:
            case 4704:
            case 4705: //lesser
                return 67;

            case 941://green dragon
            case 55:
            case 53:
            case 4669:
            case 4670:
            case 4671:
            case 742:
            case 1589:
            case 54:
            case 52:
                return 92;
            case 6260:
                return 7062;
            case 123: //hobgoblin
            case 3061:
                return 167;
            case 141:
                return 6570;
            case 1593:
                return 6564;
            case 152:
            case 45:
            case 1558: //wolf
            case 1954:
                return 78;

            case 1459://monkey guard
                return 1404;

            case 78:
            case 412: //bat
                return 36;

            case 1766:
            case 1767:
                return 0x03E;

            case 1017:
            case 2693:
            case 41: // chicken
                return 5389;

            case 1585:
            case 110:
            case 1582:
            case 1583:
            case 1584:
            case 1586: //giant
            case 4291:
            case 4292:
                return 4673;
            case 111:
            case 4687:
                return 4673;
            case 4690:
            case 4691:
            case 4692:
            case 4693:
            case 117:
            case 116:
            case 112:
            case 1587:
            case 1588:
                return 4653;

            case 2455:
            case 2454:
            case 2456:
            case 1338: // dagannoth
            case 1340:
            case 1342:
                return 1342;

            case 125: // ice warrior
                return 843;

            default:
                return 2304;
        }
    }

    public static int getDefend(int npc) {
        if (npc == 3776) {
            return 3895;
        }
        if (npc == 3761) {
            return 3881;
        }
        if (npc == 3760) {
            return 3881;
        }
        if (npc == 3771) {
            return 3921;
        }
        if (npc == 3751 || npc == 3750 || npc == 3749 || npc == 3748 || npc == 3747) {
            return 3909;
        }
        if (npc == 3741) {
            return 3902;
        }

        switch (npc) {
            case 6279:
            case 6280:
            case 6281:
            case 6282:
                return 6183;
            case 5529:
                return 5783;
            case 6260:
                return 7061;
            case 5247:
                return 5413;
            case 6116: // Seagull
                return 6810;
            case 5233:
            case 5232:
            case 5231:
            case 5230:
                return 5396;
            case 5219:
            case 5218:
            case 5217:
            case 5216:
            case 5215:
            case 5214:
            case 5213:
                return 5096; // Penance
            case 113:
                return 129;
            case 114:
                return 360;
            case 3058:
                return 2937;
            case 3061:
                return 2961;
            case 3063:
                return 2937;
            case 1608:
                return 1514;
            case 131:
                return 5670;
            case 1676:
                return 1627;
            case 1677:
                return 1617;
            case 1678:
                return 1613;
            case 1019:
                return 1030;
            case 1020:
                return 1036;
            case 1021:
                return 1042;
            case 1022:
                return 1046;
            case 914:
                return 194;
            case 4353:
            case 4354:
            case 4355:
                return 4232;
            case 4404:
            case 4405:
            case 4406:
                return 4267;
            case 3065:
                return 2720;
            case 1620:
            case 1621:
                return 1560;
            case 3066:
                return 2926;
            case 1265:
                return 1313;
            case 118:
                return 100;
            case 2263:
                return 2181;
            case 82:
            case 84:
            case 4702:
            case 4703:
            case 4704:
            case 4705:
            case 752:
            case 4694:
            case 4695:
            case 4696:
            case 4697:
            case 3064:
            case 83: // lesser
                return 65;
            case 3347:
            case 3346:
                return 3325;
            case 1192:
                return 1244;
            case 3062:
                return 2953;
            case 3060:
                return 2964;
            case 2892: //Spinolyp
            case 2894: //Spinolyp
            case 2896: //Spinolyp
                return 2869;
            case 1624:
            case 1625:
                return 1555;
            case 3200:
                return 3148;
            case 1354:
            case 1341:
            case 2455:// dagannoth
            case 2454:
            case 2456:
                return 1340;
            case 127:
                return 186;
            case 119:
                return 100;
            case 2881: // supreme
            case 2882: // prime
            case 2883: // rex
                return 2852;
            case 3452:// penance queen
                return 5413;
            case 2745:
                return 2653;
            case 2743:
                return 2645;
            case 1590://metal dragon
            case 1591:
            case 1592:
                return 89;
            case 2598:
            case 2599:
            case 2600:
            case 2610:
            case 2601:
            case 2602:
            case 2603:
            case 2606://tzhaar-xil
            case 2591:
            case 2604://tzhar-hur
                return 2606;
            case 66:
            case 67:
            case 168:
            case 169:
            case 162:
            case 68://gnomes
                return 193;
            case 160:
            case 161:
                return 194;
            case 163:
            case 164:
                return 193;
            case 438:
            case 439:
            case 440:
            case 441:
            case 442: // Tree spirit
            case 443:
                return 95;
            case 391:
            case 392:
            case 393:
            case 394:
            case 395://river troll
            case 396:
            case 1560:
            case 1566:
                return 285;
            case 413:
            case 414:
            case 415:
            case 416:
            case 417://rock golem
            case 418:
                return 154;

            case 1153:
            case 1154:
            case 1155:
            case 1156:
            case 1157:
            case 1158: // kalphite
                return 6232;
            case 1160: // kalphite
                return 6237;
            case 2783://dark beast
                return 2732;
            case 2734:
            case 2627://tzhaar 
                return 2622;
            case 2630:
            case 2629:
            case 2736:
            case 2738:
                return 2626;
            case 2631:
            case 2632:
                return 2629;
            case 2741:
                return 2635;

            case 908:
                return 129;
            case 909:
                return 147;
            case 911:
                return 65;

            case 1459://monkey guard
                return 1403;

            case 1633: // pyrefiend
            case 3406:
                return 1581;

            case 1612://banshee
                return 1525;

            case 1648:
            case 1649:
            case 1650:
            case 1651:
            case 1652:
            case 1653:
            case 1654:
            case 1655:
            case 1656:
            case 1657://crawling hand
                return 1591;

            case 1604:
            case 1605:
            case 1606:
            case 1607://aberrant specter
                return 1509;

            case 1618:
            case 1619://bloodveld
                return 1550;

            case 1643:
            case 1644:
            case 1645:
            case 1646:
            case 1647://infernal mage
                return 430;

            case 1613://nechryael
                return 1529;

            case 1610:
            case 1611://gargoyle
                return 1519;

            case 1615://abyssal demon
                return 1537;

            case 1770:
            case 1771:
            case 1772:
            case 1773:
            case 2678:
            case 2679:
            case 1774:
            case 1775:
            case 1776:// goblins		
                return 312;

            case 132: //monkey
                return 221;

            case 6006: //wolfman
                return 6538;

            case 1456://monkey archer
                return 1395;

            case 108://scorpion
            case 1477:
                return 6255;
            case 107:
            case 144:
                return 6255;

            case 1125://dad
                return 285;

            case 1096:
            case 1097:
            case 1098:
            case 1942:
            case 1108:
            case 1109:
            case 1110:
            case 1111:
            case 1112:
            case 1116:
            case 1117:
            case 1101://troll 
            case 1106:
                return 285;
            case 1095:
                return 285;

            case 123:
            case 122://hobgoblin
                return 165;

            case 49://hellhound
            case 142:
            case 95:
                return 6578;
            case 141:
                return 6578;
            case 1593:
                return 6563;
            case 152:
            case 45:
            case 1558: //wolf
            case 1954:
                return 76;

            case 89:
                return 6375;
            case 133: //unicorns
                return 290;

            case 105:
            case 1195:
            case 1196://bear
                return 4921;

            case 74:
            case 75:
            case 76:
                return 5574;

            case 73:
            case 751: //zombie
            case 77:
                return 5574;

            case 60:
            case 64:
            case 59:
            case 61:
            case 63:
            case 134:
            case 2035: //spider
            case 62:
            case 1009:
                return 5328;

            case 103:
            case 749:
            case 104:
            case 655:
            case 491: //ghost
                return 124;

            case 1585:
            case 110:
            case 1582:
            case 1583:
            case 1584:
            case 1586: //giant
            case 4291:
            case 4292:
                return 4671;
            case 111:
            case 4687:
                return 4671;
            case 4690:
            case 4691:
            case 4692:
            case 4693:
            case 117:
            case 116:
            case 112:
            case 1587:
            case 1588:
                return 4651;

            case 50: //kbd
                return 89;

            case 941://green dragon
            case 55:
            case 742:
            case 1589:
            case 53:
            case 4669:
            case 4670:
            case 4671:
            case 52:
            case 54:
                return 89;
            case 2889:
                return 2860;
            case 81: //cow
            case 397:
                return 5850;

            case 708: //imp
                return 170;

            case 86:
            case 87:
                return 139;
            case 47://rat
                return 2706;
            case 2457:
                return 2366;
            case 128: //snake
            case 1479:
                return 276;

            case 1017:
            case 2693:
            case 41: // chicken
                return 5388;

            case 90:
            case 91:
            case 5359:
            case 5384:
            case 92:
            case 93: //skeleton
                return 5489;

            default:
                return 404;
        }
    }

    public static int getAttack(int npc) {
        if (npc == 3761) {
            return 3880;
        }
        if (npc == 3760) {
            return 3880;
        }
        if (npc == 3771) {
            return 3922;
        }

        switch (npc) {
            case 1317:
            case 2267:
            case 2268:
            case 2269:
            case 3181:
            case 3182:
            case 3183:
            case 3184:
            case 3185:
                return 451;
            case 6279:
            case 6280:
            case 6281:
            case 6282:
                return 6185;
            case 5529:
                return 5782;
            case 6116: // Seagull
                return 6811;
            case 5247:
                return 5411;
            case 6260:
                return 7060;
            case 5219:
            case 5218:
            case 5217:
            case 5216:
            case 5215:
            case 5214:
            case 5213:
                return 5097; // Penance
            case 5233:
            case 5232:
            case 5231:
            case 5230:
                return 5395;
            case 3761:
            case 3760:
                return 3880;
            case 3771:
                return 3922;
            case 3062:
                return 2955;
            case 131:
                return 5669;
            case 4404:
            case 4405:
            case 4406:
                return 4266;
            case 1019:
                return 1029;
            case 1020:
                return 1035;
            case 1021:
                return 1040;
            case 1022:
                return 1044;
            case 1676:
                return 1626;
            case 1677:
                return 1616;
            case 1678:
                return 1612;
            case 195:
            case 196:
            case 202:
            case 204:
            case 203:
                return 412;
            case 4353:
            case 4354:
            case 4355:
                return 4234;
            case 2709:
            case 2710:
            case 2711:
            case 2712:
                return 1162;

            case 1007:
                return 811;
            case 1608:
                return 1512;
            case 3058:
                return 2930;
            case 113:
                return 128;
            case 114:
                return 359;
            case 1265:
                return 1312;
            case 118:
                return 99;
            case 127:
                return 185;
            case 914:
                return 197;
            case 1620:
            case 1621:
                return 1562;
            case 678:
                return 426;
            case 1192:
                return 1245;
            case 119:
                return 99;
            case 2263:
                return 2182;
            case 3347:
            case 3346:
                return 3326;
            case 3063:
                return 2930;
            case 3061:
                return 2958;
            case 3066:
                return 2927;
            case 3452:// penance queen
                return 5411;
            case 2783://dark beast
                return 2731;
            case 908:
                return 128;
            case 909:
                return 143;
            case 911:
                return 64;
            case 1624:
            case 1625:
                return 1557;
            case 3060:
                return 2962;
            case 2598:
            case 2599:
            case 2600:
            case 2601:
            case 2602:
            case 2603:
            case 2604://tzhar-hur
                return 2609;
            case 2892://Spinolyp
                return 2869;
            case 2881: // supreme
                return 2855;
            case 2882: // prime
                return 2854;
            case 2883: // rex
                return 2853;
            case 2457:
                return 2365;
            case 1590://metal dragon
            case 1591:
            case 1592:
                return 80;
            case 1341:
                return 1341;
            case 2606://tzhaar-xil
                return 2609;
            case 66:
            case 67:
            case 168:
            case 169:
            case 162:
            case 68://gnomes
                return 190;
            case 913:
            case 912:
                return 1162;
            case 160:
            case 161:
                return 191;
            case 163:
            case 164:
                return 192;
            case 438:
            case 439:
            case 440:
            case 441:
            case 442: // Tree spirit
            case 443:
                return 94;
            case 391:
            case 392:
            case 393:
            case 394:
            case 395://river troll
            case 396:
                return 284;
            case 413:
            case 414:
            case 415:
            case 416:
            case 417://rock golem
            case 418:
                return 153;

            case 3741: // Pest control
                return 3901;
            case 3776:
                return 3897;
            case 3751:
            case 3750:
            case 3749:
            case 3748:
            case 3747:
                return 3908;
            case 1153:
            case 1154:
            case 1155:
            case 1156:
            case 1157:
            case 1158: // kalphite
                return 6223;
            case 1160: // kalphite
                return 6235;

            case 2734:
            case 2627://tzhaar 
                return 2621;
            case 2630:
            case 2738:
            case 2736:
            case 2629:
                return 2625;
            case 2631:
            case 2632:
                return 2633;
            case 2741:
                return 2636;

            case 60:
            case 62:
            case 64:
            case 59:
            case 61:
            case 63:
            case 134:
            case 1009:
            case 2035: //spider
                return 5327;

            case 6006: //wolfman
                return 6547;

            case 1612://banshee
                return 1523;

            case 1648:
            case 1649:
            case 1650:
            case 1651:
            case 1652:
            case 1653:
            case 1654:
            case 1655:
            case 1656:
            case 1657://crawling hand
                return 1592;

            case 1604:
            case 1605:
            case 1606:
            case 1607://aberrant specter
                return 1507;

            case 1618:
            case 1619://bloodveld
                return 1552;

            case 1643:
            case 1644:
            case 1645:
            case 1646:
            case 1647://infernal mage
                return 429;

            case 1613://nechryael
                return 1528;

            case 1610:
            case 1611://gargoyle
                return 1517;

            case 1615://abyssal demon
                return 1537;

            case 1633: // pyrefiend
            case 3406:
                return 1582;

            case 1459://monkey guard
                return 1402;

            case 1456://monkey archer
                return 1394;

            case 1125://dad
                return 284;

            case 1096:
            case 1097:
            case 1098:
            case 1106:
            case 1942:
            case 1108:
            case 1109:
            case 1110:
            case 1111:
            case 1112:
            case 1116:
            case 1117:
            case 1101://troll 
                return 284;
            case 1095:
                return 1142;

            case 49://hellhound
            case 142:
            case 95:
                return 6581;

            case 74:
            case 75:
            case 76:
                return 5571;

            case 73:
            case 751: //zombie
            case 77:
                return 5568;

            case 50: //kbd
                return 91;

            case 103:
            case 104:
            case 655:
            case 749:
            case 491: //ghost
                return 123;

            case 708: //imp
                return 169;

            case 397:
                return 59;

            case 172:
                return 1162;

            case 86:
            case 87:
                return 186;
            case 47://rat
                return 2705;

            case 122://hobgoblin
                return 164;

            case 1770:
            case 1771:
            case 1772:
            case 1773:
            case 2678:
            case 2679:
            case 1774:
            case 1775:
            case 1776:// goblins		
                return 309;
            case 141:
                return 6579;
            case 1593:
                return 6562;
            case 1954:
            case 152:
            case 45:
            case 1558: //wolf
                return 75;

            case 1560: //troll
            case 1566:
                return 284;

            case 89:
                return 6376;
            case 133: //unicorns
                return 289;

            case 1585:
            case 110:
            case 1582:
            case 1583:
            case 1584:
            case 1586: //giant
            case 4291:
            case 4292:
                return 4666;
            case 111:
            case 4687:
                return 4672;
            case 4690:
            case 4691:
            case 4692:
            case 4693:
            case 117:
            case 116:
            case 112:
            case 1587:
            case 1588:
                return 4652;

            case 128: //snake
            case 1479:
                return 275;

            case 2591:
            case 2620: //tzhaar
                return 2609;

            case 2610:
            case 2619: //tzhaar
                return 2610;

            case 2033: //rat
            case 748:
                return 138;

            case 2031: // bloodveld
                return 2070;

            case 101: // goblin
                return 309;

            case 90:
            case 91:
            case 5359:
            case 5384:
            case 92:
            case 93: //skeleton
                return 5485;

            case 1766:
            case 1767:
            case 81: // cow
                return 5849;
            case 3065:
                return 2721;
            case 21: // hero
                return 451;

            case 1017:
            case 2693:
            case 41: // chicken
                return 5387;

            case 82:
            case 83:
            case 752:
            case 3064:
            case 4694:
            case 4695:
            case 4696:
            case 4697:
            case 84:
            case 4702:
            case 4703:
            case 4704:
            case 4705: //lesser
                return 64;

            case 123: //hobgoblin
                return 163;

            case 9: // guard
            case 32: // guard
            case 20: // paladin
                return 451;

            case 2456:
                return 1343;
            case 2455:
            case 2454:
            case 1338: // dagannoth
            case 1340:
            case 1342:
                return 1341;

            case 941://green dragon
            case 55:
            case 54:
            case 742:
            case 1589:
            case 52:
            case 53:
            case 4669:
            case 4670:
            case 4671:
                return 91;

            case 1092:
            case 19: // white knight
                return 406;

            case 125: // ice warrior
                return 451;

            case 78:
            case 412: //bat
                return 30;

            case 105:
            case 1195:
            case 1196://bear
                return 4922;
            case 2889:
                return 2859;
            case 132: //monkey
                return 220;

            case 108://scorpion
            case 1477:
                return 6254;
            case 107:
            case 144:
                return 6254;

            case 2028: // karil
                return 2075;

            case 2025: // ahrim
                return 729;

            case 2026: // dharok
                return 2067;

            case 2027: // guthan
                return 2080;

            case 2029: // torag
                return 0x814;

            case 2030: // verac
                return 2062;
            default:
                return 0x326;
        }
    }
}
