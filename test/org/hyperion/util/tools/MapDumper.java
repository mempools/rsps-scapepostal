package org.hyperion.util.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.hyperion.cache.Cache;
import org.hyperion.cache.InvalidCacheException;
import org.hyperion.cache.index.impl.MapIndex;
import org.hyperion.cache.map.LandscapeListener;
import org.hyperion.cache.map.LandscapeParser;
import org.hyperion.rs2.model.GameObject;

public class MapDumper implements LandscapeListener {

    public static void main(String[] args) throws InvalidCacheException, IOException {
        new MapDumper().start();
    }

    private FileOutputStream os;
    private Cache cache;
    private int counter = 0;

    public MapDumper() throws InvalidCacheException, FileNotFoundException {
        System.out.print("Converting: ");
        cache = new Cache(new File("./data/cache/"));
        os = new FileOutputStream("./data/worldmap.xml");
    }

    public void start() throws IOException {
        try {
            int processedAreas = 0;
            int oldPercentage = -1;
            MapIndex[] indices = cache.getIndexTable().getMapIndices();
            for (MapIndex index : indices) {
                int percent = (int) Math.round((float) processedAreas / (float) indices.length * 100F);
                if (percent != oldPercentage && percent % 10 == 0) {
                    oldPercentage = percent;
                    System.out.print(percent + "% ");
                }
                new LandscapeParser(cache, index.getIdentifier(), this).parse();
                processedAreas++;
            }
            System.out.println();
            os.flush();
            System.out.println(counter + " objects.");
        } finally {
            os.close();
        }
    }

    @Override
    public void objectParsed(GameObject obj) {
        counter++;
        try {
            os.write(obj.getDefinition().getId() >> 8);
            os.write(obj.getDefinition().getId());
            os.write(obj.getLocation().getX() >> 8);
            os.write(obj.getLocation().getX());
            os.write(obj.getLocation().getY() >> 8);
            os.write(obj.getLocation().getY());
            os.write(obj.getLocation().getZ());
            os.write(obj.getType());
            os.write(obj.getRotation());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
