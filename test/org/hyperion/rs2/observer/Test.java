/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hyperion.rs2.observer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.hyperion.rs2.util.TextUtils;

/**
 *
 * @author Global <http://rune-server.org/members/global>
 */
public class Test {

    static Container container = new Container();

    public static void main(String[] args) {
        pageDescription(1, "11111111");
        pageDescription(2, "222222222");
        System.out.println(description.get(1));
    }
    /**
     * The page description.
     */
    private static final List<String> description = new ArrayList<>();

    /**
     * Sets the page description
     *
     * @param page The page
     * @param desc The page description lines
     * @return The description in an array
     */
    public static String[] pageDescription(int page, String... desc) {
        for (String line : desc) {
            String[] lines = TextUtils.wrapText(false, line, 24);
            description.addAll(Arrays.asList(lines));
        }
        String[] array = new String[description.size()];
        description.toArray(array);
        return array;
    }
}
