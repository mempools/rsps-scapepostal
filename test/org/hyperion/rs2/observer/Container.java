/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hyperion.rs2.observer;

/**
 *
 * @author Global <http://rune-server.org/members/global>
 */
public class Container {

    /**
     * The observable for the container events
     */
    public final Observable<ContainerEvent> observable = new Observable<>();

    /**
     * @param observer
     * @see
     * org.hannes.observer.Observable#register(org.hannes.observer.Observer)
     */
    public void register(Observer<ContainerEvent> observer) {
        observable.register(observer);
    }

    /**
     * @param observer
     * @see
     * org.hannes.observer.Observable#unregister(org.hannes.observer.Observer)
     */
    public void unregister(Observer<ContainerEvent> observer) {
        observable.unregister(observer);
    }

    /**
     * pushes an update
     *
     * @param type
     */
    public void pushUpdate(EventType type) {
        observable.pushUpdate(new ContainerEvent(type, this));
    }
}
