/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hyperion.rs2.observer;

/**
 *
 * @author Global <http://rune-server.org/members/global>
 */
public class Output extends Container implements Observer<ContainerEvent> {

    private final int id;

    public Output(int id) {
        this.id = id;
    }

    @Override
    public void update(Observable<ContainerEvent> observable, ContainerEvent object) throws Exception {
        System.out.println(id + " type=" + object.getEventType());
    }

    @Override
    public void exceptionCaught(Observable<ContainerEvent> observable, Throwable exception) {
        exception.printStackTrace();
    }

}
