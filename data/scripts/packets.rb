java_import 'org.hyperion.observer.impl.PacketObserver'

# The packets
@packets = {}

# handles packet listeners
class ProcPacketEvent < PacketObserver
  def initialize(block)
    super()
    @block = block
  end

  def update(observable, player, context)
    @block.call observable, player, context
  end
end

# Handles a packet
def handle_packet(name, player, context)
  (@packets[name.to_sym] || []).each { |packet| packet.execute(player, context) }
end

# Executes a packet from the handler
def on_packet(args, block)
  (@packets[args[0]] ||= []) << ProcPacketEvent.new(block)
end