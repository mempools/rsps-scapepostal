java_import 'org.hyperion.rs2.model.definition.ItemDefinition'
java_import 'org.hyperion.rs2.action.Action'
java_import 'org.hyperion.rs2.action.impl.DestructionAction'

FOODS = {}

on :packet, :item_click do |observable, player, packet|
  food = FOODS[packet.get_id]
  if_drink = is_drink?(packet.get_id)
  if food != nil
    if !player.get_attributes.get("canDrink") and if_drink
      case food.itemId
      when 1917 # beer
        player.get_action_sender.send_message "Your stomach cannot take any more beer!"
      else
        player.get_action_sender.send_message "You cannot drink at the moment"
      end
    else
      player.get_action_queue.add_action EatingAction.new(player, food, packet.get_slot)
    end
  end
end

# Converted Linux's 'eating and drinking' tutorial
# @author Korsakoff
class Food

  attr_reader :itemId, :health, :newId, :is_drink

  def initialize(itemId, health, newId)
    @itemId = itemId
    @health = health
    @newId = newId
    @is_drink = false
  end

  def set_drink(d)
    @is_drink = d
  end

end
# adds a food
def add_food(food)
  FOODS[food.itemId] = food
end

# adds a drink
def add_drink(drink)
  add_food(drink)
  drink.set_drink true
end

# checks if an item is a food
def is_food?(id)
  get_food(id) != nil
end

# checks if the item is a drink
def is_drink?(id)
  if FOODS[id] != nil
    FOODS[id].is_drink
  end
end

add_food Food.new(319, 1, 0)
add_food Food.new(315, 3, 0)
add_food Food.new(2140, 3, 0)
add_food Food.new(2142, 3, 0)
add_food Food.new(1891, 4, 1893)
add_food Food.new(2309, 5, 0)
add_food Food.new(347, 5, 0)
add_food Food.new(333, 7, 0)
add_food Food.new(339, 7, 0)
add_food Food.new(351, 8, 0)
add_food Food.new(329, 9, 0)
add_food Food.new(361, 10, 0)
add_food Food.new(379, 12, 0)
add_food Food.new(365, 13, 0)
add_food Food.new(373, 14, 0)
add_food Food.new(7946, 16, 0)
add_food Food.new(385, 20, 0)
add_food Food.new(397, 21, 0)
add_food Food.new(391, 22, 0)

# drinks
add_drink Food.new(1917, 0, 1919) # beer

class EatingAction < DestructionAction

  attr_reader :player, :food, :slot

  def initialize(player, food, slot)
    super player
    @player = get_actor
    @food = food
    @slot = slot
  end

  def getQueuePolicy()
    Action::QueuePolicy::NEVER
  end

  def getWalkablePolicy()
    Action::WalkablePolicy::FOLLOW
  end

  def getDestructionDelay()
    3
  end

  def init
    player = @player
    food_def = ItemDefinition.for_id @food.itemId
    player.get_action_sender.send_message "You #{is_drink?(@food.itemId) ? "drink" :"eat"} the #{food_def.get_name.downcase}."
    player.play_animation Model::Animation.create(is_drink?(@food.itemId) ? 1327 : 829)
    item = Model::Item.new @food.itemId
    if @food.newId == 0
      player.get_inventory.remove(item, @slot)
    else
      player.get_inventory.remove(item, @slot)
      player.get_inventory.add Model::Item.new @food.newId
    end
  end

  def giveRewards(player)
    player.heal @food.health
    apply_effects player, @food.itemId
  end
end

# some items may do effects like setting bonuses or other things
def apply_effects(player, food)
  # BEER !
  if food == 1917
    player.get_action_queue.add_action DrunkAction.new player, food
  end
end