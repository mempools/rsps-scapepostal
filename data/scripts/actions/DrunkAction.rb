java_import 'org.hyperion.rs2.action.Action'

# beer drink count attribute
BEERS = "beers"

# can drink flag
CAN_DRINK = "canDrink"

# drunk flag
IS_DRUNK = "isDrunk"

# the limit of beers to reach the feeling
LIMIT = 5

# duration (in ticks) of the drunk feeling
# example: Every 10 ticks will decrease the feeling by 1
# depending on how much beers they drink
DURATION = LIMIT * 10

# checks if the player is drunk
on :login do |observable, player, args|
  drink_amount = player.get_attributes.get("beers").to_i
  if drink_amount > LIMIT
    player.get_action_queue.add_action DrunkAction.new player, 1917
  end
end

# The restoration tick
on :tick, DURATION do |tick|
  for player in $world.get_world.get_players
    attribute = player.get_attributes
    if attribute.get(BEERS) != nil
      if attribute.get(BEERS) > LIMIT + 1
        attribute.decrease_int(BEERS, 1)
        if attribute.get(BEERS) == 0
          attribute.set(CAN_DRINK, true)
          if attribute.get(BEERS) < 0
            attribute.set(BEERS, 0)
          end
          player.get_action_sender.send_message "You start to feel sober.."
          player.get_equipment_animations.set_default
          player.get_update_flags.flag Model::UpdateFlags::UpdateFlag::APPEARANCE
          tick.stop
        end
      end
    end
  end
end

# The drunk action
# An action that handles people drinking beer lol
class DrunkAction < Action

  attr_reader :player, :food

  def initialize(player, food)
    super player, 2
    @player = get_actor
    @food = food
  end

  def getQueuePolicy()
    Action::QueuePolicy::ALWAYS
  end

  def getWalkablePolicy()
    Action::WalkablePolicy::NON_WALKABLE
  end

  def execute
    if @player.is_dead || @food != 1917
      stop
    end
    attribute = @player.get_attributes
    drinks = attribute.get(BEERS).to_i

    unless attribute.get(IS_DRUNK)
      attribute.increase_int BEERS, 1
      # u feel more strong and confident when ur drunk
      @player.get_skills.increase_level Model::Skills::HITPOINTS, 1
      @player.get_skills.increase_level Model::Skills::STRENGTH, 1
      # too dizzy at the same time, accuracy becomes worse
      @player.get_skills.decrease_level Model::Skills::MAGIC, 2
      @player.get_skills.decrease_level Model::Skills::RANGE, 2
      @player.get_action_sender.send_message "You start to feel dizzy...."
      stop
    end

    if !attribute.get(IS_DRUNK) and drinks >= LIMIT
      activate_drunk @player, true
      stop
    end

  end

  # makes the player drunk
  def activate_drunk(player, reset)
    player.get_attributes.set(IS_DRUNK, true)
    player.get_attributes.set(CAN_DRINK, false)
    drinks = player.get_attributes.get(BEERS).to_i
    notice = ["You've had over #{drinks} beers, slow down!", "*You start to wonder if the feeling will ever go away...*",
      "You're drunk!"]
    player.get_action_sender.send_message notice[rand(notice.size)]
    animation = player.get_equipment_animations
    animation.set_stand_animation Model::Animation.create(2770)
    animation.set_walk_animation Model::Animation.create(3039)
    animation.set_stand_turn_animation Model::Animation.create(3039)
    animation.set_turn180_animation Model::Animation.create(3039)
    animation.set_turn90_cw_animation Model::Animation.create(2769)
    animation.set_turn90_ccw_animation Model::Animation.create(2769)
    player.get_update_flags.flag Model::UpdateFlags::UpdateFlag::APPEARANCE
    player.get_action_queue.add_action DrunkAction.new(player, 1917)
  end
end