#on :hook, :global_event do |actor, args|
#  $world.get_world.submit GlobalAnnouncement.new
#end

TICK = 520 # 8 minutes (ticks)  i think

class GlobalAnnouncement < Tick::Tickable

  def initialize()
    super(TICK)
  end

  def execute
    for player in $world.get_world.get_players
      player.get_action_sender.send_message "[] Don't forget to register on the forums for other features!"
    end
  end
end

$world.get_world.submit GlobalAnnouncement.new