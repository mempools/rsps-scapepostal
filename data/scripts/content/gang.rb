java_import 'org.hyperion.rs2.content.gang.GangManager'

on :packet, :command do |observable, player, packet|
  command = packet.get_name
  args = packet.get_args
  action = player.get_action_sender

  case command
  when "gload"
    GangManager.load
  when "gcreate"
    #GangManager.create player, "3rd street saints" # idk why i pick this name dont ask
    GangManager.create player, "SAINTS"
  when "gdisband"
    player.get_gang.disband player
  when "gturfs"
    action.send_string 8144, "Turfs(#{player.get_gang.get_turfs.size})"
    for i in 8147..8348
      action.send_string i, ""
    end
    for i in 1..player.get_gang.get_turfs.size
      action.send_string 8145+i+1, player.get_gang.get_turfs.get(i - 1).get_name
    end
    action.send_interface 8134, false
  else
  end

end