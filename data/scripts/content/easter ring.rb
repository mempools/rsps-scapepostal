java_import 'java.lang.Math'

# wielding the rings
on :packet, :wield do |observable, player, packet|
  case packet.get_id
  when 7927
    for i in 1..14
      player.get_action_sender.send_sidebar_interface i, 6014
      player.set_transform_id(3689 + (Math.random() * (5 + 1)))
      player.get_walking_queue.can_walk false
    end
  when 6583
    for i in 1..14
      player.get_action_sender.send_sidebar_interface i, 6014
      player.set_transform_id 2626
      player.get_walking_queue.can_walk false
    end
  else
  end
end

# the un-morph button
on :packet, :action_button do |observable, player, packet|
  case packet.get_button
  when 6020
    player.set_transform_id -1
    player.get_action_sender.send_sidebar_interfaces
    player.get_walking_queue.can_walk true
  else
  end
end
