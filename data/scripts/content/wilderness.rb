# The wilderness notify attribute
WILDERNESS_NOTIFY = "wildernessNotify"

# update wilderness status, set default attribute
on :login do |observable, player, args|
  player.get_attributes.set(WILDERNESS_NOTIFY, true)
  # update_wilderness(args[0])
end

# check wilderness level
on :packet, :walking do |observable, player, packet|
  # update_wilderness(player)
end

# Sends wilderness warning screen
# @param player The player to send the warning to
def send_wilderness_warning(player)
  if player.get_attributes.get(WILDERNESS_NOTIFY)
    player.get_action_sender.send_string 6940, "WARNING!"
    player.get_action_sender.send_string(6939, "Proceed with caution. If you go much further north you will enter
the\nwilderness. This is a very dangerous area where other players can attack you!")
    player.get_action_sender.send_string(6941, "The further north you go the more dangerous it becomes,
 but there is more\ntreasure to be found.")
    player.get_action_sender.send_string(6942, "In the wilderness an indicator at the bottom-right of the screen
\nwill show the current level of danger.")
    player.get_action_sender.send_interface 1908, false
    player.get_attributes.set WILDERNESS_NOTIFY, false
  end
end

# Checks if is in the wilderness
# @param player The player to check
def in_wilderness?(player)
  x = player.get_location.get_x
  y = player.get_location.get_y
  if x > 2941 && x < 3392 && y > 3518 && y < 3966 || x > 2941 && x < 3060 && y > 3314 && y < 3399 || x > 2941 && x < 3392 && y > 9918 && y < 10366|| x > 2583 && x < 2729 && y > 3255 && y < 3343
    true
  else
    false
  end
end

# Updates the wilderness status upon login or entering the area
# @param player The player to update
def update_wilderness(player)
  if in_wilderness?(player) && player.get_attributes.get(WILDERNESS_NOTIFY) then
    player.get_action_sender.send_wilderness_warning
  end

  if in_wilderness?(player)
    wilderness_level = 1 + (player.get_location.get_y - 3520) / 8
    player.get_action_sender.send_interaction_option(wilderness_level == 0 ? "Attack" : "null", 1, false)
    player.get_action_sender.send_interface 197, true
    player.get_action_sender.send_string 199, "Level: #{wilderness_level}"
  else
    player.get_action_sender.send_interface -1, true
    player.get_action_sender.send_interaction_option "null", 1, true
    player.get_action_sender.send_interaction_option "null", 2, false
  end
end