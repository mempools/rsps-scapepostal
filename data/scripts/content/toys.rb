#
# Toy Horses
#
yell = [
  "Come on Dobbin, we can win the race!",
  "Hi-ho Silver, and away!",
  "Neaahhhyyy! Giddy-up horsey!"
]

# all the horse item ids
(2520..2526).step(2) { |item|
  on :packet, :item_click do |observable, player, packet|
    if packet.get_id == item
      player.force_chat yell[rand(yell.size)]
      player.play_animation Model::Animation.create(918 + (item - 2520) / 2)
    end
  end
}