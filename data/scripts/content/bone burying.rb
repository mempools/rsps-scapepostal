java_import 'org.hyperion.rs2.model.definition.ItemDefinition'
java_import 'org.hyperion.rs2.action.Action'
java_import 'org.hyperion.rs2.action.impl.DestructionAction'

# An action for burying bones
# 
# @author Korsakoff
#

BONES = {}

class Bone

  attr_reader :itemId, :exp

  def initialize(itemId, exp)
    @itemId = itemId
    @exp = exp
  end

end

def add_bone(bone)
  BONES[bone.itemId] = bone
end

add_bone Bone.new(526, 100.0)
add_bone Bone.new(528, 100.0)
add_bone Bone.new(2859, 100.0)
add_bone Bone.new(3183, 125.0)
add_bone Bone.new(530, 125.0)
add_bone Bone.new(532, 200.0)
add_bone Bone.new(3125, 200.0)
add_bone Bone.new(4812, 250.0)
add_bone Bone.new(3123, 300.0)
add_bone Bone.new(534, 350.0)
add_bone Bone.new(6812, 400.0)
add_bone Bone.new(536, 500.0)
add_bone Bone.new(4830, 525.0)
add_bone Bone.new(4832, 550.0)
add_bone Bone.new(6729, 650.0)
add_bone Bone.new(4834, 750.0)

class BuryingAction < DestructionAction

  attr_reader :player, :bone, :slot

  def initialize(player, bone, slot)
    super player
    @player = get_actor
    @bone = bone
    @slot = slot
  end

  def getDestructionDelay()
    3
  end

  def init
    player = @player
    item = Model::Item.new(@bone.itemId)
    player.get_action_sender.send_message "You dig a hole in the ground..."
    player.play_animation Model::Animation.create(827)
    player.get_inventory.remove(item, @slot)
  end

  def giveRewards(player)
    player.get_skills.add_experience Model::Skills::PRAYER, @bone.exp
    player.get_action_sender.send_message "You bury the bones."
  end
end

on :packet, :item_click do |observable, player, packet|
  bone = BONES[packet.get_id]
  if bone != nil
    player.get_action_queue.add_action BuryingAction.new(player, bone, packet.get_slot)
  end
end