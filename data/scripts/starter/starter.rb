#java_import 'org.hyperion.rs2.model.Item'

on :login do |observable, player, args|
  items = [Item.new(995, 20000), Item.new(4151)]
  for item in items
    player.get_inventory.add item
  end

  player.set_rights ADMINISTRATOR_RIGHTS
  player.get_action_sender.send_message "Greetings!"
  
end

STARTER_NPC = 2949

class StarterDialogue < Dialogue
  def handle(player, actor, id)
    npc = actor.get_definition.get_id
    name = actor.get_definition.get_name
    case npc
    when STARTER_NPC
      case id
      when 1
        player.get_action_sender.send_dialogue(name, DialogueType::NPC, npc, Animation::FACE_ANNOYED,
          "Hello!")
        player.get_interface_state.set_next_dialogue_id 0, 2
      when 2
        player.get_action_sender.send_dialogue(name, DialogueType::NPC, npc, Animation::FACE_HAPPY,
          "Are you looking for... some chronic?", "I know a guy who can hook",
          "you up with some.. for a price")
        player.get_interface_state.set_next_dialogue_id 0, 3
      when 3
        player.get_action_sender.send_dialogue(player.get_name, DialogueType::OPTION, -1, Animation::FACE_DEFAULT,
          "Sure...(1K gp)", "No thanks")
        has_money = player.get_inventory.has_item Model::Item.new(995, 1000)
        player.get_interface_state.set_next_dialogue_id 0, has_money ? 5 : 4
        player.get_interface_state.set_next_dialogue_id 1, 0
      when 4 # not enough coins
        player.get_action_sender.send_dialogue(name, DialogueType::STATEMENT, npc, Animation::FACE_ANNOYED,
          "You do not have enough coins.")
        player.get_action_sender.send_message "You do not have enough coins."
        player.get_interface_state.set_next_dialogue_id 0, 0
      when 5
        player.get_inventory.remove Model::Item.new(995, 1000)
        player.get_action_sender.send_dialogue(name, DialogueType::NPC, npc, Animation::FACE_DEFAULT,
          "Alright, you could find some at the",
          "abandoned church in the wilderness.")
        player.get_interface_state.set_next_dialogue_id 0, 6
      when 6
        player.get_action_sender.send_dialogue(name, DialogueType::NPC, npc, Animation::FACE_HAPPY,
          "Remember, you didn't hear it from me!")
        player.get_interface_state.set_next_dialogue_id 0, 0
      end
    end
  end
end