java_import 'org.hyperion.rs2.model.Damage'

=begin
 ~~~ DEATH EVENT EXAMPLE ~~~  (pardon my english)
 The death event is unique cause there is two different stages.
 The first stage is when the player dies and the second one is when
 the player re-spawns.
 *** YOU MUST DECLARE THE SPECIFIC STAGES ! (see below)

 CORRECT:
 on :death do |actor, stage|
   case stage
   when DEATH_BEFORE
   //do actions
  
   when DEATH_AFTER
   //do actions
   end
 end
 
 INCORRECT:
 on :death do |actor, stage|
   //do actions
 end

=end


# Live example
on :death do |victim, killer, stage|
  unless victim.is_npc
    case stage # this is what I mean by declaring the stages!
    
    when DEATH_BEFORE # the first death stage
      # do actions....
      victim.get_action_sender.send_message "Oh dear, you are dead!"
      
    when DEATH_ACTION 
      victim.get_action_sender.send_message "ACTION!!"
    
      # The stage after the player died... drop loot w/e
    when DEATH_AFTER 
      
      # the stage where the player re-spawns! 
      victim.get_action_sender.send_message "You were killed by #{killer.get_name}."
    end
  end
end

# Here is a command to test it out
on :command, :die, PLAYER_RIGHTS do |player, command|
  hp = player.get_skills.get_level 3
  hit = Damage::Hit.new hp, Damage::HitType::NORMAL_DAMAGE
  player.inflict_damage hit
end