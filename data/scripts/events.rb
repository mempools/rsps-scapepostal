java_import 'org.hyperion.script.listener.event.LoginEvent'
java_import 'org.hyperion.script.listener.event.CommandEvent'
java_import 'org.hyperion.script.listener.event.DeathEvent'
java_import 'org.hyperion.rs2.model.player.Player'

# Login listener
class ProcLoginListener < LoginEvent
  def initialize(block)
    super()
    @block = block
  end

  def update(observable, player, ctx)
    @block.call observable, player, ctx
  end
end

# On login
def on_login(args, proc)
  $world.get_login_listener.add ProcLoginListener.new(proc)
end

# The death event listener
class ProcDeathEventListener < DeathEvent
  def initialize(block)
    super()
    @block = block
  end

  def handleDeath(victim, killer, stage)
    @block.call victim, killer, stage
  end
end

# On death
def on_death(args, proc)
  $world.get_death_event_listener.add ProcDeathEventListener.new(proc)
end

# command listener
class ProcCommandListener < CommandEvent
  def initialize(rights, block)
    super rights
    @block = block
  end

  def handleCommand(player, command)
    @block.call player, command
  end
end

# A game command
def on_command(args, proc)
  if args.length != 1 and args.length != 2
    raise "command event must have one or two arguments"
  end

  rights = PLAYER_RIGHTS
  if args.length == 2
    rights = args[1]
  end

  $world.get_command_listener.add args[0].to_s, ProcCommandListener.new(rights, proc)
end