java_import 'org.hyperion.rs2.model.player.Player'
java_import 'org.hyperion.rs2.model.definition.ItemDefinition'

on :command, :song, ADMINISTRATOR_RIGHTS do |player, command|
  player.get_action_sender.send_song command.get_args[1].to_i
end

on :command, :hint, ADMINISTRATOR_RIGHTS do |player, command|
  player.get_head_icon.set_hint_icon Model::HeadIcon::HintIcon::ARROW
  player.get_action_sender.send_hint_arrow player.get_location, 1
end

on :command, :config, ADMINISTRATOR_RIGHTS do |player, command|
  id = command.get_args[1].to_i
  val = command.get_args[2].to_i
  player.get_action_sender.send_config id, val
  player.get_action_sender.send_message "[Config]: #{id}, #{val}"
end

on :command, :interface, ADMINISTRATOR_RIGHTS do |player, command|
  interfaceId = command.get_args[1].to_i
  player.get_action_sender.send_interface interfaceId, false
  player.get_action_sender.send_message "[Interface]: #{interfaceId}"
end

on :command, :interfaceanim, ADMINISTRATOR_RIGHTS do |player, command|
  interfaceId = player.get_interface_state.get_current_interface
  anim = command.get_args[1].to_i
  player.get_action_sender.send_interface interfaceId, false
  player.get_action_sender.send_interface_animation interfaceId, anim
  player.get_action_sender.send_message "[Interface Animation]: #{interfaceId}, #{anim}"
end

on :command, :cbinterface, ADMINISTRATOR_RIGHTS do |player, command|
  interfaceId = command.get_args[1].to_i
  player.get_action_sender.send_chatbox_interface interfaceId
  player.get_action_sender.send_message "[ChatBox Interface]: #{interfaceId}"
end

on :command, :tele, ADMINISTRATOR_RIGHTS do |player, command|
  if (1..4).include? command.get_args.length
    if command.get_args.length == 2
      x = player.get_location.get_x
      y = player.get_location.get_y
      z = command.get_args[1].to_i
    else
      x = command.get_args[1].to_i
      y = command.get_args[2].to_i
      z = player.get_location.get_z
    end
    if command.get_args.length == 4
      z = command.get_args[3].to_i
    end
    player.set_teleport_target(Model::Location.create(x, y, z))
  else
    player.get_action_sender.send_message "Syntax: ::tele [x] [y] [z]."
  end
end

on :command, :item, ADMINISTRATOR_RIGHTS do |player, command|
  if (1..3).include? command.get_args.length
    id = command.get_args[1].to_i
    definition = ItemDefinition.for_id id
    if definition != nil
      amount = command.get_args.length == 3 ? command.get_args[2].to_i : 1

      player.get_inventory.add Model::Item.new(id, amount)
    else
      player.get_action_sender.send_message "That item does not exist!"
    end
  else
    player.get_action_sender.send_message "Syntax: ::item [id] [amount]"
  end
  
end

on :command, :anim, ADMINISTRATOR_RIGHTS do |player, command|
  if (1..3).include? command.get_args.length
    id = command.get_args[1].to_i
    delay = command.get_args.length == 3 ? command.get_args[2].to_i : 0

    player.play_animation Model::Animation.create(id, delay)
  else
    player.get_action_sender.send_message "Syntax: ::anim [id] [delay]"
  end
end

on :command, :gfx, ADMINISTRATOR_RIGHTS do |player, command|
  if (1..3).include? command.get_args.length
    id = command.get_args[1].to_i
    delay = command.get_args.length == 3 ? command.get_args[2].to_i : 0

    player.play_graphics Model::Graphic.create(id, delay)
  else
    player.get_action_sender.send_message "Syntax: ::gfx [id] [delay]"
  end
end