on :command, :xteletome, ADMINISTRATOR_RIGHTS do |player, command|
  if command.get_args.length == 2
    target = $world.get_world.get_player command.get_args[1].to_s
    if target != nil
      target.set_teleport_target player.get_location
      target.get_action_sender.send_message "[NOTICE]: #{player.get_name} has teleported you"
      player.get_action_sender.send_message "[XTELETOME]: You teleported #{args[1].to_s} to you"
    else
      player.get_action_sender.send_message "[XTELETOME]: #{args[1].to_s} doesn't exist!"
    end
  else
    player.get_action_sender.send_message "[XTELETOME]: Enter a player's name!"
  end
end
on :command, :xteleto, ADMINISTRATOR_RIGHTS do |player, command|
  if command.get_args.length == 2
    target = $world.get_world.get_player command.get_args[1].to_s
    if target != nil
      player.set_teleport_target target.get_location
      target.get_action_sender.send_message "[NOTICE]: #{player.get_name} has teleported to you"
      player.get_action_sender.send_message "[XTELETO]: You teleported to #{args[1].to_s}"
    else
      player.get_action_sender.send_message "[XTELETO]: #{args[1].to_s} doesn't exist!"
    end
  else
    player.get_action_sender.send_message "[XTELETO]: Enter a player's name!"
  end
end

on :command, :xteletome, ADMINISTRATOR_RIGHTS do |player, command|
  if command.get_args.length == 2
    target = $world.get_world.get_player command.get_args[1].to_s
    if target != nil
      player.set_teleport_target target.get_location
      target.get_action_sender.send_message "[NOTICE]: #{player.get_name} has teleported to you"
      player.get_action_sender.send_message "[XTELETOME]: You teleported to #{args[1].to_s}"
    else
      player.get_action_sender.send_message "[XTELETO]: #{args[1].to_s} doesn't exist!"
    end
  else
    player.get_action_sender.send_message "[XTELETO]: Enter a player's name!"
  end
end