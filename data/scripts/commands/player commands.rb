java_import 'org.hyperion.rs2.model.combat.MagicData'

on :command, :wildy, PLAYER_RIGHTS do |player, command|
  player.set_teleport_target Model::Location.create(3245, 3500, 0)
end

on :command, :char, PLAYER_RIGHTS do |player, command|
  player.get_action_sender.send_interface 3559, false
end

on :command, :pos, PLAYER_RIGHTS do |player, command|
  player.get_action_sender.send_message "You're at #{player.get_location}."
end

on :command, :spell, PLAYER_RIGHTS do |player, command|
  if command.get_args.length == 2
    id = command.get_args[1].to_i
    spell_book = MagicData::SpellBook.for_id id
    if spell_book != nil
      player.get_combat_session.set_spell_book spell_book
    else
      player.get_action_sender.send_message "Spell Book '#{id}' doesn't exist"
    end
  end
end