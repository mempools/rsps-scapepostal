on :packet, :walking do |observable, player, packet|
  if player.get_attributes.get("TELE_CLICK") != nil
    player.set_teleport_target Model::Location.create(packet.get_first_x, packet.get_first_y, player.get_location.get_z)
    # puts "X: #{packet.get_first_x}, Y: #{packet.get_first_y} , Z: #{player.get_location.get_z}"
    player.get_attributes.remove("TELE_CLICK")
  end
end

on :command, :teleclick, ADMINISTRATOR_RIGHTS do |player, command|
  player.get_attributes.set("TELE_CLICK", true)
  player.get_action_sender.send_message "[TELEPORT CLICK #{player.get_attributes.get("TELE_CLICK") ? "ENABLED" :"DISABLED"}]"
end