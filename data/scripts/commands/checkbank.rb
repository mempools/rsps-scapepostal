on :command, :checkbank, ADMINISTRATOR_RIGHTS do |player, command|
  if command.get_args.length == 2
    target = $world.get_world.get_player command.get_args[1].to_s
    open_bank player, target
  else
    player.get_action_sender.send_message "[CHECK BANK] Enter a name!"
  end
end

#
# Opens a player's bank
#
def open_bank(player, target)
  if target != nil
    Container::Bank.open player, target.get_bank
  else
    player.get_action_sender.send_message "[CHECK BANK] #{target.get_name} isn't online!"
  end
end