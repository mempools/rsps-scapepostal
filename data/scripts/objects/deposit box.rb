# The deposit box interface
deposit_inventory_interface = 7423

# Item Option Packet
on :packet, :item_option do |observable, player, packet|
  option = packet.get_option
  slot = packet.get_slot
  interface = packet.get_interface
  id = packet.get_id

  case option
  when 1
    case interface
    when deposit_inventory_interface
      if slot >= 0 && slot < Container::Inventory::SIZE then
        Container::Bank.deposit player, slot, id, 1
      end
    else
    end
  when 2
    case interface
    when deposit_inventory_interface
      if slot >= 0 && slot < Container::Inventory::SIZE then
        Container::Bank.deposit player, slot, id, 5
      end
    else
    end
  when 3
    case interface
    when deposit_inventory_interface
      if slot >= 0 && slot < Container::Inventory::SIZE then
        Container::Bank.deposit player, slot, id, 10
      end
    else
    end
  when 4
    case interface
    when deposit_inventory_interface
      if slot >= 0 && slot < Container::Inventory::SIZE then
        Container::Bank.deposit player, slot, id, player.get_inventory.get_count(id)
      end
    else
    end
  when 5
    case interface
    when deposit_inventory_interface
      if slot >= 0 && slot < Container::Inventory::SIZE then
        Container::Bank.deposit player, slot, id, player.get_interface_state.open_enter_amount_interface(interface, slot, id)
      end
    else
    end
  else
  end
end

# Enter Amount Packet
on :packet, :enter_amount do |observable, player, packet|
  interface = player.get_interface_state
  case player.get_interface_state.get_enter_amount_interface_id
  when deposit_inventory_interface
    Container::Bank.deposit player, interface.get_enter_amount_slot, interface.get_enter_amount_id, packet.get_amount
  else
  end
end

# Object Option Packet
on :packet, :object_option do |observable, player, packet|
  case packet.get_option
  when 1
    case packet.get_game_object.get_definition.get_id
    when 9398
      if player.get_location.is_within_interaction_distance packet.get_location
        open_deposit_box player
      end
    else
    end
  else
  end
end

# Opens the deposit box
def open_deposit_box(player)
  player.get_bank.shift
  player.get_action_sender.send_inventory_interface 4465, 197
  player.get_interface_state.add_listener player.get_inventory, ContainerImpl::InterfaceContainerListener.new(player, 7423)
end