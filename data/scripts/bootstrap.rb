require 'java'
require './data/scripts/events.rb'
require './data/scripts/packets.rb'
java_import 'org.hyperion.rs2.model.player.Player'
java_import 'org.hyperion.rs2.content.dialogue.DialogueType'
java_import 'org.hyperion.rs2.packet.context.DeathContext'

# Really wasn't sure how to start this off, so I used parabolika's as
# a start... Apollo was also a huge reference (just with the ruby stuff) credit to team apollo !
# @link: parabolika https://github.com/parabolika/osgi-server

# some constants !!
# player rights
PLAYER_RIGHTS = Player::Rights::PLAYER
MODERATOR_RIGHTS = Player::Rights::MODERATOR
ADMINISTRATOR_RIGHTS = Player::Rights::ADMINISTRATOR

# death stages!
DEATH_BEFORE = DeathContext::DeathStage::BEFORE
DEATH_ACTION = DeathContext::DeathStage::ACTION
DEATH_AFTER = DeathContext::DeathStage::AFTER

# dialogue options
DIALOGUE_NPC = DialogueType::NPC
DIALOGUE_PLAYER = DialogueType::PLAYER
DIALOGUE_OPTION = DialogueType::OPTION
DIALOGUE_STATEMENT = DialogueType::STATEMENT
DIALOGUE_MODEL = DialogueType::MESSAGE_MODEL
DIALOGUE_INFO = DialogueType::INFO


#
# Java Modules
#
modules = {
  'Net' => 'org.hyperion.rs2.net',
  'Action' => 'org.hyperion.rs2.action',
  'ActionListener' => 'org.hyperion.listener.impl',
  'Model' => 'org.hyperion.rs2.model',
  'Attribute' => 'org.hyperion.rs2.model.attribute',
  'Container' => 'org.hyperion.rs2.model.container',
  'ContainerImpl' => 'org.hyperion.rs2.model.container.impl',
  'Utils' => 'org.hyperion.rs2.util',
  'RS2' => 'org.hyperion.rs2',
  'Tick' => 'org.hyperion.rs2.tickable',
  'Task' => 'org.hyperion.rs2.task'
}

modules.each do |k, v|
  eval <<-RUBY
    module #{k}
      include_package '#{v}'
    end
  RUBY
end

# executes blocks(?)
#
# @param [Object] type
#       :packet   for incoming packets
#       :hook     handles hook actions
#       :tick     submits a new tick
#       :login    on login listener
#       :button   on button listener
# @param [Object] args The arguments
# @param [Object] block The block
def on(type, *args, &block)
  case type
  when :packet then
    on_packet(args, block)
  when :tick then
    on_tick(args, block)
  when :command then
    on_command(args, block)
  when :login then
    on_login(args, block)
  when :button then
    on_button(args, block)
  when :death then
    on_death(args, block)
  else
    raise "unknown type"
  end
end

# Tickable handler
class ProcTickable < Tick::Tickable

  def initialize(tick, block)
    super tick
    @block = block
  end

  def execute
    @block.call self
  end
end

# Submits a tick
def on_tick(args, block)
  if args.length != 1
    raise "tick must have a given argument"
  end

  tick = args[0]
  $world.get_world.submit ProcTickable.new(tick, block)
end

# on game button
def on_button(args, block)
  if args.length != 1
    raise "must contain button number"
  end

  id = args[0].to_i

  on :packet, :action_button do |observable, player, packet|
    if packet.get_button == id
      block.call player
    end
  end
end