java_import 'org.hyperion.rs2.action.Action'
java_import 'org.hyperion.rs2.action.impl.DialogueAction'
java_import 'org.hyperion.rs2.model.Animation'
java_import 'org.hyperion.rs2.content.dialogue.Dialogue'
java_import 'org.hyperion.rs2.content.dialogue.DialogueType'
java_import 'org.hyperion.rs2.content.mission.impl.HitmanMission'

on :packet, :npc_option do |observable, player, packet|
  if packet.get_option == 2
    if packet.get_target.get_definition.get_id == 2
      action = DialogueAction.new(player, packet.get_target, Bum.new)
      player.add_distance_action player, packet.get_target, action, 1
    end
  end
end

class Bum < Dialogue
  def handle(player, actor, id)
    npc = actor.get_definition.get_id
    mission = HitmanMission.new
    case npc
    when 2
      case id
      when 1
        if mission.get_requirements player
          player.get_action_sender.send_dialogue("bum", DialogueType::NPC, npc, Animation::FACE_EVIL_LAUGH,
            "hey guy, need a contract?")
          player.get_interface_state.set_next_dialogue_id 0, 2
        else
          player.get_action_sender.send_dialogue("bum", DialogueType::NPC, npc, Animation::FACE_EVIL_LAUGH,
            "law states that im not allowed", "to beg for money.", "bye")
          player.get_interface_state.set_next_dialogue_id 0, 0
        end
      when 2
        player.get_action_sender.send_dialogue(player.get_name, DialogueType::PLAYER, -1, Animation::FACE_DEFAULT,
          "Sure!")
        player.get_interface_state.set_next_dialogue_id 0, 3
      when 3
        player.set_mission mission
        player.get_mission.start player
        player.get_action_sender.send_dialogue("BUM", DialogueType::NPC, npc, Animation::FACE_EVIL_LAUGH,
          "Alright, he's just down the street", "from where i'm at. Come back ", "once you get it done alright?")
        player.get_interface_state.set_next_dialogue_id 0, 0
      end
    end
  end
end