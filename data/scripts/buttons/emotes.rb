#
# The emote tab
#
on :packet, :action_button do |observable, player, packet|
  case packet.get_button
  when 161
    player.play_animation Model::Animation::CRY

  when 162
    player.play_animation Model::Animation::THINKING

  when 163
    player.play_animation Model::Animation::WAVE

  when 164
    player.play_animation Model::Animation::BOW

  when 165
    player.play_animation Model::Animation::ANGRY

  when 166
    player.play_animation Model::Animation::DANCE

  when 167
    player.play_animation Model::Animation::BECKON

  when 168
    player.play_animation Model::Animation::YES_EMOTE

  when 169
    player.play_animation Model::Animation::NO_EMOTE

  when 170
    player.play_animation Model::Animation::LAUGH

  when 171
    player.play_animation Model::Animation::CHEER

  when 172
    player.play_animation Model::Animation::CLAP

  when 13362
    player.play_animation Model::Animation::PANIC

  when 13363
    player.play_animation Model::Animation::JIG

  when 13364
    player.play_animation Model::Animation::SPIN

  when 13365
    player.play_animation Model::Animation::HEADBANG

  when 13366
    player.play_animation Model::Animation::JOYJUMP

  when 13367
    player.play_animation Model::Animation::RASPBERRY

  when 13368
    player.play_animation Model::Animation::YAWN

  when 13383
    player.play_animation Model::Animation::GOBLIN_BOW

  when 13384
    player.play_animation Model::Animation::GOBLIN_DANCE

  when 13369
    player.play_animation Model::Animation::SALUTE

  when 13370
    player.play_animation Model::Animation::SHRUG

  when 11100
    player.play_animation Model::Animation::BLOW_KISS

  when 667
    player.play_animation Model::Animation::GLASS_BOX

  when 6503
    player.play_animation Model::Animation::CLIMB_ROPE

  when 6506
    player.play_animation Model::Animation::LEAN

  when 666
    player.play_animation Model::Animation::GLASS_WALL
  else
  end
end